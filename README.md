```
 ________  ________  _________  ________           _____ ______   ________      
|\   __  \|\   __  \|\___   ___\\   ____\         |\   _ \  _   \|\   ____\     
\ \  \|\ /\ \  \|\  \|___ \  \_\ \  \___|_        \ \  \\\__\ \  \ \  \___|_    
 \ \   __  \ \  \\\  \   \ \  \ \ \_____  \        \ \  \\|__| \  \ \_____  \   
  \ \  \|\  \ \  \\\  \   \ \  \ \|____|\  \        \ \  \    \ \  \|____|\  \  
   \ \_______\ \_______\   \ \__\  ____\_\  \        \ \__\    \ \__\____\_\  \ 
    \|_______|\|_______|    \|__| |\_________\        \|__|     \|__|\_________\
                                  \|_________|                      \|_________|
                                                                                
                                                                                
```
This repository is Node engine for multi bot constructor. You can use any bot to construct complex logic for bot's users.

### Tech dependency
* Redis
* PostgreSQL

### Relative repositories
* Base models - https://gitlab.com/nikita.morozov/bot-ms-core
* Base handlers for actions - https://gitlab.com/nikita.morozov/bot-ms-common-handlers
* Proto files for GRPC - https://gitlab.com/nikita.morozov/bots-ms-proto

### Example of Frontend implementation of Bot engine
![graph](assets/graph.jpeg)
![state-machine](assets/sm.jpeg)

### Base entities
* ```Bot``` - information about bot, API keys, and settings
* ```AuthRequest``` - authorization request list
* ```BotUser``` - it is store for authorized  
* ```CommonMessage``` - saved messages from chat
* ```Action``` - method of node engine
* ```Socket``` - inner and outer sockets of action
* ```Connector``` - connection between inner and outer socket
* ```Scenario``` - it is a canvas with actions connected each other and implemented some logic
* ```ScenarioAction``` - instance of action placed into a scenario canvas 
* ```Event``` - trigger to start a scenario
* ```Trigger``` - an entities to store trigger data, such as context of user
* ```StateMachine``` - a canvas contains states of a current bot for current user
* ```Context``` - a session object, contain `ID` of user and result of nodes execution

### Bot engine modes
* `Event` - Sceneries triggering by event from bot and execute without keeping current user state.
* `State` - Using `StateMachine` for keeping current state of user, it is get possibility to choose different commands of each of state.

