package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"regexp"
	"testing"
	"time"
)

type EventSuite struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository repositories.EventRepository
}

func (s *EventSuite) BeforeTest(_, _ string) {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open(postgres.New(postgres.Config{Conn: db}), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix: "bots-ms_",
		},
	})
	require.NoError(s.T(), err)

	s.repository = NewGormEventRepository(s.DB, 10)
}

func (s *EventSuite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestEvent(t *testing.T) {
	suite.Run(t, new(EventSuite))
}

// ------------------------------------------------
//
// # GetById
//
// ------------------------------------------------
func (s *EventSuite) Test_gormEvent_GetById() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_events" WHERE "bots-ms_events"."id" = $1 AND "bots-ms_events"."deleted_at" IS NULL ORDER BY "bots-ms_events"."id" LIMIT 1`)).
		WithArgs(uint(10)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "bot_id", "scenario_id", "command"}).
			AddRow("1", time.Now(), time.Now(), nil, "1234", "1", "daaatttaa"))

	item, err := s.repository.GetById(uint(10))
	require.NoError(s.T(), err)
	require.Equal(s.T(), item.ID, uint(1))
}

// ------------------------------------------------
//
// # GetByHandlerName
//
// ------------------------------------------------
func (s *EventSuite) Test_gormEvent_GetByHandlerName() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT "bots-ms_events"."id","bots-ms_events"."created_at","bots-ms_events"."updated_at","bots-ms_events"."deleted_at","bots-ms_events"."bot_id","bots-ms_events"."scenario_id","bots-ms_events"."command" FROM "bots-ms_events" JOIN "bots-ms_bots" as bot ON bot.id = "bots-ms_events".bot_id AND bot.slug = $1 WHERE command = $2 AND "bots-ms_events"."deleted_at" IS NULL`)).
		WithArgs("bot-slug", "eventName").
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "bot_id", "scenario_id", "command"}).
			AddRow("1", time.Now(), time.Now(), nil, "1234", "1", "daaatttaa"))

	item, err := s.repository.GetByHandlerName("eventName", "bot-slug")
	require.NoError(s.T(), err)
	require.Equal(s.T(), item.ID, uint(1))
}

// ------------------------------------------------
//
// # Update
//
// ------------------------------------------------
func (s *EventSuite) Test_gormEvent_Update() {
	item := models.Event{
		Model: gorm.Model{
			ID: uint(1),
		},
		BotID:      uint(12),
		ScenarioID: uint(12),
		Command:    "commannnd",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "bots-ms_events" SET "created_at"=$1,"updated_at"=$2,"deleted_at"=$3,"bot_id"=$4,"scenario_id"=$5,"command"=$6 WHERE "bots-ms_events"."deleted_at" IS NULL AND "id" = $7`)).
		WithArgs(sharedModels.Any{}, sharedModels.Any{}, nil, item.BotID, item.ScenarioID, item.Command, item.ID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	err := s.repository.Update(&item)
	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *EventSuite) Test_gormEvent_Create() {
	item := models.Event{
		BotID:      uint(12),
		ScenarioID: uint(12),
		Command:    "commannnd",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "bots-ms_events" ("created_at","updated_at","deleted_at","bot_id","scenario_id","command") VALUES ($1,$2,$3,$4,$5,$6) RETURNING "id"`)).
		WithArgs(sharedModels.Any{}, sharedModels.Any{}, nil, item.BotID, item.ScenarioID, item.Command).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow("1"))
	s.mock.ExpectCommit()

	err := s.repository.Create(&item)
	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *EventSuite) Test_gormEvent_Delete() {
	item := models.Event{
		Model: gorm.Model{
			ID: uint(1),
		},
		BotID:      uint(12),
		ScenarioID: uint(12),
		Command:    "commannnd",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "bots-ms_events" SET "deleted_at"=$1 WHERE "bots-ms_events"."id" = $2 AND "bots-ms_events"."deleted_at" IS NULL`)).
		WithArgs(sharedModels.Any{}, uint(1)).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	err := s.repository.Delete(&item)
	require.NoError(s.T(), err)
}
