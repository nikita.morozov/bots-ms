package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"errors"
	"gorm.io/gorm"
)

type gormAuthRequestRepo struct {
	DB *gorm.DB
}

func (r *gormAuthRequestRepo) GetById(id uint) (*models.AuthRequest, error) {
	var item *models.AuthRequest
	result := r.DB.First(&item, id)

	if item != nil && item.ID == 0 {
		return nil, errors.New("bots-ms.auth-request.not-found")
	}

	return item, result.Error
}

func (r *gormAuthRequestRepo) GetByHash(hash string) (*models.AuthRequest, error) {
	var item *models.AuthRequest
	result := r.DB.Where("hash = ?", hash).First(&item)

	if item != nil && item.ID == 0 {
		return nil, errors.New("bots-ms.auth-request.not-found")
	}

	return item, result.Error
}

func (r *gormAuthRequestRepo) GetByMeta(id uint, data string) (*models.AuthRequest, error) {
	var item *models.AuthRequest
	result := r.DB.Where("bot_id = ? AND meta = ?", id, data).First(&item)

	if item != nil && item.ID == 0 {
		return nil, errors.New("bots-ms.auth-request.not-found")
	}

	return item, result.Error
}

func (r *gormAuthRequestRepo) Create(item *models.AuthRequest) error {
	result := r.DB.Create(&item)
	return result.Error
}

func (r *gormAuthRequestRepo) Update(item *models.AuthRequest) error {
	result := r.DB.Save(&item)
	return result.Error
}

func (r *gormAuthRequestRepo) Delete(item *models.AuthRequest) error {
	result := r.DB.Unscoped().Delete(&item)
	return result.Error
}

func NewGormAuthRequestRepository(db *gorm.DB) repositories.AuthRequestRepository {
	return &gormAuthRequestRepo{
		DB: db,
	}
}
