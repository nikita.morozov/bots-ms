package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"regexp"
	"testing"
	"time"
)

type AuthRequestSuite struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository repositories.AuthRequestRepository
}

func (s *AuthRequestSuite) BeforeTest(_, _ string) {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open(postgres.New(postgres.Config{Conn: db}), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix: "bots-ms_",
		},
	})
	require.NoError(s.T(), err)

	s.repository = NewGormAuthRequestRepository(s.DB)
}

func (s *AuthRequestSuite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestAuthRequest(t *testing.T) {
	suite.Run(t, new(AuthRequestSuite))
}

// ------------------------------------------------
//
// # GetByHash
//
// ------------------------------------------------
func (s *AuthRequestSuite) Test_gormAuthRequest_GetByHash() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_auth_requests" WHERE hash = $1 AND "bots-ms_auth_requests"."deleted_at" IS NULL ORDER BY "bots-ms_auth_requests"."id" LIMIT 1`)).
		WithArgs("hashValue").
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "bot_id", "meta", "hash"}).
			AddRow("1", time.Now(), time.Now(), nil, "10", "", ""))

	item, err := s.repository.GetByHash("hashValue")
	require.NoError(s.T(), err)
	require.Equal(s.T(), item.ID, uint(1))
}

// ------------------------------------------------
//
// # GetByMeta
//
// ------------------------------------------------
func (s *AuthRequestSuite) Test_gormAuthRequest_GetByMeta() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_auth_requests" WHERE (bot_id = $1 AND meta = $2) AND "bots-ms_auth_requests"."deleted_at" IS NULL ORDER BY "bots-ms_auth_requests"."id" LIMIT 1`)).
		WithArgs(1, "metaValue").
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "bot_id", "meta", "hash"}).
			AddRow("1", time.Now(), time.Now(), nil, "10", "", ""))

	item, err := s.repository.GetByMeta(1, "metaValue")
	require.NoError(s.T(), err)
	require.Equal(s.T(), item.ID, uint(1))
}

// ------------------------------------------------
//
// # GetById
//
// ------------------------------------------------
func (s *AuthRequestSuite) Test_gormAuthRequest_GetById() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_auth_requests" WHERE "bots-ms_auth_requests"."id" = $1 AND "bots-ms_auth_requests"."deleted_at" IS NULL ORDER BY "bots-ms_auth_requests"."id" LIMIT 1`)).
		WithArgs(int64(10)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "bot_id", "meta", "hash"}).
			AddRow("1", time.Now(), time.Now(), nil, "10", "", ""))

	item, err := s.repository.GetById(uint(10))
	require.NoError(s.T(), err)
	require.Equal(s.T(), item.ID, uint(1))
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *AuthRequestSuite) Test_gormAuthRequest_Create() {
	item := models.AuthRequest{
		BotID: uint(1),
		Meta:  "",
		Hash:  "",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "bots-ms_auth_requests" ("created_at","updated_at","deleted_at","bot_id","meta","hash") VALUES ($1,$2,$3,$4,$5,$6) RETURNING "id"`)).
		WithArgs(sharedModels.Any{}, sharedModels.Any{}, nil, item.BotID, item.Meta, item.Hash).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow("1"))
	s.mock.ExpectCommit()

	err := s.repository.Create(&item)
	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *AuthRequestSuite) Test_gormAuthRequest_Delete() {
	item := models.AuthRequest{
		Model: gorm.Model{
			ID: uint(1),
		},
		BotID: uint(1),
		Meta:  "",
		Hash:  "",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`DELETE FROM "bots-ms_auth_requests" WHERE "bots-ms_auth_requests"."id" = $1`)).
		WithArgs(uint(1)).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	err := s.repository.Delete(&item)
	require.NoError(s.T(), err)
}
