package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"errors"
	"gorm.io/gorm"
)

type gormStateRepo struct {
	DB *gorm.DB
}

func (r *gormStateRepo) List(stateMachineId uint) (*[]models.State, error) {
	var items *[]models.State
	result := r.DB.Preload("Transitions").Where("state_machine_id = ?", stateMachineId)

	result = result.Find(&items)
	return items, result.Error
}

func (r *gormStateRepo) GetById(id uint) (*models.State, error) {
	var item *models.State
	result := r.DB.Preload("Transitions").First(&item, id)

	if item.ID == 0 {
		return nil, errors.New("bots-ms.state.not-found")
	}

	return item, result.Error
}

func (r *gormStateRepo) GetByScenarioId(smId uint, scenarioId uint) (*models.State, error) {
	var item *models.State
	result := r.DB.Preload("Transitions").Where("state_machine_id = ? and scenario_id = ?", smId, scenarioId).First(&item)

	if item.ID == 0 {
		return nil, errors.New("bots-ms.state.not-found")
	}

	return item, result.Error
}

func (r *gormStateRepo) Update(item *models.State) error {
	result := r.DB.Save(&item)
	return result.Error
}

func (r *gormStateRepo) Create(item *models.State) error {
	result := r.DB.Create(&item)
	return result.Error
}

func (r *gormStateRepo) Delete(item *models.State) error {
	result := r.DB.Delete(&item)
	return result.Error
}

func (r *gormStateRepo) DeleteByStateMachineId(id uint) error {
	result := r.DB.Where("state_machine_id = ?", id).Delete(models.State{})
	return result.Error
}

func NewGormStateRepository(db *gorm.DB) repositories.StateRepository {
	return &gormStateRepo{
		DB: db,
	}
}
