package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"errors"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/gorm"
)

type gormScenarioRepo struct {
	DB           *gorm.DB
	defaultLimit int
}

func (r *gormScenarioRepo) List(opts *sharedModels.ListOptions) (*[]models.Scenario, sharedModels.Paginator, error) {
	var items *[]models.Scenario
	var count int64
	r.DB.Model(&models.Scenario{}).Count(&count)

	if opts == nil {
		result := r.DB.Order("id desc").Find(&items)
		return items, sharedModels.Paginator{Offset: 0, TotalCount: int(count), CountPerPage: r.defaultLimit}, result.Error
	}

	result := r.DB.Limit(opts.GetLimit(r.defaultLimit)).Offset(opts.Offset).Order(opts.GetOrder()).Find(&items)
	return items, sharedModels.Paginator{
		Offset:       opts.Offset,
		TotalCount:   int(count),
		CountPerPage: opts.GetLimit(r.defaultLimit),
	}, result.Error
}

func (r *gormScenarioRepo) Count() (uint64, error) {
	var count int64
	r.DB.Model(&models.Scenario{}).Count(&count)
	return uint64(count), nil
}

func (r *gormScenarioRepo) GetById(id uint) (*models.Scenario, error) {
	var item *models.Scenario
	result := r.DB.First(&item, id)

	if item.ID == 0 {
		return nil, errors.New("bots-ms.scenario.not-found")
	}

	return item, result.Error
}

func (r *gormScenarioRepo) Update(item *models.Scenario) error {
	result := r.DB.Save(&item)
	return result.Error
}

func (r *gormScenarioRepo) Create(item *models.Scenario) error {
	result := r.DB.Create(&item)
	return result.Error
}

func (r *gormScenarioRepo) Delete(item *models.Scenario) error {
	result := r.DB.Delete(&item)
	return result.Error
}

func NewGormScenarioRepository(db *gorm.DB, defaultLimit int) repositories.ScenarioRepository {
	return &gormScenarioRepo{
		DB:           db,
		defaultLimit: defaultLimit,
	}
}
