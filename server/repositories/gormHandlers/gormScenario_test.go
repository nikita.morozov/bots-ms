package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"regexp"
	"testing"
	"time"
)

type ScenarioSuite struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository repositories.ScenarioRepository
}

func (s *ScenarioSuite) BeforeTest(_, _ string) {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open(postgres.New(postgres.Config{Conn: db}), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix: "bots-ms_",
		},
	})
	require.NoError(s.T(), err)

	s.repository = NewGormScenarioRepository(s.DB, 10)
}

func (s *ScenarioSuite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestScenario(t *testing.T) {
	suite.Run(t, new(ScenarioSuite))
}

// ------------------------------------------------
//
// List
//
// ------------------------------------------------
//func (s *ScenarioSuite) Test_gormScenario_List() {
//	s.mock.ExpectQuery(regexp.QuoteMeta(
//		`SELECT * FROM "bots-ms_scenarios" WHERE "bots-ms_scenarios"."deleted_at" IS NULL`)).
//		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "name", "method", "scheme"}).
//			AddRow("1", time.Now(), time.Now(), nil, "1234", "1", "daaatttaa"))
//
//	items, err := s.repository.List(nil)
//	require.NoError(s.T(), err)
//	require.Equal(s.T(), len(*items), int(1))
//}

// ------------------------------------------------
//
// # GetById
//
// ------------------------------------------------
func (s *ScenarioSuite) Test_gormScenario_GetById() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_scenarios" WHERE "bots-ms_scenarios"."id" = $1 AND "bots-ms_scenarios"."deleted_at" IS NULL ORDER BY "bots-ms_scenarios"."id" LIMIT 1`)).
		WithArgs(uint(10)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "bot_id", "scenario_id", "command"}).
			AddRow("1", time.Now(), time.Now(), nil, "1234", "1", "daaatttaa"))

	item, err := s.repository.GetById(uint(10))
	require.NoError(s.T(), err)
	require.Equal(s.T(), item.ID, uint(1))
}

// ------------------------------------------------
//
// # Update
//
// ------------------------------------------------
func (s *ScenarioSuite) Test_gormScenario_Update() {
	initial := uint(1)
	item := models.Scenario{
		Model: gorm.Model{
			ID: uint(1),
		},
		Title:           "First",
		InitialActionId: &initial,
		Actions: []models.ScenarioAction{
			{
				Id:         uint(1),
				Meta:       "",
				ScenarioId: uint(1),
				Data:       "",
				ActionId:   uint(1),
			},
		},
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "bots-ms_scenarios" SET "created_at"=$1,"updated_at"=$2,"deleted_at"=$3,"title"=$4,"category"=$5,"initial_action_id"=$6 WHERE "bots-ms_scenarios"."deleted_at" IS NULL AND "id" = $7`)).
		WithArgs(sharedModels.Any{}, sharedModels.Any{}, nil, "First", "", uint(1), item.ID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "bots-ms_scenario_actions" ("meta","scenario_id","action_id","data","id") VALUES ($1,$2,$3,$4,$5) ON CONFLICT ("id") DO UPDATE SET "scenario_id"="excluded"."scenario_id" RETURNING "id"`)).
		WithArgs("", uint(1), uint(1), "", uint(1)).
		WillReturnRows(sqlmock.NewRows([]string{"1"}))
	s.mock.ExpectCommit()

	err := s.repository.Update(&item)
	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *ScenarioSuite) Test_gormScenario_Create() {
	initial := uint(1)
	item := models.Scenario{
		Title:           "First",
		InitialActionId: &initial,
		Actions:         []models.ScenarioAction{},
	}

	s.mock.ExpectBegin()
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "bots-ms_scenarios" ("created_at","updated_at","deleted_at","title","category","initial_action_id") VALUES ($1,$2,$3,$4,$5,$6) RETURNING "id"`)).
		WithArgs(sharedModels.Any{}, sharedModels.Any{}, nil, "First", "", uint(1)).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow("1"))
	s.mock.ExpectCommit()

	err := s.repository.Create(&item)
	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *ScenarioSuite) Test_gormScenario_Delete() {
	item := models.Scenario{
		Model: gorm.Model{
			ID: uint(1),
		},
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "bots-ms_scenarios" SET "deleted_at"=$1 WHERE "bots-ms_scenarios"."id" = $2 AND "bots-ms_scenarios"."deleted_at" IS NULL`)).
		WithArgs(sharedModels.Any{}, uint(1)).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	err := s.repository.Delete(&item)
	require.NoError(s.T(), err)
}
