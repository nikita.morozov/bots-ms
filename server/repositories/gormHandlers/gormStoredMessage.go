package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"errors"
	"gorm.io/gorm"
)

type gormStoredMessageRepo struct {
	DB *gorm.DB
}

func (r *gormStoredMessageRepo) Get(id uint) (*models.StoredMessage, error) {
	var item *models.StoredMessage
	result := r.DB.First(&item, id)

	if item.ID == 0 {
		return nil, errors.New("bots-ms.stored-message.not-found")
	}

	return item, result.Error
}

func (r *gormStoredMessageRepo) Create(item *models.StoredMessage) error {
	result := r.DB.Create(&item)
	return result.Error
}

func (r *gormStoredMessageRepo) Delete(item *models.StoredMessage) error {
	result := r.DB.Delete(&item)
	return result.Error
}

func NewGormStoredMessageRepository(db *gorm.DB) repositories.StoredMessagesRepository {
	return &gormStoredMessageRepo{
		DB: db,
	}
}
