package gormHandlers

import (
	"bots-ms/repositories"
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"regexp"
	"testing"
)

type ActionsSuite struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository repositories.ActionRepository
}

func (s *ActionsSuite) BeforeTest(_, _ string) {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open(postgres.New(postgres.Config{Conn: db}), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix: "bots-ms_",
		},
	})
	require.NoError(s.T(), err)

	s.repository = NewGormActionRepository(s.DB)
}

func (s *ActionsSuite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestActions(t *testing.T) {
	suite.Run(t, new(ActionsSuite))
}

// ------------------------------------------------
//
// # GetByHandlerName
//
// ------------------------------------------------
func (s *ActionsSuite) Test_gormAction_GetByHandlerName() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_actions" WHERE handler = $1`)).
		WithArgs("handlerName").
		WillReturnRows(sqlmock.NewRows([]string{"id", "data", "handler_id"}).
			AddRow("1", "", "10"))

	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_sockets" WHERE "bots-ms_sockets"."action_id" = $1`)).
		WithArgs(int64(1)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "direction", "type", "title", "slug", "action_id"}).
			AddRow("1", "1", "1", "First", "first", "1"))

	item, err := s.repository.GetByHandlerName("handlerName")
	require.NoError(s.T(), err)
	require.Equal(s.T(), item.Id, uint(1))
}

// ------------------------------------------------
//
// # GetById
//
// ------------------------------------------------
func (s *ActionsSuite) Test_gormAction_GetById() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_actions" WHERE "bots-ms_actions"."id" = $1 ORDER BY "bots-ms_actions"."id" LIMIT 1`)).
		WithArgs(int64(10)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "data", "handler_id"}).
			AddRow("1", "", "10"))

	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_sockets" WHERE "bots-ms_sockets"."action_id" = $1`)).
		WithArgs(int64(1)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "direction", "type", "title", "slug", "action_id"}).
			AddRow("1", "1", "1", "First", "first", "1"))

	item, err := s.repository.GetById(uint(10))
	require.NoError(s.T(), err)
	require.Equal(s.T(), item.Id, uint(1))
}

// ------------------------------------------------
//
// # Update
//
// ------------------------------------------------
func (s *ActionsSuite) Test_gormAction_Update() {
	item := coreModel.Action{
		Id:      uint(1),
		Name:    "testAction",
		Handler: "testHandler",
		Scheme:  "testScheme",
		Sockets: []coreModel.Socket{
			{
				Direction: coreModel.SocketDirectionIn,
				Type:      coreModel.SocketTypeString,
				Title:     "Url",
				Slug:      "url",
			},
		},
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "bots-ms_actions" SET "name"=$1,"handler"=$2,"scheme"=$3 WHERE "id" = $4`)).
		WithArgs(item.Name, item.Handler, item.Scheme, uint(1)).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "bots-ms_sockets" ("direction","type","title","slug","action_id","manual") VALUES ($1,$2,$3,$4,$5,$6) ON CONFLICT ("id") DO UPDATE SET "action_id"="excluded"."action_id" RETURNING "id"`)).
		WithArgs(coreModel.SocketDirectionIn, coreModel.SocketTypeString, "Url", "url", uint(1), false).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow("1"))
	s.mock.ExpectCommit()

	err := s.repository.Update(&item)
	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *ActionsSuite) Test_gormAction_Create() {
	item := coreModel.Action{
		Name:    "testAction",
		Handler: "testHandler",
		Scheme:  "testScheme",
		Sockets: []coreModel.Socket{
			{
				Direction: coreModel.SocketDirectionIn,
				Type:      coreModel.SocketTypeString,
				Title:     "Url",
				Slug:      "url",
			},
		},
	}

	s.mock.ExpectBegin()
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "bots-ms_actions" ("name","handler","scheme") VALUES ($1,$2,$3) RETURNING "id"`)).
		WithArgs(item.Name, item.Handler, item.Scheme).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow("1"))
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "bots-ms_sockets" ("direction","type","title","slug","action_id","manual") VALUES ($1,$2,$3,$4,$5,$6) ON CONFLICT ("id") DO UPDATE SET "action_id"="excluded"."action_id" RETURNING "id"`)).
		WithArgs(coreModel.SocketDirectionIn, coreModel.SocketTypeString, "Url", "url", uint(1), false).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow("1"))
	s.mock.ExpectCommit()

	err := s.repository.Create(&item)
	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *ActionsSuite) Test_gormAction_Delete() {
	item := coreModel.Action{
		Id:      uint(1),
		Name:    "testAction",
		Handler: "testHandler",
		Scheme:  "testScheme",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`DELETE FROM "bots-ms_actions" WHERE "bots-ms_actions"."id" = $1`)).
		WithArgs(uint(1)).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	err := s.repository.Delete(&item)
	require.NoError(s.T(), err)
}
