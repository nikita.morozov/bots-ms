package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type gormCommonMessageRepo struct {
	DB *gorm.DB
}

func (r *gormCommonMessageRepo) GetByMessageId(messageId string) (*models.CommonMessage, error) {
	var item *models.CommonMessage
	result := r.DB.Preload(clause.Associations).Joins("JOIN \"bots-ms_stored_messages\" as sm ON sm.id = \"bots-ms_common_messages\".message_id AND sm.message_id = ?", messageId).First(&item)
	if item != nil && item.ID == 0 {
		return nil, errors.New("bots-ms.common-message.not-found")
	}

	return item, result.Error
}

func (r *gormCommonMessageRepo) Get(Data string) (*models.CommonMessage, error) {
	var item *models.CommonMessage
	result := r.DB.Where("data = ?", Data).First(&item)

	if item != nil && item.ID == 0 {
		return nil, errors.New("bots-ms.common-message.not-found")
	}

	return item, result.Error
}

// TODO: Check it, why here First method
func (r *gormCommonMessageRepo) ListByMessageId(MessageID uint) (*[]models.CommonMessage, error) {
	var items *[]models.CommonMessage
	result := r.DB.Where("message_id = ?", MessageID).First(&items)
	return items, result.Error
}

func (r *gormCommonMessageRepo) ListBySender(sender string) (*[]models.CommonMessage, error) {
	var items *[]models.CommonMessage
	result := r.DB.Where("data like ?", fmt.Sprintf("%%_%s", sender)).Find(&items)
	return items, result.Error
}

func (r *gormCommonMessageRepo) Create(item *models.CommonMessage) error {
	result := r.DB.Create(&item)
	return result.Error
}

func (r *gormCommonMessageRepo) Delete(item *models.CommonMessage) error {
	result := r.DB.Unscoped().Delete(&item)
	return result.Error
}

func NewGormCommonMessageRepository(db *gorm.DB) repositories.CommonMessage {
	return &gormCommonMessageRepo{
		DB: db,
	}
}
