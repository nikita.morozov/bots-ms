package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"errors"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/gorm"
)

type gormBotUserRepo struct {
	DB           *gorm.DB
	defaultLimit int
}

func (r *gormBotUserRepo) Count() (uint64, error) {
	var count int64
	r.DB.Model(&models.BotUser{}).Count(&count)
	return uint64(count), nil
}

func (r *gormBotUserRepo) List(handler string, opts *sharedModels.ListOptions) (*[]models.BotUser, sharedModels.Paginator, error) {
	var items *[]models.BotUser
	var count int64
	r.DB.Model(&models.BotUser{}).Count(&count)

	if opts == nil {
		result := r.DB.Joins("JOIN \"bots-ms_bots\" as bot ON bot.id = \"bots-ms_bot_users\".bot_id AND bot.handler = ?", handler).Find(&items)
		return items, sharedModels.Paginator{Offset: 0, TotalCount: int(count), CountPerPage: r.defaultLimit}, result.Error
	}

	result := r.DB.Joins("JOIN \"bots-ms_bots\" as bot ON bot.id = \"bots-ms_bot_users\".bot_id AND bot.handler = ?", handler).Limit(opts.GetLimit(r.defaultLimit)).Offset(opts.Offset).Order(opts.GetOrder()).Find(&items)
	return items, sharedModels.Paginator{
		Offset:       opts.Offset,
		TotalCount:   int(count),
		CountPerPage: opts.GetLimit(r.defaultLimit),
	}, result.Error
}

func (r *gormBotUserRepo) GetById(id uint) (*models.BotUser, error) {
	var item *models.BotUser
	result := r.DB.First(&item, id)

	if item != nil && item.ID == 0 {
		return nil, errors.New("bot-user.not-found")
	}

	return item, result.Error
}

func (r *gormBotUserRepo) GetByUserId(botID uint, userId string) (*models.BotUser, error) {
	var item *models.BotUser
	result := r.DB.Where("user_id = ? AND bot_id = ?", userId, botID).Find(&item)

	if item != nil && item.ID == 0 {
		return nil, errors.New("bots-ms.bot-user.not-found")
	}

	return item, result.Error
}

func (r *gormBotUserRepo) GetByMeta(botID uint, meta string) (*models.BotUser, error) {
	var item *models.BotUser
	result := r.DB.Where("meta = ? AND bot_id = ?", meta, botID).First(&item)

	if item != nil && item.ID == 0 {
		return nil, errors.New("bots-ms.bot-user.not-found")
	}

	return item, result.Error
}

func (r *gormBotUserRepo) Update(item *models.BotUser) error {
	result := r.DB.Save(&item)
	return result.Error
}

func (r *gormBotUserRepo) Create(item *models.BotUser) error {
	result := r.DB.Create(&item)
	return result.Error
}

func (r *gormBotUserRepo) Delete(item *models.BotUser) error {
	result := r.DB.Unscoped().Delete(&item)
	return result.Error
}

func NewGormBotUserRepository(db *gorm.DB, defaultLimit int) repositories.BotUserRepository {
	return &gormBotUserRepo{
		DB:           db,
		defaultLimit: defaultLimit,
	}
}
