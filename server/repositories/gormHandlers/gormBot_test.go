package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"regexp"
	"testing"
	"time"
)

type BotSuite struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository repositories.BotRepository
}

func (s *BotSuite) BeforeTest(_, _ string) {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open(postgres.New(postgres.Config{Conn: db}), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix: "bots-ms_",
		},
	})
	require.NoError(s.T(), err)

	s.repository = NewGormBotRepository(s.DB, 10)
}

func (s *BotSuite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestBot(t *testing.T) {
	suite.Run(t, new(BotSuite))
}

// ------------------------------------------------
//
// # GetById
//
// ------------------------------------------------
func (s *BotSuite) Test_gormBot_GetById() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_bots" WHERE "bots-ms_bots"."id" = $1 AND "bots-ms_bots"."deleted_at" IS NULL ORDER BY "bots-ms_bots"."id" LIMIT 1`)).
		WithArgs(int64(10)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "token", "slug", "handler"}).
			AddRow("1", time.Now(), time.Now(), nil, "token", "slugValue", "telegram"))

	item, err := s.repository.GetById(uint(10))
	require.NoError(s.T(), err)
	require.Equal(s.T(), item.ID, uint(1))
}

// ------------------------------------------------
//
// # GetBySlug
//
// ------------------------------------------------
func (s *BotSuite) Test_gormBot_GetBySlug() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_bots" WHERE slug = $1 AND "bots-ms_bots"."deleted_at" IS NULL ORDER BY "bots-ms_bots"."id" LIMIT 1`)).
		WithArgs("slugValue").
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "token", "slug", "handler"}).
			AddRow("1", time.Now(), time.Now(), nil, "token", "slugValue", "telegram"))

	item, err := s.repository.GetBySlug("slugValue")
	require.NoError(s.T(), err)
	require.Equal(s.T(), item.ID, uint(1))
}

// ------------------------------------------------
//
// List
//
// ------------------------------------------------
//func (s *BotSuite) Test_gormBot_List() {
//	s.mock.ExpectQuery(regexp.QuoteMeta(
//		`SELECT * FROM "bots-ms_bots" WHERE "bots-ms_bots"."deleted_at" IS NULL`)).
//		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "token", "slug", "handler"}).
//			AddRow("1", time.Now(), time.Now(), nil, "token1", "slugValue1", "telegram").
//			AddRow("2", time.Now(), time.Now(), nil, "token2", "slugValue2", "telegram").
//			AddRow("3", time.Now(), time.Now(), nil, "token3", "slugValue3", "telegram").
//			AddRow("4", time.Now(), time.Now(), nil, "token4", "slugValue4", "telegram"))
//
//	items, err := s.repository.List(nil)
//	require.NoError(s.T(), err)
//	require.Equal(s.T(), len(*items), 4)
//}

// ------------------------------------------------
//
// # Update
//
// ------------------------------------------------
func (s *BotSuite) Test_gormBot_Update() {
	item := models.Bot{
		Model: gorm.Model{
			ID: uint(1),
		},
		Token:   "",
		Slug:    "",
		Handler: "",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "bots-ms_bots" SET "created_at"=$1,"updated_at"=$2,"deleted_at"=$3,"env"=$4,"token"=$5,"slug"=$6,"handler"=$7,"executor_handler"=$8,"allow_plain_command"=$9,"disable_triggers"=$10,"auto_remove_messages"=$11 WHERE "bots-ms_bots"."deleted_at" IS NULL AND "id" = $12`)).
		WithArgs(sharedModels.Any{}, sharedModels.Any{}, nil, item.Env, item.Token, item.Slug, item.Handler, item.ExecutorHandler, item.AllowPlainCommand, item.DisableTriggers, item.AutoRemoveMessages, uint(1)).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	err := s.repository.Update(&item)
	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *BotSuite) Test_gormBot_Create() {
	item := models.Bot{
		Token:           "",
		Slug:            "",
		Handler:         "",
		Env:             "production",
		ExecutorHandler: "event",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "bots-ms_bots" ("created_at","updated_at","deleted_at","env","token","slug","handler","executor_handler","allow_plain_command","disable_triggers","auto_remove_messages") VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) RETURNING "id"`)).
		WithArgs(sharedModels.Any{}, sharedModels.Any{}, nil, item.Env, item.Token, item.Slug, item.Handler, item.ExecutorHandler, item.AllowPlainCommand, item.DisableTriggers, item.AutoRemoveMessages).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow("1"))
	s.mock.ExpectCommit()

	err := s.repository.Create(&item)
	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *BotSuite) Test_gormBot_Delete() {
	item := models.Bot{
		Model: gorm.Model{
			ID: uint(1),
		},
		Token:   "",
		Slug:    "",
		Handler: "",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "bots-ms_bots" SET "deleted_at"=$1 WHERE "bots-ms_bots"."id" = $2 AND "bots-ms_bots"."deleted_at" IS NULL`)).
		WithArgs(sharedModels.Any{}, uint(1)).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	err := s.repository.Delete(&item)
	require.NoError(s.T(), err)
}
