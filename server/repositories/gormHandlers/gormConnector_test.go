package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"regexp"
	"testing"
	"time"
)

type ConnectorSuite struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository repositories.ConnectorRepository
}

func (s *ConnectorSuite) BeforeTest(_, _ string) {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open(postgres.New(postgres.Config{Conn: db}), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix: "bots-ms_",
		},
	})
	require.NoError(s.T(), err)

	s.repository = NewGormConnectorRepository(s.DB)
}

func (s *ConnectorSuite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestConnector(t *testing.T) {
	suite.Run(t, new(ConnectorSuite))
}

// ------------------------------------------------
//
// # List
//
// ------------------------------------------------
func (s *ConnectorSuite) Test_gormConnector_List() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_connectors" WHERE scenario_id = $1`)).
		WithArgs(uint(12)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "name", "method", "scheme"}).
			AddRow("1", time.Now(), time.Now(), nil, "1234", "1", "daaatttaa"))

	items, err := s.repository.List(uint(12))
	require.NoError(s.T(), err)
	require.Equal(s.T(), len(*items), 1)
}

// ------------------------------------------------
//
// # GetById
//
// ------------------------------------------------
func (s *ConnectorSuite) Test_gormConnector_GetById() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_connectors" WHERE "bots-ms_connectors"."id" = $1 ORDER BY "bots-ms_connectors"."id" LIMIT 1`)).
		WithArgs(uint(10)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "name", "method", "scheme"}).
			AddRow("1", time.Now(), time.Now(), nil, "1234", "1", "daaatttaa"))

	item, err := s.repository.GetById(uint(10))
	require.NoError(s.T(), err)
	require.Equal(s.T(), item.Id, uint(1))
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *ConnectorSuite) Test_gormConnector_Create() {
	item := models.Connector{
		FromScenarioAction: uint(1),
		FromSocket:         uint(1),
		ToScenarioAction:   uint(2),
		ToSocket:           uint(2),
		ScenarioId:         uint(1),
	}

	s.mock.ExpectBegin()
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "bots-ms_connectors" ("from_scenario_action","from_socket","to_scenario_action","to_socket","scenario_id") VALUES ($1,$2,$3,$4,$5) RETURNING "id"`)).
		WithArgs(uint(1), uint(1), uint(2), uint(2), uint(1)).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow("1"))
	s.mock.ExpectCommit()

	err := s.repository.Create(&item)
	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *ConnectorSuite) Test_gormConnector_Delete() {
	item := models.Connector{
		Id:                 uint(1),
		FromScenarioAction: uint(1),
		FromSocket:         uint(1),
		ToScenarioAction:   uint(2),
		ToSocket:           uint(2),
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`DELETE FROM "bots-ms_connectors" WHERE "bots-ms_connectors"."id" = $1`)).
		WithArgs(uint(1)).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	err := s.repository.Delete(&item)
	require.NoError(s.T(), err)
}
