package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"regexp"
	"testing"
	"time"
)

type BotUserSuite struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository repositories.BotUserRepository
}

func (s *BotUserSuite) BeforeTest(_, _ string) {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open(postgres.New(postgres.Config{Conn: db}), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix: "bots-ms_",
		},
	})
	require.NoError(s.T(), err)

	s.repository = NewGormBotUserRepository(s.DB, 10)
}

func (s *BotUserSuite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestBotUser(t *testing.T) {
	suite.Run(t, new(BotUserSuite))
}

// ------------------------------------------------
//
// # GetById
//
// ------------------------------------------------
func (s *BotUserSuite) Test_gormBotUser_GetById() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_bot_users" WHERE "bots-ms_bot_users"."id" = $1 AND "bots-ms_bot_users"."deleted_at" IS NULL ORDER BY "bots-ms_bot_users"."id" LIMIT 1`)).
		WithArgs(int64(10)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "token", "slug", "handler"}).
			AddRow("1", time.Now(), time.Now(), nil, "token", "slugValue", "telegram"))

	item, err := s.repository.GetById(uint(10))
	require.NoError(s.T(), err)
	require.Equal(s.T(), item.ID, uint(1))
}

// ------------------------------------------------
//
// # GetByUserId
//
// ------------------------------------------------
func (s *BotUserSuite) Test_gormBotUser_GetByUserId() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_bot_users" WHERE (user_id = $1 AND bot_id = $2) AND "bots-ms_bot_users"."deleted_at" IS NULL`)).
		WithArgs("userIdValue", uint(12)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "bot_id", "user_id", "meta"}).
			AddRow("1", time.Now(), time.Now(), nil, "12", "userIdValue", "metaUserBot"))

	item, err := s.repository.GetByUserId(uint(12), "userIdValue")
	require.NoError(s.T(), err)
	require.Equal(s.T(), item.ID, uint(1))
	require.Equal(s.T(), item.BotID, uint(12))
}

// ------------------------------------------------
//
// # GetByUserId
//
// ------------------------------------------------
func (s *BotUserSuite) Test_gormBotUser_GetByMeta() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_bot_users" WHERE (meta = $1 AND bot_id = $2) AND "bots-ms_bot_users"."deleted_at" IS NULL`)).
		WithArgs("metaValue", uint(12)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "bot_id", "user_id", "meta"}).
			AddRow("1", time.Now(), time.Now(), nil, "12", "userIdValue", "metaUserBot"))

	item, err := s.repository.GetByMeta(uint(12), "metaValue")
	require.NoError(s.T(), err)
	require.Equal(s.T(), item.ID, uint(1))
	require.Equal(s.T(), item.BotID, uint(12))
}

//
//// ------------------------------------------------
////
//// List
////
//// ------------------------------------------------
//func (s *BotUserSuite) Test_gormBotUser_List() {
//	s.mock.ExpectQuery(regexp.QuoteMeta(
//		`SELECT "bots-ms_bot_users"."id","bots-ms_bot_users"."created_at","bots-ms_bot_users"."updated_at","bots-ms_bot_users"."deleted_at","bots-ms_bot_users"."bot_id","bots-ms_bot_users"."user_id","bots-ms_bot_users"."nickname","bots-ms_bot_users"."meta","bots-ms_bot_users"."token","bots-ms_bot_users"."blocked" FROM "bots-ms_bot_users" JOIN "bots-ms_bots" as bot ON bot.id = "bots-ms_bot_users".bot_id AND bot.handler = $1 WHERE "bots-ms_bot_users"."deleted_at" IS NULL ORDER BY id desc`)).
//		WithArgs("handler").
//		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "bot_id", "user_id", "meta", "token"}).
//			AddRow("1", time.Now(), time.Now(), nil, "1", "user1", "meta1", "token1").
//			AddRow("2", time.Now(), time.Now(), nil, "2", "user2", "meta12", "token2").
//			AddRow("3", time.Now(), time.Now(), nil, "3", "user3", "meta13", "token3").
//			AddRow("4", time.Now(), time.Now(), nil, "4", "user4", "meta14", "token4"))
//
//	_, err := s.repository.List("handler", nil)
//	require.NoError(s.T(), err)
//	//require.Equal(s.T(), len(*items), 4)
//}

// ------------------------------------------------
//
// # Update
//
// ------------------------------------------------
func (s *BotUserSuite) Test_gormBotUser_Update() {
	item := models.BotUser{
		Model: gorm.Model{
			ID: uint(1),
		},
		BotID:  uint(12),
		UserID: "useeeer",
		Meta:   "meeeta",
		Token:  "token",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "bots-ms_bot_users" SET "created_at"=$1,"updated_at"=$2,"deleted_at"=$3,"bot_id"=$4,"user_id"=$5,"nickname"=$6,"meta"=$7,"token"=$8,"blocked"=$9 WHERE "bots-ms_bot_users"."deleted_at" IS NULL AND "id" = $10`)).
		WithArgs(sharedModels.Any{}, sharedModels.Any{}, nil, item.BotID, item.UserID, item.Nickname, item.Meta, item.Token, item.Blocked, item.ID).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	err := s.repository.Update(&item)
	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *BotUserSuite) Test_gormBotUser_Create() {
	item := models.BotUser{
		BotID:  uint(12),
		UserID: "useeeer",
		Meta:   "meeeta",
		Token:  "token",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "bots-ms_bot_users" ("created_at","updated_at","deleted_at","bot_id","user_id","nickname","meta","token","blocked") VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9) RETURNING "id"`)).
		WithArgs(sharedModels.Any{}, sharedModels.Any{}, nil, item.BotID, item.UserID, item.Nickname, item.Meta, item.Token, item.Blocked).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow("1"))
	s.mock.ExpectCommit()

	err := s.repository.Create(&item)
	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *BotUserSuite) Test_gormBotUser_Delete() {
	item := models.BotUser{
		Model: gorm.Model{
			ID: uint(1),
		},
		BotID:  uint(12),
		UserID: "useeeer",
		Meta:   "meeeta",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`DELETE FROM "bots-ms_bot_users" WHERE "bots-ms_bot_users"."id" = $1`)).
		WithArgs(uint(1)).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	err := s.repository.Delete(&item)
	require.NoError(s.T(), err)
}
