package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	models2 "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"regexp"
	"testing"
	"time"
)

type StoredMessageSuite struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository repositories.StoredMessagesRepository
}

func (s *StoredMessageSuite) BeforeTest(_, _ string) {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open(postgres.New(postgres.Config{Conn: db}), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix: "bots-ms_",
		},
	})
	require.NoError(s.T(), err)

	s.repository = NewGormStoredMessageRepository(s.DB)
}

func (s *StoredMessageSuite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestStoredMessage(t *testing.T) {
	suite.Run(t, new(StoredMessageSuite))
}

// ------------------------------------------------
//
// # Get
//
// ------------------------------------------------
func (s *StoredMessageSuite) Test_gormStoredMessage_Get() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_stored_messages" WHERE "bots-ms_stored_messages"."id" = $1 AND "bots-ms_stored_messages"."deleted_at" IS NULL ORDER BY "bots-ms_stored_messages"."id" LIMIT 1`)).
		WithArgs(uint(10)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "bot_id", "command"}).
			AddRow("1", time.Now(), time.Now(), nil, "1234", "daaatttaa"))

	item, err := s.repository.Get(uint(10))
	require.NoError(s.T(), err)
	require.Equal(s.T(), item.ID, uint(1))
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *StoredMessageSuite) Test_gormStoredMessage_Create() {
	item := models.StoredMessage{
		MessageId: "abc",
		Text:      "Message text",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "bots-ms_stored_messages" ("created_at","updated_at","deleted_at","message_id","text") VALUES ($1,$2,$3,$4,$5) RETURNING "id"`)).
		WithArgs(models2.Any{}, models2.Any{}, nil, item.MessageId, item.Text).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow("1"))
	s.mock.ExpectCommit()

	err := s.repository.Create(&item)
	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *StoredMessageSuite) Test_gormStoredMessage_Delete() {
	item := models.StoredMessage{
		Model: gorm.Model{
			ID: uint(1),
		},
		Text: "Message text",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "bots-ms_stored_messages" SET "deleted_at"=$1 WHERE "bots-ms_stored_messages"."id" = $2 AND "bots-ms_stored_messages"."deleted_at" IS NULL`)).
		WithArgs(models2.Any{}, uint(1)).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	err := s.repository.Delete(&item)
	require.NoError(s.T(), err)
}
