package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"regexp"
	"testing"
	"time"
)

type ScenarioActionSuite struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository repositories.ScenarioActionRepository
}

func (s *ScenarioActionSuite) BeforeTest(_, _ string) {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open(postgres.New(postgres.Config{Conn: db}), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix: "bots-ms_",
		},
	})
	require.NoError(s.T(), err)

	s.repository = NewGormScenarioActionRepository(s.DB, 10)
}

func (s *ScenarioActionSuite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestScenarioAction(t *testing.T) {
	suite.Run(t, new(ScenarioActionSuite))
}

// ------------------------------------------------
//
// # List
//
// ------------------------------------------------
func (s *ScenarioActionSuite) Test_gormScenarioAction_List() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_scenario_actions" WHERE scenario_id = $1 ORDER BY id desc LIMIT 10`)).
		WithArgs(uint(12)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "name", "method", "scheme"}).
			AddRow("1", time.Now(), time.Now(), nil, "1234", "1", "daaatttaa"))

	items, err := s.repository.List(uint(12), &sharedModels.ListOptions{})
	require.NoError(s.T(), err)
	require.Equal(s.T(), len(*items), 1)
}

// ------------------------------------------------
//
// # GetById
//
// ------------------------------------------------
func (s *ScenarioActionSuite) Test_gormScenarioAction_GetById() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_scenario_actions" WHERE "bots-ms_scenario_actions"."id" = $1 ORDER BY "bots-ms_scenario_actions"."id" LIMIT 1`)).
		WithArgs(uint(10)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "bot_id", "scenario_id", "command"}).
			AddRow("1", time.Now(), time.Now(), nil, "1234", "1", "daaatttaa"))

	item, err := s.repository.GetById(uint(10))
	require.NoError(s.T(), err)
	require.Equal(s.T(), item.Id, uint(1))
}

// ------------------------------------------------
//
// # Update
//
// ------------------------------------------------
func (s *ScenarioActionSuite) Test_gormScenarioAction_Update() {
	item := models.ScenarioAction{
		Id:         uint(1),
		ScenarioId: uint(3),
		ActionId:   uint(4),
		Data:       "newData",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`UPDATE "bots-ms_scenario_actions" SET "meta"=$1,"scenario_id"=$2,"action_id"=$3,"data"=$4 WHERE "id" = $5`)).
		WithArgs("", item.ScenarioId, item.ActionId, item.Data, item.Id).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	err := s.repository.Update(&item)
	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *ScenarioActionSuite) Test_gormScenarioAction_Create() {
	item := models.ScenarioAction{
		ScenarioId: uint(3),
		ActionId:   uint(4),
		Data:       "data",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "bots-ms_scenario_actions" ("meta","scenario_id","action_id","data") VALUES ($1,$2,$3,$4) RETURNING "id"`)).
		WithArgs("", item.ScenarioId, item.ActionId, item.Data).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow("1"))
	s.mock.ExpectCommit()

	err := s.repository.Create(&item)
	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *ScenarioActionSuite) Test_gormScenarioAction_Delete() {
	item := models.ScenarioAction{
		Id:         uint(1),
		ScenarioId: uint(3),
		ActionId:   uint(4),
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`DELETE FROM "bots-ms_scenario_actions" WHERE "bots-ms_scenario_actions"."id" = $1`)).
		WithArgs(uint(1)).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	err := s.repository.Delete(&item)
	require.NoError(s.T(), err)
}
