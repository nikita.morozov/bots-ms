package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"errors"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/gorm"
)

type gormStateMachineRepo struct {
	DB           *gorm.DB
	defaultLimit int
}

func (r *gormStateMachineRepo) List(opts *sharedModels.ListOptions) (*[]models.StateMachine, sharedModels.Paginator, error) {
	var items *[]models.StateMachine
	var count int64
	r.DB.Model(&models.StateMachine{}).Count(&count)

	if opts == nil {
		result := r.DB.Order("id desc").Find(&items)
		return items, sharedModels.Paginator{Offset: 0, TotalCount: int(count), CountPerPage: r.defaultLimit}, result.Error
	}

	result := r.DB.Limit(opts.GetLimit(r.defaultLimit)).Offset(opts.Offset).Order(opts.GetOrder()).Find(&items)
	return items, sharedModels.Paginator{
		Offset:       opts.Offset,
		TotalCount:   int(count),
		CountPerPage: opts.GetLimit(r.defaultLimit),
	}, result.Error
}

func (r *gormStateMachineRepo) Count() (uint64, error) {
	var count int64
	r.DB.Model(&models.StateMachine{}).Count(&count)
	return uint64(count), nil
}

func (r *gormStateMachineRepo) GetById(id uint) (*models.StateMachine, error) {
	var item *models.StateMachine
	result := r.DB.First(&item, id)

	if item.ID == 0 {
		return nil, errors.New("bots-ms.scenario-action.not-found")
	}

	return item, result.Error
}

func (r *gormStateMachineRepo) GetByBotId(botId uint) (*models.StateMachine, error) {
	var item *models.StateMachine
	result := r.DB.Where("bot_id=?", botId).First(&item)

	if item.ID == 0 {
		return nil, errors.New("bots-ms.scenario-action.not-found")
	}

	return item, result.Error
}

func (r *gormStateMachineRepo) Update(item *models.StateMachine) error {
	result := r.DB.Save(&item)
	return result.Error
}

func (r *gormStateMachineRepo) Create(item *models.StateMachine) error {
	result := r.DB.Create(&item)
	return result.Error
}

func (r *gormStateMachineRepo) Delete(item *models.StateMachine) error {
	result := r.DB.Delete(&item)
	return result.Error
}

func NewGormStateMachineRepository(db *gorm.DB, listLimit int) repositories.StateMachineRepository {
	return &gormStateMachineRepo{
		DB:           db,
		defaultLimit: listLimit,
	}
}
