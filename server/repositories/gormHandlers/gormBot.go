package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"errors"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/gorm"
)

type gormBotRepo struct {
	DB           *gorm.DB
	defaultLimit int
}

func (r *gormBotRepo) GetById(id uint) (*models.Bot, error) {
	var item *models.Bot
	result := r.DB.First(&item, id)

	if item != nil && item.ID == 0 {
		return nil, errors.New("bots-ms.bot.not-found")
	}

	return item, result.Error
}

func (r *gormBotRepo) GetBySlug(slug string) (*models.Bot, error) {
	var item *models.Bot
	result := r.DB.Where("slug = ?", slug).First(&item)

	if item != nil && item.ID == 0 {
		return nil, errors.New("bots-ms.bot.not-found")
	}

	return item, result.Error
}

func (r *gormBotRepo) Update(item *models.Bot) error {
	result := r.DB.Save(&item)
	return result.Error
}

func (r *gormBotRepo) Create(item *models.Bot) error {
	result := r.DB.Create(&item)
	return result.Error
}

func (r *gormBotRepo) Delete(item *models.Bot) error {
	result := r.DB.Delete(&item)
	return result.Error
}

func (r *gormBotRepo) Count() (uint64, error) {
	var count int64
	r.DB.Model(&models.Bot{}).Count(&count)
	return uint64(count), nil
}

func (r *gormBotRepo) List(opts *sharedModels.ListOptions) (*[]models.Bot, sharedModels.Paginator, error) {
	var items *[]models.Bot
	var count int64
	r.DB.Model(&models.Bot{}).Count(&count)

	if opts == nil {
		result := r.DB.Order("id desc").Find(&items)
		return items, sharedModels.Paginator{Offset: 0, TotalCount: int(count), CountPerPage: r.defaultLimit}, result.Error
	}

	result := r.DB.Limit(opts.GetLimit(r.defaultLimit)).Offset(opts.Offset).Order(opts.GetOrder()).Find(&items)
	return items, sharedModels.Paginator{
		Offset:       opts.Offset,
		TotalCount:   int(count),
		CountPerPage: opts.GetLimit(r.defaultLimit),
	}, result.Error
}

func (r *gormBotRepo) ListOfEnv(env string) (*[]models.Bot, sharedModels.Paginator, error) {
	var items *[]models.Bot
	var count int64
	r.DB.Model(&models.Bot{}).Count(&count)

	result := r.DB.Order("id desc").Where("env = ?", env).Find(&items)
	return items, sharedModels.Paginator{Offset: 0, TotalCount: int(count), CountPerPage: r.defaultLimit}, result.Error
}

func NewGormBotRepository(db *gorm.DB, defaultLimit int) repositories.BotRepository {
	return &gormBotRepo{
		DB:           db,
		defaultLimit: defaultLimit,
	}
}
