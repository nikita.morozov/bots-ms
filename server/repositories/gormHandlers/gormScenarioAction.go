package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"errors"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/gorm"
)

type gormScenarioActionRepo struct {
	DB           *gorm.DB
	defaultLimit int
}

func (r *gormScenarioActionRepo) List(scenarioId uint, opt *sharedModels.ListOptions) (*[]models.ScenarioAction, error) {
	var items *[]models.ScenarioAction
	result := r.DB.Where("scenario_id = ?", scenarioId)

	if opt != nil {
		result = result.Limit(opt.GetLimit(r.defaultLimit)).Offset(opt.Offset).Order(opt.GetOrder())
	}

	result = result.Find(&items)
	return items, result.Error
}

func (r *gormScenarioActionRepo) GetById(id uint) (*models.ScenarioAction, error) {
	var item *models.ScenarioAction
	result := r.DB.First(&item, id)

	if item.Id == 0 {
		return nil, errors.New("bots-ms.scenario-action.not-found")
	}

	return item, result.Error
}

func (r *gormScenarioActionRepo) Update(item *models.ScenarioAction) error {
	result := r.DB.Save(&item)
	return result.Error
}

func (r *gormScenarioActionRepo) Create(item *models.ScenarioAction) error {
	result := r.DB.Create(&item)
	return result.Error
}

func (r *gormScenarioActionRepo) Delete(item *models.ScenarioAction) error {
	result := r.DB.Delete(&item)
	return result.Error
}

func NewGormScenarioActionRepository(db *gorm.DB, listLimit int) repositories.ScenarioActionRepository {
	return &gormScenarioActionRepo{
		DB:           db,
		defaultLimit: listLimit,
	}
}
