package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"errors"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/gorm"
)

type gormEventRepo struct {
	DB           *gorm.DB
	defaultLimit int
}

func (r *gormEventRepo) GetById(id uint) (*models.Event, error) {
	var item *models.Event
	result := r.DB.First(&item, id)

	if item.ID == 0 {
		return nil, errors.New("bots-ms.event.not-found")
	}

	return item, result.Error
}

func (r *gormEventRepo) List(opts *sharedModels.ListOptions) (*[]models.Event, sharedModels.Paginator, error) {
	var items *[]models.Event
	var count int64
	r.DB.Model(&models.Event{}).Count(&count)

	if opts == nil {
		result := r.DB.Order("id desc").Limit(opts.GetLimit(r.defaultLimit)).Offset(opts.Offset).Order(opts.GetOrder()).Find(&items)
		return items, sharedModels.Paginator{Offset: 0, TotalCount: int(count), CountPerPage: r.defaultLimit}, result.Error
	}

	result := r.DB.Limit(opts.GetLimit(r.defaultLimit)).Offset(opts.Offset).Order(opts.GetOrder()).Find(&items)
	return items, sharedModels.Paginator{
		Offset:       opts.Offset,
		TotalCount:   int(count),
		CountPerPage: opts.GetLimit(r.defaultLimit),
	}, result.Error
}

func (r *gormEventRepo) GetByHandlerName(name string, botSlug string) (*models.Event, error) {
	var item *models.Event
	result := r.DB.Joins("JOIN \"bots-ms_bots\" as bot ON bot.id = \"bots-ms_events\".bot_id AND bot.slug = ?", botSlug).Where("command = ?", name).Find(&item)
	if item.ID == 0 {
		return nil, errors.New("bots-ms.event.not-found")
	}

	return item, result.Error
}

func (r *gormEventRepo) Update(item *models.Event) error {
	result := r.DB.Save(&item)
	return result.Error
}

func (r *gormEventRepo) Create(item *models.Event) error {
	result := r.DB.Create(&item)
	return result.Error
}

func (r *gormEventRepo) Delete(item *models.Event) error {
	result := r.DB.Delete(&item)
	return result.Error
}

func (r *gormEventRepo) Count() (uint64, error) {
	var count int64
	r.DB.Model(&models.Event{}).Count(&count)
	return uint64(count), nil
}

func NewGormEventRepository(db *gorm.DB, defaultLimit int) repositories.EventRepository {
	return &gormEventRepo{
		DB:           db,
		defaultLimit: defaultLimit,
	}
}
