package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"errors"
	"gorm.io/gorm"
)

type gormScenarioItemRepo struct {
	DB *gorm.DB
}

func (r *gormScenarioItemRepo) List(scenarioId uint) (*[]models.ScenarioItem, error) {
	var items *[]models.ScenarioItem
	result := r.DB.Where("scenario_id = ?", scenarioId)
	result = result.Find(&items)
	return items, result.Error
}

func (r *gormScenarioItemRepo) GetById(id uint) (*models.ScenarioItem, error) {
	var item *models.ScenarioItem
	result := r.DB.First(&item, id)

	if item.Id == 0 {
		return nil, errors.New("bots-ms.scenario-action.not-found")
	}

	return item, result.Error
}

func (r *gormScenarioItemRepo) Update(item *models.ScenarioItem) error {
	result := r.DB.Save(&item)
	return result.Error
}

func (r *gormScenarioItemRepo) Create(item *models.ScenarioItem) error {
	result := r.DB.Create(&item)
	return result.Error
}

func (r *gormScenarioItemRepo) Delete(item *models.ScenarioItem) error {
	result := r.DB.Delete(&item)
	return result.Error
}

func NewGormScenarioItemRepository(db *gorm.DB) repositories.ScenarioItemRepository {
	return &gormScenarioItemRepo{
		DB: db,
	}
}
