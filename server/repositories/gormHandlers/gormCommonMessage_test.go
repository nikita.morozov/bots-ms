package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"database/sql"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"regexp"
	"testing"
	"time"
)

type CommonMessageSuite struct {
	suite.Suite
	DB   *gorm.DB
	mock sqlmock.Sqlmock

	repository repositories.CommonMessage
}

func (s *CommonMessageSuite) BeforeTest(_, _ string) {
	var (
		db  *sql.DB
		err error
	)

	db, s.mock, err = sqlmock.New()
	require.NoError(s.T(), err)

	s.DB, err = gorm.Open(postgres.New(postgres.Config{Conn: db}), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix: "bots-ms_",
		},
	})
	require.NoError(s.T(), err)

	s.repository = NewGormCommonMessageRepository(s.DB)
}

func (s *CommonMessageSuite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestCommonMessageUser(t *testing.T) {
	suite.Run(t, new(CommonMessageSuite))
}

// ------------------------------------------------
//
// # Get
//
// ------------------------------------------------
func (s *CommonMessageSuite) Test_gormCommonMessage_Get() {
	data := "someData"
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_common_messages" WHERE data = $1 AND "bots-ms_common_messages"."deleted_at" IS NULL ORDER BY "bots-ms_common_messages"."id" LIMIT 1`)).
		WithArgs(data).
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "message_id", "data"}).
			AddRow("1", time.Now(), time.Now(), nil, "12", "someData"))

	item, err := s.repository.Get(data)
	require.NoError(s.T(), err)
	require.Equal(s.T(), item.ID, uint(1))
}

// ------------------------------------------------
//
// # ListByMessageId
//
// ------------------------------------------------
func (s *CommonMessageSuite) Test_gormCommonMessage_ListByMessageId() {
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`SELECT * FROM "bots-ms_common_messages" WHERE message_id = $1 AND "bots-ms_common_messages"."deleted_at" IS NULL ORDER BY "bots-ms_common_messages"."id" LIMIT 1`)).
		WithArgs(uint(1)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "created_at", "updated_at", "deleted_at", "bot_id", "user_id", "meta"}).
			AddRow("1", time.Now(), time.Now(), nil, "1", "user1", "meta1").
			AddRow("2", time.Now(), time.Now(), nil, "2", "user2", "meta12").
			AddRow("3", time.Now(), time.Now(), nil, "3", "user3", "meta13").
			AddRow("4", time.Now(), time.Now(), nil, "4", "user4", "meta14"))

	items, err := s.repository.ListByMessageId(uint(1))
	require.NoError(s.T(), err)
	require.Equal(s.T(), len(*items), 4)
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *CommonMessageSuite) Test_gormCommonMessage_Create() {
	messageId := uint(12)
	item := models.CommonMessage{
		MessageId: &messageId,
		Data:      "Daaata",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectQuery(regexp.QuoteMeta(
		`INSERT INTO "bots-ms_common_messages" ("created_at","updated_at","deleted_at","message_id","data") VALUES ($1,$2,$3,$4,$5) RETURNING "id"`)).
		WithArgs(sharedModels.Any{}, sharedModels.Any{}, nil, item.MessageId, item.Data).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow("1"))
	s.mock.ExpectCommit()

	err := s.repository.Create(&item)
	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *CommonMessageSuite) Test_gormCommonMessage_Delete() {
	messageId := uint(12)
	item := models.CommonMessage{
		Model: gorm.Model{
			ID: uint(1),
		},
		MessageId: &messageId,
		Data:      "Daaata",
	}

	s.mock.ExpectBegin()
	s.mock.ExpectExec(regexp.QuoteMeta(
		`DELETE FROM "bots-ms_common_messages" WHERE "bots-ms_common_messages"."id" = $1`)).
		WithArgs(uint(1)).
		WillReturnResult(sqlmock.NewResult(1, 1))
	s.mock.ExpectCommit()

	err := s.repository.Delete(&item)
	require.NoError(s.T(), err)
}
