package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"errors"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type gormStateTransitionRepo struct {
	DB *gorm.DB
}

func (r *gormStateTransitionRepo) List(stateMachineId uint) (*[]models.StateTransition, error) {
	var items *[]models.StateTransition
	result := r.DB.Joins("JOIN \"bots-ms_states\" as state ON state.id = \"bots-ms_state_transitions\".state_id AND state.state_machine_id = ?", stateMachineId).Preload(clause.Associations).Find(&items)

	return items, result.Error
}

func (r *gormStateTransitionRepo) GetById(id uint) (*models.StateTransition, error) {
	var item *models.StateTransition
	result := r.DB.First(&item, id)

	if item.ID == 0 {
		return nil, errors.New("bots-ms.scenario-action.not-found")
	}

	return item, result.Error
}

func (r *gormStateTransitionRepo) Update(item *models.StateTransition) error {
	result := r.DB.Save(&item)
	return result.Error
}

func (r *gormStateTransitionRepo) Create(item *models.StateTransition) error {
	result := r.DB.Create(&item)
	return result.Error
}

func (r *gormStateTransitionRepo) Delete(item *models.StateTransition) error {
	result := r.DB.Delete(&item)
	return result.Error
}

func (r *gormStateTransitionRepo) DeleteByStateId(id uint) error {
	result := r.DB.Where("state_id = ? OR to_state_id = ?", id, id).Delete(models.StateTransition{})
	return result.Error
}

func NewGormStateTransitionRepository(db *gorm.DB) repositories.StateTransitionRepository {
	return &gormStateTransitionRepo{
		DB: db,
	}
}
