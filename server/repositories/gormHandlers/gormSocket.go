package gormHandlers

import (
	"bots-ms/repositories"
	"errors"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"gorm.io/gorm"
)

type gormSocketRepo struct {
	DB *gorm.DB
}

func (r *gormSocketRepo) List(actionId uint, direction uint8) (*[]coreModel.Socket, error) {
	var items *[]coreModel.Socket
	result := r.DB.Where("action_id = ? AND direction = ?", actionId, direction)
	result = result.Find(&items)
	return items, result.Error
}

func (r *gormSocketRepo) GetBySlug(actionId uint, direction uint8, slug string) (*coreModel.Socket, error) {
	var item *coreModel.Socket
	result := r.DB.Where("action_id = ? AND direction = ? AND slug = ?", actionId, direction, slug)
	result = result.First(&item)
	if item.Id == 0 {
		return nil, errors.New("bots-ms.socket.not-found")
	}

	return item, result.Error
}

func (r *gormSocketRepo) GetByType(actionId uint, direction uint8, sType uint8) (*[]coreModel.Socket, error) {
	var items *[]coreModel.Socket
	result := r.DB.Where("action_id = ? AND direction = ? AND type = ?", actionId, direction, sType)
	result = result.Find(&items)
	return items, result.Error
}

func (r *gormSocketRepo) GetById(id uint) (*coreModel.Socket, error) {
	var item *coreModel.Socket
	result := r.DB.First(&item, id)

	if item.Id == 0 {
		return nil, errors.New("bots-ms.socket.not-found")
	}

	return item, result.Error
}

func (r *gormSocketRepo) Update(item *coreModel.Socket) error {
	result := r.DB.Save(&item)
	return result.Error
}

func (r *gormSocketRepo) Create(item *coreModel.Socket) error {
	result := r.DB.Create(&item)
	return result.Error
}

func (r *gormSocketRepo) Delete(item *coreModel.Socket) error {
	result := r.DB.Delete(&item)
	return result.Error
}

func NewGormSocketRepository(db *gorm.DB) repositories.SocketRepository {
	return &gormSocketRepo{
		DB: db,
	}
}
