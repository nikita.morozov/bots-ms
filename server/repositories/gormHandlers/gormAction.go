package gormHandlers

import (
	"bots-ms/repositories"
	"errors"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"gorm.io/gorm"
)

type gormActionRepository struct {
	DB *gorm.DB
}

func (r *gormActionRepository) GetByHandlerName(name string) (*coreModel.Action, error) {
	var item *coreModel.Action
	result := r.DB.Preload("Sockets").Where("handler = ?", name).Find(&item)

	if item != nil && item.Id == 0 {
		return nil, errors.New("bots-ms.action.not-found")
	}

	return item, result.Error
}

func (r *gormActionRepository) List() (*[]coreModel.Action, error) {
	var items *[]coreModel.Action
	err := r.DB.Preload("Sockets").Find(&items)
	return items, err.Error
}

func (r *gormActionRepository) Count() (uint64, error) {
	var count int64
	err := r.DB.Preload("Sockets").Count(&count)
	return uint64(count), err.Error
}

func (r *gormActionRepository) GetById(id uint) (*coreModel.Action, error) {
	var item *coreModel.Action
	result := r.DB.Preload("Sockets").First(&item, id)

	if item != nil && item.Id == 0 {
		return nil, errors.New("bots-ms.action.not-found")
	}

	return item, result.Error
}

func (r *gormActionRepository) Update(item *coreModel.Action) error {
	result := r.DB.Save(&item)
	return result.Error
}

func (r *gormActionRepository) Create(item *coreModel.Action) error {
	result := r.DB.Preload("Sockets").Create(&item)
	return result.Error
}

func (r *gormActionRepository) Delete(item *coreModel.Action) error {
	result := r.DB.Delete(&item)
	return result.Error
}

func NewGormActionRepository(db *gorm.DB) repositories.ActionRepository {
	return &gormActionRepository{
		DB: db,
	}
}
