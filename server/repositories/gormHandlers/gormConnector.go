package gormHandlers

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"errors"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"gorm.io/gorm"
)

type gormConnectorRepo struct {
	DB *gorm.DB
}

func (r *gormConnectorRepo) List(scenarioId uint) (*[]models.Connector, error) {
	var items *[]models.Connector
	result := r.DB.Where("scenario_id = ?", scenarioId)
	result = result.Find(&items)
	return items, result.Error
}

func (r *gormConnectorRepo) ListWithDirection(scenarioActionId uint, direction uint8) (*[]models.Connector, error) {
	var items *[]models.Connector
	result := r.DB

	if direction == coreModel.SocketDirectionIn {
		result = result.Where("to_scenario_action = ?", scenarioActionId)
		result = result.Joins("JOIN \"bots-ms_sockets\" as sockets ON sockets.id = \"bots-ms_connectors\".to_socket AND sockets.direction = ?", coreModel.SocketDirectionIn)
	} else {
		result = result.Where("from_scenario_action = ?", scenarioActionId)
		result = result.Joins("JOIN \"bots-ms_sockets\" as sockets ON sockets.id = \"bots-ms_connectors\".from_socket AND sockets.direction = ?", coreModel.SocketDirectionOut)
	}

	result = result.Find(&items)

	return items, result.Error
}

func (r *gormConnectorRepo) GetBySocketId(scenarioActionId uint, socketId uint, direction uint8) (*models.Connector, error) {
	var item *models.Connector
	result := r.DB
	if direction == coreModel.SocketDirectionIn {
		result = result.Where("to_scenario_action = ? AND to_socket = ?", scenarioActionId, socketId)
	} else {
		result = result.Where("from_scenario_action = ? AND from_socket = ?", scenarioActionId, socketId)
	}

	result = result.First(&item)

	if item.Id == 0 {
		return nil, errors.New("bots-ms.connector.not-found")
	}

	return item, result.Error
}

func (r *gormConnectorRepo) IsExist(conn *models.Connector) bool {
	var count int64
	r.DB.Where("to_scenario_action = ? AND to_socket = ? AND from_scenario_action = ? AND from_socket = ? AND scenario_id = ?", conn.ToScenarioAction, conn.ToSocket, conn.FromScenarioAction, conn.FromSocket, conn.ScenarioId).Count(&count)
	return count > 0
}

func (r *gormConnectorRepo) GetById(id uint) (*models.Connector, error) {
	var item *models.Connector
	result := r.DB.First(&item, id)

	if item.Id == 0 {
		return nil, errors.New("bots-ms.connector.not-found")
	}

	return item, result.Error
}

func (r *gormConnectorRepo) Create(item *models.Connector) error {
	result := r.DB.Create(&item)
	return result.Error
}

func (r *gormConnectorRepo) Update(item *models.Connector) error {
	result := r.DB.Save(&item)
	return result.Error
}

func (r *gormConnectorRepo) Delete(item *models.Connector) error {
	result := r.DB.Delete(&item)
	return result.Error
}

func (r *gormConnectorRepo) DeleteByScenarioActionId(id uint) error {
	result := r.DB.Where("from_scenario_action = ? OR to_scenario_action = ?", id, id).Delete(models.Connector{})
	return result.Error
}

func NewGormConnectorRepository(db *gorm.DB) repositories.ConnectorRepository {
	return &gormConnectorRepo{
		DB: db,
	}
}
