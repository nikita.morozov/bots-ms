package redisHandler

import (
	"bots-ms/repositories"
	"github.com/go-redis/redis/v8"
	"github.com/go-redis/redismock/v8"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"testing"
	"time"
)

type SessioSuite struct {
	suite.Suite
	DB   *redis.Client
	mock redismock.ClientMock

	duration   time.Duration
	repository repositories.StorageRepo
}

func (s *SessioSuite) BeforeTest(_, _ string) {
	db, mock := redismock.NewClientMock()

	s.DB = db
	s.mock = mock
	s.duration = time.Duration(1000) * time.Second
	s.repository = NewRedisStorageHandler("scenarios", s.DB, s.duration)
}

func (s *SessioSuite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestSession(t *testing.T) {
	suite.Run(t, new(SessioSuite))
}

// ------------------------------------------------
//
// # Set
//
// ------------------------------------------------
func (s *SessioSuite) Test_redisSessionSet() {
	key := "scenarios:13:abc123DEG"
	payload := []byte("{\"userId\": 13}")

	s.mock.ExpectSet(key, "{\"userId\": 13}", s.duration).SetVal(string(payload))
	err := s.repository.Set("13", "abc123DEG", payload)

	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Get
//
// ------------------------------------------------
func (s *SessioSuite) Test_redisSessionGet() {
	key := "scenarios:13:abc123DEG"
	payload := []byte("{\"userId\": 13}")

	s.mock.ExpectGet(key).SetVal(string(payload))
	res, err := s.repository.Get("13", "abc123DEG")

	require.NoError(s.T(), err)
	require.Equal(s.T(), res, payload)
}
