package redisHandler

import (
	"bots-ms/repositories"
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"time"
)

type redisStorageHandler struct {
	Prefix   string
	Duration time.Duration
	Ctx      context.Context
	Client   *redis.Client
}

func (r *redisStorageHandler) getKey(ctx string, path string) string {
	return fmt.Sprintf("%s:%s:%s", r.Prefix, ctx, path)
}

func (r *redisStorageHandler) Set(ctx string, path string, payload []byte) error {
	return r.Client.Set(r.Ctx, r.getKey(ctx, path), string(payload), r.Duration).Err()
}

func (r *redisStorageHandler) SetWithDuration(ctx string, path string, payload []byte, duration time.Duration) error {
	return r.Client.Set(r.Ctx, r.getKey(ctx, path), string(payload), duration).Err()
}

func (r *redisStorageHandler) Get(ctx string, path string) ([]byte, error) {
	res := r.Client.Get(r.Ctx, r.getKey(ctx, path))
	if res.Err() != nil {
		return nil, res.Err()
	}

	return res.Bytes()
}

func (r *redisStorageHandler) Clear(ctx string, path string) error {
	list, err := r.Client.Keys(r.Ctx, r.getKey(ctx, path)).Result()
	if err != nil {
		return err
	}

	for _, key := range list {
		fmt.Printf("Deleting key %s", key)
		r.Client.Del(r.Ctx, key)
	}

	return nil
}

func (r *redisStorageHandler) List() (*[]string, error) {
	list, err := r.Client.Keys(r.Ctx, "*").Result()
	return &list, err
}

func NewRedisStorageHandler(prefix string, client *redis.Client, duration time.Duration) repositories.StorageRepo {
	return &redisStorageHandler{
		Prefix:   prefix,
		Duration: duration,
		Ctx:      context.Background(),
		Client:   client,
	}
}
