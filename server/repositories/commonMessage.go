package repositories

import "bots-ms/models"

type CommonMessage interface {
	Get(Data string) (*models.CommonMessage, error)
	GetByMessageId(messageID string) (*models.CommonMessage, error)
	ListByMessageId(MessageID uint) (*[]models.CommonMessage, error)
	ListBySender(sender string) (*[]models.CommonMessage, error)
	Create(item *models.CommonMessage) error
	Delete(item *models.CommonMessage) error
}
