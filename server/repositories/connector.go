package repositories

import (
	"bots-ms/models"
)

type ConnectorRepository interface {
	List(scenarioId uint) (*[]models.Connector, error)
	ListWithDirection(scenarioActionId uint, direction uint8) (*[]models.Connector, error)
	GetBySocketId(scenarioAction uint, socketId uint, direction uint8) (*models.Connector, error)
	GetById(id uint) (*models.Connector, error)
	IsExist(conn *models.Connector) bool
	Create(item *models.Connector) error
	Update(item *models.Connector) error
	Delete(item *models.Connector) error
	DeleteByScenarioActionId(id uint) error
}
