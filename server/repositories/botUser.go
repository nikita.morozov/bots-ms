package repositories

import (
	"bots-ms/models"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
)

type BotUserRepository interface {
	List(handler string, opts *sharedModels.ListOptions) (*[]models.BotUser, sharedModels.Paginator, error)
	Count() (uint64, error)
	GetById(id uint) (*models.BotUser, error)
	GetByUserId(botID uint, userId string) (*models.BotUser, error)
	GetByMeta(botID uint, meta string) (*models.BotUser, error)
	Update(item *models.BotUser) error
	Create(item *models.BotUser) error
	Delete(item *models.BotUser) error
}
