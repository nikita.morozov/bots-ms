package repositories

import (
	"bots-ms/models"
)

type StateTransitionRepository interface {
	List(stateMachineId uint) (*[]models.StateTransition, error)
	GetById(id uint) (*models.StateTransition, error)
	Update(item *models.StateTransition) error
	Create(item *models.StateTransition) error
	Delete(item *models.StateTransition) error
	DeleteByStateId(id uint) error
}
