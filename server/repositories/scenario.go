package repositories

import (
	"bots-ms/models"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
)

type ScenarioRepository interface {
	List(opts *sharedModels.ListOptions) (*[]models.Scenario, sharedModels.Paginator, error)
	Count() (uint64, error)
	GetById(id uint) (*models.Scenario, error)
	Update(item *models.Scenario) error
	Create(item *models.Scenario) error
	Delete(item *models.Scenario) error
}
