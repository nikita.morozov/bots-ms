// Code generated by mockery v2.10.2. DO NOT EDIT.

package mocks

import (
	models "bots-ms/models"

	mock "github.com/stretchr/testify/mock"
)

// AuthRequestRepository is an autogenerated mock type for the AuthRequestRepository type
type AuthRequestRepository struct {
	mock.Mock
}

// Create provides a mock function with given fields: item
func (_m *AuthRequestRepository) Create(item *models.AuthRequest) error {
	ret := _m.Called(item)

	var r0 error
	if rf, ok := ret.Get(0).(func(*models.AuthRequest) error); ok {
		r0 = rf(item)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Delete provides a mock function with given fields: item
func (_m *AuthRequestRepository) Delete(item *models.AuthRequest) error {
	ret := _m.Called(item)

	var r0 error
	if rf, ok := ret.Get(0).(func(*models.AuthRequest) error); ok {
		r0 = rf(item)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetByHash provides a mock function with given fields: hash
func (_m *AuthRequestRepository) GetByHash(hash string) (*models.AuthRequest, error) {
	ret := _m.Called(hash)

	var r0 *models.AuthRequest
	if rf, ok := ret.Get(0).(func(string) *models.AuthRequest); ok {
		r0 = rf(hash)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.AuthRequest)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(hash)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetById provides a mock function with given fields: id
func (_m *AuthRequestRepository) GetById(id uint) (*models.AuthRequest, error) {
	ret := _m.Called(id)

	var r0 *models.AuthRequest
	if rf, ok := ret.Get(0).(func(uint) *models.AuthRequest); ok {
		r0 = rf(id)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.AuthRequest)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(uint) error); ok {
		r1 = rf(id)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetByMeta provides a mock function with given fields: id, data
func (_m *AuthRequestRepository) GetByMeta(id uint, data string) (*models.AuthRequest, error) {
	ret := _m.Called(id, data)

	var r0 *models.AuthRequest
	if rf, ok := ret.Get(0).(func(uint, string) *models.AuthRequest); ok {
		r0 = rf(id, data)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.AuthRequest)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(uint, string) error); ok {
		r1 = rf(id, data)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Update provides a mock function with given fields: item
func (_m *AuthRequestRepository) Update(item *models.AuthRequest) error {
	ret := _m.Called(item)

	var r0 error
	if rf, ok := ret.Get(0).(func(*models.AuthRequest) error); ok {
		r0 = rf(item)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
