package repositories

import (
	"bots-ms/models"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
)

type StateMachineRepository interface {
	List(opt *sharedModels.ListOptions) (*[]models.StateMachine, sharedModels.Paginator, error)
	Count() (uint64, error)
	GetById(id uint) (*models.StateMachine, error)
	GetByBotId(id uint) (*models.StateMachine, error)
	Update(item *models.StateMachine) error
	Create(item *models.StateMachine) error
	Delete(item *models.StateMachine) error
}
