package repositories

import (
	"bots-ms/models"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
)

type EventRepository interface {
	GetById(id uint) (*models.Event, error)
	Count() (uint64, error)
	List(opts *sharedModels.ListOptions) (*[]models.Event, sharedModels.Paginator, error)
	GetByHandlerName(name string, botSlug string) (*models.Event, error)
	Update(item *models.Event) error
	Create(item *models.Event) error
	Delete(item *models.Event) error
}
