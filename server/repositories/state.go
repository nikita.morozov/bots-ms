package repositories

import (
	"bots-ms/models"
)

type StateRepository interface {
	List(stateMachineId uint) (*[]models.State, error)
	GetById(id uint) (*models.State, error)
	GetByScenarioId(smId uint, scenarioId uint) (*models.State, error)
	Update(item *models.State) error
	Create(item *models.State) error
	Delete(item *models.State) error
	DeleteByStateMachineId(id uint) error
}
