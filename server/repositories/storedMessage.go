package repositories

import "bots-ms/models"

type StoredMessagesRepository interface {
	Get(id uint) (*models.StoredMessage, error)
	Create(item *models.StoredMessage) error
	Delete(item *models.StoredMessage) error
}
