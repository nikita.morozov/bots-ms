package repositories

import "bots-ms/models"

type AuthRequestRepository interface {
	GetByHash(hash string) (*models.AuthRequest, error)
	GetByMeta(id uint, data string) (*models.AuthRequest, error)
	GetById(id uint) (*models.AuthRequest, error)
	Create(item *models.AuthRequest) error
	Update(item *models.AuthRequest) error
	Delete(item *models.AuthRequest) error
}
