package repositories

import coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"

type ActionRepository interface {
	GetById(id uint) (*coreModel.Action, error)
	List() (*[]coreModel.Action, error)
	Count() (uint64, error)
	GetByHandlerName(name string) (*coreModel.Action, error)
	Update(item *coreModel.Action) error
	Create(item *coreModel.Action) error
	Delete(item *coreModel.Action) error
}
