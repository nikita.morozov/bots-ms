package repositories

import coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"

type SocketRepository interface {
	List(actionId uint, direction uint8) (*[]coreModel.Socket, error)
	GetBySlug(actionId uint, direction uint8, slug string) (*coreModel.Socket, error)
	GetByType(actionId uint, direction uint8, sType uint8) (*[]coreModel.Socket, error)
	GetById(id uint) (*coreModel.Socket, error)
	Update(item *coreModel.Socket) error
	Create(item *coreModel.Socket) error
	Delete(item *coreModel.Socket) error
}
