package repositories

import (
	"bots-ms/models"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
)

type BotRepository interface {
	GetById(id uint) (*models.Bot, error)
	GetBySlug(slug string) (*models.Bot, error)
	List(opts *sharedModels.ListOptions) (*[]models.Bot, sharedModels.Paginator, error)
	ListOfEnv(env string) (*[]models.Bot, sharedModels.Paginator, error)
	Count() (uint64, error)
	Update(item *models.Bot) error
	Create(item *models.Bot) error
	Delete(item *models.Bot) error
}
