package repositories

import (
	"bots-ms/models"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
)

type ScenarioActionRepository interface {
	List(scenarioId uint, opt *sharedModels.ListOptions) (*[]models.ScenarioAction, error)
	GetById(id uint) (*models.ScenarioAction, error)
	Update(item *models.ScenarioAction) error
	Create(item *models.ScenarioAction) error
	Delete(item *models.ScenarioAction) error
}
