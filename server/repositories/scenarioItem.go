package repositories

import (
	"bots-ms/models"
)

type ScenarioItemRepository interface {
	List(scenarioId uint) (*[]models.ScenarioItem, error)
	GetById(id uint) (*models.ScenarioItem, error)
	Update(item *models.ScenarioItem) error
	Create(item *models.ScenarioItem) error
	Delete(item *models.ScenarioItem) error
}
