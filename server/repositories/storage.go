package repositories

import "time"

type StorageRepo interface {
	Set(ctx string, path string, payload []byte) error
	SetWithDuration(ctx string, path string, payload []byte, duration time.Duration) error
	Get(ctx string, path string) ([]byte, error)
	Clear(ctx string, path string) error
	List() (*[]string, error)
}
