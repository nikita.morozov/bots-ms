// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v4.23.4
// source: scenarioAction.proto

package proto

import (
	proto "gitlab.com/nikita.morozov/ms-shared/proto"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
	reflect "reflect"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

var File_scenarioAction_proto protoreflect.FileDescriptor

var file_scenarioAction_proto_rawDesc = []byte{
	0x0a, 0x14, 0x73, 0x63, 0x65, 0x6e, 0x61, 0x72, 0x69, 0x6f, 0x41, 0x63, 0x74, 0x69, 0x6f, 0x6e,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x05, 0x62, 0x6f, 0x74, 0x4d, 0x73, 0x1a, 0x1b, 0x67,
	0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x65,
	0x6d, 0x70, 0x74, 0x79, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x0f, 0x62, 0x6f, 0x74, 0x43,
	0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x11, 0x63, 0x6f, 0x6d,
	0x6d, 0x6f, 0x6e, 0x54, 0x79, 0x70, 0x65, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x32, 0x90,
	0x03, 0x0a, 0x0e, 0x53, 0x63, 0x65, 0x6e, 0x61, 0x72, 0x69, 0x6f, 0x41, 0x63, 0x74, 0x69, 0x6f,
	0x6e, 0x12, 0x3e, 0x0a, 0x06, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x12, 0x1a, 0x2e, 0x62, 0x6f,
	0x74, 0x4d, 0x73, 0x2e, 0x53, 0x63, 0x65, 0x6e, 0x61, 0x72, 0x69, 0x6f, 0x41, 0x63, 0x74, 0x69,
	0x6f, 0x6e, 0x4d, 0x6f, 0x64, 0x65, 0x6c, 0x1a, 0x16, 0x2e, 0x6e, 0x6d, 0x62, 0x6f, 0x74, 0x2e,
	0x74, 0x79, 0x70, 0x65, 0x73, 0x2e, 0x49, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x22,
	0x00, 0x12, 0x3e, 0x0a, 0x06, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x1a, 0x2e, 0x62, 0x6f,
	0x74, 0x4d, 0x73, 0x2e, 0x53, 0x63, 0x65, 0x6e, 0x61, 0x72, 0x69, 0x6f, 0x41, 0x63, 0x74, 0x69,
	0x6f, 0x6e, 0x4d, 0x6f, 0x64, 0x65, 0x6c, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22,
	0x00, 0x12, 0x43, 0x0a, 0x0b, 0x42, 0x61, 0x74, 0x63, 0x68, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x12, 0x1a, 0x2e, 0x62, 0x6f, 0x74, 0x4d, 0x73, 0x2e, 0x53, 0x63, 0x65, 0x6e, 0x61, 0x72, 0x69,
	0x6f, 0x41, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x42, 0x61, 0x74, 0x63, 0x68, 0x1a, 0x16, 0x2e, 0x67,
	0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45,
	0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x12, 0x3a, 0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65,
	0x12, 0x16, 0x2e, 0x6e, 0x6d, 0x62, 0x6f, 0x74, 0x2e, 0x74, 0x79, 0x70, 0x65, 0x73, 0x2e, 0x49,
	0x64, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x16, 0x2e, 0x6e, 0x6d, 0x62, 0x6f, 0x74,
	0x2e, 0x74, 0x79, 0x70, 0x65, 0x73, 0x2e, 0x42, 0x6f, 0x6f, 0x6c, 0x56, 0x61, 0x6c, 0x75, 0x65,
	0x22, 0x00, 0x12, 0x3b, 0x0a, 0x03, 0x47, 0x65, 0x74, 0x12, 0x16, 0x2e, 0x6e, 0x6d, 0x62, 0x6f,
	0x74, 0x2e, 0x74, 0x79, 0x70, 0x65, 0x73, 0x2e, 0x49, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x1a, 0x1a, 0x2e, 0x62, 0x6f, 0x74, 0x4d, 0x73, 0x2e, 0x53, 0x63, 0x65, 0x6e, 0x61, 0x72,
	0x69, 0x6f, 0x41, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x4d, 0x6f, 0x64, 0x65, 0x6c, 0x22, 0x00, 0x12,
	0x40, 0x0a, 0x04, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x18, 0x2e, 0x62, 0x6f, 0x74, 0x4d, 0x73, 0x2e,
	0x53, 0x63, 0x65, 0x6e, 0x61, 0x72, 0x69, 0x6f, 0x41, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65,
	0x71, 0x1a, 0x1a, 0x2e, 0x62, 0x6f, 0x74, 0x4d, 0x73, 0x2e, 0x53, 0x63, 0x65, 0x6e, 0x61, 0x72,
	0x69, 0x6f, 0x41, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x4d, 0x6f, 0x64, 0x65, 0x6c, 0x22, 0x00, 0x30,
	0x01, 0x42, 0x60, 0x0a, 0x1e, 0x70, 0x72, 0x6f, 0x2e, 0x6e, 0x6d, 0x62, 0x6f, 0x74, 0x2e, 0x62,
	0x6f, 0x74, 0x73, 0x2e, 0x73, 0x63, 0x65, 0x6e, 0x61, 0x72, 0x69, 0x6f, 0x2d, 0x61, 0x63, 0x74,
	0x69, 0x6f, 0x6e, 0x42, 0x13, 0x53, 0x63, 0x65, 0x6e, 0x61, 0x72, 0x69, 0x6f, 0x41, 0x63, 0x74,
	0x69, 0x6f, 0x6e, 0x50, 0x72, 0x6f, 0x74, 0x6f, 0x50, 0x01, 0x5a, 0x27, 0x67, 0x69, 0x74, 0x6c,
	0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x6e, 0x69, 0x6b, 0x69, 0x74, 0x61, 0x2e, 0x6d, 0x6f,
	0x72, 0x6f, 0x7a, 0x6f, 0x76, 0x2f, 0x62, 0x6f, 0x74, 0x73, 0x2d, 0x6d, 0x73, 0x2f, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var file_scenarioAction_proto_goTypes = []interface{}{
	(*ScenarioActionModel)(nil), // 0: botMs.ScenarioActionModel
	(*ScenarioActionBatch)(nil), // 1: botMs.ScenarioActionBatch
	(*proto.IdRequest)(nil),     // 2: nmbot.types.IdRequest
	(*ScenarioActionReq)(nil),   // 3: botMs.ScenarioActionReq
	(*emptypb.Empty)(nil),       // 4: google.protobuf.Empty
	(*proto.BoolValue)(nil),     // 5: nmbot.types.BoolValue
}
var file_scenarioAction_proto_depIdxs = []int32{
	0, // 0: botMs.ScenarioAction.Create:input_type -> botMs.ScenarioActionModel
	0, // 1: botMs.ScenarioAction.Update:input_type -> botMs.ScenarioActionModel
	1, // 2: botMs.ScenarioAction.BatchUpdate:input_type -> botMs.ScenarioActionBatch
	2, // 3: botMs.ScenarioAction.Delete:input_type -> nmbot.types.IdRequest
	2, // 4: botMs.ScenarioAction.Get:input_type -> nmbot.types.IdRequest
	3, // 5: botMs.ScenarioAction.List:input_type -> botMs.ScenarioActionReq
	2, // 6: botMs.ScenarioAction.Create:output_type -> nmbot.types.IdRequest
	4, // 7: botMs.ScenarioAction.Update:output_type -> google.protobuf.Empty
	4, // 8: botMs.ScenarioAction.BatchUpdate:output_type -> google.protobuf.Empty
	5, // 9: botMs.ScenarioAction.Delete:output_type -> nmbot.types.BoolValue
	0, // 10: botMs.ScenarioAction.Get:output_type -> botMs.ScenarioActionModel
	0, // 11: botMs.ScenarioAction.List:output_type -> botMs.ScenarioActionModel
	6, // [6:12] is the sub-list for method output_type
	0, // [0:6] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_scenarioAction_proto_init() }
func file_scenarioAction_proto_init() {
	if File_scenarioAction_proto != nil {
		return
	}
	file_botCommon_proto_init()
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_scenarioAction_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   0,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_scenarioAction_proto_goTypes,
		DependencyIndexes: file_scenarioAction_proto_depIdxs,
	}.Build()
	File_scenarioAction_proto = out.File
	file_scenarioAction_proto_rawDesc = nil
	file_scenarioAction_proto_goTypes = nil
	file_scenarioAction_proto_depIdxs = nil
}
