// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v4.23.4
// source: authRequest.proto

package proto

import (
	context "context"
	proto "gitlab.com/nikita.morozov/ms-shared/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// AuthRequestClient is the client API for AuthRequest service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type AuthRequestClient interface {
	Create(ctx context.Context, in *AuthRequestModel, opts ...grpc.CallOption) (*AuthRequestModel, error)
	Delete(ctx context.Context, in *proto.IdRequest, opts ...grpc.CallOption) (*proto.BoolValue, error)
}

type authRequestClient struct {
	cc grpc.ClientConnInterface
}

func NewAuthRequestClient(cc grpc.ClientConnInterface) AuthRequestClient {
	return &authRequestClient{cc}
}

func (c *authRequestClient) Create(ctx context.Context, in *AuthRequestModel, opts ...grpc.CallOption) (*AuthRequestModel, error) {
	out := new(AuthRequestModel)
	err := c.cc.Invoke(ctx, "/botMs.AuthRequest/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *authRequestClient) Delete(ctx context.Context, in *proto.IdRequest, opts ...grpc.CallOption) (*proto.BoolValue, error) {
	out := new(proto.BoolValue)
	err := c.cc.Invoke(ctx, "/botMs.AuthRequest/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// AuthRequestServer is the server API for AuthRequest service.
// All implementations must embed UnimplementedAuthRequestServer
// for forward compatibility
type AuthRequestServer interface {
	Create(context.Context, *AuthRequestModel) (*AuthRequestModel, error)
	Delete(context.Context, *proto.IdRequest) (*proto.BoolValue, error)
	mustEmbedUnimplementedAuthRequestServer()
}

// UnimplementedAuthRequestServer must be embedded to have forward compatible implementations.
type UnimplementedAuthRequestServer struct {
}

func (UnimplementedAuthRequestServer) Create(context.Context, *AuthRequestModel) (*AuthRequestModel, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedAuthRequestServer) Delete(context.Context, *proto.IdRequest) (*proto.BoolValue, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedAuthRequestServer) mustEmbedUnimplementedAuthRequestServer() {}

// UnsafeAuthRequestServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to AuthRequestServer will
// result in compilation errors.
type UnsafeAuthRequestServer interface {
	mustEmbedUnimplementedAuthRequestServer()
}

func RegisterAuthRequestServer(s grpc.ServiceRegistrar, srv AuthRequestServer) {
	s.RegisterService(&AuthRequest_ServiceDesc, srv)
}

func _AuthRequest_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AuthRequestModel)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AuthRequestServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/botMs.AuthRequest/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AuthRequestServer).Create(ctx, req.(*AuthRequestModel))
	}
	return interceptor(ctx, in, info, handler)
}

func _AuthRequest_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(proto.IdRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AuthRequestServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/botMs.AuthRequest/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AuthRequestServer).Delete(ctx, req.(*proto.IdRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// AuthRequest_ServiceDesc is the grpc.ServiceDesc for AuthRequest service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var AuthRequest_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "botMs.AuthRequest",
	HandlerType: (*AuthRequestServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _AuthRequest_Create_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _AuthRequest_Delete_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "authRequest.proto",
}
