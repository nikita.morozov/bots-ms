package registry

import (
	"bots-ms/models"
	"errors"
	"sync"
)

type BotRegistry interface {
	Add(instance *models.IBotHandler)
	Get(method string) (*models.IBotHandler, error)
	List() map[string]*models.IBotHandler
}

type botRegistry struct {
	sync.Mutex
	handler string
	items   map[string]*models.IBotHandler
}

func (h *botRegistry) List() map[string]*models.IBotHandler {
	return h.items
}

func (h *botRegistry) Add(instance *models.IBotHandler) {
	h.Lock()
	var key = (*instance).GetSlug()
	h.items[key] = instance
	h.Unlock()
}

func (h *botRegistry) Get(method string) (*models.IBotHandler, error) {
	h.Lock()
	defer h.Unlock()
	if val, ok := h.items[method]; ok {
		return val, nil
	}

	return nil, errors.New("handlers.send-message.not-found")
}

func NewBotRegistry() BotRegistry {
	return &botRegistry{
		items: make(map[string]*models.IBotHandler),
	}
}
