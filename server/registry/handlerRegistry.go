package registry

import (
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type HandlerRegistry interface {
	Add(method string, handler *models.IHandler)
	Get(method string) *models.IHandler
	AddFromModules(modules []models.ModuleHandler)
}

type handlerRegistry struct {
	items map[string]*models.IHandler
}

func (h handlerRegistry) Add(method string, handler *models.IHandler) {
	h.items[method] = handler
}

func (h handlerRegistry) AddFromModules(modules []models.ModuleHandler) {
	for _, module := range modules {
		handler := module.Handler
		h.items[module.Name] = &handler
	}
}

func (h handlerRegistry) Get(method string) *models.IHandler {
	return h.items[method]
}

func NewHandlerRegistry() HandlerRegistry {
	return &handlerRegistry{
		items: make(map[string]*models.IHandler),
	}
}
