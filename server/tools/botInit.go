package tools

import (
	"bots-ms/botHandlers"
	"bots-ms/executor"
	"bots-ms/models"
	"bots-ms/registry"
	"bots-ms/repositories"
	"log"
)

type initData struct {
	botExecutor executor.BotExecutor
	botRepo     repositories.BotRepository
	botRegistry registry.BotRegistry
	env         string
}

type InitHandler interface {
	InitBotList()
	InitBot(bot *models.Bot)
	DeInitBot(bot *models.Bot)
}

func (h *initData) InitBotList() {
	botList, _, err := h.botRepo.ListOfEnv(h.env)
	if err != nil {
		log.Fatal(err)
		return
	}
	for _, bot := range *botList {
		h.InitBot(&bot)
	}
}

func (h *initData) InitBot(bot *models.Bot) {
	var handler models.IBotHandler
	var err error
	if bot.Handler == "telegram" {
		handler, err = botHandlers.NewTelegramBot(*bot, h.botExecutor)
	}

	if bot.Env != h.env {
		log.Printf("❌🤖 Telegram bot \033[1;34m\"%s\"\033[0m has error on init, wrong env\n", bot.Slug)
		return
	}

	if err != nil {
		log.Printf("❌🤖 Telegram bot \033[1;34m\"%s\"\033[0m has error on init\n", bot.Slug)
		return
	}

	done := make(chan bool, 1)
	go handler.Start(done)

	res := <-done
	if res == true {
		h.botRegistry.Add(&handler)
	}
}

func (h *initData) DeInitBot(bot *models.Bot) {
	handler, err := h.botRegistry.Get(bot.GetSlug())
	if err == nil && handler != nil {
		(*handler).Stop()
	}
}

func NewInitHandler(env string, botExecutor executor.BotExecutor, botRepo repositories.BotRepository, botRegistry registry.BotRegistry) InitHandler {
	h := &initData{
		env:         env,
		botRepo:     botRepo,
		botRegistry: botRegistry,
		botExecutor: botExecutor,
	}

	return h
}
