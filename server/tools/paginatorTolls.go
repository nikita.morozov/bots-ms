package tools

import (
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	sharedProto "gitlab.com/nikita.morozov/ms-shared/proto"
)

func ConvertOpts(in *sharedProto.ListOptions) *sharedModels.ListOptions {
	if in != nil {
		return &sharedModels.ListOptions{
			Limit:  int(in.Limit),
			Offset: int(in.Offset),
			Order:  in.Order,
		}
	}

	return nil
}

func ConvertPaginator(in sharedModels.Paginator) *sharedProto.Paginator {
	return &sharedProto.Paginator{
		Offset:       int64(in.Offset),
		TotalCount:   int64(in.TotalCount),
		CountPerPage: int32(in.CountPerPage),
	}
}
