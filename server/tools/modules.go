package tools

import (
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"log"
	"os"
	"plugin"
)

type moduleLoader struct {
	folderName string
	modules    []models.HandlerModule
}

type ModuleLoader interface {
	Load()
	GetModules() []models.HandlerModule
}

func (ml *moduleLoader) Load() {
	if _, err := os.Stat(ml.folderName); os.IsNotExist(err) {
		log.Println("📂 External modules not found")
		return
	}

	files, err := WalkMatch(ml.folderName, "*.so")
	if err != nil {
		log.Println("Failed to load .so files")
		return
	}

	for _, v := range files {
		p, err := plugin.Open(v)
		if err != nil {
			log.Printf("Module file %s was skipped, reason: %s", v, err.Error())
			continue
		}
		moduleLink, err := p.Lookup("Module")
		if err != nil {
			panic(err)
		}

		module := moduleLink.(models.HandlerModule)

		log.Printf("🔌 Loading module: %s", module.GetName())

		ml.modules = append(ml.modules, module)
		log.Printf("✅ Module %s initializated", module.GetName())
	}
}

func (ml *moduleLoader) GetModules() []models.HandlerModule {
	return ml.modules
}

func NewModuleLoader(folderName string) ModuleLoader {
	return &moduleLoader{
		folderName: folderName,
	}
}
