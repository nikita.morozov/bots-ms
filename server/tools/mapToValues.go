package tools

import (
	"net/url"
	"reflect"
	"strconv"
)

func MapToValues(i map[string]interface{}) (values url.Values) {
	values = url.Values{}
	for k, x := range i {
		var value string
		v := reflect.ValueOf(x)
		switch x.(type) {
		case int, int8, int16, int32, int64:
			value = strconv.FormatInt(v.Int(), 10)
		case uint, uint8, uint16, uint32, uint64:
			value = strconv.FormatUint(v.Uint(), 10)
		case float32:
			value = strconv.FormatFloat(v.Float(), 'f', 4, 32)
		case float64:
			value = strconv.FormatFloat(v.Float(), 'f', 4, 64)
		case []byte:
			value = string(v.Bytes())
		case string:
			value = v.String()
		}
		values.Set(k, value)
	}

	return values
}
