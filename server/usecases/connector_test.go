package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories/mocks"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

type ConnectorSuite struct {
	suite.Suite

	repo *mocks.ConnectorRepository
	uc   ConnectorUsecase
}

func (s *ConnectorSuite) BeforeTest(_, _ string) {
	repo := new(mocks.ConnectorRepository)
	s.uc = NewConnectorUsecase(repo)
	s.repo = repo
}

func TestConnector(t *testing.T) {
	suite.Run(t, new(ConnectorSuite))
}

// ------------------------------------------------
//
// # List
//
// ------------------------------------------------
func (s *ConnectorSuite) Test_connector_List() {
	mockItems := &[]models.Connector{
		{
			Id:                 uint(1),
			FromScenarioAction: uint(1),
			FromSocket:         uint(1),
			ToScenarioAction:   uint(2),
			ToSocket:           uint(2),
		},
	}

	s.repo.On("List", uint(1)).Return(mockItems, nil)
	list, err := s.uc.List(uint(1))

	require.NoError(s.T(), err)
	require.NotNil(s.T(), list)
	require.Equal(s.T(), len(*list), 1)
}

// ------------------------------------------------
//
// # GetById
//
// ------------------------------------------------
func (s *ConnectorSuite) Test_connector_GetById() {
	mockItem := &models.Connector{
		Id:                 uint(1),
		FromScenarioAction: uint(1),
		FromSocket:         uint(1),
		ToScenarioAction:   uint(2),
		ToSocket:           uint(2),
	}

	s.repo.On("GetById", uint(1)).Return(mockItem, nil)
	item, err := s.uc.GetById(uint(1))

	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
}

// ------------------------------------------------
//
// # GetInnerBySocket
//
// ------------------------------------------------
func (s *ConnectorSuite) Test_connector_GetInnerBySocket() {
	mockItem := &models.Connector{
		Id:                 uint(1),
		FromScenarioAction: uint(1),
		FromSocket:         uint(1),
		ToScenarioAction:   uint(2),
		ToSocket:           uint(2),
	}

	s.repo.On("GetBySocketId", uint(1), uint(10), coreModel.SocketDirectionIn).Return(mockItem, nil)
	item, err := s.uc.GetInnerBySocket(uint(1), uint(10))

	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
}

// ------------------------------------------------
//
// # GetOuterBySocket
//
// ------------------------------------------------
func (s *ConnectorSuite) Test_connector_GetOuterBySocket() {
	mockItem := &models.Connector{
		Id:                 uint(1),
		FromScenarioAction: uint(1),
		FromSocket:         uint(1),
		ToScenarioAction:   uint(2),
		ToSocket:           uint(2),
		ScenarioId:         uint(1),
	}

	s.repo.On("GetBySocketId", uint(1), uint(10), coreModel.SocketDirectionOut).Return(mockItem, nil)
	item, err := s.uc.GetOuterBySocket(uint(1), uint(10))

	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
}

// ------------------------------------------------
//
// # GetInnerList
//
// ------------------------------------------------
func (s *ConnectorSuite) Test_connector_GetInnerList() {
	mockItems := &[]models.Connector{
		{
			Id:                 uint(1),
			FromScenarioAction: uint(1),
			FromSocket:         uint(1),
			ToScenarioAction:   uint(2),
			ToSocket:           uint(2),
		},
	}

	s.repo.On("ListWithDirection", uint(1), coreModel.SocketDirectionIn).Return(mockItems, nil)
	list, err := s.uc.GetInnerList(uint(1))

	require.NoError(s.T(), err)
	require.NotNil(s.T(), list)
	require.Equal(s.T(), len(*list), 1)
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *ConnectorSuite) Test_connector_Create() {
	mockItem := &models.Connector{
		FromScenarioAction: uint(1),
		FromSocket:         uint(1),
		ToScenarioAction:   uint(2),
		ToSocket:           uint(2),
	}

	s.repo.On("IsExist", mockItem).Return(false)
	s.repo.On("Create", mockItem).Return(nil)
	err := s.uc.Create(mockItem)

	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *ConnectorSuite) Test_connector_Delete() {
	mockItem := &models.Connector{
		Id:                 uint(1),
		FromScenarioAction: uint(1),
		FromSocket:         uint(1),
		ToScenarioAction:   uint(2),
		ToSocket:           uint(2),
	}

	s.repo.On("GetById", uint(1)).Return(mockItem, nil)
	s.repo.On("Delete", mockItem).Return(nil)
	err := s.uc.Delete(uint(1))

	require.NoError(s.T(), err)
}
