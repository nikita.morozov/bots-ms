package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
)

type ConnectorUsecase interface {
	List(scenarioId uint) (*[]models.Connector, error)
	GetById(id uint) (*models.Connector, error)
	GetInnerBySocket(toScenarioAction uint, socketId uint) (*models.Connector, error)
	GetOuterBySocket(fromScenarioAction uint, socketId uint) (*models.Connector, error)
	GetInnerList(scenarioActionId uint) (*[]models.Connector, error)
	Create(item *models.Connector) error
	Update(item *models.Connector) error
	Delete(id uint) error
}

type connectorUsecase struct {
	repo repositories.ConnectorRepository
}

func (uc *connectorUsecase) List(scenarioId uint) (*[]models.Connector, error) {
	return uc.repo.List(scenarioId)
}

func (uc *connectorUsecase) GetById(id uint) (*models.Connector, error) {
	return uc.repo.GetById(id)
}

func (uc *connectorUsecase) GetInnerBySocket(toScenarioAction uint, socketId uint) (*models.Connector, error) {
	return uc.repo.GetBySocketId(toScenarioAction, socketId, coreModel.SocketDirectionIn)
}

func (uc *connectorUsecase) GetOuterBySocket(fromScenarioAction uint, socketId uint) (*models.Connector, error) {
	return uc.repo.GetBySocketId(fromScenarioAction, socketId, coreModel.SocketDirectionOut)
}

func (uc *connectorUsecase) GetInnerList(scenarioActionId uint) (*[]models.Connector, error) {
	return uc.repo.ListWithDirection(scenarioActionId, coreModel.SocketDirectionIn)
}

func (uc *connectorUsecase) Create(item *models.Connector) error {
	if uc.repo.IsExist(item) {
		return nil
	}

	err := uc.repo.Create(item)
	return err
}

func (uc *connectorUsecase) Update(item *models.Connector) error {
	return uc.repo.Update(item)
}

func (uc *connectorUsecase) Delete(id uint) error {
	item, err := uc.GetById(id)
	if err != nil {
		return err
	}

	return uc.repo.Delete(item)
}

func NewConnectorUsecase(repo repositories.ConnectorRepository) ConnectorUsecase {
	return &connectorUsecase{
		repo: repo,
	}
}
