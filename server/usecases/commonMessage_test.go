package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories/mocks"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gorm.io/gorm"
	"testing"
	"time"
)

type CommonMessageSuite struct {
	suite.Suite

	repo *mocks.CommonMessage
	uc   CommonMessageUsecase
}

func (s *CommonMessageSuite) BeforeTest(_, _ string) {
	repo := new(mocks.CommonMessage)
	s.uc = NewCommonMessageUsecase(repo)
	s.repo = repo
}

func TestCommonMessage(t *testing.T) {
	suite.Run(t, new(CommonMessageSuite))
}

// ------------------------------------------------
//
// # Get
//
// ------------------------------------------------
func (s *CommonMessageSuite) Test_commonMessage_Get() {
	messageId := uint(1)

	mockItem := &models.CommonMessage{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		MessageId: &messageId,
		Data:      "message data",
	}

	s.repo.On("Get", "aaa-bbb").Return(mockItem, nil)

	item, err := s.repo.Get("aaa-bbb")

	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
}

// ------------------------------------------------
//
// # List
//
// ------------------------------------------------
func (s *CommonMessageSuite) Test_commonMessage_List() {
	messageId := uint(1)
	mockItems := &[]models.CommonMessage{
		{
			Model: gorm.Model{
				ID:        uint(1),
				CreatedAt: time.Now(),
			},
			MessageId: &messageId,
			Data:      "message data",
		},
	}

	s.repo.On("ListByMessageId", uint(1)).Return(mockItems, nil)

	list, err := s.uc.List(uint(1))

	require.NoError(s.T(), err)
	require.NotNil(s.T(), list)
	require.Equal(s.T(), len(*list), 1)
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *CommonMessageSuite) Test_commonMessage_Create() {
	messageId := uint(1)
	mockItem := &models.CommonMessage{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		MessageId: &messageId,
		Data:      "message data",
	}

	s.repo.On("Create", mockItem).Return(nil)
	err := s.uc.Create(mockItem)

	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *CommonMessageSuite) Test_commonMessage_Delete() {
	messageId := uint(1)
	mockItem := &models.CommonMessage{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		MessageId: &messageId,
		Data:      "message data",
	}

	s.repo.On("Delete", mockItem).Return(nil)
	err := s.uc.Delete(mockItem)

	require.NoError(s.T(), err)
}
