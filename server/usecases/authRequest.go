package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories"
)

type AuthRequestUsecase interface {
	GetByHash(hash string) (*models.AuthRequest, error)
	GetByMeta(id uint, data string) (*models.AuthRequest, error)
	Get(id uint) (*models.AuthRequest, error)
	Create(item *models.AuthRequest) error
	Update(item *models.AuthRequest) error
	Delete(id uint) error
}

type authRequestUsecase struct {
	repo repositories.AuthRequestRepository
}

func (uc *authRequestUsecase) GetByHash(hash string) (*models.AuthRequest, error) {
	return uc.repo.GetByHash(hash)
}

func (uc *authRequestUsecase) GetByMeta(id uint, data string) (*models.AuthRequest, error) {
	return uc.repo.GetByMeta(id, data)
}

func (uc *authRequestUsecase) Get(id uint) (*models.AuthRequest, error) {
	return uc.repo.GetById(id)
}

func (uc *authRequestUsecase) Create(item *models.AuthRequest) error {
	return uc.repo.Create(item)
}

func (uc *authRequestUsecase) Update(item *models.AuthRequest) error {
	return uc.repo.Update(item)
}

func (uc *authRequestUsecase) Delete(id uint) error {
	item, err := uc.Get(id)
	if err != nil {
		return err
	}

	return uc.repo.Delete(item)
}

func NewAuthRequestUsecase(repo repositories.AuthRequestRepository) AuthRequestUsecase {
	return &authRequestUsecase{
		repo: repo,
	}
}
