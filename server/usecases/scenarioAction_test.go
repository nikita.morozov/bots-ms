package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories/mocks"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/gorm"
	"testing"
	"time"
)

type ScenarioActionSuite struct {
	suite.Suite

	repo         *mocks.ScenarioActionRepository
	scenarioRepo *mocks.ScenarioRepository
	connRepo     *mocks.ConnectorRepository
	uc           ScenarioAcrionUsecase
}

func (s *ScenarioActionSuite) BeforeTest(_, _ string) {
	repo := new(mocks.ScenarioActionRepository)
	connRepo := new(mocks.ConnectorRepository)
	scenarioRepo := new(mocks.ScenarioRepository)
	s.uc = NewScenarioActionUsecase(repo, scenarioRepo, connRepo)
	s.repo = repo
	s.scenarioRepo = scenarioRepo
	s.connRepo = connRepo
}

func TestScenarioAction(t *testing.T) {
	suite.Run(t, new(ScenarioActionSuite))
}

// ------------------------------------------------
//
// # List
//
// ------------------------------------------------
func (s *ScenarioActionSuite) Test_scenarioAction_List() {
	mockItems := &[]models.ScenarioAction{
		{
			Id:         uint(1),
			ScenarioId: uint(1),
			ActionId:   uint(1),
		},
	}

	s.repo.On("List", uint(1), (*sharedModels.ListOptions)(nil)).Return(mockItems, nil)
	list, err := s.uc.List(uint(1), nil)

	require.NoError(s.T(), err)
	require.NotNil(s.T(), list)
	require.Equal(s.T(), len(*list), 1)
}

// ------------------------------------------------
//
// # Get
//
// ------------------------------------------------
func (s *ScenarioActionSuite) Test_scenarioAction_Get() {
	mockItem := &models.ScenarioAction{
		Id:         uint(1),
		ScenarioId: uint(1),
		ActionId:   uint(1),
	}

	s.repo.On("GetById", uint(1)).Return(mockItem, nil)
	item, err := s.uc.Get(uint(1))

	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
}

// ------------------------------------------------
//
// # Update
//
// ------------------------------------------------
func (s *ScenarioActionSuite) Test_scenarioAction_Update() {
	mockItem := &models.ScenarioAction{
		Id:         uint(1),
		ScenarioId: uint(1),
		ActionId:   uint(1),
	}

	s.repo.On("Update", mockItem).Return(nil)
	err := s.uc.Update(mockItem)

	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *ScenarioActionSuite) Test_scenarioAction_Create() {
	mockItem := &models.ScenarioAction{
		ScenarioId: uint(1),
		ActionId:   uint(1),
	}

	s.repo.On("Create", mockItem).Return(nil)
	err := s.uc.Create(mockItem)

	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *ScenarioActionSuite) Test_scenarioAction_Delete() {
	mockItem := &models.ScenarioAction{
		Id:         uint(1),
		ScenarioId: uint(1),
		ActionId:   uint(1),
	}

	mockScenario := &models.Scenario{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		Actions: []models.ScenarioAction{},
	}

	s.connRepo.On("DeleteByScenarioActionId", uint(1)).Return(nil)
	s.scenarioRepo.On("GetById", uint(1)).Return(mockScenario, nil)
	s.repo.On("GetById", uint(1)).Return(mockItem, nil)
	s.repo.On("Delete", mockItem).Return(nil)
	err := s.uc.Delete(uint(1))

	require.NoError(s.T(), err)
}
