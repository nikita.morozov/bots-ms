package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"encoding/json"
)

type TriggerStorageUsecase interface {
	Set(ctx string, slug string, data *models.TriggerData) error
	Get(ctx string, slug string) (*models.TriggerData, error)
	ClearContext(ctx string) error
}

type triggerStorageUsecase struct {
	repo repositories.StorageRepo
}

func (s *triggerStorageUsecase) Set(ctx string, slug string, data *models.TriggerData) error {
	payload, err := json.Marshal(data)
	if err != nil {
		return err
	}

	return s.repo.Set(ctx, slug, payload)
}

func (s *triggerStorageUsecase) Get(ctx string, slug string) (*models.TriggerData, error) {
	payload, err := s.repo.Get(ctx, slug)
	if err != nil {
		return nil, err
	}

	var data models.TriggerData
	err = json.Unmarshal(payload, &data)
	if err != nil {
		return nil, err
	}

	return &data, nil
}

func (s *triggerStorageUsecase) ClearContext(ctx string) error {
	return s.repo.Clear(ctx, "*")
}

func NewTriggerStorageUsecase(repo repositories.StorageRepo) TriggerStorageUsecase {
	return &triggerStorageUsecase{
		repo: repo,
	}
}
