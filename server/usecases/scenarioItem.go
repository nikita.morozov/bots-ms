package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories"
)

type ScenarioItemUsecase interface {
	List(scenarioId uint) (*[]models.ScenarioItem, error)
	Get(id uint) (*models.ScenarioItem, error)
	Update(item *models.ScenarioItem) error
	BatchUpdate(items *[]models.ScenarioItem) error
	Create(item *models.ScenarioItem) error
	Delete(id uint) error
}

type scenarioItemUsecase struct {
	repo repositories.ScenarioItemRepository
}

func (uc *scenarioItemUsecase) List(scenarioId uint) (*[]models.ScenarioItem, error) {
	return uc.repo.List(scenarioId)
}

func (uc *scenarioItemUsecase) Get(id uint) (*models.ScenarioItem, error) {
	return uc.repo.GetById(id)
}

func (uc *scenarioItemUsecase) Update(item *models.ScenarioItem) error {
	err := uc.repo.Update(item)
	return err
}

func (uc *scenarioItemUsecase) BatchUpdate(items *[]models.ScenarioItem) error {
	for _, item := range *items {
		err := uc.repo.Update(&item)
		if err != nil {
			return err
		}
	}

	return nil
}

func (uc *scenarioItemUsecase) Create(item *models.ScenarioItem) error {
	err := uc.repo.Create(item)
	return err
}

func (uc *scenarioItemUsecase) Delete(id uint) error {
	item, err := uc.Get(id)
	if err != nil {
		return err
	}
	return uc.repo.Delete(item)
}

func NewScenarioItemUsecase(repo repositories.ScenarioItemRepository) ScenarioItemUsecase {
	return &scenarioItemUsecase{
		repo: repo,
	}
}
