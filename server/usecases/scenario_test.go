package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories/mocks"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gorm.io/gorm"
	"testing"
	"time"
)

type ScenarioSuite struct {
	suite.Suite

	repo *mocks.ScenarioRepository
	uc   ScenarioUsecase
}

func (s *ScenarioSuite) BeforeTest(_, _ string) {
	repo := new(mocks.ScenarioRepository)
	s.uc = NewScenarioUsecase(repo)
	s.repo = repo
}

func TestScenario(t *testing.T) {
	suite.Run(t, new(ScenarioSuite))
}

// ------------------------------------------------
//
// List
//
// ------------------------------------------------
//func (s *ScenarioSuite) Test_scenario_List() {
//	mockItems := &[]models.Scenario{
//		{
//			Model: gorm.Model{
//				ID:        uint(1),
//				CreatedAt: time.Now(),
//			},
//			Actions: []models.ScenarioAction{},
//		},
//	}
//
//	s.repo.On("List").Return(mockItems, nil)
//	list, err := s.uc.List(nil)
//
//	require.NoError(s.T(), err)
//	require.NotNil(s.T(), list)
//	require.Equal(s.T(), len(*list), 1)
//}

// ------------------------------------------------
//
// # Get
//
// ------------------------------------------------
func (s *ScenarioSuite) Test_scenario_Get() {
	mockItem := &models.Scenario{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		Actions: []models.ScenarioAction{},
	}

	s.repo.On("GetById", uint(1)).Return(mockItem, nil)
	item, err := s.uc.Get(uint(1))

	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
}

// ------------------------------------------------
//
// # Update
//
// ------------------------------------------------
func (s *ScenarioSuite) Test_scenario_Update() {
	mockItem := &models.Scenario{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		Actions: []models.ScenarioAction{},
	}

	s.repo.On("Update", mockItem).Return(nil)
	err := s.uc.Update(mockItem)

	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *ScenarioSuite) Test_scenario_Create() {
	mockItem := &models.Scenario{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		Actions: []models.ScenarioAction{},
	}

	s.repo.On("Create", mockItem).Return(nil)
	err := s.uc.Create(mockItem)

	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *ScenarioSuite) Test_scenario_Delete() {
	mockItem := &models.Scenario{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		Actions: []models.ScenarioAction{},
	}

	s.repo.On("GetById", uint(1)).Return(mockItem, nil)
	s.repo.On("Delete", mockItem).Return(nil)
	err := s.uc.Delete(uint(1))

	require.NoError(s.T(), err)
}
