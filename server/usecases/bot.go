package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
)

type BotUsecase interface {
	List(opts *sharedModels.ListOptions) (*[]models.Bot, sharedModels.Paginator, error)
	Count() (uint64, error)
	Get(id uint) (*models.Bot, error)
	GetBySlug(slug string) (*models.Bot, error)
	Update(item *models.Bot) error
	Create(item *models.Bot) error
	Delete(id uint) error
}

type botUsecase struct {
	repo repositories.BotRepository
}

func (uc *botUsecase) List(opts *sharedModels.ListOptions) (*[]models.Bot, sharedModels.Paginator, error) {
	return uc.repo.List(opts)
}

func (uc *botUsecase) Count() (uint64, error) {
	return uc.repo.Count()
}

func (uc *botUsecase) Get(id uint) (*models.Bot, error) {
	return uc.repo.GetById(id)
}

func (uc *botUsecase) GetBySlug(slug string) (*models.Bot, error) {
	return uc.repo.GetBySlug(slug)
}

func (uc *botUsecase) Update(item *models.Bot) error {
	return uc.repo.Update(item)
}

func (uc *botUsecase) Create(item *models.Bot) error {
	return uc.repo.Create(item)
}

func (uc *botUsecase) Delete(id uint) error {
	item, err := uc.Get(id)
	if err != nil {
		return err
	}

	return uc.repo.Delete(item)
}

func NewBotUsecase(repo repositories.BotRepository) BotUsecase {
	return &botUsecase{
		repo: repo,
	}
}
