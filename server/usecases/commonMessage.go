package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories"
)

type CommonMessageUsecase interface {
	Get(data string) (*models.CommonMessage, error)
	GetByMessageId(mid string) (*models.CommonMessage, error)
	List(messageID uint) (*[]models.CommonMessage, error)
	ListBySender(sender string) (*[]models.CommonMessage, error)
	Create(item *models.CommonMessage) error
	Delete(item *models.CommonMessage) error
}

type commonMessageUsecase struct {
	repo repositories.CommonMessage
}

func (uc *commonMessageUsecase) GetByMessageId(mid string) (*models.CommonMessage, error) {
	return uc.repo.GetByMessageId(mid)
}

func (uc *commonMessageUsecase) Get(data string) (*models.CommonMessage, error) {
	return uc.repo.Get(data)
}

func (uc *commonMessageUsecase) List(messageID uint) (*[]models.CommonMessage, error) {
	return uc.repo.ListByMessageId(messageID)
}

func (uc *commonMessageUsecase) ListBySender(sender string) (*[]models.CommonMessage, error) {
	return uc.repo.ListBySender(sender)
}

func (uc *commonMessageUsecase) Create(item *models.CommonMessage) error {
	return uc.repo.Create(item)
}

func (uc *commonMessageUsecase) Delete(item *models.CommonMessage) error {
	return uc.repo.Delete(item)
}

func NewCommonMessageUsecase(repo repositories.CommonMessage) CommonMessageUsecase {
	return &commonMessageUsecase{
		repo: repo,
	}
}
