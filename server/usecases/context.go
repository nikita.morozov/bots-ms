package usecases

import (
	"bots-ms/repositories"
)

type ContextStorageUserCase interface {
	Set(ctx string, slug string, payload string) error
	Get(ctx string, slug string) (string, error)
	Clear(ctx string, slug string) error
}

type contextStorageUserCase struct {
	Repo repositories.StorageRepo
}

// ctx - bot handler
// slug - user id
// payload - session id

func (s *contextStorageUserCase) Set(ctx string, slug string, payload string) error {
	return s.Repo.Set(ctx, slug, []byte(payload))
}

func (s *contextStorageUserCase) Get(ctx string, slug string) (string, error) {
	val, err := s.Repo.Get(ctx, slug)

	if err != nil {
		return "", err
	}

	return string(val), nil
}

func (s *contextStorageUserCase) Clear(ctx string, slug string) error {
	return s.Repo.Clear(ctx, slug)
}

func NewContextStorageUsecase(repo repositories.StorageRepo) ContextStorageUserCase {
	return &contextStorageUserCase{
		Repo: repo,
	}
}
