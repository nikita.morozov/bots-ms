package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
)

type BotUserUsecase interface {
	List(handler string, opts *sharedModels.ListOptions) (*[]models.BotUser, sharedModels.Paginator, error)
	Count() (uint64, error)
	Get(id uint) (*models.BotUser, error)
	GetByUserId(botID uint, userId string) (*models.BotUser, error)
	GetByMeta(botID uint, meta string) (*models.BotUser, error)
	Update(item *models.BotUser) error
	Create(item *models.BotUser) error
	Delete(id uint) error
}

type botUserUsecase struct {
	repo repositories.BotUserRepository
}

func (uc *botUserUsecase) List(handler string, opts *sharedModels.ListOptions) (*[]models.BotUser, sharedModels.Paginator, error) {
	return uc.repo.List(handler, opts)
}

func (uc *botUserUsecase) Count() (uint64, error) {
	return uc.repo.Count()
}

func (uc *botUserUsecase) Get(id uint) (*models.BotUser, error) {
	return uc.repo.GetById(id)
}

func (uc *botUserUsecase) GetByUserId(botID uint, userId string) (*models.BotUser, error) {
	return uc.repo.GetByUserId(botID, userId)
}

func (uc *botUserUsecase) GetByMeta(botID uint, meta string) (*models.BotUser, error) {
	return uc.repo.GetByMeta(botID, meta)
}

func (uc *botUserUsecase) Update(item *models.BotUser) error {
	return uc.repo.Update(item)
}

func (uc *botUserUsecase) Create(item *models.BotUser) error {
	return uc.repo.Create(item)
}

func (uc *botUserUsecase) Delete(id uint) error {
	item, err := uc.Get(id)
	if err != nil {
		return err
	}

	return uc.repo.Delete(item)
}

func NewBotUserUsecase(repo repositories.BotUserRepository) BotUserUsecase {
	return &botUserUsecase{
		repo: repo,
	}
}
