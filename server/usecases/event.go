package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
)

type EventUsecase interface {
	Get(id uint) (*models.Event, error)
	Count() (uint64, error)
	GetByName(name string, botSlug string) (*models.Event, error)
	List(opts *sharedModels.ListOptions) (*[]models.Event, sharedModels.Paginator, error)
	Update(item *models.Event) error
	Create(item *models.Event) error
	Delete(id uint) error
}

type eventUsecase struct {
	repo repositories.EventRepository
}

func (uc *eventUsecase) Get(id uint) (*models.Event, error) {
	return uc.repo.GetById(id)
}

func (uc *eventUsecase) Count() (uint64, error) {
	return uc.repo.Count()
}

func (uc *eventUsecase) GetByName(name string, botSlug string) (*models.Event, error) {
	return uc.repo.GetByHandlerName(name, botSlug)
}

func (uc *eventUsecase) List(opts *sharedModels.ListOptions) (*[]models.Event, sharedModels.Paginator, error) {
	return uc.repo.List(opts)
}

func (uc *eventUsecase) Update(item *models.Event) error {
	return uc.repo.Update(item)
}

func (uc *eventUsecase) Create(item *models.Event) error {
	return uc.repo.Create(item)
}

func (uc *eventUsecase) Delete(id uint) error {
	item, err := uc.Get(id)
	if err != nil {
		return err
	}

	return uc.repo.Delete(item)
}

func NewEventUsecase(repo repositories.EventRepository) EventUsecase {
	return &eventUsecase{
		repo: repo,
	}
}
