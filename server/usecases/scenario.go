package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
)

type ScenarioUsecase interface {
	List(opts *sharedModels.ListOptions) (*[]models.Scenario, sharedModels.Paginator, error)
	Count() (uint64, error)
	Get(id uint) (*models.Scenario, error)
	Update(item *models.Scenario) error
	Create(item *models.Scenario) error
	Delete(id uint) error
}

type scenarioUsecase struct {
	repo repositories.ScenarioRepository
}

func (uc *scenarioUsecase) List(opts *sharedModels.ListOptions) (*[]models.Scenario, sharedModels.Paginator, error) {
	return uc.repo.List(opts)
}

func (uc *scenarioUsecase) Count() (uint64, error) {
	return uc.repo.Count()
}

func (uc *scenarioUsecase) Get(id uint) (*models.Scenario, error) {
	return uc.repo.GetById(id)
}

func (uc *scenarioUsecase) Update(item *models.Scenario) error {
	return uc.repo.Update(item)
}

func (uc *scenarioUsecase) Create(item *models.Scenario) error {
	return uc.repo.Create(item)
}

func (uc *scenarioUsecase) Delete(id uint) error {
	item, err := uc.Get(id)
	if err != nil {
		return err
	}

	return uc.repo.Delete(item)
}

func NewScenarioUsecase(repo repositories.ScenarioRepository) ScenarioUsecase {
	return &scenarioUsecase{
		repo: repo,
	}
}
