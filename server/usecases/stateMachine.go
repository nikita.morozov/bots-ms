package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
)

type StateMachineUsecase interface {
	List(opt *sharedModels.ListOptions) (*[]models.StateMachine, sharedModels.Paginator, error)
	Count() (uint64, error)
	Get(id uint) (*models.StateMachine, error)
	Import(botId uint, sm *models.StateMachineExport) error
	Export(id uint) (*models.StateMachineExport, error)
	GetByBot(botId uint) (*models.StateMachine, error)
	Update(item *models.StateMachine) error
	Create(item *models.StateMachine) error
	Delete(id uint) error
}

type stateMachineUsecase struct {
	repo                 repositories.StateMachineRepository
	botRepo              repositories.BotRepository
	stateRepo            repositories.StateRepository
	scenarioRepo         repositories.ScenarioRepository
	connectorRepo        repositories.ConnectorRepository
	scenarioActionRepo   repositories.ScenarioActionRepository
	stateTransactionRepo repositories.StateTransitionRepository
}

func (uc *stateMachineUsecase) List(opt *sharedModels.ListOptions) (*[]models.StateMachine, sharedModels.Paginator, error) {
	return uc.repo.List(opt)
}

func (uc *stateMachineUsecase) Count() (uint64, error) {
	return uc.repo.Count()
}

func (uc *stateMachineUsecase) Get(id uint) (*models.StateMachine, error) {
	return uc.repo.GetById(id)
}

// TODO: Move it to serializer uc
func (uc *stateMachineUsecase) Import(botId uint, sm *models.StateMachineExport) error {
	scenariesMap := make(map[uint]uint)
	scenarioActionsMap := make(map[uint]uint)
	statesMap := make(map[uint]uint)

	bot, err := uc.botRepo.GetById(botId)
	if err != nil {
		return err
	}

	stateMachine := sm.StateMachine
	previousStateMachineId := stateMachine.ID
	stateMachine.ID = 0
	stateMachine.BotID = bot.ID
	stateMachine.StartStateId = 0

	err = uc.repo.Create(&stateMachine)
	if err != nil {
		return err
	}

	for _, scenery := range sm.Sceneries {
		var actions []models.ScenarioAction

		actions = scenery.Actions

		previousScenarioId := scenery.ID
		scenery.ID = 0
		scenery.Actions = nil
		err := uc.scenarioRepo.Create(&scenery)
		if err != nil {
			return err
		}

		scenariesMap[previousScenarioId] = scenery.ID

		for _, action := range actions {
			previousSAId := action.Id
			action.ScenarioId = scenariesMap[action.ScenarioId]
			action.Id = 0
			err := uc.scenarioActionRepo.Create(&action)
			if err != nil {
				return err
			}

			scenarioActionsMap[previousSAId] = action.Id
		}

		if scenery.InitialActionId != nil {
			newInitial := scenarioActionsMap[*scenery.InitialActionId]
			scenery.InitialActionId = &newInitial
			err = uc.scenarioRepo.Update(&scenery)
			if err != nil {
				return err
			}
		}
	}

	for _, connector := range sm.Connectors {
		connector.Id = 0
		connector.ScenarioId = scenariesMap[connector.ScenarioId]
		connector.FromScenarioAction = scenarioActionsMap[connector.FromScenarioAction]
		connector.ToScenarioAction = scenarioActionsMap[connector.ToScenarioAction]

		err := uc.connectorRepo.Create(&connector)
		if err != nil {
			return err
		}
	}

	var transitions []models.StateTransition
	for _, state := range *sm.States {
		transitions = append(transitions, state.Transitions...)

		previousStateId := state.ID
		state.ID = 0
		state.StateMachineID = stateMachine.ID
		state.ScenarioID = scenariesMap[state.ScenarioID]
		state.Transitions = nil
		err := uc.stateRepo.Create(&state)
		if err != nil {
			return err
		}

		statesMap[previousStateId] = state.ID
	}

	for _, transition := range transitions {
		transition.ID = 0
		transition.StateID = statesMap[transition.StateID]
		if transition.ToStateID != nil {
			value := statesMap[*transition.ToStateID]
			transition.ToStateID = &value
		}

		err = uc.stateTransactionRepo.Create(&transition)
		if err != nil {
			return err
		}
	}

	stateMachine.StartStateId = statesMap[previousStateMachineId]
	err = uc.repo.Update(&stateMachine)

	return nil
}

// TODO: Move it to serializer uc
func (uc *stateMachineUsecase) Export(id uint) (*models.StateMachineExport, error) {
	sm, err := uc.repo.GetById(id)
	if err != nil {
		return nil, err
	}

	states, err := uc.stateRepo.List(sm.ID)
	if err != nil {
		return nil, err
	}

	var connectors []models.Connector
	var sceneries []models.Scenario
	for _, state := range *states {
		scenario, err := uc.scenarioRepo.GetById(state.ScenarioID)
		if err != nil {
			continue
		}

		actions, _ := uc.scenarioActionRepo.List(scenario.ID, nil)
		if actions != nil {
			scenario.Actions = *actions
		}

		connectorsList, _ := uc.connectorRepo.List(scenario.ID)
		connectors = append(connectors, *connectorsList...)

		sceneries = append(sceneries, *scenario)
	}

	return &models.StateMachineExport{
		StateMachine: *sm,
		States:       states,
		Sceneries:    sceneries,
		Connectors:   connectors,
	}, nil
}

func (uc *stateMachineUsecase) GetByBot(botId uint) (*models.StateMachine, error) {
	return uc.repo.GetByBotId(botId)
}

func (uc *stateMachineUsecase) Update(item *models.StateMachine) error {
	err := uc.repo.Update(item)
	return err
}

func (uc *stateMachineUsecase) Create(item *models.StateMachine) error {
	err := uc.repo.Create(item)
	return err
}

func (uc *stateMachineUsecase) Delete(id uint) error {
	item, err := uc.Get(id)
	if err != nil {
		return err
	}

	err = uc.stateRepo.DeleteByStateMachineId(id)
	if err != nil {
		return err
	}

	return uc.repo.Delete(item)
}

func NewStateMachineUsecase(
	repo repositories.StateMachineRepository,
	botRepo repositories.BotRepository,
	stateRepo repositories.StateRepository,
	scenarioRepo repositories.ScenarioRepository,
	connectorRepo repositories.ConnectorRepository,
	scenarioActionRepo repositories.ScenarioActionRepository,
	stateTransactionRepo repositories.StateTransitionRepository,
) StateMachineUsecase {
	return &stateMachineUsecase{
		repo:                 repo,
		botRepo:              botRepo,
		stateRepo:            stateRepo,
		scenarioRepo:         scenarioRepo,
		connectorRepo:        connectorRepo,
		scenarioActionRepo:   scenarioActionRepo,
		stateTransactionRepo: stateTransactionRepo,
	}
}
