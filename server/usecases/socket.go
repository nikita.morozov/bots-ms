package usecases

import (
	"bots-ms/repositories"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
)

type SocketUsecase interface {
	ListInner(actionId uint) (*[]coreModel.Socket, error)
	ListOuter(actionId uint) (*[]coreModel.Socket, error)
	GetById(id uint) (*coreModel.Socket, error)
	GetByInnerSlug(slug string, actionId uint) (*coreModel.Socket, error)
	GetByOuterSlug(slug string, actionId uint) (*coreModel.Socket, error)
	GetByOuterEvents(actionId uint) (*[]coreModel.Socket, error)
	Update(item *coreModel.Socket) error
	Create(item *coreModel.Socket) error
	Delete(item *coreModel.Socket) error
}

type socketUsecase struct {
	repo repositories.SocketRepository
}

func (s *socketUsecase) ListInner(actionId uint) (*[]coreModel.Socket, error) {
	return s.repo.List(actionId, coreModel.SocketDirectionIn)
}

func (s *socketUsecase) ListOuter(actionId uint) (*[]coreModel.Socket, error) {
	return s.repo.List(actionId, coreModel.SocketDirectionOut)
}

func (s *socketUsecase) GetById(id uint) (*coreModel.Socket, error) {
	return s.repo.GetById(id)
}

func (s *socketUsecase) GetByInnerSlug(slug string, actionId uint) (*coreModel.Socket, error) {
	return s.repo.GetBySlug(actionId, coreModel.SocketDirectionIn, slug)
}

func (s *socketUsecase) GetByOuterSlug(slug string, actionId uint) (*coreModel.Socket, error) {
	return s.repo.GetBySlug(actionId, coreModel.SocketDirectionOut, slug)
}

func (s *socketUsecase) GetByOuterEvents(actionId uint) (*[]coreModel.Socket, error) {
	return s.repo.GetByType(actionId, coreModel.SocketDirectionOut, coreModel.SocketTypeEvent)
}

func (s *socketUsecase) Update(item *coreModel.Socket) error {
	return s.repo.Update(item)
}

func (s *socketUsecase) Create(item *coreModel.Socket) error {
	return s.repo.Create(item)
}

func (s *socketUsecase) Delete(item *coreModel.Socket) error {
	return s.repo.Delete(item)
}

func NewSocketUsecase(repo repositories.SocketRepository) SocketUsecase {
	return &socketUsecase{
		repo: repo,
	}
}
