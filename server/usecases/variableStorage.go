package usecases

import (
	"bots-ms/repositories"
	"strconv"
	"time"
)

type VariableStorageUsecase interface {
	SetString(ctx string, slug string, data string) error
	SetStringWithDuration(ctx string, slug string, data string, duration time.Duration) error

	SetInt(ctx string, slug string, data int) error
	SetIntWithDuration(ctx string, slug string, data int, duration time.Duration) error

	GetString(ctx string, slug string) (string, error)
	GetInt(ctx string, slug string) (int, error)
	ClearContext(ctx string) error
}

type variableStorageUsecase struct {
	repo repositories.StorageRepo
}

func (s *variableStorageUsecase) SetString(ctx string, slug string, data string) error {
	return s.repo.Set(ctx, slug, []byte(data))
}

func (s *variableStorageUsecase) SetStringWithDuration(ctx string, slug string, data string, duration time.Duration) error {
	return s.repo.SetWithDuration(ctx, slug, []byte(data), duration)
}

func (s *variableStorageUsecase) GetString(ctx string, slug string) (string, error) {
	payload, err := s.repo.Get(ctx, slug)
	if err != nil {
		return "", err
	}

	return string(payload), nil
}

func (s *variableStorageUsecase) SetInt(ctx string, slug string, data int) error {
	return s.repo.Set(ctx, slug, []byte(strconv.Itoa(data)))
}

func (s *variableStorageUsecase) SetIntWithDuration(ctx string, slug string, data int, duration time.Duration) error {
	return s.repo.SetWithDuration(ctx, slug, []byte(strconv.Itoa(data)), duration)
}

func (s *variableStorageUsecase) GetInt(ctx string, slug string) (int, error) {
	payload, err := s.repo.Get(ctx, slug)
	if err != nil {
		return 0, err
	}

	value, err := strconv.ParseInt(string(payload), 10, 64)
	if err != nil {
		return 0, err
	}

	return int(value), nil
}

func (s *variableStorageUsecase) ClearContext(ctx string) error {
	return s.repo.Clear(ctx, "*")
}

func NewVariableStorageUsecase(repo repositories.StorageRepo) VariableStorageUsecase {
	return &variableStorageUsecase{
		repo: repo,
	}
}
