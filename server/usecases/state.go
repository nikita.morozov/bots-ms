package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories"
	"errors"
)

type StateUsecase interface {
	List(stateMachineId uint) (*[]models.State, error)
	Get(id *uint) (*models.State, error)
	GetByScenarioId(smId uint, id uint) (*models.State, error)
	Update(item *models.State) error
	BatchUpdate(items *[]models.State) error
	Create(item *models.State) error
	Delete(id uint) error
}

type stateUsecase struct {
	repo           repositories.StateRepository
	transitionRepo repositories.StateTransitionRepository
	smRepo         repositories.StateMachineRepository
}

func (uc *stateUsecase) List(stateMachineId uint) (*[]models.State, error) {
	return uc.repo.List(stateMachineId)
}

func (uc *stateUsecase) Get(id *uint) (*models.State, error) {
	if id == nil {
		return nil, errors.New("state.not-found")
	}

	return uc.repo.GetById(*id)
}

func (uc *stateUsecase) GetByScenarioId(smId uint, id uint) (*models.State, error) {
	return uc.repo.GetByScenarioId(smId, id)
}

func (uc *stateUsecase) Update(item *models.State) error {
	err := uc.repo.Update(item)
	return err
}

func (uc *stateUsecase) BatchUpdate(items *[]models.State) error {
	for _, item := range *items {
		err := uc.repo.Update(&item)
		if err != nil {
			return err
		}
	}

	return nil
}

func (uc *stateUsecase) Create(item *models.State) error {
	err := uc.repo.Create(item)
	return err
}

func (uc *stateUsecase) Delete(id uint) error {
	item, err := uc.Get(&id)
	if err != nil {
		return err
	}

	sm, err := uc.smRepo.GetById(item.StateMachineID)
	if err != nil {
		return err
	}

	if sm.StartStateId == item.ID {
		sm.StartStateId = 0
		err = uc.smRepo.Update(sm)
		if err != nil {
			return err
		}
	}

	err = uc.transitionRepo.DeleteByStateId(id)
	if err != nil {
		return err
	}

	return uc.repo.Delete(item)
}

func NewStateUsecase(repo repositories.StateRepository, smRepo repositories.StateMachineRepository, transitionRepo repositories.StateTransitionRepository) StateUsecase {
	return &stateUsecase{
		repo:           repo,
		transitionRepo: transitionRepo,
		smRepo:         smRepo,
	}
}
