package usecases

import (
	"bots-ms/registry"
	"bots-ms/repositories"
	"errors"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
)

type ActionUsecase interface {
	Execute(action *coreModel.Action, params coreModel.NodeParams) coreModel.NodeData
	Get(id uint) (*coreModel.Action, error)
	Update(item *coreModel.Action) error
	Create(item *coreModel.Action) error
	List() (*[]coreModel.Action, error)
	Count() (uint64, error)
	Delete(id uint) error
}

type actionUsecase struct {
	repo     repositories.ActionRepository
	registry registry.HandlerRegistry
}

func (uc *actionUsecase) resolve(action *coreModel.Action) (*coreModel.IHandler, error) {
	handler := uc.registry.Get(action.Handler)
	if handler == nil {
		return nil, errors.New("bot-ms.action.failed-resolve-handler")
	}

	return handler, nil
}

func (uc *actionUsecase) Execute(action *coreModel.Action, params coreModel.NodeParams) coreModel.NodeData {
	if action == nil {
		return coreModel.NodeDataError("bot-ms.action-uc.execute-action.action-is-nil")
	}

	handler, err := uc.resolve(action)
	if err != nil {
		return coreModel.NodeDataError(err.Error())
	}

	if handler != nil {
		return (*handler).Execute(params)
	}

	return coreModel.NodeDataError("bot-ms.action-uc.execute-action.action-handler-not-found")
}

func (uc *actionUsecase) Get(id uint) (*coreModel.Action, error) {
	item, err := uc.repo.GetById(id)
	return item, err
}

func (uc *actionUsecase) Update(item *coreModel.Action) error {
	return uc.repo.Update(item)
}

func (uc *actionUsecase) Create(item *coreModel.Action) error {
	err := uc.repo.Create(item)
	return err
}

func (uc *actionUsecase) List() (*[]coreModel.Action, error) {
	return uc.repo.List()
}

func (uc *actionUsecase) Count() (uint64, error) {
	return uc.repo.Count()
}

func (uc *actionUsecase) Delete(id uint) error {
	item, err := uc.repo.GetById(id)
	if err != nil {
		return err
	}

	err = uc.repo.Delete(item)
	return err
}

func NewActionUsecase(repo repositories.ActionRepository, registry registry.HandlerRegistry) ActionUsecase {
	return &actionUsecase{
		repo:     repo,
		registry: registry,
	}
}
