package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories"
)

type StoredMessageUsecase interface {
	Get(id uint) (*models.StoredMessage, error)
	Create(item *models.StoredMessage) error
	Delete(item *models.StoredMessage) error
}

type storedMessageUsecase struct {
	repo repositories.StoredMessagesRepository
}

func (uc *storedMessageUsecase) Get(id uint) (*models.StoredMessage, error) {
	return uc.repo.Get(id)
}

func (uc *storedMessageUsecase) Create(item *models.StoredMessage) error {
	return uc.repo.Create(item)
}

func (uc *storedMessageUsecase) Delete(item *models.StoredMessage) error {
	return uc.repo.Delete(item)
}

func NewStoredMessageUsecase(repo repositories.StoredMessagesRepository) StoredMessageUsecase {
	return &storedMessageUsecase{
		repo: repo,
	}
}
