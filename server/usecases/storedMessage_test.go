package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories/mocks"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gorm.io/gorm"
	"testing"
	"time"
)

type StoredMessageSuite struct {
	suite.Suite

	repo *mocks.StoredMessagesRepository
	uc   StoredMessageUsecase
}

func (s *StoredMessageSuite) BeforeTest(_, _ string) {
	repo := new(mocks.StoredMessagesRepository)
	s.uc = NewStoredMessageUsecase(repo)
	s.repo = repo
}

func TestStoredMessage(t *testing.T) {
	suite.Run(t, new(StoredMessageSuite))
}

// ------------------------------------------------
//
// # Get
//
// ------------------------------------------------
func (s *StoredMessageSuite) Test_storedMessage_Get() {
	mockItem := &models.StoredMessage{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		Text: "",
	}

	s.repo.On("Get", uint(1)).Return(mockItem, nil)
	item, err := s.uc.Get(uint(1))

	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *StoredMessageSuite) Test_storedMessage_Create() {
	mockItem := &models.StoredMessage{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		Text: "",
	}

	s.repo.On("Create", mockItem).Return(nil)
	err := s.uc.Create(mockItem)

	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *StoredMessageSuite) Test_storedMessage_Delete() {
	mockItem := &models.StoredMessage{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		Text: "",
	}

	s.repo.On("Delete", mockItem).Return(nil)
	err := s.uc.Delete(mockItem)

	require.NoError(s.T(), err)
}
