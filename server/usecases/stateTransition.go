package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories"
)

type StateTransitionUsecase interface {
	List(stateMachineId uint) (*[]models.StateTransition, error)
	Get(id uint) (*models.StateTransition, error)
	Update(item *models.StateTransition) error
	Create(item *models.StateTransition) error
	Delete(id uint) error
}

type stateTransitionUsecase struct {
	repo repositories.StateTransitionRepository
}

func (uc *stateTransitionUsecase) List(stateMachineId uint) (*[]models.StateTransition, error) {
	return uc.repo.List(stateMachineId)
}

func (uc *stateTransitionUsecase) Get(id uint) (*models.StateTransition, error) {
	return uc.repo.GetById(id)
}

func (uc *stateTransitionUsecase) Update(item *models.StateTransition) error {
	err := uc.repo.Update(item)
	return err
}

func (uc *stateTransitionUsecase) Create(item *models.StateTransition) error {
	err := uc.repo.Create(item)
	return err
}

func (uc *stateTransitionUsecase) Delete(id uint) error {
	item, err := uc.Get(id)
	if err != nil {
		return err
	}

	return uc.repo.Delete(item)
}

func NewStateTransitionUsecase(repo repositories.StateTransitionRepository) StateTransitionUsecase {
	return &stateTransitionUsecase{
		repo: repo,
	}
}
