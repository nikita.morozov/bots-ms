package usecases

import (
	registryMocks "bots-ms/registry/mocks"
	repoMocks "bots-ms/repositories/mocks"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	coreMock "gitlab.com/nikita.morozov/bot-ms-core/models/mocks"
	"testing"
)

type ActionSuite struct {
	suite.Suite

	actioRepo       *repoMocks.ActionRepository
	handlerRegestry *registryMocks.HandlerRegistry
	uc              ActionUsecase
}

func (s *ActionSuite) BeforeTest(_, _ string) {
	actioRepo := new(repoMocks.ActionRepository)
	handlerRegestry := new(registryMocks.HandlerRegistry)

	s.uc = NewActionUsecase(actioRepo, handlerRegestry)

	s.actioRepo = actioRepo
	s.handlerRegestry = handlerRegestry
}

func TestAction(t *testing.T) {
	suite.Run(t, new(ActionSuite))
}

// ------------------------------------------------
//
// # Execute
//
// ------------------------------------------------
func (s *ActionSuite) Test_action_ExecuteByName() {
	mockAction := &coreModel.Action{
		Id:      uint(1),
		Name:    "sendMessage",
		Handler: "sendMessageMethod",
	}

	var handler coreModel.IHandler
	methodHandler := new(coreMock.IHandler)
	handler = methodHandler

	params := coreModel.NodeParams{
		"oil": []byte("bad"),
	}

	data := coreModel.NodeData{
		"result": []byte("goooood"),
	}

	s.handlerRegestry.On("Get", "sendMessageMethod").Return(&handler, nil)
	s.handlerRegestry.On("Resolve", mockAction).Return(&handler, nil)
	methodHandler.On("Execute", params).Return(data)

	res := s.uc.Execute(mockAction, params)

	require.Equal(s.T(), data, res)
}

// ------------------------------------------------
//
// # Get
//
// ------------------------------------------------
func (s *ActionSuite) Test_action_Get() {
	mockItem := &coreModel.Action{
		Id:      uint(1),
		Name:    "sendMessage",
		Handler: "sendMessageMethod",
	}

	s.actioRepo.On("GetById", uint(1)).Return(mockItem, nil)

	action, err := s.uc.Get(uint(1))

	require.NoError(s.T(), err)
	require.NotNil(s.T(), action)
}

// ------------------------------------------------
//
// # Update
//
// ------------------------------------------------
func (s *ActionSuite) Test_action_Update() {
	mockItem := &coreModel.Action{
		Id:      uint(1),
		Name:    "sendMessage",
		Handler: "sendMessageMethod",
	}

	s.actioRepo.On("Update", mockItem).Return(nil)
	err := s.uc.Update(mockItem)

	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *ActionSuite) Test_action_Create() {
	mockItem := &coreModel.Action{
		Id:      uint(1),
		Name:    "sendMessage",
		Handler: "sendMessageMethod",
	}

	s.actioRepo.On("Create", mockItem).Return(nil)
	err := s.uc.Create(mockItem)

	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *ActionSuite) Test_action_Delete() {
	mockItem := &coreModel.Action{
		Id:      uint(1),
		Name:    "sendMessage",
		Handler: "sendMessageMethod",
	}

	s.actioRepo.On("GetById", uint(1)).Return(mockItem, nil)
	s.actioRepo.On("Delete", mockItem).Return(nil)
	err := s.uc.Delete(uint(1))

	require.NoError(s.T(), err)
}
