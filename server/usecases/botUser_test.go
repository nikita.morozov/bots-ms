package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories/mocks"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/gorm"
	"testing"
	"time"
)

type BotUserSuite struct {
	suite.Suite

	repo *mocks.BotUserRepository
	uc   BotUserUsecase
}

func (s *BotUserSuite) BeforeTest(_, _ string) {
	repo := new(mocks.BotUserRepository)
	s.uc = NewBotUserUsecase(repo)
	s.repo = repo
}

func TestBotUser(t *testing.T) {
	suite.Run(t, new(BotUserSuite))
}

// ------------------------------------------------
//
// # List
//
// ------------------------------------------------
func (s *BotUserSuite) Test_botUser_List() {
	mockItems := &[]models.BotUser{
		{
			Model: gorm.Model{
				ID:        uint(1),
				CreatedAt: time.Now(),
			},
			BotID:  uint(1),
			UserID: "userId",
		},
	}

	s.repo.On("List", "telegram", (*sharedModels.ListOptions)(nil)).Return(mockItems, sharedModels.Paginator{Offset: 0, TotalCount: 1, CountPerPage: 10}, nil)

	list, _, err := s.uc.List("telegram", nil)

	require.NoError(s.T(), err)
	require.NotNil(s.T(), list)
	require.Equal(s.T(), len(*list), 1)
}

// ------------------------------------------------
//
// # Get
//
// ------------------------------------------------
func (s *BotUserSuite) Test_botUser_Get() {
	mockItem := &models.BotUser{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		BotID:  uint(1),
		UserID: "userId",
	}

	s.repo.On("GetById", uint(1)).Return(mockItem, nil)
	item, err := s.uc.Get(uint(1))

	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
}

// ------------------------------------------------
//
// # GetByUserId
//
// ------------------------------------------------
func (s *BotUserSuite) Test_botUser_GetByUserId() {
	mockItem := &models.BotUser{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		BotID:  uint(1),
		UserID: "userId",
	}

	s.repo.On("GetByUserId", uint(1), "userId").Return(mockItem, nil)

	item, err := s.uc.GetByUserId(uint(1), "userId")

	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
}

// ------------------------------------------------
//
// # Update
//
// ------------------------------------------------
func (s *BotUserSuite) Test_botUser_Update() {
	mockItem := &models.BotUser{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		BotID:  uint(1),
		UserID: "userId",
	}

	s.repo.On("Update", mockItem).Return(nil)
	err := s.uc.Update(mockItem)

	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *BotUserSuite) Test_botUser_Create() {
	mockItem := &models.BotUser{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		BotID:  uint(1),
		UserID: "userId",
	}

	s.repo.On("Create", mockItem).Return(nil)
	err := s.uc.Create(mockItem)

	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *BotUserSuite) Test_botUser_Delete() {
	mockItem := &models.BotUser{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		BotID:  uint(1),
		UserID: "userId",
	}

	s.repo.On("GetById", uint(1)).Return(mockItem, nil)
	s.repo.On("Delete", mockItem).Return(nil)
	err := s.uc.Delete(uint(1))

	require.NoError(s.T(), err)
}
