package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories/mocks"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/gorm"
	"testing"
	"time"
)

type EventSuite struct {
	suite.Suite

	eventRepository *mocks.EventRepository
	uc              EventUsecase
}

func (s *EventSuite) BeforeTest(_, _ string) {
	eventRepository := new(mocks.EventRepository)
	s.uc = NewEventUsecase(eventRepository)
	s.eventRepository = eventRepository
}

func TestEvent(t *testing.T) {
	suite.Run(t, new(EventSuite))
}

// ------------------------------------------------
//
// # Get
//
// ------------------------------------------------
func (s *EventSuite) Test_event_Get() {
	mockItem := &models.Event{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		BotID:      uint(1),
		ScenarioID: uint(1),
		Command:    "command",
	}

	s.eventRepository.On("GetById", uint(1)).Return(mockItem, nil)
	item, err := s.uc.Get(uint(1))

	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
}

// ------------------------------------------------
//
// # List
//
// ------------------------------------------------
func (s *EventSuite) Test_event_List() {
	mockItems := &[]models.Event{
		{
			Model: gorm.Model{
				ID:        uint(1),
				CreatedAt: time.Now(),
			},
			BotID:      uint(1),
			ScenarioID: uint(1),
			Command:    "command",
		},
	}

	s.eventRepository.On("List", (*sharedModels.ListOptions)(nil)).Return(mockItems, sharedModels.Paginator{Offset: 0, TotalCount: 1, CountPerPage: 10}, nil)
	list, _, err := s.uc.List(nil)

	require.NoError(s.T(), err)
	require.NotNil(s.T(), list)
	require.Equal(s.T(), len(*list), 1)
}

// ------------------------------------------------
//
// # Update
//
// ------------------------------------------------
func (s *EventSuite) Test_event_Update() {
	mockItem := &models.Event{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		BotID:      uint(1),
		ScenarioID: uint(1),
		Command:    "command",
	}

	s.eventRepository.On("Update", mockItem).Return(nil)
	err := s.uc.Update(mockItem)

	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *EventSuite) Test_event_Create() {
	mockItem := &models.Event{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		BotID:      uint(1),
		ScenarioID: uint(1),
		Command:    "command",
	}

	s.eventRepository.On("Create", mockItem).Return(nil)
	err := s.uc.Create(mockItem)

	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *EventSuite) Test_event_Delete() {
	mockItem := &models.Event{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		BotID:      uint(1),
		ScenarioID: uint(1),
		Command:    "command",
	}

	s.eventRepository.On("GetById", uint(1)).Return(mockItem, nil)
	s.eventRepository.On("Delete", mockItem).Return(nil)
	err := s.uc.Delete(uint(1))

	require.NoError(s.T(), err)
}
