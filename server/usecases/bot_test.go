package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories/mocks"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/gorm"
	"testing"
	"time"
)

type BotSuite struct {
	suite.Suite

	repo *mocks.BotRepository
	uc   BotUsecase
}

func (s *BotSuite) BeforeTest(_, _ string) {
	repo := new(mocks.BotRepository)
	s.uc = NewBotUsecase(repo)
	s.repo = repo
}

func TestBot(t *testing.T) {
	suite.Run(t, new(BotSuite))
}

// ------------------------------------------------
//
// # GetBySlug
//
// ------------------------------------------------
func (s *BotSuite) Test_bot_GetBySlug() {
	mockItem := &models.Bot{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		Token:   "token",
		Slug:    "abc_slug",
		Handler: "telegram",
	}

	s.repo.On("GetBySlug", "abc_slug").Return(mockItem, nil)

	item, err := s.uc.GetBySlug("abc_slug")

	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
}

// ------------------------------------------------
//
// # List
//
// ------------------------------------------------
func (s *BotSuite) Test_bot_List() {
	mockItems := &[]models.Bot{
		{
			Model: gorm.Model{
				ID:        uint(1),
				CreatedAt: time.Now(),
			},
			Token:   "token",
			Slug:    "abc_slug",
			Handler: "telegram",
		},
	}

	s.repo.On("List", (*sharedModels.ListOptions)(nil)).Return(mockItems, sharedModels.Paginator{Offset: 0, TotalCount: 1, CountPerPage: 10}, nil)

	list, _, err := s.uc.List(nil)

	require.NoError(s.T(), err)
	require.NotNil(s.T(), list)
	require.Equal(s.T(), len(*list), 1)
}

// ------------------------------------------------
//
// # Get
//
// ------------------------------------------------
func (s *BotSuite) Test_bot_Get() {
	mockItem := &models.Bot{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		Token:   "token",
		Slug:    "abc_slug",
		Handler: "telegram",
	}

	s.repo.On("GetById", uint(1)).Return(mockItem, nil)

	item, err := s.uc.Get(uint(1))

	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
}

// ------------------------------------------------
//
// # Update
//
// ------------------------------------------------
func (s *BotSuite) Test_bot_Update() {
	mockItem := &models.Bot{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		Token:   "token",
		Slug:    "abc_slug",
		Handler: "telegram",
	}

	s.repo.On("Update", mockItem).Return(nil)
	err := s.uc.Update(mockItem)

	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *BotSuite) Test_bot_Create() {
	mockItem := &models.Bot{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		Token:   "token",
		Slug:    "abc_slug",
		Handler: "telegram",
	}

	s.repo.On("Create", mockItem).Return(nil)
	err := s.uc.Create(mockItem)

	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *BotSuite) Test_bot_Delete() {
	mockItem := &models.Bot{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		Token:   "token",
		Slug:    "abc_slug",
		Handler: "telegram",
	}

	s.repo.On("GetById", uint(1)).Return(mockItem, nil)
	s.repo.On("Delete", mockItem).Return(nil)
	err := s.uc.Delete(uint(1))

	require.NoError(s.T(), err)
}
