package usecases

import (
	"bots-ms/repositories"
	"fmt"
)

type ScenarioStorageUsecase interface {
	Set(ctx string, scenarioActionId uint, socketId uint, payload []byte) error
	Get(ctx string, scenarioActionId uint, socketId uint) ([]byte, error)
	Clear(ctx string, scenarioActionId uint, socketId uint) error
	ClearContext(ctx string) error
}

type scenarioStorageUsecase struct {
	repo repositories.StorageRepo
}

func (s *scenarioStorageUsecase) getPath(scenarioActionId uint, socketId uint) string {
	return fmt.Sprintf("scenarioActionId_%d_socketId_%d", scenarioActionId, socketId)
}

func (s *scenarioStorageUsecase) Set(ctx string, scenarioActionId uint, socketId uint, payload []byte) error {
	return s.repo.Set(ctx, s.getPath(scenarioActionId, socketId), payload)
}

func (s *scenarioStorageUsecase) Get(ctx string, scenarioActionId uint, socketId uint) ([]byte, error) {
	return s.repo.Get(ctx, s.getPath(scenarioActionId, socketId))
}

func (s *scenarioStorageUsecase) Clear(ctx string, scenarioActionId uint, socketId uint) error {
	return s.repo.Clear(ctx, s.getPath(scenarioActionId, socketId))
}

func (s *scenarioStorageUsecase) ClearContext(ctx string) error {
	return s.repo.Clear(ctx, "*")
}

func NewScenarioStorageUsecase(repo repositories.StorageRepo) ScenarioStorageUsecase {
	return &scenarioStorageUsecase{
		repo: repo,
	}
}
