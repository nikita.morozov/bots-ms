package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
)

type ScenarioAcrionUsecase interface {
	List(scenarioId uint, opt *sharedModels.ListOptions) (*[]models.ScenarioAction, error)
	Get(id uint) (*models.ScenarioAction, error)
	Update(item *models.ScenarioAction) error
	BatchUpdate(items *[]models.ScenarioAction) error
	Create(item *models.ScenarioAction) error
	Delete(id uint) error
}

type scenarioActionUsecase struct {
	repo         repositories.ScenarioActionRepository
	connRepo     repositories.ConnectorRepository
	scenarioRepo repositories.ScenarioRepository
}

func (uc *scenarioActionUsecase) List(scenarioId uint, opt *sharedModels.ListOptions) (*[]models.ScenarioAction, error) {
	return uc.repo.List(scenarioId, opt)
}

func (uc *scenarioActionUsecase) Get(id uint) (*models.ScenarioAction, error) {
	return uc.repo.GetById(id)
}

func (uc *scenarioActionUsecase) Update(item *models.ScenarioAction) error {
	err := uc.repo.Update(item)
	return err
}

func (uc *scenarioActionUsecase) BatchUpdate(items *[]models.ScenarioAction) error {
	for _, item := range *items {
		err := uc.repo.Update(&item)
		if err != nil {
			return err
		}
	}

	return nil
}

func (uc *scenarioActionUsecase) Create(item *models.ScenarioAction) error {
	err := uc.repo.Create(item)
	return err
}

func (uc *scenarioActionUsecase) Delete(id uint) error {
	item, err := uc.Get(id)
	if err != nil {
		return err
	}

	scenario, err := uc.scenarioRepo.GetById(item.ScenarioId)
	if err != nil {
		return err
	}

	if scenario.InitialActionId != nil && *scenario.InitialActionId == id {
		scenario.InitialActionId = nil
		err = uc.scenarioRepo.Update(scenario)
		if err != nil {
			return err
		}
	}

	err = uc.connRepo.DeleteByScenarioActionId(id)
	if err != nil {
		return err
	}

	return uc.repo.Delete(item)
}

func NewScenarioActionUsecase(repo repositories.ScenarioActionRepository, scenarioRepo repositories.ScenarioRepository, connRepo repositories.ConnectorRepository) ScenarioAcrionUsecase {
	return &scenarioActionUsecase{
		repo:         repo,
		connRepo:     connRepo,
		scenarioRepo: scenarioRepo,
	}
}
