package usecases

import (
	"bots-ms/models"
	"errors"
	"fmt"
	"github.com/tidwall/gjson"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"log"
)

type AsyncExecutor interface {
	FinishExecutionWithParams(context string, scenarioActionId uint, data coreModel.NodeData) error
}

type ScenarioEngine interface {
	AsyncExecutor
	Execute(scenarioId uint, data *coreModel.NodeParams) error
	ExecuteTrigger(params *coreModel.NodeParams) error
}

type scenarioEngine struct {
	actionUsecase         ActionUsecase
	scenarioUsecase       ScenarioUsecase
	scenarioActionUsecase ScenarioAcrionUsecase
	connectorInteractor   ConnectorUsecase
	triggerStorage        TriggerStorageUsecase
	contextStorage        ContextStorageUserCase
	scenarioStorage       ScenarioStorageUsecase
	socketUsecase         SocketUsecase
}

func (e *scenarioEngine) isUniqueSocket(connector models.Connector, connectors *[]models.Connector) bool {
	for _, c := range *connectors {
		if c.Id == connector.Id {
			continue
		}

		if c.ToSocket == connector.ToSocket && c.ToScenarioAction == connector.ToScenarioAction {
			return false
		}
	}

	return true
}

func (e *scenarioEngine) ExecuteScenarioAction(context string, scenarioActionId uint, data *coreModel.NodeParams) (*uint, error) {
	scenarioAction, err := e.scenarioActionUsecase.Get(scenarioActionId)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Scenario action with ID: %d, has error: %s", scenarioActionId, err.Error()))
	}

	action, err := e.actionUsecase.Get(scenarioAction.ActionId)

	innerConnectors, err := e.connectorInteractor.GetInnerList(scenarioActionId)
	if err != nil {
		return nil, err
	}
	// If inner connectors not found
	params := coreModel.NodeParamsFromSource(*data)
	params.Set("context", context)
	params.SetUint("scenarioActionId", scenarioActionId)

	var jsonMap map[string]interface{}
	scenarioActionDataExist := len(scenarioAction.Data) > 0

	//Set data from scenario action to params
	//TODO: Cache db requests
	if scenarioActionDataExist {
		var ok bool
		jsonMap, ok = gjson.Parse(scenarioAction.Data).Value().(map[string]interface{})
		if !ok {
			return nil, errors.New("scenario-action-data-unmarshal-failed")
		}

		sockets, err := e.socketUsecase.ListInner(scenarioAction.ActionId)
		if err != nil {
			return nil, err
		}

		for _, socket := range *sockets {
			if _, ok := jsonMap[socket.Slug]; ok {
				switch socket.Type {
				case coreModel.SocketTypeBool:
					if value, ok := jsonMap[socket.Slug].(bool); ok {
						params.SetBool(socket.Slug, value)
						continue
					}
				case coreModel.SocketTypeInt:
					if value, ok := jsonMap[socket.Slug].(int); ok {
						params.SetInt(socket.Slug, value)
						continue
					}
				}

				params.Set(socket.Slug, fmt.Sprintf("%s", jsonMap[socket.Slug]))
			}
		}
	}

	if len(*innerConnectors) > 0 {
		for _, connector := range *innerConnectors {
			toSocket, _ := e.socketUsecase.GetById(connector.ToSocket)
			if _, ok := params[toSocket.Slug]; ok {
				continue
			}

			val, err := e.scenarioStorage.Get(context, connector.FromScenarioAction, connector.FromSocket)
			isUnique := e.isUniqueSocket(connector, innerConnectors)

			if isUnique {
				if err != nil {
					// Execute action and take value again
					e.ExecuteScenarioAction(context, connector.FromScenarioAction, &coreModel.NodeParams{"backward": []byte("true")})
					val, _ = e.scenarioStorage.Get(context, connector.FromScenarioAction, connector.FromSocket)
				}
				params[toSocket.Slug] = val
			} else {
				if len(val) > 0 {
					params[toSocket.Slug] = val
				}
			}
		}
	}

	// Set new values
	values := e.actionUsecase.Execute(action, params)
	if values != nil {
		if scenarioActionDataExist {
			for key, val := range jsonMap {
				// Skip all null data
				if val == "" {
					continue
				}

				switch val.(type) {
				case string:
					values.Set(key, val.(string))
				case bool:
					values.SetBool(key, val.(bool))
				default:
					fmt.Printf("!! Key %s type is unknown", key)
				}

			}
		}

		e.setOuterParamsToStorage(context, scenarioAction, values)

		// Execute events
		return e.getScenarioActionByEventEmission(scenarioAction, values)
	}

	return nil, err
}

func (e *scenarioEngine) getScenarioActionByEventEmission(scenarioAction *models.ScenarioAction, values coreModel.NodeData) (*uint, error) {
	eventSockets, err := e.socketUsecase.GetByOuterEvents(scenarioAction.ActionId)
	if err != nil {
		return nil, err
	}

	for _, eventSocket := range *eventSockets {
		if values.IsExist(eventSocket.Slug) {
			connector, err := e.connectorInteractor.GetOuterBySocket(scenarioAction.Id, eventSocket.Id)
			if connector != nil {
				return &connector.ToScenarioAction, err
			}
		}
	}

	return nil, nil
}

func (e *scenarioEngine) setOuterParamsToStorage(context string, scenarioAction *models.ScenarioAction, values coreModel.NodeData) {
	for key, value := range values {
		socket, _ := e.socketUsecase.GetByOuterSlug(key, scenarioAction.ActionId)
		if socket == nil {
			log.Printf("💭🤖 Socket %s for action %d not found", key, scenarioAction.ActionId)
			continue
		}
		err := e.scenarioStorage.Set(context, scenarioAction.Id, socket.Id, value)
		if err != nil {
			log.Printf("💭🤖 Socket %s save to storage err: %s", key, err.Error())
			continue
		}
	}
}

func (e *scenarioEngine) Execute(scenarioId uint, data *coreModel.NodeParams) error {
	botHandler := data.GetString("botHandler")
	sender := data.GetString("sender")
	context := data.GetString("context")

	// Start new session
	err := e.contextStorage.Set(botHandler, sender, context)
	if err != nil {
		return err
	}

	// Get initial scenario action
	scenario, err := e.scenarioUsecase.Get(scenarioId)
	if err != nil {
		return err
	}

	currentScenarioAction := scenario.InitialActionId
	if currentScenarioAction == nil {
		return errors.New(fmt.Sprintf("Scenario %d has no initial action", scenarioId))
	}

	params := *data

	// Run node script
	err = e.run(context, currentScenarioAction, params)
	if err != nil {
		return err
	}

	//TODO Clear context

	return nil
}

func (e *scenarioEngine) run(context string, scenarioActionId *uint, params coreModel.NodeParams) error {
	var err error
	currentScenarioAction := scenarioActionId

	for currentScenarioAction != nil {
		currentScenarioAction, err = e.ExecuteScenarioAction(context, *currentScenarioAction, &params)
		if err != nil {
			return err
		}

		if params != nil {
			params = nil
		}
	}

	return nil
}

func (e scenarioEngine) FinishExecutionWithParams(context string, scenarioActionId uint, data coreModel.NodeData) error {
	scenarioAction, err := e.scenarioActionUsecase.Get(scenarioActionId)
	if err != nil {
		return errors.New(fmt.Sprintf("Scenario action with ID: %d, has error: %s", scenarioActionId, err.Error()))
	}

	e.setOuterParamsToStorage(context, scenarioAction, data)

	// Execute events
	ToScenarioAction, err := e.getScenarioActionByEventEmission(scenarioAction, data)
	if ToScenarioAction != nil {
		return e.run(context, ToScenarioAction, nil)
	}

	return nil
}

//TODO: Check code on repeats

func (e *scenarioEngine) ExecuteTrigger(params *coreModel.NodeParams) error {
	botHandler := params.GetString("botHandler")
	sender := params.GetString("sender")
	event := params.GetString("event")
	context, err := e.contextStorage.Get(botHandler, sender)
	if err != nil {
		log.Printf("🛑🤖 Trigger context getting for %s with ctx %s not found, details: %s", sender, botHandler, err.Error())
	}

	trigger, err := e.triggerStorage.Get(context, event)
	content := params.GetString("content")

	if err != nil {
		log.Printf("🛑🤖 Trigger %s not found, details: %s", event, err.Error())
		return err
	}

	data := coreModel.NodeParams{}
	// TODO: Maybe it unnecessary
	data.Set("execute", trigger.EventName)
	data.Set("value", content)

	// TODO: Refactoring plzzz
	// Find next scenario action
	scenarioAction, err := e.scenarioActionUsecase.Get(trigger.ScenarioActionID)
	if err != nil {
		return errors.New(fmt.Sprintf("Scenario action with ID: %d, has error: %s", trigger.ScenarioActionID, err.Error()))
	}

	if len(trigger.ContentField) > 0 {
		valueSockets, _ := e.socketUsecase.GetByOuterSlug(trigger.ContentField, scenarioAction.ActionId)
		if valueSockets != nil {
			e.scenarioStorage.Set(context, trigger.ScenarioActionID, valueSockets.Id, []byte(content))
		}
	}

	//Set event
	eventSockets, err := e.socketUsecase.GetByOuterEvents(scenarioAction.ActionId)
	if err != nil {
		return err
	}

	for _, eventSocket := range *eventSockets {
		if eventSocket.Slug == trigger.EventName {
			// Save current value
			err = e.scenarioStorage.Set(context, trigger.ScenarioActionID, eventSocket.Id, []byte("true"))

			// Clear triggers
			err = e.triggerStorage.ClearContext(context)

			connector, err := e.connectorInteractor.GetOuterBySocket(trigger.ScenarioActionID, eventSocket.Id)
			if err != nil {
				return err
			}

			if connector != nil {
				return e.run(context, &connector.ToScenarioAction, data)
			}
		}
	}

	return err
}

func NewScenarioEngine(actionUsecase ActionUsecase, scenarioUsecase ScenarioUsecase, triggerStorage TriggerStorageUsecase, contextStorage ContextStorageUserCase, scenarioActionUsecase ScenarioAcrionUsecase, connectorInteractor ConnectorUsecase, scenarioStorage ScenarioStorageUsecase, socketUsecase SocketUsecase) ScenarioEngine {
	return &scenarioEngine{
		actionUsecase:         actionUsecase,
		scenarioUsecase:       scenarioUsecase,
		scenarioActionUsecase: scenarioActionUsecase,
		connectorInteractor:   connectorInteractor,
		triggerStorage:        triggerStorage,
		contextStorage:        contextStorage,
		scenarioStorage:       scenarioStorage,
		socketUsecase:         socketUsecase,
	}
}
