package usecases

import (
	"bots-ms/models"
	"bots-ms/repositories/mocks"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gorm.io/gorm"
	"testing"
	"time"
)

type AuthRequestSuite struct {
	suite.Suite

	repo *mocks.AuthRequestRepository
	uc   AuthRequestUsecase
}

func (s *AuthRequestSuite) BeforeTest(_, _ string) {
	repo := new(mocks.AuthRequestRepository)
	s.uc = NewAuthRequestUsecase(repo)
	s.repo = repo
}

func TestAuthRequest(t *testing.T) {
	suite.Run(t, new(AuthRequestSuite))
}

// ------------------------------------------------
//
// # GetByHash
//
// ------------------------------------------------
func (s *AuthRequestSuite) Test_authRequest_GetByHash() {
	mockItem := &models.AuthRequest{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		Meta:  "",
		Hash:  "",
		BotID: uint(1),
	}

	s.repo.On("GetByHash", "abcabc").Return(mockItem, nil)
	authRequest, err := s.uc.GetByHash("abcabc")

	require.NoError(s.T(), err)
	require.NotNil(s.T(), authRequest)
}

// ------------------------------------------------
//
// # GetByMeta
//
// ------------------------------------------------
func (s *AuthRequestSuite) Test_authRequest_GetByMeta() {
	mockItem := &models.AuthRequest{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		Meta:  "",
		Hash:  "",
		BotID: uint(1),
	}

	s.repo.On("GetByMeta", uint(1), "abcabc").Return(mockItem, nil)

	authRequest, err := s.uc.GetByMeta(uint(1), "abcabc")

	require.NoError(s.T(), err)
	require.NotNil(s.T(), authRequest)
}

// ------------------------------------------------
//
// # Get
//
// ------------------------------------------------
func (s *AuthRequestSuite) Test_authRequest_Get() {
	mockItem := &models.AuthRequest{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		Meta:  "",
		Hash:  "",
		BotID: uint(1),
	}

	s.repo.On("GetById", uint(1)).Return(mockItem, nil)

	authRequest, err := s.uc.Get(uint(1))

	require.NoError(s.T(), err)
	require.NotNil(s.T(), authRequest)
}

// ------------------------------------------------
//
// # Create
//
// ------------------------------------------------
func (s *AuthRequestSuite) Test_authRequest_Create() {
	mockItem := &models.AuthRequest{
		Meta:  "",
		Hash:  "",
		BotID: uint(1),
	}

	s.repo.On("Create", mockItem).Return(nil)
	err := s.uc.Create(mockItem)

	require.NoError(s.T(), err)
}

// ------------------------------------------------
//
// # Delete
//
// ------------------------------------------------
func (s *AuthRequestSuite) Test_authRequest_Delete() {
	mockItem := &models.AuthRequest{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		Meta:  "",
		Hash:  "",
		BotID: uint(1),
	}

	s.repo.On("GetById", uint(1)).Return(mockItem, nil)
	s.repo.On("Delete", mockItem).Return(nil)
	err := s.uc.Delete(uint(1))

	require.NoError(s.T(), err)
}
