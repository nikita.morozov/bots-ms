package botHandlers

import (
	"bots-ms/executor"
	"bots-ms/models"
	"bots-ms/models/dto"
	"fmt"
	tb "gopkg.in/tucnak/telebot.v3"
	"log"
	"strings"
	"time"
)

type recipient struct {
	user string
}

func (r recipient) Recipient() string {
	return r.user
}

func NewRecipient(user string) tb.Recipient {
	return &recipient{
		user: user,
	}
}

type telegramBot struct {
	bot      models.Bot
	instance *tb.Bot

	botExecutor executor.BotExecutor
}

func (t telegramBot) GetBot() *models.Bot {
	return &t.bot
}

func (t telegramBot) Delete(message models.CommonMessage) {
	err := t.instance.Delete(message)
	if err != nil {
		log.Printf("⚠️🤖 Telegram bot \033[1;34m\"%s\"\033[0m delete error %s", t.bot.Slug, err.Error())
	}
}

func (t telegramBot) SendAction(sender string, action string) {
	var recipient = NewRecipient(sender)

	if action == "typing" {
		t.instance.Notify(recipient, tb.Typing)
	}
}

func (t telegramBot) ShowKeyboard(data models.KeyboardInfo) {
	selector := &tb.ReplyMarkup{}

	extra := data.Extra
	var recipient = NewRecipient(data.Sender)

	var splitButton int
	if data.SplitButtons > 0 {
		splitButton = data.SplitButtons
	} else {
		splitButton = 3
	}

	if data.Type == models.KeyboardDtoTypeReply {
		if extra["forceReply"] == "true" {
			selector.ForceReply = true
		}

		if extra["oneTimeKeyboard"] == "true" {
			selector.OneTimeKeyboard = true
		}

		selector.ResizeKeyboard = true

		var btns []tb.Btn
		for _, button := range data.Buttons {
			btn := selector.Text(button.Name)
			btns = append(btns, btn)
		}

		selector.Reply(selector.Split(splitButton, btns)...)
	}

	if data.Type == models.KeyboardDtoTypeInline {
		var btns []tb.Btn
		for _, btn := range data.Buttons {
			btns = append(btns, selector.Data(btn.Name, btn.Slug))
		}

		selector.Inline(selector.Split(splitButton, btns)...)
	}

	style := tb.ModeDefault
	if extra != nil && extra["markdown"] == "true" {
		style = tb.ModeMarkdown
	}

	_, err := t.instance.Send(recipient, data.Text, &tb.SendOptions{ReplyMarkup: selector, ParseMode: style})
	if err != nil {
		t.instance.Send(recipient, data.Text, selector)
	}
}

func (t telegramBot) RemoveKeyboard(sender string, text string, extra map[string]string) {
	selector := &tb.ReplyMarkup{}
	selector.RemoveKeyboard = true
	var recipient = NewRecipient(sender)

	style := tb.ModeDefault
	if extra != nil && extra["markdown"] == "true" {
		style = tb.ModeMarkdown
	}

	_, err := t.instance.Send(recipient, text, &tb.SendOptions{ReplyMarkup: selector, ParseMode: style})
	if err != nil {
		t.instance.Send(recipient, text, selector)
	}
}

func (t telegramBot) Edit(messageId string, content string) interface{} {
	var message = models.CommonMessage{
		Data: messageId,
	}

	log.Printf("🤖 Telegram bot \033[1;34m\"%s\"\033[0m edit message %s to %s", t.bot.Slug, messageId, content)
	_, err := t.instance.Edit(message, content)
	if err != nil {
		log.Printf("⚠️🤖 Telegram bot \033[1;34m\"%s\"\033[0m edit error %s", t.bot.Slug, err)
	}

	return err
}

func (t telegramBot) SendError(sender string, content string) {
	t.Send(sender, "⚠️ Error: "+content, nil)
}

func (t telegramBot) GetSlug() string {
	return t.bot.GetSlug()
}

func (t telegramBot) GetId(message *tb.Message) string {
	if message == nil {
		return ""
	}

	id, chatId := message.MessageSig()
	return fmt.Sprintf("%s_%d", id, chatId)
}

func (t telegramBot) Send(sender string, content string, extra map[string]string) interface{} {
	var recipient = NewRecipient(sender)

	style := tb.ModeDefault
	if extra != nil && extra["markdown"] == "true" {
		style = tb.ModeMarkdown
	}

	message, err := t.instance.Send(recipient, content, &tb.SendOptions{ParseMode: style})
	if err != nil {
		message, _ = t.instance.Send(recipient, content)
	}

	return t.GetId(message)
}

func (t telegramBot) Stop() {
	t.instance.Stop()
}

func (t telegramBot) Start(done chan bool) {
	defer func() {
		done <- false
	}()

	t.instance.Handle(tb.OnText, func(ctx tb.Context) error {
		m := ctx.Message()

		if t.bot.AutoRemoveMessages {
			t.instance.Delete(m)
		}

		var messageID = t.GetId(m)
		var replayToMID = t.GetId(m.ReplyTo)
		log.Printf("💭🤖 Telegram bot \033[1;34m\"%s\"\033[0m new message: \"%s\"\n", t.bot.Slug, m.Text)

		if !t.bot.DisableTriggers {
			err := t.botExecutor.ExecuteTrigger(
				dto.BotContentDto{
					Bot:      t.bot,
					Data:     m.Text,
					Event:    "input",
					Sender:   m.Sender.Recipient(),
					Nickname: ctx.Sender().Username,
					ReplayTo: replayToMID,
				},
			)

			if err == nil {
				return nil
			}
		}

		err := t.botExecutor.ExecuteCommand(
			m.Text,
			dto.BotContentDto{
				Bot:       t.bot,
				External:  m.Chat.ID < 0,
				Sender:    m.Sender.Recipient(),
				MessageId: messageID,
				Nickname:  ctx.Sender().Username,
				ReplayTo:  replayToMID,
			},
		)

		log.Printf("⚠️🤖 Telegram bot \033[1;34m\"%s\"\033[0m command error: %s\n", t.bot.Slug, err)

		return nil
	})

	t.instance.Handle(tb.OnCallback, func(ctx tb.Context) error {
		if t.bot.DisableTriggers {
			return nil
		}
		r := ctx.Callback()
		log.Printf("💭🤖 Telegram bot \033[1;34m\"%s\"\033[0m new message: \"%s\"\n", t.bot.Slug, r.Data)

		t.botExecutor.ExecuteTrigger(
			dto.BotContentDto{
				Bot:      t.bot,
				Event:    strings.Replace(r.Data, "\f", "", 1),
				Sender:   r.Sender.Recipient(),
				Nickname: ctx.Sender().Username,
			},
		)

		return nil
	})

	go t.instance.Start()
	log.Printf("✅🤖 Telegram bot \033[1;34m\"%s\"\033[0m started\n", t.bot.Slug)
	done <- true
}

func NewTelegramBot(bot models.Bot, botExecutor executor.BotExecutor) (models.IBotHandler, error) {
	log.Printf("🤖 Telegram bot \033[1;34m\"%s\"\033[0m initialization\n", bot.Slug)
	poller := &tb.LongPoller{Timeout: 15 * time.Second}

	b, err := tb.NewBot(tb.Settings{
		Token:  bot.Token,
		Poller: poller,
	})

	if err != nil {
		return nil, err
	}

	return &telegramBot{
		bot:         bot,
		instance:    b,
		botExecutor: botExecutor,
	}, err
}
