package models

import "gorm.io/gorm"

type Scenario struct {
	gorm.Model
	Title           string           `json:"title"`
	Category        string           `json:"category"`
	InitialActionId *uint            `json:"initial"`
	Actions         []ScenarioAction `json:"actions"`
}
