package models

import (
	"fmt"
	"gorm.io/gorm"
)

type Bot struct {
	gorm.Model
	Env                string `json:"env" gorm:"default:production"`
	Token              string `json:"token"`
	Slug               string `json:"slug"`
	Handler            string `json:"handler"`
	ExecutorHandler    string `json:"executorHandler" gorm:"default:event"`
	AllowPlainCommand  bool   `json:"allowPlainCommand"`
	DisableTriggers    bool   `json:"disableTriggers"`
	AutoRemoveMessages bool   `json:"autoRemoveMessages"`
}

const KeyboardDtoTypeInline = uint8(0)
const KeyboardDtoTypeReply = uint8(1)

type KeyboardButton struct {
	Slug string `json:"slug"`
	Name string `json:"name"`
}

type KeyboardInfo struct {
	Sender       string            `json:"sender"`
	Text         string            `json:"text"`
	SplitButtons int               `json:"splitButtons"`
	Buttons      []KeyboardButton  `json:"buttons"`
	Type         uint8             `json:"type"`
	Extra        map[string]string `json:"extra"`
}

type IBotHandler interface {
	Start(done chan bool)
	Stop()
	GetSlug() string
	GetBot() *Bot
	Send(sender string, content string, extra map[string]string) interface{}
	ShowKeyboard(data KeyboardInfo)
	RemoveKeyboard(sender string, text string, extra map[string]string)
	SendAction(sender string, action string)
	Edit(messageId string, content string) interface{}
	Delete(message CommonMessage)
	SendError(sender string, content string)
}

func (b *Bot) GetSlug() string {
	return fmt.Sprintf("%s_%s", b.Slug, b.Handler)
}
