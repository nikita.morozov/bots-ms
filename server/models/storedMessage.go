package models

import "gorm.io/gorm"

type StoredMessage struct {
	gorm.Model
	MessageId string `json:"messageId"`
	Text      string `json:"text"`
}
