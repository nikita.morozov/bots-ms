package models

type Connector struct {
	Id                 uint `json:"id" gorm:"primarykey"`
	FromScenarioAction uint `json:"fromScenarioAction" gorm:"index"`
	FromSocket         uint `json:"fromSocket"`
	ToScenarioAction   uint `json:"toScenarioAction" gorm:"index"`
	ToSocket           uint `json:"toSocket"`
	ScenarioId         uint `json:"scenario_id"`
}
