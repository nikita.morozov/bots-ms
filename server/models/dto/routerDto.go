package dto

type RouterDto struct {
	Items []RouterItem `json:"items"`
}

type RouterItem struct {
	Command    string `json:"command"`
	ScenarioId uint   `json:"scenarioId"`
}

func (r RouterDto) FindIndex(command string) int {
	for i, item := range r.Items {
		if item.Command == command {
			return i
		}
	}
	return -1
}
