package dto

type AuthRequestDto struct {
	BotSlug string `json:"botSlug"`
	Meta    string `json:"meta"`
	Hash    string `json:"hash"`
}
