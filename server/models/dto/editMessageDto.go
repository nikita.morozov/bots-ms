package dto

type EditDto struct {
	Prefix string `json:"prefix"`
	Suffix string `json:"suffix"`
}
