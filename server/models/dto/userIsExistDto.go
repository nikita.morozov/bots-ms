package dto

type UserIsExistDto struct {
	BotSlug string `json:"botSlug"`
	Meta    string `json:"meta"`
}
