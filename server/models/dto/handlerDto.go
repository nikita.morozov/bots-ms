package dto

type HandlerDto struct {
	Name    string `json:"name"`
	BotSlug string `json:"botSlug"`
	Data    string `json:"data"`
}
