package dto

type ExecuteEventDto struct {
	BotSlug string  `json:"botSlug"`
	Command string  `json:"command"`
	Content string  `json:"content"`
	Sender  *string `json:"sender"`
}
