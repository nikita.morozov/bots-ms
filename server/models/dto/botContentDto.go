package dto

import "bots-ms/models"

type BotContentDto struct {
	Context   string `json:"context"`
	Bot       models.Bot
	External  bool   `json:"external"`
	Sender    string `json:"sender"`
	Data      string `json:"data"`
	Event     string `json:"event"`
	MessageId string `json:"message_id"`
	Nickname  string `json:"nickname"`
	ReplayTo  string `json:"replayTo"`
	Command   string `json:"command"`
}

type BotContentWithScenarioDto struct {
	BotContentDto
	ScenarioId uint `json:"scenarioId"`
}
