package models

type TriggerData struct {
	ScenarioActionID uint   `json:"scenarioId"`
	EventName        string `json:"eventName"`
	ContentField     string `json:"contentField"`
}
