package models

import (
	"fmt"
	"gorm.io/gorm"
	"strconv"
	"strings"
)

type CommonMessage struct {
	gorm.Model
	MessageId *uint          `json:"message_id"`
	Message   *StoredMessage `json:"message"`
	Data      string         `json:"data"`
}

// MessageSig Telegram interface
func (x CommonMessage) MessageSig() (string, int64) {
	if len(x.Data) == 0 || !strings.Contains(x.Data, "_") {
		return "", int64(0)
	}

	var items = strings.Split(x.Data, "_")
	var ChatID int64
	ChatID, _ = strconv.ParseInt(items[1], 10, 64)

	return items[0], ChatID
}

func NewTelegramMessage(Message StoredMessage, messageID int, chatID int64) *CommonMessage {
	return &CommonMessage{
		Message:   &Message,
		MessageId: &Message.ID,
		Data:      fmt.Sprintf("%d_%d", messageID, chatID),
	}
}
