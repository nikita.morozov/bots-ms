package models

import "gorm.io/gorm"

type BotUser struct {
	gorm.Model
	BotID    uint   `json:"botId" gorm:"index:idx_name,unique"`
	Bot      Bot    `json:"bot"`
	UserID   string `json:"userId" gorm:"index:idx_name,unique"`
	Nickname string `json:"nickname"`
	Meta     string `json:"meta"`
	Token    string `json:"token"`
	Blocked  bool   `json:"blocked"`
}
