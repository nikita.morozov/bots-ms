package models

import (
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"gorm.io/gorm"
)

type Migration interface {
	Migrate() error
}

type gormMigration struct {
	DB *gorm.DB
}

func (g gormMigration) Migrate() error {
	err := g.DB.Migrator().AutoMigrate(&Bot{})
	if err != nil {
		return err
	}

	err = g.DB.Migrator().AutoMigrate(&BotUser{})
	if err != nil {
		return err
	}

	err = g.DB.Migrator().AutoMigrate(&coreModel.Action{})
	if err != nil {
		return err
	}

	err = g.DB.Migrator().AutoMigrate(&ScenarioAction{})
	if err != nil {
		return err
	}

	err = g.DB.Migrator().AutoMigrate(&Scenario{})
	if err != nil {
		return err
	}

	err = g.DB.Migrator().AutoMigrate(&Event{})
	if err != nil {
		return err
	}

	err = g.DB.Migrator().AutoMigrate(&StoredMessage{})
	if err != nil {
		return err
	}

	err = g.DB.Migrator().AutoMigrate(&CommonMessage{})
	if err != nil {
		return err
	}

	err = g.DB.Migrator().AutoMigrate(&AuthRequest{})
	if err != nil {
		return err
	}

	err = g.DB.Migrator().AutoMigrate(&coreModel.Socket{})
	if err != nil {
		return err
	}

	err = g.DB.Migrator().AutoMigrate(&Connector{})
	if err != nil {
		return err
	}

	err = g.DB.Migrator().AutoMigrate(&ScenarioItem{})
	if err != nil {
		return err
	}

	err = g.DB.Migrator().AutoMigrate(&StateTransition{})
	if err != nil {
		return err
	}

	err = g.DB.Migrator().AutoMigrate(&State{})
	if err != nil {
		return err
	}

	err = g.DB.Migrator().AutoMigrate(&StateMachine{})
	if err != nil {
		return err
	}

	return err
}

func NewGormMigration(DB *gorm.DB) Migration {
	return &gormMigration{
		DB: DB,
	}
}
