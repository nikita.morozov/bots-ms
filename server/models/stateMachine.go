package models

type StateMachine struct {
	ID           uint   `json:"id" gorm:"primarykey"`
	Name         string `json:"name"`
	BotID        uint   `json:"botId"`
	Bot          Bot    `json:"bot"`
	StartStateId uint   `json:"startState"`
	Meta         string `json:"meta"`
}

type State struct {
	ID             uint              `json:"id" gorm:"primarykey"`
	Name           string            `json:"name"`
	ScenarioID     uint              `json:"scenarioId"`
	Scenario       Scenario          `json:"scenario"`
	StateMachineID uint              `json:"stateMachineId"`
	StateMachine   StateMachine      `json:"stateMachine"`
	Transitions    []StateTransition `json:"transition"`
	Meta           string            `json:"meta"`
}

func (e *State) GetTransition(trigger string) *StateTransition {
	for _, t := range e.Transitions {
		if t.Trigger == trigger {
			return &t
		}
	}

	return nil
}

type StateTransition struct {
	ID        uint   `json:"id" gorm:"primarykey"`
	StateID   uint   `json:"stateId"`
	State     *State `json:"state"`
	Trigger   string `json:"trigger"`
	ToStateID *uint  `json:"toStateId"`
	ToState   *State `json:"toState"`
	Meta      string `json:"meta"`
}

type StateMachineExport struct {
	StateMachine StateMachine `json:"stateMachine"`
	States       *[]State     `json:"states"`
	Sceneries    []Scenario   `json:"sceneries"`
	Connectors   []Connector  `json:"connectors"`
}
