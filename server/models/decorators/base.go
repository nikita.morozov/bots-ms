package decorators

import coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"

type Decorator interface {
	HandleBefore(action *coreModel.Action, botSlug string, data interface{}) error
	HandleAfter() error
}
