package models

type ScenarioItem struct {
	Id         uint   `json:"id" gorm:"primarykey"`
	Type       string `json:"type"`
	Meta       string `json:"meta"`
	ScenarioId uint   `json:"scenarioId" gorm:"index"`
}
