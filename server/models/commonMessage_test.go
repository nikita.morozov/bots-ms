package models

import (
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
	"testing"
)

func TestMessage_Sig(t *testing.T) {
	message := StoredMessage{
		Text: "Text",
	}

	m := CommonMessage{
		MessageId: &message.ID,
		Message:   &message,
		Data:      "abc_190",
	}

	val1, val2 := m.MessageSig()
	assert.Equal(t, val1, "abc")
	assert.Equal(t, val2, int64(190))
}

func TestMessage_Sig_Empty(t *testing.T) {
	message := StoredMessage{
		Text: "Text",
	}

	m := CommonMessage{
		MessageId: &message.ID,
		Message:   &message,
		Data:      "",
	}

	val1, val2 := m.MessageSig()
	assert.Equal(t, val1, "")
	assert.Equal(t, val2, int64(0))
}

func TestMessage_Sig_Invalid(t *testing.T) {
	message := StoredMessage{
		Text: "Text",
	}

	m := CommonMessage{
		MessageId: &message.ID,
		Message:   &message,
		Data:      "abc-190",
	}

	val1, val2 := m.MessageSig()
	assert.Equal(t, val1, "")
	assert.Equal(t, val2, int64(0))
}

func TestMessage_Constructor(t *testing.T) {
	storage := StoredMessage{
		Model: gorm.Model{
			ID: uint(12),
		},
		Text: "Text",
	}

	m := NewTelegramMessage(storage, 10, int64(12))
	assert.Equal(t, *m.MessageId, uint(12))
	assert.Equal(t, m.Data, "10_12")
}
