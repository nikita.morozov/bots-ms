package models

import "gorm.io/gorm"

type Event struct {
	gorm.Model
	BotID      uint     `json:"botId"`
	Bot        Bot      `json:"bot"`
	ScenarioID uint     `json:"scenarioId"`
	Scenario   Scenario `json:"scenario"`
	Command    string   `json:"command"`
}
