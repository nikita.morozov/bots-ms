package models

import (
	"gorm.io/gorm"
)

type AuthRequest struct {
	gorm.Model
	BotID uint   `json:"botId" gorm:"index:auth_name,unique"`
	Bot   Bot    `json:"bot"`
	Meta  string `json:"meta" gorm:"index:auth_name,unique"`
	Hash  string `json:"hash"`
}
