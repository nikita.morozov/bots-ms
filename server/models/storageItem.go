package models

type StorageItem struct {
	Type uint8  `json:"type"`
	Data []byte `json:"data"`
}
