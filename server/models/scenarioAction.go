package models

type ScenarioAction struct {
	Id         uint   `json:"id" gorm:"primarykey"`
	Meta       string `json:"meta"`
	ScenarioId uint   `json:"scenarioId" gorm:"index"`
	ActionId   uint   `json:"actionId"`
	Data       string `json:"data"`
}
