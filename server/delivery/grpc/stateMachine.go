package grpc

import (
	"bots-ms/models"
	"bots-ms/proto"
	"bots-ms/tools"
	"bots-ms/usecases"
	"context"
	sharedProto "gitlab.com/nikita.morozov/ms-shared/proto"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
)

type stateMachineServer struct {
	proto.UnimplementedStateMachineServiceServer
	useCase usecases.StateMachineUsecase
}

func (h *stateMachineServer) Create(ctx context.Context, in *proto.StateMachine) (*sharedProto.IdRequest, error) {
	model := models.StateMachine{
		Name:         in.Name,
		BotID:        uint(in.BotId),
		StartStateId: uint(in.StartStateId),
		Meta:         in.Meta,
	}

	err := h.useCase.Create(&model)
	if err != nil {
		return nil, err
	}

	return &sharedProto.IdRequest{Id: uint32(model.ID)}, nil
}

func (h *stateMachineServer) Update(ctx context.Context, in *proto.StateMachine) (*emptypb.Empty, error) {
	model := FromStateMachine(in)

	err := h.useCase.Update(model)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

func (h *stateMachineServer) Delete(ctx context.Context, in *sharedProto.IdRequest) (*sharedProto.BoolValue, error) {
	err := h.useCase.Delete(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return &sharedProto.BoolValue{Value: true}, nil
}

func FromStateMachine(in *proto.StateMachine) *models.StateMachine {
	return &models.StateMachine{
		ID:           uint(in.Id),
		Name:         in.Name,
		BotID:        uint(in.BotId),
		StartStateId: uint(in.StartStateId),
		Meta:         in.Meta,
	}
}

func NewStateMachine(item *models.StateMachine) *proto.StateMachine {
	return &proto.StateMachine{
		Id:           uint32(item.ID),
		Name:         item.Name,
		BotId:        uint32(item.BotID),
		StartStateId: uint32(item.StartStateId),
		Meta:         item.Meta,
	}
}

func (h *stateMachineServer) mapItems(in *[]models.StateMachine) []*proto.StateMachine {
	items := make([]*proto.StateMachine, len(*in))

	for index, item := range *in {
		items[index] = NewStateMachine(&item)
	}

	return items
}

func (h *stateMachineServer) Get(ctx context.Context, in *sharedProto.IdRequest) (*proto.StateMachine, error) {
	item, err := h.useCase.Get(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return NewStateMachine(item), nil
}

func (h *stateMachineServer) Export(ctx context.Context, in *sharedProto.IdRequest) (*proto.StateMachineExport, error) {
	item, err := h.useCase.Export(uint(in.Id))
	if err != nil {
		return nil, err
	}

	states := make([]*proto.State, len(*item.States))
	for i, state := range *item.States {
		states[i] = NewState(&state)
	}

	sceneries := make([]*proto.ScenarioModel, len(item.Sceneries))
	for i, scenario := range item.Sceneries {
		sceneries[i] = NewScenario(&scenario)
	}

	connectors := make([]*proto.ConnectorModel, len(item.Connectors))
	for i, connector := range item.Connectors {
		connectors[i] = NewConnectorModel(&connector)
	}

	return &proto.StateMachineExport{
		StateMachine: NewStateMachine(&item.StateMachine),
		States:       states,
		Scenarios:    sceneries,
		Connectors:   connectors,
	}, nil
}

func (h *stateMachineServer) List(ctx context.Context, in *sharedProto.ListOptions) (*proto.StateMachineResp, error) {
	list, paginator, err := h.useCase.List(tools.ConvertOpts(in))
	if err != nil {
		return nil, err
	}

	return &proto.StateMachineResp{
		Items:     h.mapItems(list),
		Paginator: tools.ConvertPaginator(paginator),
	}, nil
}

func (h *stateMachineServer) Count(ctx context.Context, req *emptypb.Empty) (*sharedProto.UInt64Value, error) {
	count, err := h.useCase.Count()
	return &sharedProto.UInt64Value{Value: count}, err
}

func NewStateMachineGrpcHandler(s *grpc.Server, useCase usecases.StateMachineUsecase) {
	proto.RegisterStateMachineServiceServer(s, &stateMachineServer{useCase: useCase})
}
