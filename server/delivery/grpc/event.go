package grpc

import (
	"bots-ms/models"
	"bots-ms/proto"
	"bots-ms/tools"
	"bots-ms/usecases"
	"context"
	sharedProto "gitlab.com/nikita.morozov/ms-shared/proto"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
	"gorm.io/gorm"
)

type eventServer struct {
	proto.UnimplementedEventServer
	useCase        usecases.EventUsecase
	scenarioEngine usecases.ScenarioEngine
}

func (h *eventServer) Create(ctx context.Context, in *proto.EventModel) (*sharedProto.IdRequest, error) {
	model := models.Event{
		BotID:      uint(in.BotId),
		ScenarioID: uint(in.ScenarioId),
		Command:    in.Command,
	}

	err := h.useCase.Create(&model)
	if err != nil {
		return nil, err
	}

	return &sharedProto.IdRequest{Id: uint32(model.ID)}, nil
}

func (h *eventServer) Update(ctx context.Context, in *proto.EventModel) (*emptypb.Empty, error) {
	model := models.Event{
		Model: gorm.Model{
			ID: uint(in.Id),
		},
		BotID:      uint(in.BotId),
		ScenarioID: uint(in.ScenarioId),
		Command:    in.Command,
	}

	err := h.useCase.Update(&model)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

func (h *eventServer) Delete(ctx context.Context, in *sharedProto.IdRequest) (*sharedProto.BoolValue, error) {
	err := h.useCase.Delete(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return &sharedProto.BoolValue{Value: true}, nil
}

func newEvent(item *models.Event) *proto.EventModel {
	return &proto.EventModel{
		Id:         uint32(item.ID),
		CreatedAt:  item.CreatedAt.Unix(),
		UpdatedAt:  item.UpdatedAt.Unix(),
		BotId:      uint32(item.BotID),
		ScenarioId: uint32(item.ScenarioID),
		Command:    item.Command,
	}
}

func (h *eventServer) Get(ctx context.Context, in *sharedProto.IdRequest) (*proto.EventModel, error) {
	item, err := h.useCase.Get(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return newEvent(item), nil
}

func (h *eventServer) Execute(ctx context.Context, in *proto.ExecuteReq) (*emptypb.Empty, error) {
	//h.scenarioEngine.Execute()
	//_, err := h.useCase.Execute(in.Name, in.BotSlug, in.Data)
	//if err != nil {
	//	return nil, err
	//}

	return &emptypb.Empty{}, nil
}

func (h *eventServer) mapItems(in *[]models.Event) []*proto.EventModel {
	items := make([]*proto.EventModel, len(*in))

	for index, item := range *in {
		items[index] = newEvent(&item)
	}

	return items
}

func (h *eventServer) Count(ctx context.Context, req *emptypb.Empty) (*sharedProto.UInt64Value, error) {
	count, err := h.useCase.Count()
	return &sharedProto.UInt64Value{Value: count}, err
}

func (h *eventServer) List(ctx context.Context, in *sharedProto.ListOptions) (*proto.EvenListResp, error) {
	list, paginator, err := h.useCase.List(tools.ConvertOpts(in))
	if err != nil {
		return nil, err
	}

	return &proto.EvenListResp{
		Items:     h.mapItems(list),
		Paginator: tools.ConvertPaginator(paginator),
	}, nil
}

func NewEventGrpcHandler(s *grpc.Server, useCase usecases.EventUsecase, scenarioEngine usecases.ScenarioEngine) {
	proto.RegisterEventServer(s, &eventServer{useCase: useCase, scenarioEngine: scenarioEngine})
}
