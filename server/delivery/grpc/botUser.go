package grpc

import (
	"bots-ms/models"
	"bots-ms/proto"
	"bots-ms/tools"
	"bots-ms/usecases"
	"context"
	sharedProto "gitlab.com/nikita.morozov/ms-shared/proto"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
	"gorm.io/gorm"
)

type botUserServer struct {
	proto.UnimplementedBotUserServer
	useCase usecases.BotUserUsecase
}

func (h *botUserServer) Create(ctx context.Context, in *proto.BotUserModel) (*sharedProto.IdRequest, error) {
	model := models.BotUser{
		BotID:  uint(in.BotId),
		UserID: in.UserId,
		Meta:   in.Meta,
	}

	err := h.useCase.Create(&model)
	if err != nil {
		return nil, err
	}

	return &sharedProto.IdRequest{Id: uint32(model.ID)}, err
}

func (h *botUserServer) Update(ctx context.Context, in *proto.BotUserModel) (*emptypb.Empty, error) {
	model := models.BotUser{
		Model: gorm.Model{
			ID: uint(in.Id),
		},
		BotID:  uint(in.BotId),
		UserID: in.UserId,
		Meta:   in.Meta,
	}

	err := h.useCase.Update(&model)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

func (h *botUserServer) Delete(ctx context.Context, in *sharedProto.IdRequest) (*sharedProto.BoolValue, error) {
	err := h.useCase.Delete(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return &sharedProto.BoolValue{Value: true}, nil
}

func newBotUser(item *models.BotUser) *proto.BotUserModel {
	return &proto.BotUserModel{
		Id:        uint32(item.ID),
		CreatedAt: item.CreatedAt.Unix(),
		UpdatedAt: item.UpdatedAt.Unix(),
		BotId:     uint32(item.BotID),
		UserId:    item.UserID,
		Meta:      item.Meta,
	}
}

func (h *botUserServer) Get(ctx context.Context, in *sharedProto.IdRequest) (*proto.BotUserModel, error) {
	item, err := h.useCase.Get(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return newBotUser(item), nil
}

func (h *botUserServer) mapItems(in *[]models.BotUser) []*proto.BotUserModel {
	items := make([]*proto.BotUserModel, len(*in))

	for index, item := range *in {
		items[index] = newBotUser(&item)
	}

	return items
}

func (h *botUserServer) Count(ctx context.Context, req *emptypb.Empty) (*sharedProto.UInt64Value, error) {
	count, err := h.useCase.Count()
	return &sharedProto.UInt64Value{Value: count}, err
}

func (h *botUserServer) List(ctx context.Context, in *proto.BotUserList) (*proto.BotUserListResp, error) {
	list, paginator, err := h.useCase.List(in.Handler, tools.ConvertOpts(in.Opts))
	if err != nil {
		return nil, err
	}

	return &proto.BotUserListResp{
		Items:     h.mapItems(list),
		Paginator: tools.ConvertPaginator(paginator),
	}, nil
}

func NewBotUserGrpcHandler(s *grpc.Server, useCase usecases.BotUserUsecase) {
	proto.RegisterBotUserServer(s, &botUserServer{useCase: useCase})
}
