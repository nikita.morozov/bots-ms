package grpc

import (
	"bots-ms/models"
	"bots-ms/proto"
	"bots-ms/usecases"
	"context"
	sharedProto "gitlab.com/nikita.morozov/ms-shared/proto"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
)

type stateTransitionServer struct {
	proto.UnimplementedTransitionServiceServer
	useCase usecases.StateTransitionUsecase
}

func (h *stateTransitionServer) Create(ctx context.Context, in *proto.Transition) (*sharedProto.IdRequest, error) {
	var ToStateId *uint
	if in.ToStateId != nil {
		val := uint(*in.ToStateId)
		ToStateId = &val
	}

	model := models.StateTransition{
		StateID:   uint(in.StateId),
		Trigger:   in.Trigger,
		ToStateID: ToStateId,
		Meta:      in.Meta,
	}

	err := h.useCase.Create(&model)
	if err != nil {
		return nil, err
	}

	return &sharedProto.IdRequest{Id: uint32(model.ID)}, nil
}

func (h *stateTransitionServer) Update(ctx context.Context, in *proto.Transition) (*emptypb.Empty, error) {
	var ToStateId *uint
	if in.ToStateId != nil {
		val := uint(*in.ToStateId)
		ToStateId = &val
	}

	model := models.StateTransition{
		ID:        uint(in.Id),
		StateID:   uint(in.StateId),
		Trigger:   in.Trigger,
		ToStateID: ToStateId,
		Meta:      in.Meta,
	}

	err := h.useCase.Update(&model)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

func (h *stateTransitionServer) Delete(ctx context.Context, in *sharedProto.IdRequest) (*sharedProto.BoolValue, error) {
	err := h.useCase.Delete(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return &sharedProto.BoolValue{Value: true}, nil
}

func newStateTransition(item *models.StateTransition) *proto.Transition {
	var ToStateId *uint32
	if item.ToStateID != nil {
		val := uint32(*item.ToStateID)
		ToStateId = &val
	}

	return &proto.Transition{
		Id:        uint32(item.ID),
		StateId:   uint32(item.StateID),
		Trigger:   item.Trigger,
		ToStateId: ToStateId,
		Meta:      item.Meta,
	}
}

func (h *stateTransitionServer) Get(ctx context.Context, in *sharedProto.IdRequest) (*proto.Transition, error) {
	item, err := h.useCase.Get(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return newStateTransition(item), nil
}

func (h *stateTransitionServer) List(in *sharedProto.IdRequest, listStream proto.TransitionService_ListServer) error {
	list, err := h.useCase.List(uint(in.Id))
	if err != nil {
		return err
	}

	for _, item := range *list {
		err = listStream.Send(newStateTransition(&item))
		if err != nil {
			return err
		}
	}

	return nil
}

func NewStateTransitionGrpcHandler(s *grpc.Server, useCase usecases.StateTransitionUsecase) {
	proto.RegisterTransitionServiceServer(s, &stateTransitionServer{useCase: useCase})
}
