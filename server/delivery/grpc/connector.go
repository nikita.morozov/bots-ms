package grpc

import (
	"bots-ms/models"
	"bots-ms/proto"
	"bots-ms/usecases"
	"context"
	sharedProto "gitlab.com/nikita.morozov/ms-shared/proto"
	"google.golang.org/grpc"
)

type connectorServer struct {
	proto.UnimplementedConnectorServer
	useCase usecases.ConnectorUsecase
}

func (h *connectorServer) Create(ctx context.Context, in *proto.ConnectorModel) (*sharedProto.IdRequest, error) {
	model := FromConnectionModel(in)

	err := h.useCase.Create(model)
	if err != nil {
		return nil, err
	}

	return &sharedProto.IdRequest{Id: uint32(model.Id)}, nil
}

func (h *connectorServer) Delete(ctx context.Context, in *sharedProto.IdRequest) (*sharedProto.BoolValue, error) {
	err := h.useCase.Delete(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return &sharedProto.BoolValue{Value: true}, nil
}

func NewConnectorModel(item *models.Connector) *proto.ConnectorModel {
	return &proto.ConnectorModel{
		Id:                 uint32(item.Id),
		FromScenarioAction: uint32(item.FromScenarioAction),
		FromSocket:         uint32(item.FromSocket),
		ToScenarioAction:   uint32(item.ToScenarioAction),
		ToSocket:           uint32(item.ToSocket),
		ScenarioId:         uint32(item.ScenarioId),
	}
}

func FromConnectionModel(in *proto.ConnectorModel) *models.Connector {
	return &models.Connector{
		Id:                 uint(in.Id),
		FromScenarioAction: uint(in.FromScenarioAction),
		FromSocket:         uint(in.FromSocket),
		ToScenarioAction:   uint(in.ToScenarioAction),
		ToSocket:           uint(in.ToSocket),
		ScenarioId:         uint(in.ScenarioId),
	}
}

func (h *connectorServer) Update(ctx context.Context, in *proto.ConnectorModel) (*proto.ConnectorModel, error) {
	item := FromConnectionModel(in)

	err := h.useCase.Update(item)
	if err != nil {
		return nil, err
	}

	return NewConnectorModel(item), nil
}

func (h *connectorServer) Get(ctx context.Context, in *sharedProto.IdRequest) (*proto.ConnectorModel, error) {
	item, err := h.useCase.GetById(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return NewConnectorModel(item), nil
}

func (h *connectorServer) List(in *proto.ConnectorReq, listStream proto.Connector_ListServer) error {
	list, err := h.useCase.List(uint(in.ScenarioId))
	if err != nil {
		return err
	}

	for _, item := range *list {
		err = listStream.Send(NewConnectorModel(&item))
		if err != nil {
			return err
		}
	}

	return nil
}

func NewConnectorGrpcHandler(s *grpc.Server, useCase usecases.ConnectorUsecase) {
	proto.RegisterConnectorServer(s, &connectorServer{useCase: useCase})
}
