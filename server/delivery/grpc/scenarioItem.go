package grpc

import (
	"bots-ms/models"
	"bots-ms/proto"
	"bots-ms/usecases"
	"context"
	sharedProto "gitlab.com/nikita.morozov/ms-shared/proto"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
)

type scenarioItemServer struct {
	proto.UnimplementedScenarioItemServer
	useCase usecases.ScenarioItemUsecase
}

func (h *scenarioItemServer) Create(ctx context.Context, in *proto.ScenarioItemModel) (*sharedProto.IdRequest, error) {
	model := models.ScenarioItem{
		Type:       in.Type,
		Meta:       in.Meta,
		ScenarioId: uint(in.ScenarioId),
	}

	err := h.useCase.Create(&model)
	if err != nil {
		return nil, err
	}

	return &sharedProto.IdRequest{Id: uint32(model.Id)}, nil
}

func (h *scenarioItemServer) Update(ctx context.Context, in *proto.ScenarioItemModel) (*emptypb.Empty, error) {
	model := models.ScenarioItem{
		Id:         uint(in.Id),
		Type:       in.Type,
		Meta:       in.Meta,
		ScenarioId: uint(in.ScenarioId),
	}

	err := h.useCase.Update(&model)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

func (h *scenarioItemServer) BatchUpdate(ctx context.Context, in *proto.ScenarioItemBatch) (*emptypb.Empty, error) {
	var items []models.ScenarioItem
	for _, in := range in.Items {
		items = append(items, models.ScenarioItem{
			Id:         uint(in.Id),
			Type:       in.Type,
			Meta:       in.Meta,
			ScenarioId: uint(in.ScenarioId),
		})
	}

	err := h.useCase.BatchUpdate(&items)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

func (h *scenarioItemServer) Delete(ctx context.Context, in *sharedProto.IdRequest) (*sharedProto.BoolValue, error) {
	err := h.useCase.Delete(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return &sharedProto.BoolValue{Value: true}, nil
}

func newScenarioItemModel(item *models.ScenarioItem) *proto.ScenarioItemModel {
	return &proto.ScenarioItemModel{
		Id:         uint32(item.Id),
		Type:       item.Type,
		Meta:       item.Meta,
		ScenarioId: uint32(item.ScenarioId),
	}
}

func (h *scenarioItemServer) Get(ctx context.Context, in *sharedProto.IdRequest) (*proto.ScenarioItemModel, error) {
	item, err := h.useCase.Get(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return newScenarioItemModel(item), nil
}

func (h *scenarioItemServer) List(in *sharedProto.IdRequest, listStream proto.ScenarioItem_ListServer) error {
	list, err := h.useCase.List(uint(in.Id))
	if err != nil {
		return err
	}

	for _, item := range *list {
		err = listStream.Send(newScenarioItemModel(&item))
		if err != nil {
			return err
		}
	}

	return nil
}

func NewScenarioItemGrpcHandler(s *grpc.Server, useCase usecases.ScenarioItemUsecase) {
	proto.RegisterScenarioItemServer(s, &scenarioItemServer{useCase: useCase})
}
