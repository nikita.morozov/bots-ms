package grpc

import (
	"bots-ms/proto"
	"bots-ms/usecases"
	"context"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	sharedProto "gitlab.com/nikita.morozov/ms-shared/proto"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
)

type actionServer struct {
	proto.UnimplementedActionServer
	useCase usecases.ActionUsecase
}

func (h *actionServer) Create(ctx context.Context, in *proto.ActionModel) (*sharedProto.IdRequest, error) {
	model := coreModel.Action{
		Name:    in.Name,
		Handler: in.Handler,
		Scheme:  in.Scheme,
		Sockets: h.mapSockets(in.Sockets),
	}

	err := h.useCase.Create(&model)
	if err != nil {
		return nil, err
	}

	return &sharedProto.IdRequest{Id: uint32(model.Id)}, nil
}

func (h *actionServer) Update(ctx context.Context, in *proto.ActionModel) (*emptypb.Empty, error) {
	model := coreModel.Action{
		Id:      uint(in.Id),
		Name:    in.Name,
		Handler: in.Handler,
		Scheme:  in.Scheme,
		Sockets: h.mapSockets(in.Sockets),
	}

	err := h.useCase.Update(&model)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

func (h *actionServer) Delete(ctx context.Context, in *sharedProto.IdRequest) (*sharedProto.BoolValue, error) {
	err := h.useCase.Delete(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return &sharedProto.BoolValue{Value: true}, nil
}

func (h *actionServer) Get(ctx context.Context, in *sharedProto.IdRequest) (*proto.ActionModel, error) {
	item, err := h.useCase.Get(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return &proto.ActionModel{
		Id:      uint32(item.Id),
		Name:    item.Name,
		Handler: item.Handler,
		Scheme:  item.Scheme,
		Sockets: h.mapToSockets(item.Sockets),
	}, nil
}

func (h *actionServer) List(_ *emptypb.Empty, listStream proto.Action_ListServer) error {
	list, err := h.useCase.List()
	if err != nil {
		return err
	}

	for _, item := range *list {
		err = listStream.Send(&proto.ActionModel{
			Id:      uint32(item.Id),
			Name:    item.Name,
			Handler: item.Handler,
			Scheme:  item.Scheme,
			Sockets: h.mapToSockets(item.Sockets),
		})
		if err != nil {
			return err
		}
	}

	return nil
}

func (h *actionServer) Count(ctx context.Context, req *emptypb.Empty) (*sharedProto.UInt64Value, error) {
	count, err := h.useCase.Count()

	return &sharedProto.UInt64Value{
		Value: count,
	}, err
}

func (h *actionServer) mapSockets(in []*proto.SocketModel) []coreModel.Socket {
	sockets := make([]coreModel.Socket, len(in))

	for index, item := range in {
		sockets[index] = coreModel.Socket{
			Id:        uint(item.Id),
			Direction: uint8(item.Direction),
			Type:      uint8(item.Type),
			Title:     item.Title,
			Slug:      item.Slug,
			ActionId:  uint(item.ActionId),
			Manual:    item.Manual,
		}
	}

	return sockets
}

func (h *actionServer) mapToSockets(in []coreModel.Socket) []*proto.SocketModel {
	sockets := make([]*proto.SocketModel, len(in))

	for index, item := range in {
		sockets[index] = &proto.SocketModel{
			Id:        uint32(item.Id),
			Direction: uint32(item.Direction),
			Type:      uint32(item.Type),
			Title:     item.Title,
			Slug:      item.Slug,
			ActionId:  uint32(item.ActionId),
			Manual:    item.Manual,
		}
	}

	return sockets
}

func NewActionGrpcHandler(s *grpc.Server, useCase usecases.ActionUsecase) {
	proto.RegisterActionServer(s, &actionServer{useCase: useCase})
}
