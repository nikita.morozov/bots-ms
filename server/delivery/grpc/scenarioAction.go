package grpc

import (
	"bots-ms/models"
	"bots-ms/proto"
	"bots-ms/usecases"
	"context"
	models2 "gitlab.com/nikita.morozov/ms-shared/models"
	sharedProto "gitlab.com/nikita.morozov/ms-shared/proto"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
)

type scenarioActionServer struct {
	proto.UnimplementedScenarioActionServer
	useCase usecases.ScenarioAcrionUsecase
}

func (h *scenarioActionServer) Create(ctx context.Context, in *proto.ScenarioActionModel) (*sharedProto.IdRequest, error) {
	model := models.ScenarioAction{
		Meta:       in.Meta,
		ScenarioId: uint(in.ScenarioId),
		ActionId:   uint(in.ActionId),
		Data:       in.Data,
	}

	err := h.useCase.Create(&model)
	if err != nil {
		return nil, err
	}

	return &sharedProto.IdRequest{Id: uint32(model.Id)}, nil
}

func (h *scenarioActionServer) Update(ctx context.Context, in *proto.ScenarioActionModel) (*emptypb.Empty, error) {
	model := models.ScenarioAction{
		Id:         uint(in.Id),
		Meta:       in.Meta,
		ScenarioId: uint(in.ScenarioId),
		ActionId:   uint(in.ActionId),
		Data:       in.Data,
	}

	err := h.useCase.Update(&model)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

func (h *scenarioActionServer) BatchUpdate(ctx context.Context, in *proto.ScenarioActionBatch) (*emptypb.Empty, error) {
	var items []models.ScenarioAction
	for _, in := range in.Items {
		items = append(items, models.ScenarioAction{
			Id:         uint(in.Id),
			Meta:       in.Meta,
			ScenarioId: uint(in.ScenarioId),
			ActionId:   uint(in.ActionId),
			Data:       in.Data,
		})
	}

	err := h.useCase.BatchUpdate(&items)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

func (h *scenarioActionServer) Delete(ctx context.Context, in *sharedProto.IdRequest) (*sharedProto.BoolValue, error) {
	err := h.useCase.Delete(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return &sharedProto.BoolValue{Value: true}, nil
}

func newScenarioActionModel(item *models.ScenarioAction) *proto.ScenarioActionModel {
	return &proto.ScenarioActionModel{
		Id:         uint32(item.Id),
		Meta:       item.Meta,
		Data:       item.Data,
		ScenarioId: uint32(item.ScenarioId),
		ActionId:   uint32(item.ActionId),
	}
}

func (h *scenarioActionServer) Get(ctx context.Context, in *sharedProto.IdRequest) (*proto.ScenarioActionModel, error) {
	item, err := h.useCase.Get(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return newScenarioActionModel(item), nil
}

func (h *scenarioActionServer) List(in *proto.ScenarioActionReq, listStream proto.ScenarioAction_ListServer) error {
	var opt *models2.ListOptions
	if in.Opt != nil {
		opt = &models2.ListOptions{
			Offset: int(in.Opt.Offset),
			Limit:  int(in.Opt.Limit),
			Order:  in.Opt.Order,
		}
	}

	list, err := h.useCase.List(uint(in.ScenarioId), opt)
	if err != nil {
		return err
	}

	for _, item := range *list {
		err = listStream.Send(newScenarioActionModel(&item))
		if err != nil {
			return err
		}
	}

	return nil
}

func NewScenarioActionGrpcHandler(s *grpc.Server, useCase usecases.ScenarioAcrionUsecase) {
	proto.RegisterScenarioActionServer(s, &scenarioActionServer{useCase: useCase})
}
