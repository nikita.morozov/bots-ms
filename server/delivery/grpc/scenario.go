package grpc

import (
	"bots-ms/models"
	"bots-ms/proto"
	"bots-ms/tools"
	"bots-ms/usecases"
	"context"
	sharedProto "gitlab.com/nikita.morozov/ms-shared/proto"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
	"gorm.io/gorm"
)

type scenarioServer struct {
	proto.UnimplementedScenarioServer
	useCase usecases.ScenarioUsecase
}

func (h *scenarioServer) Create(_ context.Context, in *proto.ScenarioModel) (*sharedProto.IdRequest, error) {
	model := FromScenario(in)

	err := h.useCase.Create(model)
	if err != nil {
		return nil, err
	}

	return &sharedProto.IdRequest{Id: uint32(model.ID)}, nil
}

func (h *scenarioServer) Update(_ context.Context, in *proto.ScenarioModel) (*emptypb.Empty, error) {
	actions := make([]models.ScenarioAction, len(in.Actions))
	for i, item := range in.Actions {
		actions[i] = *NewScenarioAction(item)
	}

	var initial *uint
	if in.Initial != nil {
		tmp := uint(*in.Initial)
		initial = &tmp
	}

	model := models.Scenario{
		Model: gorm.Model{
			ID: uint(in.Id),
		},
		Title:           in.Title,
		Category:        in.Category,
		InitialActionId: initial,
		Actions:         actions,
	}

	err := h.useCase.Update(&model)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

func (h *scenarioServer) Delete(_ context.Context, in *sharedProto.IdRequest) (*sharedProto.BoolValue, error) {
	err := h.useCase.Delete(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return &sharedProto.BoolValue{Value: true}, nil
}

func FromScenario(in *proto.ScenarioModel) *models.Scenario {
	actions := make([]models.ScenarioAction, len(in.Actions))
	for i, item := range in.Actions {
		actions[i] = *NewScenarioAction(item)
	}
	var initial *uint
	if in.Initial != nil {
		tmp := uint(*in.Initial)
		initial = &tmp
	}
	return &models.Scenario{
		Model: gorm.Model{
			ID: uint(in.Id),
		},
		Title:           in.Title,
		Category:        in.Category,
		Actions:         actions,
		InitialActionId: initial,
	}
}

func NewScenarioAction(item *proto.ScenarioActionModel) *models.ScenarioAction {
	meta := item.Meta
	data := item.Data

	return &models.ScenarioAction{
		Id:         uint(item.Id),
		Meta:       meta,
		Data:       data,
		ScenarioId: uint(item.ScenarioId),
		ActionId:   uint(item.ActionId),
	}
}

func NewScenario(item *models.Scenario) *proto.ScenarioModel {
	actions := make([]*proto.ScenarioActionModel, len(item.Actions))
	for i, item := range item.Actions {
		actions[i] = newScenarioActionModel(&item)
	}

	var initial *uint32
	if item.InitialActionId != nil {
		tmp := uint32(*item.InitialActionId)
		initial = &tmp
	}

	return &proto.ScenarioModel{
		Id:        uint32(item.ID),
		CreatedAt: item.CreatedAt.Unix(),
		UpdatedAt: item.CreatedAt.Unix(),
		Title:     item.Title,
		Category:  item.Category,
		Initial:   initial,
		Actions:   actions,
	}
}

func (h *scenarioServer) Get(_ context.Context, in *sharedProto.IdRequest) (*proto.ScenarioModel, error) {
	item, err := h.useCase.Get(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return NewScenario(item), nil
}

func (h *scenarioServer) SetInitialActionId(_ context.Context, in *proto.ScenarioInitialRequest) (*emptypb.Empty, error) {
	item, err := h.useCase.Get(uint(in.ScenarioId))
	if err != nil {
		return nil, err
	}

	tmp := uint(in.ActionId)
	initial := &tmp

	item.InitialActionId = initial
	err = h.useCase.Update(item)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

func (h *scenarioServer) mapItems(in *[]models.Scenario) []*proto.ScenarioModel {
	items := make([]*proto.ScenarioModel, len(*in))

	for index, item := range *in {
		items[index] = NewScenario(&item)
	}

	return items
}

func (h *scenarioServer) List(ctx context.Context, in *proto.ScenarioListReq) (*proto.ScenarioModelListResp, error) {
	list, paginator, err := h.useCase.List(tools.ConvertOpts(in.Opts))
	if err != nil {
		return nil, err
	}

	return &proto.ScenarioModelListResp{
		Items:     h.mapItems(list),
		Paginator: tools.ConvertPaginator(paginator),
	}, nil
}

func (h *scenarioServer) Count(ctx context.Context, req *emptypb.Empty) (*sharedProto.UInt64Value, error) {
	count, err := h.useCase.Count()
	return &sharedProto.UInt64Value{Value: count}, err
}

func NewScenarioGrpcHandler(s *grpc.Server, useCase usecases.ScenarioUsecase) {
	proto.RegisterScenarioServer(s, &scenarioServer{useCase: useCase})
}
