package grpc

import (
	"bots-ms/models"
	"bots-ms/proto"
	"bots-ms/usecases"
	"context"
	sharedProto "gitlab.com/nikita.morozov/ms-shared/proto"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
)

type stateServer struct {
	proto.UnimplementedStateServiceServer
	useCase usecases.StateUsecase
}

func (h *stateServer) Create(ctx context.Context, in *proto.State) (*sharedProto.IdRequest, error) {
	model := FromState(in)

	err := h.useCase.Create(model)
	if err != nil {
		return nil, err
	}

	return &sharedProto.IdRequest{Id: uint32(model.ID)}, nil
}

func (h *stateServer) Update(ctx context.Context, in *proto.State) (*emptypb.Empty, error) {
	model := FromState(in)

	err := h.useCase.Update(model)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

func (h *stateServer) BatchUpdate(ctx context.Context, in *proto.StateBatch) (*emptypb.Empty, error) {
	var items []models.State

	for _, in := range in.Items {
		items = append(items, models.State{
			ID:             uint(in.Id),
			Name:           in.Name,
			ScenarioID:     uint(in.ScenarioId),
			StateMachineID: uint(in.StateMachineId),
			Meta:           in.Meta,
			Transitions:    mapTransitions(in.Transitions),
		})
	}

	err := h.useCase.BatchUpdate(&items)
	if err != nil {
		return nil, err
	}

	return &emptypb.Empty{}, nil
}

func (h *stateServer) Delete(ctx context.Context, in *sharedProto.IdRequest) (*sharedProto.BoolValue, error) {
	err := h.useCase.Delete(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return &sharedProto.BoolValue{Value: true}, nil
}

func FromState(in *proto.State) *models.State {
	return &models.State{
		ID:             uint(in.Id),
		Name:           in.Name,
		ScenarioID:     uint(in.ScenarioId),
		StateMachineID: uint(in.StateMachineId),
		Meta:           in.Meta,
		Transitions:    mapTransitions(in.Transitions),
	}
}

func mapTransitions(in []*proto.Transition) []models.StateTransition {
	transitions := make([]models.StateTransition, len(in))

	for index, item := range in {
		var ToStateId *uint
		if item.ToStateId != nil {
			val := uint(*item.ToStateId)
			ToStateId = &val
		}

		transitions[index] = models.StateTransition{
			ID:        uint(item.Id),
			StateID:   uint(item.StateId),
			ToStateID: ToStateId,
			Trigger:   item.Trigger,
			Meta:      item.Meta,
		}
	}

	return transitions
}

func mapToTransitions(in []models.StateTransition) []*proto.Transition {
	transitions := make([]*proto.Transition, len(in))

	for index, item := range in {
		var ToStateId *uint32
		if item.ToStateID != nil {
			val := uint32(*item.ToStateID)
			ToStateId = &val
		}

		transitions[index] = &proto.Transition{
			Id:        uint32(item.ID),
			StateId:   uint32(item.StateID),
			ToStateId: ToStateId,
			Trigger:   item.Trigger,
			Meta:      item.Meta,
		}
	}

	return transitions
}

func NewState(item *models.State) *proto.State {
	return &proto.State{
		Id:             uint32(item.ID),
		Name:           item.Name,
		ScenarioId:     uint32(item.ScenarioID),
		StateMachineId: uint32(item.StateMachineID),
		Meta:           item.Meta,
		Transitions:    mapToTransitions(item.Transitions),
	}
}

func (h *stateServer) Get(ctx context.Context, in *sharedProto.IdRequest) (*proto.State, error) {
	val := uint(in.Id)
	item, err := h.useCase.Get(&val)
	if err != nil {
		return nil, err
	}

	return NewState(item), nil
}

func (h *stateServer) List(in *sharedProto.IdRequest, listStream proto.StateService_ListServer) error {
	list, err := h.useCase.List(uint(in.Id))
	if err != nil {
		return err
	}

	for _, item := range *list {
		err = listStream.Send(NewState(&item))
		if err != nil {
			return err
		}
	}

	return nil
}

func NewStateGrpcHandler(s *grpc.Server, useCase usecases.StateUsecase) {
	proto.RegisterStateServiceServer(s, &stateServer{useCase: useCase})
}
