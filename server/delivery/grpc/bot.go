package grpc

import (
	"bots-ms/models"
	"bots-ms/proto"
	"bots-ms/tools"
	"bots-ms/usecases"
	"context"
	sharedProto "gitlab.com/nikita.morozov/ms-shared/proto"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
	"gorm.io/gorm"
)

type botServer struct {
	proto.UnimplementedBotServer
	useCase     usecases.BotUsecase
	initHandler tools.InitHandler
}

func (h *botServer) Create(ctx context.Context, in *proto.BotModel) (*sharedProto.IdRequest, error) {
	model := models.Bot{
		Token:              in.Token,
		Env:                in.Env,
		Slug:               in.Slug,
		Handler:            in.Handler,
		AllowPlainCommand:  in.AllowPlainCommand,
		AutoRemoveMessages: in.AutoRemoveMessages,
		DisableTriggers:    in.DisableTriggers,
		ExecutorHandler:    in.ExecutorHandler,
	}

	err := h.useCase.Create(&model)
	if err != nil {
		return nil, err
	}

	h.initHandler.InitBot(&model)

	return &sharedProto.IdRequest{Id: uint32(model.ID)}, err
}

func (h *botServer) Update(ctx context.Context, in *proto.BotModel) (*emptypb.Empty, error) {
	model := models.Bot{
		Model: gorm.Model{
			ID: uint(in.Id),
		},
		Token:              in.Token,
		Env:                in.Env,
		Slug:               in.Slug,
		Handler:            in.Handler,
		AllowPlainCommand:  in.AllowPlainCommand,
		AutoRemoveMessages: in.AutoRemoveMessages,
		DisableTriggers:    in.DisableTriggers,
		ExecutorHandler:    in.ExecutorHandler,
	}

	item, err := h.useCase.Get(uint(in.Id))
	if err != nil {
		return nil, err
	}

	h.initHandler.DeInitBot(item)

	err = h.useCase.Update(&model)
	if err != nil {
		return nil, err
	}

	go h.initHandler.InitBot(&model)

	return &emptypb.Empty{}, nil
}

func (h *botServer) Delete(ctx context.Context, in *sharedProto.IdRequest) (*sharedProto.BoolValue, error) {
	item, err := h.useCase.Get(uint(in.Id))
	if err != nil {
		return nil, err
	}

	h.initHandler.DeInitBot(item)

	err = h.useCase.Delete(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return &sharedProto.BoolValue{Value: true}, nil
}

func newBot(item *models.Bot) *proto.BotModel {
	return &proto.BotModel{
		Id:                 uint32(item.ID),
		CreatedAt:          item.CreatedAt.Unix(),
		UpdatedAt:          item.UpdatedAt.Unix(),
		Token:              item.Token,
		Slug:               item.Slug,
		Env:                item.Env,
		Handler:            item.Handler,
		ExecutorHandler:    item.ExecutorHandler,
		AllowPlainCommand:  item.AllowPlainCommand,
		AutoRemoveMessages: item.AutoRemoveMessages,
		DisableTriggers:    item.DisableTriggers,
	}
}

func (h *botServer) Get(ctx context.Context, in *sharedProto.IdRequest) (*proto.BotModel, error) {
	item, err := h.useCase.Get(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return newBot(item), nil
}

func (h *botServer) Count(ctx context.Context, req *emptypb.Empty) (*sharedProto.UInt64Value, error) {
	count, err := h.useCase.Count()
	return &sharedProto.UInt64Value{Value: count}, err
}

func (h *botServer) mapItems(in *[]models.Bot) []*proto.BotModel {
	items := make([]*proto.BotModel, len(*in))

	for index, item := range *in {
		items[index] = newBot(&item)
	}

	return items
}

func (h *botServer) List(ctx context.Context, in *sharedProto.ListOptions) (*proto.BotListResp, error) {
	list, paginator, err := h.useCase.List(tools.ConvertOpts(in))
	if err != nil {
		return nil, err
	}

	return &proto.BotListResp{
		Items:     h.mapItems(list),
		Paginator: tools.ConvertPaginator(paginator),
	}, nil
}

func NewBotGrpcHandler(s *grpc.Server, useCase usecases.BotUsecase, initHandler tools.InitHandler) {
	proto.RegisterBotServer(s, &botServer{useCase: useCase, initHandler: initHandler})
}
