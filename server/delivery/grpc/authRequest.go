package grpc

import (
	"bots-ms/models"
	"bots-ms/proto"
	"bots-ms/usecases"
	"context"
	sharedProto "gitlab.com/nikita.morozov/ms-shared/proto"
	"google.golang.org/grpc"
)

type authRequestServer struct {
	proto.UnimplementedAuthRequestServer
	useCase usecases.AuthRequestUsecase
}

func (h *authRequestServer) Create(ctx context.Context, in *proto.AuthRequestModel) (*proto.AuthRequestModel, error) {
	model := models.AuthRequest{
		BotID: uint(in.BotId),
		Meta:  in.Meta,
		Hash:  in.Hash,
	}

	err := h.useCase.Create(&model)
	if err != nil {
		return nil, err
	}

	return &proto.AuthRequestModel{
		BotId: uint32(model.BotID),
		Meta:  model.Meta,
		Hash:  model.Hash,
	}, nil
}

func (h *authRequestServer) Delete(ctx context.Context, in *sharedProto.IdRequest) (*sharedProto.BoolValue, error) {
	err := h.useCase.Delete(uint(in.Id))
	if err != nil {
		return nil, err
	}

	return &sharedProto.BoolValue{
		Value: true,
	}, nil
}

func NewAuthRequestGrpcHandler(s *grpc.Server, useCase usecases.AuthRequestUsecase) {
	proto.RegisterAuthRequestServer(s, &authRequestServer{useCase: useCase})
}
