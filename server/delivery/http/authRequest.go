package http

import (
	"bots-ms/common"
	"bots-ms/models"
	"bots-ms/models/dto"
	"bots-ms/usecases"
	"github.com/labstack/echo/v4"
	"gitlab.com/nikita.morozov/ms-shared/models/response"
	"net/http"
)

type AuthRequestHttpHandler struct {
	useCase    usecases.AuthRequestUsecase
	botUseCase usecases.BotUsecase
}

func (h *AuthRequestHttpHandler) Create(c echo.Context) error {
	var model dto.AuthRequestDto
	err := c.Bind(&model)
	if err != nil {
		return err
	}

	bot, err := h.botUseCase.GetBySlug(model.BotSlug)
	if err != nil {
		return err
	}

	oldAuth, _ := h.useCase.GetByMeta(bot.ID, model.Meta)
	if oldAuth != nil {
		oldAuth.Hash = model.Hash
		err = h.useCase.Update(oldAuth)
		if err != nil {
			return err
		}
	} else {
		authReq := models.AuthRequest{
			BotID: bot.ID,
			Bot:   *bot,
			Meta:  model.Meta,
			Hash:  model.Hash,
		}

		err = h.useCase.Create(&authReq)
		if err != nil {
			return err
		}
	}

	return c.JSON(http.StatusCreated, response.ItemResp{
		Item: model,
	})
}

func NewHttpAuthRequestHandler(e *echo.Echo, useCase usecases.AuthRequestUsecase, botUseCase usecases.BotUsecase) {
	handler := AuthRequestHttpHandler{
		useCase:    useCase,
		botUseCase: botUseCase,
	}

	e.POST(common.API_VER_1_0+"/auth-request", handler.Create)
}
