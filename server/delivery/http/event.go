package http

import (
	builder2 "bots-ms/builder"
	"bots-ms/common"
	"bots-ms/executor"
	"bots-ms/models/dto"
	"bots-ms/usecases"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"net/http"
)

type EventHttpHandler struct {
	botUc          usecases.BotUsecase
	eventUc        usecases.EventUsecase
	scenarioEngine usecases.ScenarioEngine
	botExecutor    executor.BotExecutor
}

func (h *EventHttpHandler) Execute(c echo.Context) error {
	var model dto.ExecuteEventDto
	err := c.Bind(&model)
	if err != nil {
		return err
	}

	bot, err := h.botUc.GetBySlug(model.BotSlug)
	if err != nil {
		return err
	}

	builder := builder2.NewScenarioEngineParamsBuilder()
	builder.SetBot(bot)
	builder.SetContent(model.Content)

	event, err := h.eventUc.GetByName(model.Command, model.BotSlug)
	if err != nil {
		return err
	}

	params := builder.Build()
	err = h.scenarioEngine.Execute(event.ScenarioID, &params)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, "Ok")
}

func (h *EventHttpHandler) ExecuteInSM(c echo.Context) error {
	var model dto.ExecuteEventDto
	err := c.Bind(&model)
	if err != nil {
		return err
	}

	bot, err := h.botUc.GetBySlug(model.BotSlug)
	if err != nil {
		return err
	}

	builder := builder2.NewScenarioEngineParamsBuilder()
	builder.SetBot(bot)
	builder.SetContent(model.Content)

	event, err := h.eventUc.GetByName(model.Command, model.BotSlug)
	if err != nil {
		return err
	}

	data := dto.BotContentDto{
		Context: uuid.New().String(),
		Bot:     *bot,
		Data:    model.Content,
		Command: event.Command,
	}

	if model.Sender != nil {
		data.Sender = *model.Sender
	}

	err = h.botExecutor.ExecuteScenario(event.ScenarioID, data)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, "Ok")
}

func NewHttpEventHandler(e *echo.Echo, botUc usecases.BotUsecase, eventUc usecases.EventUsecase, scenarioEngine usecases.ScenarioEngine, botExecutor executor.BotExecutor) {
	handler := EventHttpHandler{
		botUc:          botUc,
		eventUc:        eventUc,
		botExecutor:    botExecutor,
		scenarioEngine: scenarioEngine,
	}

	e.POST(common.API_VER_1_0+"/event/execute", handler.Execute)
	e.POST(common.API_VER_1_0+"/event/execute-in-sm", handler.ExecuteInSM)
}
