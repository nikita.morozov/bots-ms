package http

import (
	"bots-ms/common"
	"bots-ms/delivery/grpc"
	"bots-ms/models"
	"bots-ms/proto"
	"bots-ms/usecases"
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	"net/http"
	"strconv"
)

type SerializationHttpHandler struct {
	useCase usecases.StateMachineUsecase
}

func (h *SerializationHttpHandler) Import(c echo.Context) error {
	file, err := c.FormFile("file")
	if err != nil {
		return err
	}

	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	var model proto.StateMachineExport
	err = json.NewDecoder(src).Decode(&model)
	if err != nil {
		return err
	}

	var connectors []models.Connector
	for _, connector := range model.Connectors {
		connectors = append(connectors, *grpc.FromConnectionModel(connector))
	}

	var sceneries []models.Scenario
	for _, scenario := range model.Scenarios {
		sceneries = append(sceneries, *grpc.FromScenario(scenario))
	}

	var states []models.State
	for _, state := range model.States {
		states = append(states, *grpc.FromState(state))
	}

	item := models.StateMachineExport{
		StateMachine: *grpc.FromStateMachine(model.StateMachine),
		Connectors:   connectors,
		Sceneries:    sceneries,
		States:       &states,
	}

	err = h.useCase.Import(6, &item)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, "OK")
}

func (h *SerializationHttpHandler) Export(c echo.Context) error {
	isStr := c.Param("id")
	id, err := strconv.Atoi(isStr)
	if err != nil {
		return err
	}

	item, err := h.useCase.Export(uint(id))
	if err != nil {
		return err
	}

	states := make([]*proto.State, len(*item.States))
	for i, state := range *item.States {
		states[i] = grpc.NewState(&state)
	}

	sceneries := make([]*proto.ScenarioModel, len(item.Sceneries))
	for i, scenario := range item.Sceneries {
		sceneries[i] = grpc.NewScenario(&scenario)
	}

	connectors := make([]*proto.ConnectorModel, len(item.Connectors))
	for i, connector := range item.Connectors {
		connectors[i] = grpc.NewConnectorModel(&connector)
	}

	c.Response().Header().Set("Content-disposition", fmt.Sprint("attachment; filename=state_machine_", id, ".json"))

	return c.JSON(http.StatusOK, &proto.StateMachineExport{
		StateMachine: grpc.NewStateMachine(&item.StateMachine),
		States:       states,
		Scenarios:    sceneries,
		Connectors:   connectors,
	})
}

func NewSerializationHttpHandler(e *echo.Echo, stateMachineUC usecases.StateMachineUsecase) {
	handler := SerializationHttpHandler{
		useCase: stateMachineUC,
	}

	e.GET(common.API_VER_1_0+"/serialization/export/:id", handler.Export)
	e.POST(common.API_VER_1_0+"/serialization/import", handler.Import)
}
