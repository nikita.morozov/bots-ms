package http

import (
	"bots-ms/common"
	"bots-ms/models/dto"
	"bots-ms/usecases"
	"github.com/labstack/echo/v4"
	"gitlab.com/nikita.morozov/ms-shared/models/response"
	"net/http"
)

type BotUserHttpHandler struct {
	useCase    usecases.BotUserUsecase
	botUseCase usecases.BotUsecase
}

func (h *BotUserHttpHandler) IsExist(c echo.Context) error {
	var model dto.UserIsExistDto
	err := c.Bind(&model)
	if err != nil {
		return err
	}

	bot, err := h.botUseCase.GetBySlug(model.BotSlug)
	if err != nil {
		return err
	}

	_, err = h.useCase.GetByMeta(bot.ID, model.Meta)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, response.ItemResp{
		Item: err == nil,
	})
}

func NewHttpBotUserHandler(e *echo.Echo, useCase usecases.BotUserUsecase, botUseCase usecases.BotUsecase) {
	handler := BotUserHttpHandler{
		useCase:    useCase,
		botUseCase: botUseCase,
	}

	e.POST(common.API_VER_1_0+"/user/is-exist", handler.IsExist)
}
