package managers

import (
	"bots-ms/models"
	"bots-ms/usecases"
	"errors"
	"fmt"
	"time"
)

type UserStateManager interface {
	GetDefault(bot models.Bot, userId string) (*models.State, error)
	SetNextState(bot models.Bot, userId string, trigger string) (*models.State, error)
	SetNextStateByScenario(bot models.Bot, userId string, scenarioId uint) (*models.State, error)
}

type userStateManager struct {
	variables    usecases.VariableStorageUsecase
	stateMachine usecases.StateMachineUsecase
	stateUC      usecases.StateUsecase
	duration     time.Duration
}

func (u *userStateManager) GetDefault(bot models.Bot, userId string) (*models.State, error) {
	sm, err := u.stateMachine.GetByBot(bot.ID)
	if err != nil {
		return nil, err
	}

	ctx := fmt.Sprintf("%s%sState", bot.Slug, userId)
	err = u.variables.SetInt(ctx, "current", int(sm.StartStateId))
	return u.stateUC.Get(&sm.StartStateId)
}

func (u *userStateManager) SetNextState(bot models.Bot, userId string, trigger string) (*models.State, error) {
	ctx := fmt.Sprintf("%s%sState", bot.Slug, userId)
	stateId, err := u.variables.GetInt(ctx, "current")

	// State not defined and we
	if err != nil {
		sm, err := u.stateMachine.GetByBot(bot.ID)
		if err != nil {
			return nil, err
		}

		err = u.variables.SetIntWithDuration(ctx, "current", int(sm.StartStateId), u.duration)
		if err != nil {
			return nil, err
		}

		return u.stateUC.Get(&sm.StartStateId)
	}

	val := uint(stateId)
	state, err := u.stateUC.Get(&val)
	if err != nil {
		return nil, err
	}

	transition := state.GetTransition(trigger)
	if transition == nil || transition.ToStateID == nil || *transition.ToStateID == uint(stateId) {
		return nil, errors.New("state-manager.transition.not-found")
	}

	err = u.variables.SetIntWithDuration(ctx, "current", int(*transition.ToStateID), u.duration)
	return u.stateUC.Get(transition.ToStateID)
}

func (u *userStateManager) SetNextStateByScenario(bot models.Bot, userId string, scenarioId uint) (*models.State, error) {
	ctx := fmt.Sprintf("%s%sState", bot.Slug, userId)
	sm, err := u.stateMachine.GetByBot(bot.ID)
	if err != nil {
		return nil, err
	}

	state, err := u.stateUC.GetByScenarioId(sm.ID, scenarioId)
	if err != nil {
		return nil, err
	}

	err = u.variables.SetIntWithDuration(ctx, "current", int(state.ID), u.duration)
	if err != nil {
		return nil, err
	}

	return state, nil
}

func NewUserStateManager(duration time.Duration, variables usecases.VariableStorageUsecase, stateMachine usecases.StateMachineUsecase, stateUC usecases.StateUsecase) UserStateManager {
	return &userStateManager{
		variables:    variables,
		stateMachine: stateMachine,
		stateUC:      stateUC,
		duration:     duration,
	}
}
