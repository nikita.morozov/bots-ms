package builder

import (
	"bots-ms/models"
	"encoding/json"
	"github.com/google/uuid"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
)

type ScenarioEngineParamsBuilder interface {
	SetBot(bot *models.Bot)
	SetSender(sender string)
	SetMessageId(messageId string)
	SetContent(content string)
	SetContentObject(content interface{})
	Build() coreModel.NodeParams
}

type builder struct {
	params coreModel.NodeParams
}

func (b *builder) SetBot(bot *models.Bot) {
	b.params.SetUint("botId", bot.ID)
	b.params.Set("botHandler", bot.Handler)
	b.params.Set("botSlug", bot.GetSlug())
}

func (b *builder) SetSender(sender string) {
	b.params.Set("sender", sender)
}

func (b *builder) SetMessageId(messageId string) {
	b.params.Set("messageId", messageId)
}

func (b *builder) SetContent(content string) {
	b.params.Set("content", content)
}

func (b *builder) SetContentObject(content interface{}) {
	if content != nil {
		payload, err := json.Marshal(content)
		if err != nil {
			return
		}

		b.params.Set("content", string(payload))
	}
}

func (b *builder) Build() coreModel.NodeParams {
	return b.params
}

func NewScenarioEngineParamsBuilder() ScenarioEngineParamsBuilder {
	b := &builder{
		params: coreModel.NodeParams{},
	}

	b.params.Set("context", uuid.New().String())

	return b
}
