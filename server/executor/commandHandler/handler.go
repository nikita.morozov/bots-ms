package commandHandler

import "bots-ms/models/dto"

type CommandHandler interface {
	Execute(command string, data *dto.BotContentDto) (uint, error)
	IsDefaultAvailable() bool
	ExecuteDefault(data *dto.BotContentDto) (uint, error)
	ExecuteScenario(scenarioId uint, data *dto.BotContentDto) (uint, error)
}
