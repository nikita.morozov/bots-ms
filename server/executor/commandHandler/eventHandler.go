package commandHandler

import (
	"bots-ms/models/dto"
	"bots-ms/usecases"
	"errors"
	"log"
	"strings"
)

type eventCommandHandler struct {
	eventUsecase    usecases.EventUsecase
	scenarioUsecase usecases.ScenarioUsecase
}

func (e *eventCommandHandler) ExecuteScenario(scenarioId uint, data *dto.BotContentDto) (uint, error) {
	//TODO implement me
	panic("implement me")
}

func (e *eventCommandHandler) Execute(_command string, data *dto.BotContentDto) (uint, error) {
	bot := data.Bot
	passPlainCommand := false

	commandPrefix := strings.HasPrefix(_command, "/")
	if !commandPrefix && bot.AllowPlainCommand {
		event, _ := e.eventUsecase.GetByName(_command, bot.Slug)
		if event != nil {
			passPlainCommand = true
		}
	}

	if commandPrefix || passPlainCommand {
		var command string
		var commandData string

		if commandPrefix {
			var values = strings.Split(_command, " ")
			command = values[0]
			commandData = strings.Join(values[1:], " ")
		} else if bot.AllowPlainCommand {
			command = _command
		}

		data.Data = commandData
		log.Printf("💭🤖 Telegram command \033[1;34m\"%s\"\033[0m from: \"%s\" with data: %s \n", bot.Slug, data.Sender, data.Data)

		event, err := e.eventUsecase.GetByName(command, bot.Slug)
		if event == nil {
			log.Printf("💭🤖 Telegram bot \033[1;34m\"%s\"\033[0m event not found", command)
			return 0, err
		}

		scenario, err := e.scenarioUsecase.Get(event.ScenarioID)
		if scenario == nil || scenario.ID == 0 {
			log.Printf("💭🤖 Telegram bot \033[1;34m\"%s\"\033[0m scenario not found", bot.Slug)
			return 0, err
		}

		return scenario.ID, nil
	}

	return 0, errors.New("command-handler.scenario-not-found")
}

func (e *eventCommandHandler) ExecuteDefault(data *dto.BotContentDto) (uint, error) {
	return 0, errors.New("not-implemented")
}

func (e *eventCommandHandler) IsDefaultAvailable() bool {
	return false
}

func NewEventCommandHandler(eventUsecase usecases.EventUsecase, scenarioUsecase usecases.ScenarioUsecase) CommandHandler {
	return &eventCommandHandler{
		eventUsecase:    eventUsecase,
		scenarioUsecase: scenarioUsecase,
	}
}
