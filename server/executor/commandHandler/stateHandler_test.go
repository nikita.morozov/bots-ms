package commandHandler

import (
	mocks2 "bots-ms/managers/mocks"
	"bots-ms/models"
	"bots-ms/models/dto"
	"bots-ms/usecases/mocks"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"gorm.io/gorm"
	"testing"
)

type BotStateHandlerSuite struct {
	suite.Suite
	scenarioUC *mocks.ScenarioUsecase
	stateMng   *mocks2.UserStateManager

	handler CommandHandler
}

func (s *BotStateHandlerSuite) BeforeTest(_, _ string) {
	scenarioUC := new(mocks.ScenarioUsecase)
	stateMng := new(mocks2.UserStateManager)
	s.stateMng = stateMng
	s.scenarioUC = scenarioUC

	s.handler = NewStateCommandHandler(scenarioUC, stateMng)
}

func TestBotStateHandler(t *testing.T) {
	suite.Run(t, new(BotStateHandlerSuite))
}

func (s *BotStateHandlerSuite) Test_stateHandler_Execute() {
	data := dto.BotContentDto{
		Context: "context",
		Bot: models.Bot{
			Model: gorm.Model{
				ID: 1,
			},
			Token:              "bot-token",
			Handler:            "telegram",
			ExecutorHandler:    "state",
			AllowPlainCommand:  true,
			DisableTriggers:    false,
			AutoRemoveMessages: false,
		},
		Sender:    "123123",
		MessageId: "mid_123123",
		Nickname:  "nickname",
	}

	scenario := models.Scenario{
		Model: gorm.Model{
			ID: 8,
		},
		Title:    "Scenario",
		Category: "Category",
	}

	state := models.State{
		ID:             12,
		Name:           "start",
		ScenarioID:     13,
		Scenario:       scenario,
		StateMachineID: 8,
		Meta:           "",
	}

	s.stateMng.On("SetNextState", data.Bot, data.Sender, "/start").Return(&state, nil)
	s.scenarioUC.On("Get", state.ScenarioID).Return(&scenario, nil)

	sid, err := s.handler.Execute("/start eyJyZWZfaWQiOiIyMDE5MyJ9", &data)

	require.NoError(s.T(), err)
	require.Equal(s.T(), uint(8), sid)
	require.Equal(s.T(), "eyJyZWZfaWQiOiIyMDE5MyJ9", data.Data)
}
