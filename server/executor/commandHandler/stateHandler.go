package commandHandler

import (
	"bots-ms/managers"
	"bots-ms/models/dto"
	"bots-ms/usecases"
	"errors"
	"log"
	"strings"
)

type stateCommandHandler struct {
	scenarioUsecase usecases.ScenarioUsecase
	stateMng        managers.UserStateManager
}

func (e *stateCommandHandler) Execute(_command string, data *dto.BotContentDto) (uint, error) {
	if data.External {
		return 0, errors.New("state.execute-external.not-allowed")
	}

	bot := data.Bot

	var command string
	var commandData string

	var values = strings.Split(_command, " ")
	command = values[0]
	commandData = strings.Join(values[1:], " ")

	data.Data = commandData
	log.Printf("💭🤖 Telegram command \033[1;34m\"%s\"\033[0m from: \"%s\" with data: %s \n", bot.Slug, data.Sender, data.Data)

	state, err := e.stateMng.SetNextState(data.Bot, data.Sender, command)
	if err != nil {
		state, err = e.stateMng.GetDefault(data.Bot, data.Sender)
		if err != nil {
			log.Printf("💭🤖 Telegram bot \033[1;34m\"%s\"\033[0m state not found", command)
			return 0, err
		}
	}

	scenario, err := e.scenarioUsecase.Get(state.ScenarioID)
	if scenario == nil || scenario.ID == 0 {
		log.Printf("💭🤖 Telegram bot \033[1;34m\"%s\"\033[0m scenario not found", bot.Slug)
		return 0, err
	}

	return scenario.ID, nil
}

func (e *stateCommandHandler) ExecuteDefault(data *dto.BotContentDto) (uint, error) {
	state, err := e.stateMng.GetDefault(data.Bot, data.Sender)
	if err != nil {
		return 0, err
	}

	return state.ScenarioID, nil
}

func (e *stateCommandHandler) ExecuteScenario(scenarioId uint, data *dto.BotContentDto) (uint, error) {
	state, err := e.stateMng.SetNextStateByScenario(data.Bot, data.Sender, scenarioId)
	if err != nil {
		return 0, err
	}

	return state.ScenarioID, nil
}

func (e *stateCommandHandler) IsDefaultAvailable() bool {
	return true
}

func NewStateCommandHandler(scenarioUsecase usecases.ScenarioUsecase, stateMng managers.UserStateManager) CommandHandler {
	return &stateCommandHandler{
		scenarioUsecase: scenarioUsecase,
		stateMng:        stateMng,
	}
}
