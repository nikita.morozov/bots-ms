package executor

import (
	"bots-ms/executor/commandHandler"
	"bots-ms/models/dto"
	"bots-ms/usecases"
	"errors"
	"github.com/google/uuid"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
)

type BotExecutor interface {
	ExecuteScenario(scenarioId uint, data dto.BotContentDto) error
	ExecuteCommand(command string, data dto.BotContentDto) error
	ExecuteTrigger(data dto.BotContentDto) error
}

type botExecutor struct {
	scenarioEngine  usecases.ScenarioEngine
	commonMessageUC usecases.CommonMessageUsecase
	handler         map[string]commandHandler.CommandHandler
}

// TODO Dry plzz
func (b *botExecutor) executeScenarioInEngine(scenarioId uint, data dto.BotContentDto) error {
	params := coreModel.NodeParams{}
	params.SetUint("botId", data.Bot.ID)
	params.Set("context", uuid.New().String())
	params.Set("botHandler", data.Bot.Handler)
	params.Set("botSlug", data.Bot.GetSlug())
	params.Set("sender", data.Sender)
	params.Set("messageId", data.MessageId)
	params.Set("replayOnMessageId", data.ReplayTo)
	params.Set("content", data.Data)
	params.Set("nickname", data.Nickname)
	params.Set("command", data.Command)

	return b.scenarioEngine.Execute(scenarioId, &params)
}

func (b *botExecutor) ExecuteScenario(scenarioId uint, _data dto.BotContentDto) error {
	data := _data
	eHandler := data.Bot.ExecutorHandler

	if handler, ok := b.handler[eHandler]; ok {
		scenarioId, err := handler.ExecuteScenario(scenarioId, &data)
		if err != nil {
			return err
		}

		return b.executeScenarioInEngine(scenarioId, data)
	}

	return errors.New("bot-executor.handler-not-found")
}

func (b *botExecutor) ExecuteCommand(command string, _data dto.BotContentDto) error {
	data := _data
	eHandler := data.Bot.ExecutorHandler
	data.Command = command

	if handler, ok := b.handler[eHandler]; ok {
		scenarioId, err := handler.Execute(command, &data)
		if err != nil {
			return err
		}

		return b.executeScenarioInEngine(scenarioId, data)
	}

	return errors.New("bot-executor.handler-not-found")
}

func (b *botExecutor) ExecuteTrigger(data dto.BotContentDto) error {
	params := coreModel.NodeParams{}
	params.SetUint("botId", data.Bot.ID)
	params.Set("botHandler", data.Bot.Handler)
	params.Set("botSlug", data.Bot.GetSlug())
	params.Set("sender", data.Sender)
	params.Set("messageId", data.MessageId)
	params.Set("replayOnMessageId", data.ReplayTo)
	params.Set("content", data.Data)
	params.Set("event", data.Event)
	params.Set("nickname", data.Nickname)

	return b.scenarioEngine.ExecuteTrigger(&params)
}

func NewBotExecutor(scenarioEngine usecases.ScenarioEngine, commonMessageUC usecases.CommonMessageUsecase, handler map[string]commandHandler.CommandHandler) BotExecutor {
	return &botExecutor{
		handler:         handler,
		scenarioEngine:  scenarioEngine,
		commonMessageUC: commonMessageUC,
	}
}
