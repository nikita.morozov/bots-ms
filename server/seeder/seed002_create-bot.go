package seeder

import (
	"bots-ms/models"
	"bots-ms/repositories/gormHandlers"
	gs "github.com/randree/gormseeder"
	"gorm.io/gorm"
)

func init() {
	gs.Seed(gs.State{
		Tag: "seed002_create-bot",
		Perform: func(db *gorm.DB) error {
			repo := gormHandlers.NewGormBotRepository(db, 10)

			repo.Create(&models.Bot{
				Token:   "1879613333:AAFzqJ6j3WQKiVRdKJnS4l4ugPx4Qe0jwi4",
				Slug:    "testBot",
				Handler: "telegram",
			})

			return nil
		},
	})
}
