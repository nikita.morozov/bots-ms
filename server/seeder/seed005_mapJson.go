package seeder

import (
	"bots-ms/repositories/gormHandlers"
	gs "github.com/randree/gormseeder"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"gorm.io/gorm"
)

func init() {
	gs.Seed(gs.State{
		Tag: "seed005_mapJson",
		Perform: func(db *gorm.DB) error {
			repo := gormHandlers.NewGormActionRepository(db)

			repo.Create(&coreModel.Action{
				Name:    "JSON Map",
				Handler: "mapJson",
				Sockets: []coreModel.Socket{
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeEvent,
						Title:     "OnStart",
						Slug:      coreModel.NodeDataOnStart,
					},
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeString,
						Title:     "JSON Data",
						Slug:      "data",
					},
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeString,
						Title:     "Path",
						Slug:      "path",
					},
					// Out
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeEvent,
						Title:     coreModel.NodeDataOnComplete,
						Slug:      coreModel.NodeDataOnComplete,
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeEvent,
						Title:     coreModel.NodeDataOnError,
						Slug:      coreModel.NodeDataOnError,
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeString,
						Title:     "Value",
						Slug:      "value",
					},
				},
			})

			return nil
		},
	})
}
