package seeder

import (
	"bots-ms/repositories/gormHandlers"
	gs "github.com/randree/gormseeder"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"gorm.io/gorm"
	"log"
)

type seederRunner struct {
	tableName string
	db        *gorm.DB
	seeders   []coreModel.ModuleSeed
}

type SeederRunner interface {
	Add(seeds []coreModel.ModuleSeed)
	Run()
}

func (sr *seederRunner) Add(seeds []coreModel.ModuleSeed) {
	for _, seed := range seeds {
		sr.seeders = append(sr.seeders, seed)
	}
}

func (sr *seederRunner) Run() {
	var err error
	repo := gormHandlers.NewGormActionRepository(sr.db)

	for _, seed := range sr.seeders {
		currentSeed := seed
		gs.Seed(gs.State{
			Tag: currentSeed.Name,
			Perform: func(db *gorm.DB) error {
				for _, action := range currentSeed.Actions {
					log.Printf("Seed action %s", action.Name)
					err = repo.Create(&action)
					if err != nil {
						return err
					}
				}

				return nil
			},
		})
	}

	gs.InitSeeder(sr.db, sr.tableName)
}

// "bots-ms_seeders"
func NewSeederRunner(db *gorm.DB, tableName string) SeederRunner {
	return &seederRunner{
		db:        db,
		tableName: tableName,
	}
}
