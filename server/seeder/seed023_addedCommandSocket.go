package seeder

import (
	"bots-ms/repositories/gormHandlers"
	gs "github.com/randree/gormseeder"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"gorm.io/gorm"
)

func init() {
	gs.Seed(gs.State{
		Tag: "seed023_addedCommandSocket",
		Perform: func(db *gorm.DB) error {
			repo := gormHandlers.NewGormActionRepository(db)
			repoSocket := gormHandlers.NewGormSocketRepository(db)

			actionBotEvent, err := repo.GetByHandlerName("botEvent")
			if err != nil {
				return err
			}

			repoSocket.Create(&coreModel.Socket{
				ActionId:  actionBotEvent.Id,
				Direction: coreModel.SocketDirectionOut,
				Type:      coreModel.SocketTypeString,
				Title:     "Command",
				Slug:      "command",
				Manual:    false,
			})

			return nil
		},
	})
}
