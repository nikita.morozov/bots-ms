package seeder

import (
	"bots-ms/repositories/gormHandlers"
	gs "github.com/randree/gormseeder"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"gorm.io/gorm"
)

func init() {
	gs.Seed(gs.State{
		Tag: "seed019_addedMarkdownSocket",
		Perform: func(db *gorm.DB) error {
			repo := gormHandlers.NewGormActionRepository(db)
			repoSocket := gormHandlers.NewGormSocketRepository(db)

			actionBotSendMessage, err := repo.GetByHandlerName("sendMessage")
			if err != nil {
				return err
			}

			repoSocket.Create(&coreModel.Socket{
				ActionId:  actionBotSendMessage.Id,
				Direction: coreModel.SocketDirectionIn,
				Type:      coreModel.SocketTypeBool,
				Title:     "Markdown",
				Slug:      "markdown",
				Manual:    false,
			})

			actionBotRemoveKeyboard, err := repo.GetByHandlerName("removeKeyboard")
			if err != nil {
				return err
			}

			repoSocket.Create(&coreModel.Socket{
				ActionId:  actionBotRemoveKeyboard.Id,
				Direction: coreModel.SocketDirectionIn,
				Type:      coreModel.SocketTypeBool,
				Title:     "Markdown",
				Slug:      "markdown",
				Manual:    false,
			})

			actionBotReplyKeyboard, err := repo.GetByHandlerName("replyKeyboard")
			if err != nil {
				return err
			}

			repoSocket.Create(&coreModel.Socket{
				ActionId:  actionBotReplyKeyboard.Id,
				Direction: coreModel.SocketDirectionIn,
				Type:      coreModel.SocketTypeBool,
				Title:     "Markdown",
				Slug:      "markdown",
				Manual:    false,
			})

			repoSocket.Create(&coreModel.Socket{
				ActionId:  actionBotReplyKeyboard.Id,
				Direction: coreModel.SocketDirectionIn,
				Type:      coreModel.SocketTypeInt,
				Title:     "Buttons in row",
				Slug:      "splitButtons",
				Manual:    false,
			})

			actionBotInlineKeyboard, err := repo.GetByHandlerName("inlineKeyboard")
			if err != nil {
				return err
			}

			repoSocket.Create(&coreModel.Socket{
				ActionId:  actionBotInlineKeyboard.Id,
				Direction: coreModel.SocketDirectionIn,
				Type:      coreModel.SocketTypeBool,
				Title:     "Markdown",
				Slug:      "markdown",
				Manual:    false,
			})

			repoSocket.Create(&coreModel.Socket{
				ActionId:  actionBotInlineKeyboard.Id,
				Direction: coreModel.SocketDirectionIn,
				Type:      coreModel.SocketTypeInt,
				Title:     "Buttons in row",
				Slug:      "splitButtons",
				Manual:    false,
			})

			return nil
		},
	})
}
