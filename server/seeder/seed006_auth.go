package seeder

import (
	"bots-ms/repositories/gormHandlers"
	gs "github.com/randree/gormseeder"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"gorm.io/gorm"
)

func init() {
	gs.Seed(gs.State{
		Tag: "seed006_auth",
		Perform: func(db *gorm.DB) error {
			repo := gormHandlers.NewGormActionRepository(db)

			repo.Create(&coreModel.Action{
				Name:    "Register User",
				Handler: "registerUser",
				Sockets: []coreModel.Socket{
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeEvent,
						Title:     "OnStart",
						Slug:      coreModel.NodeDataOnStart,
					},
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeString,
						Title:     "Bot Id",
						Slug:      "botId",
					},
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeString,
						Title:     "Sender",
						Slug:      "sender",
					},
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeString,
						Title:     "Auth hash",
						Slug:      "hash",
					},
					// Out
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeEvent,
						Title:     coreModel.NodeDataOnComplete,
						Slug:      coreModel.NodeDataOnComplete,
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeEvent,
						Title:     coreModel.NodeDataOnError,
						Slug:      coreModel.NodeDataOnError,
					},
				},
			})

			repo.Create(&coreModel.Action{
				Name:    "Check auth",
				Handler: "checkAuth",
				Sockets: []coreModel.Socket{
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeEvent,
						Title:     "OnStart",
						Slug:      coreModel.NodeDataOnStart,
					},
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeString,
						Title:     "Bot Id",
						Slug:      "botId",
					},
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeString,
						Title:     "Sender",
						Slug:      "sender",
					},
					// Out
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeEvent,
						Title:     coreModel.NodeDataOnComplete,
						Slug:      coreModel.NodeDataOnComplete,
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeEvent,
						Title:     coreModel.NodeDataOnError,
						Slug:      coreModel.NodeDataOnError,
					},
				},
			})

			return nil
		},
	})
}
