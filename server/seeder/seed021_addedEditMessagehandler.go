package seeder

import (
	"bots-ms/repositories/gormHandlers"
	gs "github.com/randree/gormseeder"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"gorm.io/gorm"
)

func init() {
	gs.Seed(gs.State{
		Tag: "seed021_addedEditMessageHandler",
		Perform: func(db *gorm.DB) error {
			repo := gormHandlers.NewGormActionRepository(db)

			repo.Create(&coreModel.Action{
				Name:    "Edit message",
				Handler: "editMessage",
				Sockets: []coreModel.Socket{
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeEvent,
						Title:     "OnStart",
						Slug:      coreModel.NodeDataOnStart,
					},
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeString,
						Title:     "Text",
						Slug:      "text",
					},
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeString,
						Title:     "Message ID",
						Slug:      "messageId",
					},
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeString,
						Title:     "Bot slug",
						Slug:      "botSlug",
					},
					// Out
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeEvent,
						Title:     coreModel.NodeDataOnComplete,
						Slug:      coreModel.NodeDataOnComplete,
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeEvent,
						Title:     coreModel.NodeDataOnError,
						Slug:      coreModel.NodeDataOnError,
					},
				},
			})

			return nil
		},
	})
}
