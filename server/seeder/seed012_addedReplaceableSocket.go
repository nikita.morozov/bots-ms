package seeder

import (
	"bots-ms/repositories/gormHandlers"
	gs "github.com/randree/gormseeder"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"gorm.io/gorm"
)

func init() {
	gs.Seed(gs.State{
		Tag: "seed012_addedReplaceableSocket",
		Perform: func(db *gorm.DB) error {
			repo := gormHandlers.NewGormActionRepository(db)
			repoSocket := gormHandlers.NewGormSocketRepository(db)

			action, err := repo.GetByHandlerName("sendMessage")
			if err != nil {
				return err
			}

			return repoSocket.Create(&coreModel.Socket{
				ActionId:  action.Id,
				Direction: coreModel.SocketDirectionIn,
				Type:      coreModel.SocketTypeString,
				Title:     "Replaceable",
				Slug:      "replaceable",
				Manual:    false,
			})
		},
	})
}
