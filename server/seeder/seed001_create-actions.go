package seeder

import (
	"bots-ms/repositories/gormHandlers"
	gs "github.com/randree/gormseeder"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"gorm.io/gorm"
)

func init() {
	gs.Seed(gs.State{
		Tag: "seed001_create-actions",
		Perform: func(db *gorm.DB) error {
			repo := gormHandlers.NewGormActionRepository(db)

			repo.Create(&coreModel.Action{
				Name:    "Send message",
				Handler: "sendMessage",
				Sockets: []coreModel.Socket{
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeEvent,
						Title:     "OnStart",
						Slug:      coreModel.NodeDataOnStart,
					},
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeString,
						Title:     "Text",
						Slug:      "text",
					},
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeString,
						Title:     "Send to",
						Slug:      "sender",
					},
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeString,
						Title:     "Bot slug",
						Slug:      "botSlug",
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeEvent,
						Title:     coreModel.NodeDataOnComplete,
						Slug:      coreModel.NodeDataOnComplete,
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeEvent,
						Title:     coreModel.NodeDataOnError,
						Slug:      coreModel.NodeDataOnError,
					},
				},
			})

			repo.Create(&coreModel.Action{
				Name:    "String value",
				Handler: "stringVariable",
				Sockets: []coreModel.Socket{
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeString,
						Title:     "Value",
						Slug:      "value",
						Manual:    true,
					},
				},
			})

			repo.Create(&coreModel.Action{
				Name:    "Event bot",
				Handler: "botEvent",
				Sockets: []coreModel.Socket{
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeEvent,
						Title:     "OnStart",
						Slug:      coreModel.NodeDataOnStart,
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeString,
						Title:     "Context",
						Slug:      "context",
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeString,
						Title:     "Bot handler",
						Slug:      "botHandler",
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeString,
						Title:     "Sender",
						Slug:      "sender",
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeString,
						Title:     "Bot slug",
						Slug:      "botSlug",
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeString,
						Title:     "MessageId",
						Slug:      "messageId",
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeString,
						Title:     "Content",
						Slug:      "content",
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeEvent,
						Title:     coreModel.NodeDataOnComplete,
						Slug:      coreModel.NodeDataOnComplete,
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeEvent,
						Title:     coreModel.NodeDataOnError,
						Slug:      coreModel.NodeDataOnError,
					},
				},
			})

			return nil
		},
	})
}
