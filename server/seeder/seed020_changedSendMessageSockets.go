package seeder

import (
	"bots-ms/repositories/gormHandlers"
	gs "github.com/randree/gormseeder"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"gorm.io/gorm"
)

func init() {
	gs.Seed(gs.State{
		Tag: "seed020_changedSendMessageSockets",
		Perform: func(db *gorm.DB) error {
			repo := gormHandlers.NewGormActionRepository(db)
			repoSocket := gormHandlers.NewGormSocketRepository(db)

			actionBotEvent, err := repo.GetByHandlerName("botEvent")
			if err != nil {
				return err
			}

			repoSocket.Create(&coreModel.Socket{
				ActionId:  actionBotEvent.Id,
				Direction: coreModel.SocketDirectionOut,
				Type:      coreModel.SocketTypeString,
				Title:     "Replay MID",
				Slug:      "replayOnMessageId",
				Manual:    false,
			})

			sendMessageAction, err := repo.GetByHandlerName("sendMessage")
			if err != nil {
				return err
			}

			repoSocket.Create(&coreModel.Socket{
				ActionId:  sendMessageAction.Id,
				Direction: coreModel.SocketDirectionOut,
				Type:      coreModel.SocketTypeString,
				Title:     "Message ID",
				Slug:      "messageId",
				Manual:    false,
			})

			repo.Create(&coreModel.Action{
				Name:    "Save message",
				Handler: "saveMessage",
				Sockets: []coreModel.Socket{
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeEvent,
						Title:     "OnStart",
						Slug:      coreModel.NodeDataOnStart,
					},
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeString,
						Title:     "Text",
						Slug:      "text",
					},
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeString,
						Title:     "Message ID",
						Slug:      "messageId",
					},
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeString,
						Title:     "Data",
						Slug:      "data",
					},
					// Out
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeEvent,
						Title:     coreModel.NodeDataOnComplete,
						Slug:      coreModel.NodeDataOnComplete,
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeEvent,
						Title:     coreModel.NodeDataOnError,
						Slug:      coreModel.NodeDataOnError,
					},
				},
			})

			repo.Create(&coreModel.Action{
				Name:    "Load message",
				Handler: "loadMessage",
				Sockets: []coreModel.Socket{
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeEvent,
						Title:     "OnStart",
						Slug:      coreModel.NodeDataOnStart,
					},
					{
						Direction: coreModel.SocketDirectionIn,
						Type:      coreModel.SocketTypeString,
						Title:     "Message ID",
						Slug:      "messageId",
					},

					// Out
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeEvent,
						Title:     coreModel.NodeDataOnComplete,
						Slug:      coreModel.NodeDataOnComplete,
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeEvent,
						Title:     coreModel.NodeDataOnError,
						Slug:      coreModel.NodeDataOnError,
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeString,
						Title:     "Text",
						Slug:      "text",
					},
					{
						Direction: coreModel.SocketDirectionOut,
						Type:      coreModel.SocketTypeString,
						Title:     "Data",
						Slug:      "data",
					},
				},
			})

			return nil
		},
	})
}
