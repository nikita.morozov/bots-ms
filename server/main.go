package main

import (
	grpchandler "bots-ms/delivery/grpc"
	httphandler "bots-ms/delivery/http"
	"bots-ms/executor"
	"bots-ms/executor/commandHandler"
	"bots-ms/managers"
	methodHandlers2 "bots-ms/methodHandlers"
	"bots-ms/models"
	"bots-ms/registry"
	"bots-ms/repositories/gormHandlers"
	"bots-ms/repositories/redisHandler"
	"bots-ms/seeder"
	tools2 "bots-ms/tools"
	"bots-ms/usecases"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/labstack/echo/v4"
	core "gitlab.com/nikita.morozov/bot-ms-common-handlers/module"
	"gitlab.com/nikita.morozov/ms-shared/common"
	"gitlab.com/nikita.morozov/ms-shared/middleware"
	"gitlab.com/nikita.morozov/ms-shared/tools"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"log"
	"net"
	"os"
	"time"
)

const DEFAULT_ITEMS_PER_PAGE = 10

func main() {
	// Load env variables
	httpServerAddress := os.Getenv("HTTP_SERVER")
	grpcServerAddress := os.Getenv("GRPC_SERVER")
	redisAddress := os.Getenv("REDIS_SERVER")
	dbHost := os.Getenv("DB_HOST")
	dbUser := os.Getenv("DB_USERNAME")
	dbName := os.Getenv("DB_DATABASE")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbPort := os.Getenv("DB_PORT")
	initBots := os.Getenv("INIT_BOTS")
	env := os.Getenv("ENV")

	if len(env) == 0 {
		log.Fatal("ENV is not set")
	}

	// Init redis
	redisClient := redis.NewClient(&redis.Options{
		Addr:     redisAddress,
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	// Init DB
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s", dbHost, dbUser, dbPassword, dbName, dbPort)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix: "bots-ms_",
		},
	})

	if err != nil {
		log.Fatal(err)
		return
	}

	err = models.NewGormMigration(db).Migrate()
	if err != nil {
		log.Fatal(err)
		return
	}

	// Init echo server
	e := echo.New()
	e.HideBanner = true
	middL := middleware.InitMiddleware()
	e.Use(middL.CORS)
	e.HTTPErrorHandler = common.MsErrorHandler

	// Init grpc server
	lis, err := net.Listen("tcp", grpcServerAddress)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)

	// Init repository layer
	botRepo := gormHandlers.NewGormBotRepository(db, DEFAULT_ITEMS_PER_PAGE)
	botUserRepo := gormHandlers.NewGormBotUserRepository(db, DEFAULT_ITEMS_PER_PAGE)
	actionRepo := gormHandlers.NewGormActionRepository(db)
	eventRepo := gormHandlers.NewGormEventRepository(db, DEFAULT_ITEMS_PER_PAGE)
	authRequestRepo := gormHandlers.NewGormAuthRequestRepository(db)
	scenarioRepo := gormHandlers.NewGormScenarioRepository(db, DEFAULT_ITEMS_PER_PAGE)
	scenarioActionRepo := gormHandlers.NewGormScenarioActionRepository(db, 10)
	storedMessageRepo := gormHandlers.NewGormStoredMessageRepository(db)
	commonMessageRepo := gormHandlers.NewGormCommonMessageRepository(db)
	connectorRepo := gormHandlers.NewGormConnectorRepository(db)
	scenarioStorage := redisHandler.NewRedisStorageHandler("scenarios", redisClient, time.Duration(180)*time.Minute)
	triggerStorage := redisHandler.NewRedisStorageHandler("trigger", redisClient, time.Duration(15)*time.Minute)
	contextStorage := redisHandler.NewRedisStorageHandler("contexts", redisClient, time.Duration(180)*time.Minute)
	variablesStorage := redisHandler.NewRedisStorageHandler("variables", redisClient, time.Duration(15)*time.Minute)
	socketRepo := gormHandlers.NewGormSocketRepository(db)
	scenarioItemRepo := gormHandlers.NewGormScenarioItemRepository(db)
	stateMachineRepo := gormHandlers.NewGormStateMachineRepository(db, 10)
	stateRepo := gormHandlers.NewGormStateRepository(db)
	stateTransitionRepo := gormHandlers.NewGormStateTransitionRepository(db)

	// Init handlers
	handlerRegistry := registry.NewHandlerRegistry()
	botRegistry := registry.NewBotRegistry()

	// Init use cases layer
	actionUseCases := usecases.NewActionUsecase(actionRepo, handlerRegistry)
	eventUseCases := usecases.NewEventUsecase(eventRepo)
	botUseCases := usecases.NewBotUsecase(botRepo)
	botUserUseCases := usecases.NewBotUserUsecase(botUserRepo)
	authReqUseCases := usecases.NewAuthRequestUsecase(authRequestRepo)
	triggerUseCases := usecases.NewTriggerStorageUsecase(triggerStorage)
	contextStorageUseCases := usecases.NewContextStorageUsecase(contextStorage)
	scenarioUseCases := usecases.NewScenarioUsecase(scenarioRepo)
	scenarioActionUseCases := usecases.NewScenarioActionUsecase(scenarioActionRepo, scenarioRepo, connectorRepo)
	storedMessageUseCases := usecases.NewStoredMessageUsecase(storedMessageRepo)
	commonMessageUseCases := usecases.NewCommonMessageUsecase(commonMessageRepo)
	connectorUseCases := usecases.NewConnectorUsecase(connectorRepo)
	scenarioStorageUsecase := usecases.NewScenarioStorageUsecase(scenarioStorage)
	socketUsecase := usecases.NewSocketUsecase(socketRepo)
	variablesUsecase := usecases.NewVariableStorageUsecase(variablesStorage)
	scenarioItemUsecase := usecases.NewScenarioItemUsecase(scenarioItemRepo)
	stateMachineUsecase := usecases.NewStateMachineUsecase(stateMachineRepo, botRepo, stateRepo, scenarioRepo, connectorRepo, scenarioActionRepo, stateTransitionRepo)
	stateUsecase := usecases.NewStateUsecase(stateRepo, stateMachineRepo, stateTransitionRepo)
	stateTransitionUsecase := usecases.NewStateTransitionUsecase(stateTransitionRepo)

	// Init scenario engine
	engine := usecases.NewScenarioEngine(
		actionUseCases,
		scenarioUseCases,
		triggerUseCases,
		contextStorageUseCases,
		scenarioActionUseCases,
		connectorUseCases,
		scenarioStorageUsecase,
		socketUsecase,
	)

	// Init bot command executor
	stateManager := managers.NewUserStateManager(time.Duration(720)*time.Hour, variablesUsecase, stateMachineUsecase, stateUsecase)
	botExecutHandlers := map[string]commandHandler.CommandHandler{
		"event": commandHandler.NewEventCommandHandler(eventUseCases, scenarioUseCases),
		"state": commandHandler.NewStateCommandHandler(scenarioUseCases, stateManager),
	}
	botCommandExecutor := executor.NewBotExecutor(engine, commonMessageUseCases, botExecutHandlers)

	// Init bots
	initHandler := tools2.NewInitHandler(env, botCommandExecutor, botRepo, botRegistry)
	// Init bot handlers
	if initBots == "TRUE" {
		initHandler.InitBotList()
	}

	//moduleLoader := tools2.NewModuleLoader("./modules")
	//moduleLoader.Load()

	// Init method handlers
	sendMessageHandler := methodHandlers2.NewSendMessageHandler(botRegistry, commonMessageUseCases)
	sendMessageBroadcastHandler := methodHandlers2.NewSendMessageBroadcastHandler(botRegistry, botUseCases, botUserUseCases, storedMessageUseCases, commonMessageUseCases)
	sendMessageBroadcastStoredHandler := methodHandlers2.NewMessageBroadcastWithStoreHandler(botRegistry, botUseCases, botUserUseCases, storedMessageUseCases, commonMessageUseCases)
	getStoredMessageHandler := methodHandlers2.NewGetStoredMessageHandler(commonMessageUseCases, storedMessageUseCases)
	chatAuthHandler := methodHandlers2.NewChatAuthHandler(botUserUseCases, authReqUseCases)
	removeAuthByMetaHandler := methodHandlers2.NewRemoveAuthByMetaHandler(authReqUseCases)
	removeAuthByHashHandler := methodHandlers2.NewRemoveAuthByHashHandler(authReqUseCases)
	manualRegisterHandler := methodHandlers2.NewManualRegisterHandler(botRegistry, botUseCases, botUserUseCases)
	checkAuthHandler := methodHandlers2.NewCheckAuthHandler(botUserUseCases)
	editMessageHandler := methodHandlers2.NewEditMessageHandler(botRegistry, commonMessageUseCases)
	botEventHandler := methodHandlers2.NewBotEventHandler(variablesUsecase)
	inlineKeyboardHandler := methodHandlers2.NewInlineKeyboardHandler(8, botRegistry, triggerUseCases)
	httpRequestHandler := methodHandlers2.NewHttpRequestHandler(engine)
	httpRequestWithRefreshHandler := methodHandlers2.NewHttpRequestWithRefreshHandler(engine, variablesUsecase)
	mapJsonHandler := methodHandlers2.NewMapJsonHandler()
	getSenderByEIDHandler := methodHandlers2.NewAuthGetSenderByExternalIdHandler(botUserUseCases)
	getEIDBySenderHandler := methodHandlers2.NewAuthGetExternalIdBySenderHandler(botUserUseCases)
	inputTextHandler := methodHandlers2.NewTextInputHandler(botRegistry, triggerUseCases)
	replyKeyboardHandler := methodHandlers2.NewReplyKeyboardHandler(8, botRegistry, triggerUseCases)
	removeKeyboardHandler := methodHandlers2.NewRemoveKeyboardHandler(botRegistry)
	setTypingHandler := methodHandlers2.NewSendTypingHandler(botRegistry)
	removeMessageHandler := methodHandlers2.NewRemoveMessageHandler(botRegistry)
	setStringVariableHandler := methodHandlers2.NewSetStringVariableHandler(variablesUsecase)
	getStringVariableHandler := methodHandlers2.NewGetStringVariableHandler(variablesUsecase)
	setIntVariableHandler := methodHandlers2.NewSetIntVariableHandler(variablesUsecase)
	getIntVariableHandler := methodHandlers2.NewGetIntVariableHandler(variablesUsecase)
	executeCommandHandler := methodHandlers2.NewExecuteCommandHandler(botCommandExecutor, variablesUsecase, botUseCases)
	saveMessageHandler := methodHandlers2.NewSaveMessageHandler(botRegistry, commonMessageUseCases)
	loadMessageHandler := methodHandlers2.NewLoadMessageHandler(botRegistry, commonMessageUseCases)
	removeSavedMessageHandler := methodHandlers2.NewRemoveSavedMessageHandler(commonMessageUseCases)

	handlerRegistry.Add("sendMessage", &sendMessageHandler)
	handlerRegistry.Add("sendMessageBroadcast", &sendMessageBroadcastHandler)
	handlerRegistry.Add("sendMessageStoredBroadcast", &sendMessageBroadcastStoredHandler)
	handlerRegistry.Add("sendMessageStoredBroadcast", &sendMessageBroadcastStoredHandler)
	handlerRegistry.Add("getStoredMessage", &getStoredMessageHandler)
	handlerRegistry.Add("registerUser", &chatAuthHandler)
	handlerRegistry.Add("manualRegisterUser", &manualRegisterHandler)
	handlerRegistry.Add("checkAuth", &checkAuthHandler)
	handlerRegistry.Add("removeAuthByMetaHandler", &removeAuthByMetaHandler)
	handlerRegistry.Add("removeAuthByHashHandler", &removeAuthByHashHandler)
	handlerRegistry.Add("editMessage", &editMessageHandler)
	handlerRegistry.Add("botEvent", &botEventHandler)
	handlerRegistry.Add("inlineKeyboard", &inlineKeyboardHandler)
	handlerRegistry.Add("httpRequest", &httpRequestHandler)
	handlerRegistry.Add("httpRequestWithRefresh", &httpRequestWithRefreshHandler)
	handlerRegistry.Add("mapJson", &mapJsonHandler)
	handlerRegistry.Add("getSenderByEID", &getSenderByEIDHandler)
	handlerRegistry.Add("getEIDBySender", &getEIDBySenderHandler)
	handlerRegistry.Add("textInput", &inputTextHandler)
	handlerRegistry.Add("replyKeyboard", &replyKeyboardHandler)
	handlerRegistry.Add("removeKeyboard", &removeKeyboardHandler)
	handlerRegistry.Add("setTyping", &setTypingHandler)
	handlerRegistry.Add("removeMessage", &removeMessageHandler)
	handlerRegistry.Add("variables.set-string", &setStringVariableHandler)
	handlerRegistry.Add("variables.get-string", &getStringVariableHandler)
	handlerRegistry.Add("variables.set-int", &setIntVariableHandler)
	handlerRegistry.Add("variables.get-int", &getIntVariableHandler)
	handlerRegistry.Add("executeCommand", &executeCommandHandler)
	handlerRegistry.Add("saveMessage", &saveMessageHandler)
	handlerRegistry.Add("loadMessage", &loadMessageHandler)
	handlerRegistry.Add("removeSavedMessage", &removeSavedMessageHandler)

	//handlerRegistry.AddFromModules(*coreModule.Module.GetHandlers())

	// Init grpc delivery layer
	grpchandler.NewActionGrpcHandler(grpcServer, actionUseCases)
	grpchandler.NewAuthRequestGrpcHandler(grpcServer, authReqUseCases)
	grpchandler.NewBotGrpcHandler(grpcServer, botUseCases, initHandler)
	grpchandler.NewBotUserGrpcHandler(grpcServer, botUserUseCases)
	grpchandler.NewEventGrpcHandler(grpcServer, eventUseCases, engine)
	grpchandler.NewScenarioGrpcHandler(grpcServer, scenarioUseCases)
	grpchandler.NewScenarioActionGrpcHandler(grpcServer, scenarioActionUseCases)
	grpchandler.NewConnectorGrpcHandler(grpcServer, connectorUseCases)
	grpchandler.NewScenarioItemGrpcHandler(grpcServer, scenarioItemUsecase)
	grpchandler.NewStateMachineGrpcHandler(grpcServer, stateMachineUsecase)
	grpchandler.NewStateGrpcHandler(grpcServer, stateUsecase)
	grpchandler.NewStateTransitionGrpcHandler(grpcServer, stateTransitionUsecase)

	// Init http delivery layer
	httphandler.NewHttpEventHandler(e, botUseCases, eventUseCases, engine, botCommandExecutor)
	httphandler.NewHttpAuthRequestHandler(e, authReqUseCases, botUseCases)
	httphandler.NewHttpBotUserHandler(e, botUserUseCases, botUseCases)
	httphandler.NewSerializationHttpHandler(e, stateMachineUsecase)

	grpc_health_v1.RegisterHealthServer(grpcServer, health.NewServer())

	coreModule := core.ExportModuleHandler{}

	// Init modules
	seedRunner := seeder.NewSeederRunner(db, "bots-ms_seeders")

	handlerRegistry.AddFromModules(coreModule.GetHandlers())
	log.Printf("🔌 Init handlers: %s completed ✅", coreModule.GetName())
	seedRunner.Add(coreModule.GetSeeds())
	log.Printf("🔌 Init seeders: %s completed ✅", coreModule.GetName())

	seedRunner.Run()
	// Init modules end

	// Banner
	tools.DisplayServiceName("bots-ms")

	go func() {
		// Start HTTP server
		err = e.Start(httpServerAddress)
		if err != nil {
			log.Fatal(err)
		}
	}()

	// Start GRPC server
	err = grpcServer.Serve(lis)
	if err != nil {
		log.Fatal(err)
	}
}
