package methodHandlers

import (
	"bots-ms/usecases"
	"bytes"
	"fmt"
	"github.com/tidwall/gjson"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"io"
	"net/http"
	"strings"
	"time"
)

type httpRequestWithRefreshHandler struct {
	asyncExecutor usecases.AsyncExecutor
	storage       usecases.VariableStorageUsecase
}

// XXX Custom
func (h *httpRequestWithRefreshHandler) Execute(params models.NodeParams) models.NodeData {
	context := params.GetString("context")
	scenarioActionId := params.GetUint("scenarioActionId")

	urlPath := params.GetString("url")
	method := params.GetString("method")
	contentType := params.GetString("contentType")
	authorizationKey := params.GetString("authorizationKey")
	authorizationPrefix := params.GetString("authorizationPrefix")
	refreshUrlPath := params.GetString("refreshUrl")
	refreshUrlPostfix := params.GetString("refreshUrlPostfix")

	resp := models.NodeData{}

	go func() {
		if contentType == "" {
			contentType = "application/json"
		}

		token := ""
		useToken := len(authorizationKey) > 0
		if useToken {
			token, _ = h.storage.GetString(context, authorizationKey)
		}

		client := &http.Client{
			Timeout: time.Second * 5,
		}
		data := params.GetByte("data")
		req, err := http.NewRequest(strings.ToUpper(method), urlPath, bytes.NewBuffer(data))
		if err != nil {
			resp.Error(err.Error())
			return
		}

		req.Header.Add("Content-Type", contentType)

		if useToken {
			req.Header.Add("Authorization", token)
		}

		response, err := client.Do(req)
		if err == nil {
			defer response.Body.Close()
		}

		if response.StatusCode >= 400 && response.StatusCode < 500 && len(refreshUrlPath) > 0 {
			respRefresh, err := http.Get(fmt.Sprintf("%s%s", refreshUrlPath, refreshUrlPostfix))
			if err != nil {
				resp.Error(err.Error())
				return
			}

			defer respRefresh.Body.Close()
			body, err := io.ReadAll(respRefresh.Body)
			newTokenValue := gjson.Get(string(body), "token")
			token = fmt.Sprintf("%s%s", authorizationPrefix, newTokenValue)

			err = h.storage.SetString(context, authorizationKey, token)
			if err != nil {
				resp.Error(err.Error())
				return
			}
			req, err = http.NewRequest(strings.ToUpper(method), urlPath, bytes.NewBuffer(data))
			if err != nil {
				resp.Error(err.Error())
				return
			}

			req.Header.Set("Content-Type", contentType)
			req.Header.Set("Authorization", token)

			response, err = client.Do(req)
			if err != nil {
				resp.Error(err.Error())
				return
			}
			defer response.Body.Close()
		}

		body, err := io.ReadAll(response.Body)
		if err != nil {
			resp.Error(err.Error())
			return
		}

		resp.Set("response", string(body))
		resp.Set("status", response.Status)

		if response.StatusCode >= 400 {
			resp.Error("httpRequestWithRefreshHandler.execute.failed")
			h.asyncExecutor.FinishExecutionWithParams(context, scenarioActionId, resp)
			return
		}

		resp.Complete(nil)

		h.asyncExecutor.FinishExecutionWithParams(context, scenarioActionId, resp)
	}()

	return nil
}

func NewHttpRequestWithRefreshHandler(asyncExecutor usecases.AsyncExecutor, storage usecases.VariableStorageUsecase) models.IHandler {
	return &httpRequestWithRefreshHandler{
		asyncExecutor: asyncExecutor,
		storage:       storage,
	}
}
