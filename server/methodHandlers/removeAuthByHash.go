package methodHandlers

import (
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"log"
)

type removeAuthByHashHandler struct {
	authReqUC usecases.AuthRequestUsecase
}

func (h *removeAuthByHashHandler) Execute(params models.NodeParams) models.NodeData {
	hash := params.GetString("hash")

	resp := models.NodeDataFromParams(params)

	var err error
	log.Printf("🤖 Remove auth handler by hash executing %s and params: %s", hash, params)

	auth, err := h.authReqUC.GetByHash(hash)
	if auth != nil || err != nil {
		return resp.Error("bot-ms.auth-handler.auth-request-not-found")
	}

	err = h.authReqUC.Delete(auth.ID)
	if err != nil {
		return resp.Error(err.Error())
	}

	return resp.Complete(nil)
}

func NewRemoveAuthByHashHandler(authReqUC usecases.AuthRequestUsecase) models.IHandler {
	return &removeAuthByMetaHandler{
		authReqUC: authReqUC,
	}
}
