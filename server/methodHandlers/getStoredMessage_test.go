package methodHandlers

import (
	"bots-ms/models"
	"bots-ms/usecases/mocks"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"gorm.io/gorm"
	"testing"
)

type GetStoredMessageSuite struct {
	suite.Suite

	commonUc *mocks.CommonMessageUsecase
	storedUc *mocks.StoredMessageUsecase
	handler  coreModel.IHandler
}

func (s *GetStoredMessageSuite) BeforeTest(_, _ string) {
	commonUc := new(mocks.CommonMessageUsecase)
	storedUc := new(mocks.StoredMessageUsecase)

	s.handler = NewGetStoredMessageHandler(commonUc, storedUc)

	s.commonUc = commonUc
	s.storedUc = storedUc
}

func TestGetStoredMessage(t *testing.T) {
	suite.Run(t, new(GetStoredMessageSuite))
}

// ------------------------------------------------
//
// # Execute
//
// ------------------------------------------------
func (s *GetStoredMessageSuite) Test_getStoredMessage_Execute() {
	params := coreModel.NodeParams{
		"messageId": []byte("messageId12"),
	}

	messageId := uint(12)
	mockMessages := models.CommonMessage{
		MessageId: &messageId,
		Data:      "Test",
	}

	mockStore := models.StoredMessage{
		Model: gorm.Model{
			ID: uint(1),
		},
		Text: "textValue",
	}

	s.commonUc.On("Get", "messageId12").Return(&mockMessages, nil)
	s.storedUc.On("Get", uint(12)).Return(&mockStore, nil)

	data := s.handler.Execute(params)

	require.True(s.T(), data.IsComplete())
	require.Equal(s.T(), "textValue", data.GetString("text"))
	require.Equal(s.T(), uint(1), data.GetUint("storedMessageId"))
}

// ------------------------------------------------
//
// Execute_NoStoredMsg
//
// ------------------------------------------------
func (s *GetStoredMessageSuite) Test_getStoredMessage_Execute_NoStoredMsg() {
	params := coreModel.NodeParams{
		"messageId": []byte("messageId12"),
	}

	messageId := uint(12)
	mockMessages := models.CommonMessage{
		MessageId: &messageId,
		Data:      "Test",
	}

	s.commonUc.On("Get", "messageId12").Return(&mockMessages, nil)
	s.storedUc.On("Get", uint(12)).Return(nil, nil)

	data := s.handler.Execute(params)

	require.True(s.T(), data.IsError())
	require.Equal(s.T(), "Stored message not found", data.GetError())
}

// ------------------------------------------------
//
// Execute_NoCommonMsg
//
// ------------------------------------------------
func (s *GetStoredMessageSuite) Test_getStoredMessage_Execute_NoCommonMsg() {
	params := coreModel.NodeParams{
		"messageId": []byte("messageId12"),
	}

	s.commonUc.On("Get", "messageId12").Return(nil, nil)

	data := s.handler.Execute(params)

	require.True(s.T(), data.IsError())
	require.Equal(s.T(), "Common message not found", data.GetError())
}

// ------------------------------------------------
//
// Execute_NoMsgId
//
// ------------------------------------------------
func (s *GetStoredMessageSuite) Test_getStoredMessage_Execute_NoMsgId() {
	params := coreModel.NodeParams{}

	data := s.handler.Execute(params)

	require.True(s.T(), data.IsError())
	require.Equal(s.T(), "Edit action: Message ID is empty", data.GetError())
}
