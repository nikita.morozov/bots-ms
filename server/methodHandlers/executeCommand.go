package methodHandlers

import (
	"bots-ms/executor"
	"bots-ms/models/dto"
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type executeCommandHandler struct {
	executor executor.BotExecutor
	storage  usecases.VariableStorageUsecase
	botUC    usecases.BotUsecase
}

func (h executeCommandHandler) Execute(params models.NodeParams) models.NodeData {
	context := params.GetString("context")
	command := params.GetString("command")

	sender, err := h.storage.GetString(context, "sender")
	messageId, err := h.storage.GetString(context, "messageId")
	nickname, err := h.storage.GetString(context, "nickname")
	botId, err := h.storage.GetInt(context, "botId")

	if err != nil {
		return models.NodeDataError(err.Error())
	}

	bot, err := h.botUC.Get(uint(botId))
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	err = h.executor.ExecuteCommand(
		command,
		dto.BotContentDto{
			Bot:       *bot,
			Sender:    sender,
			MessageId: messageId,
			Nickname:  nickname,
		},
	)

	if err != nil {
		return models.NodeDataError(err.Error())
	}

	return models.NodeDataComplete(nil)
}

func NewExecuteCommandHandler(executor executor.BotExecutor, storage usecases.VariableStorageUsecase, botUC usecases.BotUsecase) models.IHandler {
	return &executeCommandHandler{
		executor: executor,
		storage:  storage,
		botUC:    botUC,
	}
}
