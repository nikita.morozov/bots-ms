package methodHandlers

import (
	"bots-ms/registry"
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type loadMessageHandler struct {
	registry        registry.BotRegistry
	commonMessageUC usecases.CommonMessageUsecase
}

func (h loadMessageHandler) Execute(params models.NodeParams) models.NodeData {
	messageId := params.GetString("messageId")

	msg, err := h.commonMessageUC.GetByMessageId(messageId)
	if err != nil || msg == nil {
		return models.NodeDataError(err.Error())
	}

	resp := models.NodeData{}
	resp.Set("text", msg.Message.Text)
	resp.Set("data", msg.Data)
	return resp.Complete(nil)
}

func NewLoadMessageHandler(registry registry.BotRegistry, commonMessageUC usecases.CommonMessageUsecase) models.IHandler {
	return &loadMessageHandler{
		registry:        registry,
		commonMessageUC: commonMessageUC,
	}
}
