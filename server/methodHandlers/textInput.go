package methodHandlers

import (
	models2 "bots-ms/models"
	"bots-ms/registry"
	"bots-ms/usecases"
	"fmt"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type textInputHandler struct {
	registry       registry.BotRegistry
	triggerStorage usecases.TriggerStorageUsecase
}

func (h *textInputHandler) Execute(params models.NodeParams) models.NodeData {
	context := params.GetString("context")
	scenarioActionId := params.GetUint("scenarioActionId")

	h.triggerStorage.Set(context, "input", &models2.TriggerData{
		EventName:        fmt.Sprintf(models.NodeDataOnComplete),
		ContentField:     "outValue",
		ScenarioActionID: scenarioActionId,
	})

	return nil
}

func NewTextInputHandler(registry registry.BotRegistry, triggerStorage usecases.TriggerStorageUsecase) models.IHandler {
	return &textInputHandler{
		triggerStorage: triggerStorage,
		registry:       registry,
	}
}
