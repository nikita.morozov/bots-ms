package methodHandlers

import (
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"log"
)

type authGetSenderByExternalIdHandler struct {
	userUC usecases.BotUserUsecase
}

// Execute
// In params:
// • botId
// • sender
// Response
// • OnComplete
// • OnError

func (h *authGetSenderByExternalIdHandler) Execute(params models.NodeParams) models.NodeData {
	botId := params.GetUint("botId")
	meta := params.GetString("externalId")

	log.Printf("🤖 Auth get sender by external id executing %s and botId: %d", meta, botId)
	user, _ := h.userUC.GetByMeta(botId, meta)
	if user != nil && !user.Blocked {
		resp := models.NodeData{}
		resp.Set("sender", user.UserID)
		resp.Set("nickname", user.Nickname)
		return resp.Complete(nil)
	}

	return models.NodeDataError("bot-ms.auth-user.not-found")
}

func NewAuthGetSenderByExternalIdHandler(userUC usecases.BotUserUsecase) models.IHandler {
	return &authGetSenderByExternalIdHandler{
		userUC: userUC,
	}
}
