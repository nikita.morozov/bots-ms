package methodHandlers

import (
	"bots-ms/models"
	"bots-ms/usecases/mocks"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"gorm.io/gorm"
	"testing"
	"time"
)

type CheckAuthHandlerSuite struct {
	suite.Suite

	userUc  *mocks.BotUserUsecase
	handler coreModel.IHandler
}

func (s *CheckAuthHandlerSuite) BeforeTest(_, _ string) {
	userUc := new(mocks.BotUserUsecase)
	s.handler = NewCheckAuthHandler(userUc)
	s.userUc = userUc
}

func TestCheckAuthHandler(t *testing.T) {
	suite.Run(t, new(CheckAuthHandlerSuite))
}

// ------------------------------------------------
//
// # Execute
//
// ------------------------------------------------
func (s *CheckAuthHandlerSuite) Test_checkAuth_Execute() {
	params := coreModel.NodeParams{
		"botId":  []byte("23"),
		"sender": []byte("testUser"),
	}

	botUser := &models.BotUser{
		Model: gorm.Model{
			ID:        uint(23),
			CreatedAt: time.Now(),
		},
	}

	s.userUc.On("GetByUserId", uint(23), "testUser").Return(botUser, nil)

	data := s.handler.Execute(params)

	require.True(s.T(), data.IsComplete())
}

// ------------------------------------------------
//
// Execute_NotFound
//
// ------------------------------------------------
func (s *CheckAuthHandlerSuite) Test_checkAuth_Execute_NotFound() {
	params := coreModel.NodeParams{
		"botId":  []byte("23"),
		"sender": []byte("testUser"),
	}

	s.userUc.On("GetByUserId", uint(23), "testUser").Return(nil, nil)

	data := s.handler.Execute(params)

	require.True(s.T(), data.IsError())
	require.Equal(s.T(), "bot-ms.check-auth-handler.user-access-denied", data.GetError())
}
