package methodHandlers

import (
	"bots-ms/tools"
	"encoding/json"
	"github.com/stretchr/testify/require"
	"github.com/tidwall/gjson"
	"net/url"
	"testing"
)

func TestMapJson(t *testing.T) {
	data := "{\"items\": [{\"command\": \"command1\", \"scenarioId\": 1},\n{\"command\": \"command2\", \"scenarioId\": 2}]}"
	q := "items.0.command"
	value := gjson.Get(data, q)

	require.Equal(t, value.Str, "command1")

	mapData := "{\"command\": \"command1\", \"scenarioId\": 1}"
	var dat map[string]interface{}
	err := json.Unmarshal([]byte(mapData), &dat)
	require.NoError(t, err)

	require.Equal(t, "command1", dat["command"])
	require.Equal(t, float64(1), dat["scenarioId"])

	mockPostValue := url.Values{
		"command":    {"command1"},
		"scenarioId": {"1.0000"},
	}
	postValues := tools.MapToValues(dat)
	require.Equal(t, mockPostValue, postValues)
}

func BenchmarkMapJson(b *testing.B) {
	data := "{\"items\": [{\"command\": \"command1\", \"scenarioId\": 1},\n{\"command\": \"command2\", \"scenarioId\": 2}]}"
	q := "items.0.command"
	for i := 0; i < b.N; i++ {
		gjson.Get(data, q)
	}
}
