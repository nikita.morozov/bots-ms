package methodHandlers

import (
	"bots-ms/usecases/mocks"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

type BotEventHandlerSuite struct {
	suite.Suite
	storage *mocks.VariableStorageUsecase

	handler coreModel.IHandler
}

func (s *BotEventHandlerSuite) BeforeTest(_, _ string) {
	storage := new(mocks.VariableStorageUsecase)
	s.storage = storage

	s.handler = NewBotEventHandler(storage)
}

func TestBotEventHandler(t *testing.T) {
	suite.Run(t, new(BotEventHandlerSuite))
}

// ------------------------------------------------
//
// # Execute
//
// ------------------------------------------------
func (s *BotEventHandlerSuite) Test_checkAuth_Execute() {
	params := coreModel.NodeParams{
		"context": []byte("textContext"),
		"botId":   []byte("23"),
		"sender":  []byte("testUser"),
	}

	s.storage.On("SetString", "textContext", "sender", "testUser").Return(nil)
	s.storage.On("SetInt", "textContext", "botId", 23).Return(nil)

	data := s.handler.Execute(params)

	require.True(s.T(), data.IsComplete())
	require.Equal(s.T(), uint(23), data.GetUint("botId"))
	require.Equal(s.T(), "testUser", data.GetString("sender"))
}
