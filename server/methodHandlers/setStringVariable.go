package methodHandlers

import (
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"time"
)

type setStringVariableHandler struct {
	storage usecases.VariableStorageUsecase
}

func (h setStringVariableHandler) Execute(params models.NodeParams) models.NodeData {
	context := params.GetString("customContext")
	if len(context) == 0 {
		context = params.GetString("context")
	}

	key := params.GetString("key")
	value := params.GetString("value")
	duration := params.GetUint("duration")

	err := h.storage.SetStringWithDuration(context, key, value, time.Duration(duration)*time.Minute)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	return models.NodeDataComplete(nil)
}

func NewSetStringVariableHandler(storage usecases.VariableStorageUsecase) models.IHandler {
	return &setStringVariableHandler{
		storage: storage,
	}
}
