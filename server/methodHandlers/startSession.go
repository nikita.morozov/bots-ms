package methodHandlers

import (
	"bots-ms/usecases"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
)

type startSessionHandler struct {
	contextStorage usecases.ContextStorageUserCase
}

// Execute
// IN
// botHandler
// sender
// context
//
// OUT
// onComplete
// onError
// session
func (h *startSessionHandler) Execute(params coreModel.NodeParams) coreModel.NodeData {
	botHandler := params.GetString("botHandler")
	sender := params.GetString("sender")
	context := params.GetString("context")

	err := h.contextStorage.Set(botHandler, sender, context)
	if err != nil {
		return coreModel.NodeDataError(err.Error())
	}

	res := coreModel.NodeData{}
	res.Set("session", context)

	return res.Complete(nil)
}

func NewStartSessionHandler(contextStorage usecases.ContextStorageUserCase) coreModel.IHandler {
	return &startSessionHandler{
		contextStorage: contextStorage,
	}
}
