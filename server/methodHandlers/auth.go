package methodHandlers

import (
	models2 "bots-ms/models"
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"log"
)

type chatAuthHandler struct {
	userUC    usecases.BotUserUsecase
	authReqUC usecases.AuthRequestUsecase
}

// Execute Inner params
// botId
// sender
// hash
func (h *chatAuthHandler) Execute(params models.NodeParams) models.NodeData {
	botId := params.GetUint("botId")
	sender := params.GetString("sender")
	nickname := params.GetString("nickname")
	hash := params.GetString("hash")

	resp := models.NodeDataFromParams(params)

	var err error
	log.Printf("🤖 Auth handler executing %s and params: %s", sender, params)

	var user *models2.BotUser
	user, _ = h.userUC.GetByUserId(botId, sender)
	if user != nil {
		return resp.Error("bot-ms.auth-handler.user-already-exist")
	}

	var req *models2.AuthRequest
	req, _ = h.authReqUC.GetByHash(hash)
	if req == nil {
		return resp.Error("bot-ms.auth-handler.user-auth-not-found")
	}

	err = h.authReqUC.Delete(req.ID)
	if err != nil {
		return resp.Error("bot-ms.auth-handler.user-auth-delete-error")
	}

	var newUser = models2.BotUser{
		UserID:   sender,
		BotID:    botId,
		Nickname: nickname,
		Token:    hash,
		Meta:     req.Meta,
	}
	err = h.userUC.Create(&newUser)
	if err != nil {
		return resp.Error("bot-ms.auth-handler.user-create-error")
	}

	return resp.Complete(nil)
}

func NewChatAuthHandler(userUC usecases.BotUserUsecase, authReqUC usecases.AuthRequestUsecase) models.IHandler {
	return &chatAuthHandler{
		userUC:    userUC,
		authReqUC: authReqUC,
	}
}
