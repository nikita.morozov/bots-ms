package methodHandlers

import (
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"time"
)

type setIntVariableHandler struct {
	storage usecases.VariableStorageUsecase
}

func (h setIntVariableHandler) Execute(params models.NodeParams) models.NodeData {
	context := params.GetString("customContext")
	if len(context) == 0 {
		context = params.GetString("context")
	}

	key := params.GetString("key")
	value := params.GetUint("value")
	duration := params.GetUint("duration")

	err := h.storage.SetIntWithDuration(context, key, int(value), time.Duration(duration)*time.Minute)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	return models.NodeDataComplete(nil)
}

func NewSetIntVariableHandler(storage usecases.VariableStorageUsecase) models.IHandler {
	return &setIntVariableHandler{
		storage: storage,
	}
}
