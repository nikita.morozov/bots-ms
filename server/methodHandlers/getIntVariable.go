package methodHandlers

import (
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type getIntVariableHandler struct {
	storage usecases.VariableStorageUsecase
}

func (h getIntVariableHandler) Execute(params models.NodeParams) models.NodeData {
	context := params.GetString("customContext")
	if len(context) == 0 {
		context = params.GetString("context")
	}
	key := params.GetString("key")

	val, err := h.storage.GetInt(context, key)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	res := models.NodeData{}
	res.SetUint("outValue", uint(val))
	return res.Complete(nil)
}

func NewGetIntVariableHandler(storage usecases.VariableStorageUsecase) models.IHandler {
	return &getIntVariableHandler{
		storage: storage,
	}
}
