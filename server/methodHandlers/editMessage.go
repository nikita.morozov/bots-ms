package methodHandlers

import (
	"bots-ms/registry"
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type editMessageHandler struct {
	registry        registry.BotRegistry
	commonMessageUC usecases.CommonMessageUsecase
}

func (h editMessageHandler) Execute(params models.NodeParams) models.NodeData {
	text := params.GetString("text")
	botSlug := params.GetString("botSlug")
	messageId := params.GetString("messageId")

	handler, err := h.registry.Get(botSlug)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	msg, _ := h.commonMessageUC.GetByMessageId(messageId)
	if msg == nil {
		return models.NodeDataError("Message not found")
	}

	(*handler).Edit(messageId, text)

	return models.NodeDataComplete(nil)
}

func NewEditMessageHandler(registry registry.BotRegistry, commonMessageUC usecases.CommonMessageUsecase) models.IHandler {
	return &editMessageHandler{
		registry:        registry,
		commonMessageUC: commonMessageUC,
	}
}
