package methodHandlers

import (
	models2 "bots-ms/models"
	"bots-ms/registry"
	"fmt"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type removeMessageHandler struct {
	registry registry.BotRegistry
}

func (h removeMessageHandler) Execute(params models.NodeParams) models.NodeData {
	sender := params.GetString("sender")
	botSlug := params.GetString("botSlug")
	messageId := params.GetString("messageId")

	botHandler, err := h.registry.Get(botSlug)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	(*botHandler).Delete(models2.CommonMessage{
		Data: fmt.Sprintf("%s_%s", messageId, sender),
	})

	return models.NodeDataComplete(nil)
}

func NewRemoveMessageHandler(registry registry.BotRegistry) models.IHandler {
	return &removeMessageHandler{
		registry: registry,
	}
}
