package methodHandlers

import (
	"bots-ms/registry"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type sendTypingHandler struct {
	registry registry.BotRegistry
}

func (h *sendTypingHandler) Execute(params models.NodeParams) models.NodeData {
	sender := params.GetString("sender")
	botSlug := params.GetString("botSlug")

	botHandler, err := h.registry.Get(botSlug)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	(*botHandler).SendAction(sender, "typing")
	return models.NodeDataComplete(nil)
}

func NewSendTypingHandler(registry registry.BotRegistry) models.IHandler {
	return &sendTypingHandler{
		registry: registry,
	}
}
