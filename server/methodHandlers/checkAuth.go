package methodHandlers

import (
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"log"
)

type checkAuthHandler struct {
	userUC usecases.BotUserUsecase
}

// Execute
// In params:
// • botId
// • sender
// Response
// • OnComplete
// • OnError

func (h checkAuthHandler) Execute(params models.NodeParams) models.NodeData {
	botId := params.GetUint("botId")
	sender := params.GetString("sender")

	log.Printf("🤖 Auth check handler executing %s", sender)
	user, _ := h.userUC.GetByUserId(botId, sender)
	if user != nil && !user.Blocked {
		return models.NodeDataComplete(nil)
	}

	return models.NodeDataError("bot-ms.check-auth-handler.user-access-denied")
}

func NewCheckAuthHandler(userUC usecases.BotUserUsecase) models.IHandler {
	return &checkAuthHandler{
		userUC: userUC,
	}
}
