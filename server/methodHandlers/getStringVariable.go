package methodHandlers

import (
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type getStringVariableHandler struct {
	storage usecases.VariableStorageUsecase
}

func (h getStringVariableHandler) Execute(params models.NodeParams) models.NodeData {
	context := params.GetString("customContext")
	if len(context) == 0 {
		context = params.GetString("context")
	}
	key := params.GetString("key")

	val, err := h.storage.GetString(context, key)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	res := models.NodeData{}
	res.Set("outValue", val)
	return res.Complete(nil)
}

func NewGetStringVariableHandler(storage usecases.VariableStorageUsecase) models.IHandler {
	return &getStringVariableHandler{
		storage: storage,
	}
}
