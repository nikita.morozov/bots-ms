package methodHandlers

import (
	"bots-ms/usecases"
	"bytes"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"io"
	"net/http"
	"strings"
	"time"
)

type httpRequestHandler struct {
	asyncExecutor usecases.AsyncExecutor
}

func (h *httpRequestHandler) Execute(params models.NodeParams) models.NodeData {
	context := params.GetString("context")
	scenarioActionId := params.GetUint("scenarioActionId")

	urlPath := params.GetString("url")
	method := params.GetString("method")
	contentType := params.GetString("contentType")
	authorization := params.GetString("authorization")

	resp := models.NodeData{}

	go func() {
		if contentType == "" {
			contentType = "application/json"
		}

		client := &http.Client{
			Timeout: time.Second * 5,
		}
		data := params.GetByte("data")
		req, err := http.NewRequest(strings.ToUpper(method), urlPath, bytes.NewBuffer(data))
		if err != nil {
			resp.Error(err.Error())
			return
		}

		req.Header.Add("Content-Type", contentType)

		if len(authorization) > 0 {
			req.Header.Add("Authorization", authorization)
		}

		response, err := client.Do(req)
		if err != nil {
			resp.Error(err.Error())
			return
		}
		defer response.Body.Close()

		body, err := io.ReadAll(response.Body)
		if err != nil {
			resp.Error(err.Error())
			return
		}

		resp.Set("response", string(body))
		resp.Set("status", response.Status)

		if response.StatusCode >= 400 {
			resp.Error("httpRequestHandler.execute.failed")
			h.asyncExecutor.FinishExecutionWithParams(context, scenarioActionId, resp)
			return
		}

		resp.Complete(nil)

		h.asyncExecutor.FinishExecutionWithParams(context, scenarioActionId, resp)
	}()

	return nil
}

func NewHttpRequestHandler(asyncExecutor usecases.AsyncExecutor) models.IHandler {
	return &httpRequestHandler{
		asyncExecutor: asyncExecutor,
	}
}
