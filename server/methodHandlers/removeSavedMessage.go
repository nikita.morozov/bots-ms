package methodHandlers

import (
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type removeSavedMessageHandler struct {
	commonMessageUC usecases.CommonMessageUsecase
}

func (h removeSavedMessageHandler) Execute(params models.NodeParams) models.NodeData {
	messageId := params.GetString("messageId")

	msg, err := h.commonMessageUC.GetByMessageId(messageId)
	if msg == nil {
		return models.NodeDataError("Message not found")
	}

	err = h.commonMessageUC.Delete(msg)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	return models.NodeDataComplete(nil)
}

func NewRemoveSavedMessageHandler(commonMessageUC usecases.CommonMessageUsecase) models.IHandler {
	return &removeSavedMessageHandler{
		commonMessageUC: commonMessageUC,
	}
}
