package methodHandlers

import (
	models2 "bots-ms/models"
	"bots-ms/registry"
	"bots-ms/usecases"
	"fmt"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"log"
)

type handler struct {
	registry        registry.BotRegistry
	commonMessageUC usecases.CommonMessageUsecase
}

func (h handler) Execute(params models.NodeParams) models.NodeData {
	text := params.GetString("text")
	sender := params.GetString("sender")
	botSlug := params.GetString("botSlug")
	replaceable := params.GetBool("replaceable")
	markdown := params.GetString("markdown")

	var extra map[string]string
	if len(markdown) > 0 {
		extra = map[string]string{"markdown": markdown}
	}

	botHandler, err := h.registry.Get(botSlug)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	if replaceable {
		list, err := h.commonMessageUC.ListBySender(sender)
		if err != nil {
			return models.NodeDataError(err.Error())
		}

		for _, message := range *list {
			(*botHandler).Delete(message)
			h.commonMessageUC.Delete(&message)
		}
	}

	if len(text) == 0 {
		return models.NodeDataError("No text value")
	}

	log.Printf("🤖 Send message executed %s and data: %s", sender, text)

	messageId := (*botHandler).Send(sender, text, extra)
	if replaceable {
		var item = models2.CommonMessage{
			Data: messageId.(string),
		}
		err := h.commonMessageUC.Create(&item)
		if err != nil {
			return models.NodeDataError(err.Error())
		}
	}

	resp := models.NodeData{}
	switch v := messageId.(type) {
	case string:
		resp.Set("messageId", v)
	default:
		fmt.Println("Send message: messageId type is not recognized")
	}
	return resp.Complete(nil)
}

func NewSendMessageHandler(registry registry.BotRegistry, commonMessageUC usecases.CommonMessageUsecase) models.IHandler {
	return &handler{
		registry:        registry,
		commonMessageUC: commonMessageUC,
	}
}
