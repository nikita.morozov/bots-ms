package methodHandlers

import (
	"bots-ms/models"
	modelMock "bots-ms/models/mocks"
	regMocks "bots-ms/registry/mocks"
	"bots-ms/usecases/mocks"
	"errors"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

type EditMessageSuite struct {
	suite.Suite

	reg      *regMocks.BotRegistry
	commonUc *mocks.CommonMessageUsecase
	storedUc *mocks.StoredMessageUsecase
	handler  coreModel.IHandler
}

func (s *EditMessageSuite) BeforeTest(_, _ string) {
	reg := new(regMocks.BotRegistry)
	commonUc := new(mocks.CommonMessageUsecase)

	s.handler = NewEditMessageHandler(reg, commonUc)

	s.reg = reg
	s.commonUc = commonUc
}

func TestEditMessage(t *testing.T) {
	suite.Run(t, new(EditMessageSuite))
}

// ------------------------------------------------
//
// # Execute
//
// ------------------------------------------------
func (s *EditMessageSuite) Test_editMessage_Execute() {
	params := coreModel.NodeParams{
		"text":      []byte("Hello text"),
		"botSlug":   []byte("botSlugValue"),
		"messageId": []byte("12"),
	}

	messageId := uint(12)
	mockMessages := models.CommonMessage{
		MessageId: &messageId,
		Data:      "Test",
	}

	var handler models.IBotHandler
	methodHandler := new(modelMock.IBotHandler)
	handler = methodHandler

	s.reg.On("Get", "botSlugValue").Return(&handler, nil)
	s.commonUc.On("GetByMessageId", "12").Return(&mockMessages, nil)
	methodHandler.On("Edit", "12", "Hello text").Return(nil)

	data := s.handler.Execute(params)

	require.True(s.T(), data.IsComplete())
}

// ------------------------------------------------
//
// Execute_NoHandler
//
// ------------------------------------------------
func (s *EditMessageSuite) Test_editMessage_Execute_NoHandler() {
	params := coreModel.NodeParams{
		"text":      []byte("Hello text"),
		"botSlug":   []byte("botSlugValue"),
		"messageId": []byte("12"),
	}

	s.reg.On("Get", "botSlugValue").Return(nil, errors.New("handlers.send-message.not-found"))

	data := s.handler.Execute(params)

	require.True(s.T(), data.IsError())
	require.Equal(s.T(), "handlers.send-message.not-found", data.GetError())
}

// ------------------------------------------------
//
// Execute_NoMessages
//
// ------------------------------------------------
func (s *EditMessageSuite) Test_editMessage_Execute_NoMessages() {
	params := coreModel.NodeParams{
		"text":      []byte("Hello text"),
		"botSlug":   []byte("botSlugValue"),
		"messageId": []byte("12"),
	}

	var handler models.IBotHandler
	methodHandler := new(modelMock.IBotHandler)
	handler = methodHandler

	s.reg.On("Get", "botSlugValue").Return(&handler, nil)
	s.commonUc.On("GetByMessageId", "12").Return(nil, nil)

	data := s.handler.Execute(params)

	require.True(s.T(), data.IsError())
	require.Equal(s.T(), "Message not found", data.GetError())
}
