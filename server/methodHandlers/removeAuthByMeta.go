package methodHandlers

import (
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"log"
)

type removeAuthByMetaHandler struct {
	authReqUC usecases.AuthRequestUsecase
}

func (h *removeAuthByMetaHandler) Execute(params models.NodeParams) models.NodeData {
	botId := params.GetUint("botId")
	meta := params.GetString("meta")

	resp := models.NodeDataFromParams(params)

	var err error
	log.Printf("🤖 Remove auth handler by meta executing %s and params: %s", meta, params)

	auth, err := h.authReqUC.GetByMeta(botId, meta)
	if auth != nil || err != nil {
		return resp.Error("bot-ms.auth-handler.auth-request-not-found")
	}

	err = h.authReqUC.Delete(auth.ID)
	if err != nil {
		return resp.Error(err.Error())
	}

	return resp.Complete(nil)
}

func NewRemoveAuthByMetaHandler(authReqUC usecases.AuthRequestUsecase) models.IHandler {
	return &removeAuthByMetaHandler{
		authReqUC: authReqUC,
	}
}
