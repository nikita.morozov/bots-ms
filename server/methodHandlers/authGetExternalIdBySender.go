package methodHandlers

import (
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"log"
)

type authGetExternalIdBySender struct {
	userUC usecases.BotUserUsecase
}

// Execute
// In params:
// • botId
// • sender
// Response
// • OnComplete
// • OnError

func (h *authGetExternalIdBySender) Execute(params models.NodeParams) models.NodeData {
	botId := params.GetUint("botId")
	userId := params.GetString("userId")

	log.Printf("🤖 Auth get external id by sender executing %s and botId: %d", userId, botId)
	user, _ := h.userUC.GetByUserId(botId, userId)
	if user != nil && !user.Blocked {
		resp := models.NodeData{}
		resp.Set("externalId", user.Meta)
		return resp.Complete(nil)
	}

	return models.NodeDataError("bot-ms.auth-user.not-found")
}

func NewAuthGetExternalIdBySenderHandler(userUC usecases.BotUserUsecase) models.IHandler {
	return &authGetExternalIdBySender{
		userUC: userUC,
	}
}
