package methodHandlers

import (
	"bots-ms/models"
	"bots-ms/usecases/mocks"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"gorm.io/gorm"
	"testing"
	"time"
)

type AuthHandlerSuite struct {
	suite.Suite

	actionUc  *mocks.ActionUsecase
	userUc    *mocks.BotUserUsecase
	authReqUC *mocks.AuthRequestUsecase
	handler   coreModel.IHandler
}

func (s *AuthHandlerSuite) BeforeTest(_, _ string) {
	actionUc := new(mocks.ActionUsecase)
	userUc := new(mocks.BotUserUsecase)
	authReqUC := new(mocks.AuthRequestUsecase)

	s.handler = NewChatAuthHandler(userUc, authReqUC)

	s.actionUc = actionUc
	s.userUc = userUc
	s.authReqUC = authReqUC
}

func TestAuthHandler(t *testing.T) {
	suite.Run(t, new(AuthHandlerSuite))
}

// ------------------------------------------------
//
// # Execute
//
// ------------------------------------------------
func (s *AuthHandlerSuite) Test_authHandler_Execute() {
	params := coreModel.NodeParams{
		"botId":  []byte("23"),
		"sender": []byte("testUser"),
		"hash":   []byte("abcabc"),
	}

	authReq := &models.AuthRequest{
		Model: gorm.Model{
			ID:        uint(21),
			CreatedAt: time.Now(),
		},
		BotID: uint(23),
		Hash:  "abcabc",
	}

	s.userUc.On("GetByUserId", uint(23), "testUser").Return(nil, nil)
	s.authReqUC.On("GetByHash", "abcabc").Return(authReq, nil)
	s.authReqUC.On("Delete", uint(21)).Return(nil)
	s.userUc.On("Create", mock.Anything).Return(nil)

	data := s.handler.Execute(params)

	require.True(s.T(), data.IsComplete())
}

// ------------------------------------------------
//
// Execute_UserExist
//
// ------------------------------------------------
func (s *AuthHandlerSuite) Test_authHandler_Execute_UserExist() {
	params := coreModel.NodeParams{
		"botId":  []byte("23"),
		"sender": []byte("testUser"),
		"hash":   []byte("abcabc"),
	}

	user := models.BotUser{
		Model: gorm.Model{
			ID:        uint(1),
			CreatedAt: time.Now(),
		},
		BotID:  uint(1),
		UserID: "abcabc",
	}

	s.userUc.On("GetByUserId", uint(23), "testUser").Return(&user, nil)

	data := s.handler.Execute(params)

	require.True(s.T(), data.IsError())
	require.Equal(s.T(), "bot-ms.auth-handler.user-already-exist", data.GetError())
}

// ------------------------------------------------
//
// Execute_NoUserAuth
//
// ------------------------------------------------
func (s *AuthHandlerSuite) Test_authHandler_Execute_NoUserAuth() {
	params := coreModel.NodeParams{
		"botId":  []byte("23"),
		"sender": []byte("testUser"),
		"hash":   []byte("abcabc"),
	}

	s.userUc.On("GetByUserId", uint(23), "testUser").Return(nil, nil)
	s.authReqUC.On("GetByHash", "abcabc").Return(nil, nil)

	data := s.handler.Execute(params)

	require.True(s.T(), data.IsError())
	require.Equal(s.T(), "bot-ms.auth-handler.user-auth-not-found", data.GetError())
}
