package methodHandlers

import (
	models2 "bots-ms/models"
	"bots-ms/registry"
	"bots-ms/usecases"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type inlineKeyboardHandler struct {
	maxConnectorSize int
	registry         registry.BotRegistry
	triggerStorage   usecases.TriggerStorageUsecase
}

func (h *inlineKeyboardHandler) Execute(params models.NodeParams) models.NodeData {
	execute := params.GetString("execute")
	backward := params.GetBool("backward")
	markdown := params.GetString("markdown")
	splitButtons := params.GetInt("splitButtons")

	var extra map[string]string
	if len(markdown) > 0 {
		extra = map[string]string{"markdown": markdown}
	}

	if backward {
		return nil
	}

	if len(execute) > 0 {
		res := models.NodeData{}
		res.SetEvent(execute)
		return res
	}

	context := params.GetString("context")
	text := params.GetString("text")
	scenarioActionId := params.GetUint("scenarioActionId")
	var buttons []models2.KeyboardButton

	for i := 1; i <= h.maxConnectorSize; i++ {
		key := fmt.Sprintf("button%d", i)
		value := params.GetString(key)
		if len(value) > 0 {
			slug := uuid.New().String()
			buttons = append(buttons, models2.KeyboardButton{
				Slug: slug,
				Name: value,
			})

			h.triggerStorage.Set(context, slug, &models2.TriggerData{
				EventName:        fmt.Sprintf("onButton%d", i),
				ScenarioActionID: scenarioActionId,
			})
		}
	}

	sender := params.GetString("sender")
	botSlug := params.GetString("botSlug")
	botHandler, err := h.registry.Get(botSlug)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	data := models2.KeyboardInfo{
		Sender:       sender,
		SplitButtons: int(splitButtons),
		Buttons:      buttons,
		Text:         text,
		Type:         models2.KeyboardDtoTypeInline,
		Extra:        extra,
	}

	(*botHandler).ShowKeyboard(data)

	return nil
}

func NewInlineKeyboardHandler(maxConnectorSize int, registry registry.BotRegistry, triggerStorage usecases.TriggerStorageUsecase) models.IHandler {
	return &inlineKeyboardHandler{
		maxConnectorSize: maxConnectorSize,
		triggerStorage:   triggerStorage,
		registry:         registry,
	}
}
