package methodHandlers

import (
	"bots-ms/registry"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type removeKeyboardHandler struct {
	registry registry.BotRegistry
}

func (h *removeKeyboardHandler) Execute(params models.NodeParams) models.NodeData {
	sender := params.GetString("sender")
	text := params.GetString("text")
	botSlug := params.GetString("botSlug")
	markdown := params.GetString("markdown")
	botHandler, err := h.registry.Get(botSlug)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	var extra map[string]string
	if len(markdown) > 0 {
		extra = map[string]string{"markdown": markdown}
	}

	(*botHandler).RemoveKeyboard(sender, text, extra)

	return models.NodeDataComplete(nil)
}

func NewRemoveKeyboardHandler(registry registry.BotRegistry) models.IHandler {
	return &removeKeyboardHandler{
		registry: registry,
	}
}
