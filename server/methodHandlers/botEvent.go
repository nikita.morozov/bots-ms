package methodHandlers

import (
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type botEventHandler struct {
	storage usecases.VariableStorageUsecase
}

func (h botEventHandler) Execute(params models.NodeParams) models.NodeData {
	context := params.GetString("context")
	botSlug := params.GetString("botSlug")
	sender := params.GetString("sender")
	messageId := params.GetString("messageId")
	replayOnMessageId := params.GetString("replayOnMessageId")
	nickname := params.GetString("nickname")
	botId := params.GetUint("botId")

	if len(botSlug) > 0 {
		h.storage.SetString(context, "botSlug", botSlug)
	}

	if len(sender) > 0 {
		h.storage.SetString(context, "sender", sender)
	}

	if len(messageId) > 0 {
		h.storage.SetString(context, "messageId", messageId)
	}

	if len(replayOnMessageId) > 0 {
		h.storage.SetString(context, "replayOnMessageId", replayOnMessageId)
	}

	if len(nickname) > 0 {
		h.storage.SetString(context, "nickname", nickname)
	}

	if botId > 0 {
		h.storage.SetInt(context, "botId", int(botId))
	}

	return models.NodeDataFromParams(params).Complete(nil)
}

func NewBotEventHandler(storage usecases.VariableStorageUsecase) models.IHandler {
	return &botEventHandler{
		storage: storage,
	}
}
