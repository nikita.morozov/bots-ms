package methodHandlers

import (
	"bots-ms/models"
	modelMock "bots-ms/models/mocks"
	regMocks "bots-ms/registry/mocks"
	ucMock "bots-ms/usecases/mocks"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

type SendMessageSuite struct {
	suite.Suite

	reg     *regMocks.BotRegistry
	uc      *ucMock.CommonMessageUsecase
	handler coreModel.IHandler
}

func (s *SendMessageSuite) BeforeTest(_, _ string) {
	reg := new(regMocks.BotRegistry)
	uc := new(ucMock.CommonMessageUsecase)

	s.handler = NewSendMessageHandler(reg, uc)

	s.reg = reg
}

func TestSendMessage(t *testing.T) {
	suite.Run(t, new(SendMessageSuite))
}

// ------------------------------------------------
//
// # Execute
//
// ------------------------------------------------
func (s *SendMessageSuite) Test_sendMessage_Execute() {
	params := coreModel.NodeParams{
		"text":    []byte("text value"),
		"sender":  []byte("sender value"),
		"botSlug": []byte("botSlug value"),
	}

	var handler models.IBotHandler
	methodHandler := new(modelMock.IBotHandler)
	handler = methodHandler

	s.reg.On("Get", "botSlug value").Return(&handler, nil)
	methodHandler.On("Send", "sender value", "text value", map[string]string(nil)).Return("884784747")

	data := s.handler.Execute(params)

	require.Equal(s.T(), "884784747", data.GetString("messageId"))
	require.True(s.T(), data.IsComplete())
}
