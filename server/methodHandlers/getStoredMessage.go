package methodHandlers

import (
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type getStoredMessageHandler struct {
	commonMessageUC usecases.CommonMessageUsecase
	storedMessageUC usecases.StoredMessageUsecase
}

func (h getStoredMessageHandler) Execute(params models.NodeParams) models.NodeData {
	res := models.NodeData{}

	messageId := params.GetString("messageId")
	if messageId == "" {
		return models.NodeDataError("Edit action: Message ID is empty")
	}

	cMessage, _ := h.commonMessageUC.Get(messageId)
	if cMessage == nil || cMessage.MessageId == nil {
		return models.NodeDataError("Common message not found")
	}

	storedMessage, _ := h.storedMessageUC.Get(*cMessage.MessageId)
	if storedMessage == nil {
		return models.NodeDataError("Stored message not found")
	}

	res.Set("text", storedMessage.Text)
	res.SetUint("storedMessageId", storedMessage.ID)

	return res.Complete(nil)
}

func NewGetStoredMessageHandler(commonMessageUC usecases.CommonMessageUsecase, storedMessageUC usecases.StoredMessageUsecase) models.IHandler {
	return &getStoredMessageHandler{
		commonMessageUC: commonMessageUC,
		storedMessageUC: storedMessageUC,
	}
}
