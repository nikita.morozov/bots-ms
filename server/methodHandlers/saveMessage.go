package methodHandlers

import (
	models2 "bots-ms/models"
	"bots-ms/registry"
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type saveMessageHandler struct {
	registry        registry.BotRegistry
	commonMessageUC usecases.CommonMessageUsecase
}

func (h saveMessageHandler) Execute(params models.NodeParams) models.NodeData {
	text := params.GetString("text")
	messageId := params.GetString("messageId")
	data := params.GetString("data")

	var item = models2.CommonMessage{
		Message: &models2.StoredMessage{
			MessageId: messageId,
			Text:      text,
		},
		Data: data,
	}
	err := h.commonMessageUC.Create(&item)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	return models.NodeDataComplete(nil)
}

func NewSaveMessageHandler(registry registry.BotRegistry, commonMessageUC usecases.CommonMessageUsecase) models.IHandler {
	return &saveMessageHandler{
		registry:        registry,
		commonMessageUC: commonMessageUC,
	}
}
