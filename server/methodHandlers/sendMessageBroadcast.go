package methodHandlers

import (
	models2 "bots-ms/models"
	"bots-ms/registry"
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"log"
)

type broadcastHandler struct {
	userUC          usecases.BotUserUsecase
	storedMessageUC usecases.StoredMessageUsecase
	commonMessageUC usecases.CommonMessageUsecase
	botUC           usecases.BotUsecase
	registry        registry.BotRegistry
	storeMessage    bool
}

func (h broadcastHandler) Execute(params models.NodeParams) models.NodeData {
	text := params.GetString("text")
	sender := params.GetString("sender")
	botId := params.GetUint("botId")
	markdown := params.GetString("markdown")

	var extra map[string]string
	if len(markdown) > 0 {
		extra = map[string]string{"markdown": markdown}
	}

	bot, err := h.botUC.Get(botId)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	botHandler, err := h.registry.Get(bot.GetSlug())
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	log.Printf("🤖 Send broadcast message executing %s and text: %s", sender, text)

	var message models2.StoredMessage
	if h.storeMessage {
		message = models2.StoredMessage{
			Text: text,
		}

		err = h.storedMessageUC.Create(&message)
		if err != nil {
			return models.NodeDataError(err.Error())
		}
	}

	users, _, err := h.userUC.List(bot.Handler, nil)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	for _, user := range *users {
		if user.Blocked {
			continue
		}

		item := (*botHandler).Send(user.UserID, text, extra)

		if h.storeMessage {
			var item = models2.CommonMessage{
				MessageId: &message.ID,
				Data:      item.(string),
			}
			err = h.commonMessageUC.Create(&item)
			if err != nil {
				return models.NodeDataError(err.Error())
			}
		}
	}

	return models.NodeDataComplete(nil)
}

func NewSendMessageBroadcastHandler(registry registry.BotRegistry, botUC usecases.BotUsecase, userUC usecases.BotUserUsecase, storedMessageUC usecases.StoredMessageUsecase, commonMessageUC usecases.CommonMessageUsecase) models.IHandler {
	return &broadcastHandler{
		botUC:           botUC,
		userUC:          userUC,
		registry:        registry,
		storeMessage:    false,
		commonMessageUC: commonMessageUC,
		storedMessageUC: storedMessageUC,
	}
}

func NewMessageBroadcastWithStoreHandler(registry registry.BotRegistry, botUC usecases.BotUsecase, userUC usecases.BotUserUsecase, storedMessageUC usecases.StoredMessageUsecase, commonMessageUC usecases.CommonMessageUsecase) models.IHandler {
	return &broadcastHandler{
		botUC:           botUC,
		userUC:          userUC,
		registry:        registry,
		storeMessage:    true,
		commonMessageUC: commonMessageUC,
		storedMessageUC: storedMessageUC,
	}
}
