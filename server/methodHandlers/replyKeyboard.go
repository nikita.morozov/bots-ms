package methodHandlers

import (
	models2 "bots-ms/models"
	"bots-ms/registry"
	"bots-ms/usecases"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type replyKeyboardHandler struct {
	maxConnectorSize int
	registry         registry.BotRegistry
	triggerStorage   usecases.TriggerStorageUsecase
}

func (h *replyKeyboardHandler) Execute(params models.NodeParams) models.NodeData {
	forceReply := params.GetString("forceReply")
	oneTimeKeyboard := params.GetString("oneTimeKeyboard")
	markdown := params.GetString("markdown")
	splitButtons := params.GetInt("splitButtons")

	var buttons []models2.KeyboardButton

	for i := 1; i <= h.maxConnectorSize; i++ {
		key := fmt.Sprintf("button%d", i)
		value := params.GetString(key)
		if len(value) > 0 {
			slug := uuid.New().String()
			buttons = append(buttons, models2.KeyboardButton{
				Slug: slug,
				Name: value,
			})
		}
	}

	sender := params.GetString("sender")
	text := params.GetString("text")
	botSlug := params.GetString("botSlug")
	botHandler, err := h.registry.Get(botSlug)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	data := models2.KeyboardInfo{
		Sender:       sender,
		Buttons:      buttons,
		SplitButtons: int(splitButtons),
		Text:         text,
		Type:         models2.KeyboardDtoTypeReply,
		Extra: map[string]string{
			"forceReply":      forceReply,
			"oneTimeKeyboard": oneTimeKeyboard,
			"markdown":        markdown,
		},
	}

	(*botHandler).ShowKeyboard(data)

	return nil
}

func NewReplyKeyboardHandler(maxConnectorSize int, registry registry.BotRegistry, triggerStorage usecases.TriggerStorageUsecase) models.IHandler {
	return &replyKeyboardHandler{
		maxConnectorSize: maxConnectorSize,
		triggerStorage:   triggerStorage,
		registry:         registry,
	}
}
