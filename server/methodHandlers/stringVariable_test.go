package methodHandlers

import (
	"bots-ms/usecases/mocks"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

type StringVariableHandlerSuite struct {
	suite.Suite

	storage *mocks.VariableStorageUsecase
	handler coreModel.IHandler
}

func (s *StringVariableHandlerSuite) BeforeTest(_, _ string) {
	storage := new(mocks.VariableStorageUsecase)
	s.storage = storage

	s.handler = NewBotEventHandler(storage)
}

func TestStringVariableHandler(t *testing.T) {
	suite.Run(t, new(StringVariableHandlerSuite))
}

// ------------------------------------------------
//
// # Execute
//
// ------------------------------------------------
func (s *StringVariableHandlerSuite) Test_stringVariable_Execute() {
	params := coreModel.NodeParams{
		"context":  []byte("event context"),
		"nickname": []byte("test nick"),
		"value":    []byte("test values"),
	}

	s.storage.On("SetString", "event context", "nickname", "test nick").Return(nil)

	data := s.handler.Execute(params)

	require.True(s.T(), data.IsComplete())
	require.Equal(s.T(), "test values", data.GetString("value"))
}
