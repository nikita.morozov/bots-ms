package methodHandlers

import (
	"bots-ms/models"
	modelMock "bots-ms/models/mocks"
	regMocks "bots-ms/registry/mocks"
	"bots-ms/usecases/mocks"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	coreModel "gitlab.com/nikita.morozov/bot-ms-core/models"
	sharedModels "gitlab.com/nikita.morozov/ms-shared/models"
	"gorm.io/gorm"
	"testing"
)

type SendMessageBroadcastSuite struct {
	suite.Suite

	botUc           *mocks.BotUsecase
	userUc          *mocks.BotUserUsecase
	actionUc        *mocks.ActionUsecase
	storeMessageUC  *mocks.StoredMessageUsecase
	commonMessageUC *mocks.CommonMessageUsecase
	reg             *regMocks.BotRegistry
	handler         coreModel.IHandler
}

func (s *SendMessageBroadcastSuite) BeforeTest(_, _ string) {
	botUc := new(mocks.BotUsecase)
	reg := new(regMocks.BotRegistry)
	userUc := new(mocks.BotUserUsecase)
	actionUc := new(mocks.ActionUsecase)
	storeMessageUC := new(mocks.StoredMessageUsecase)
	commonMessageUC := new(mocks.CommonMessageUsecase)

	s.handler = NewSendMessageBroadcastHandler(reg, botUc, userUc, storeMessageUC, commonMessageUC)

	s.reg = reg
	s.botUc = botUc
	s.userUc = userUc
	s.actionUc = actionUc
	s.storeMessageUC = storeMessageUC
	s.commonMessageUC = commonMessageUC
}

func TestSendMessageBroadcast(t *testing.T) {
	suite.Run(t, new(SendMessageBroadcastSuite))
}

// ------------------------------------------------
//
// # Execute
//
// ------------------------------------------------
func (s *SendMessageBroadcastSuite) Test_sendMessageBroadcast_Execute() {
	params := coreModel.NodeParams{
		"text":   []byte("Hello my friend"),
		"sender": []byte("sender value"),
		"botId":  []byte("12"),
	}

	mockBot := models.Bot{
		Model: gorm.Model{
			ID: uint(1),
		},
		Token:   "abc_token",
		Slug:    "bot_slug",
		Handler: "telegram",
	}

	var handler models.IBotHandler
	methodHandler := new(modelMock.IBotHandler)
	handler = methodHandler

	userList := []models.BotUser{
		{
			UserID: "user1",
		},
		{
			UserID: "user2",
		},
	}

	s.botUc.On("Get", uint(12)).Return(&mockBot, nil)
	s.reg.On("Get", "bot_slug_telegram").Return(&handler, nil)
	s.storeMessageUC.On("Create", mock.Anything).Return(nil)
	s.userUc.On("List", "telegram", (*sharedModels.ListOptions)(nil)).Return(&userList, sharedModels.Paginator{Offset: 0, TotalCount: 1, CountPerPage: 10}, nil)
	methodHandler.On("Send", "user1", "Hello my friend", map[string]string(nil)).Return(nil)
	methodHandler.On("Send", "user2", "Hello my friend", map[string]string(nil)).Return(nil)

	data := s.handler.Execute(params)

	require.True(s.T(), data.IsComplete())
}
