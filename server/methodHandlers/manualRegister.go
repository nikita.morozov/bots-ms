package methodHandlers

import (
	models2 "bots-ms/models"
	"bots-ms/registry"
	"bots-ms/usecases"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"log"
)

type manualRegisterHandler struct {
	botUC       usecases.BotUsecase
	userUC      usecases.BotUserUsecase
	botRegistry registry.BotRegistry
}

// Execute Inner params
// botId
// sender
func (h *manualRegisterHandler) Execute(params models.NodeParams) models.NodeData {
	botSlug := params.GetString("botSlug")
	sender := params.GetString("sender")
	externalId := params.GetString("externalId")
	nickname := params.GetString("nickname")

	log.Printf("🤖 Manual registration handler executing for %s", sender)

	resp := models.NodeData{}

	var err error

	handler, err := h.botRegistry.Get(botSlug)
	if err != nil {
		return resp.Error("bot-ms.auth-handler.bot-handler-not-found")
	}

	bot := (*handler).GetBot()

	var user *models2.BotUser
	user, _ = h.userUC.GetByUserId(bot.ID, sender)
	if user != nil {
		return resp.Error("bot-ms.auth-handler.user-already-exist")
	}

	var newUser = models2.BotUser{
		UserID:   sender,
		BotID:    bot.ID,
		Nickname: nickname,
		Token:    "",
		Meta:     externalId,
	}
	err = h.userUC.Create(&newUser)
	if err != nil {
		return resp.Error("bot-ms.auth-handler.user-create-error")
	}

	return resp.Complete(nil)
}

func NewManualRegisterHandler(botRegistry registry.BotRegistry, botUC usecases.BotUsecase, userUC usecases.BotUserUsecase) models.IHandler {
	return &manualRegisterHandler{
		botUC:       botUC,
		userUC:      userUC,
		botRegistry: botRegistry,
	}
}
