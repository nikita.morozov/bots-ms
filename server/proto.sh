#!/bin/sh

PROTO_PATH=../proto

protoc --proto_path=$PROTO_PATH --go_out=./proto --go_opt=paths=source_relative \
    --go-grpc_out=./proto --go-grpc_opt=paths=source_relative \
    $PROTO_PATH/botCommon.proto

protoc --proto_path=$PROTO_PATH --go_out=./proto --go_opt=paths=source_relative \
    --go-grpc_out=./proto --go-grpc_opt=paths=source_relative \
    $PROTO_PATH/action.proto

protoc --proto_path=$PROTO_PATH --go_out=./proto --go_opt=paths=source_relative \
    --go-grpc_out=./proto --go-grpc_opt=paths=source_relative \
    $PROTO_PATH/authRequest.proto

protoc --proto_path=$PROTO_PATH --go_out=./proto --go_opt=paths=source_relative \
    --go-grpc_out=./proto --go-grpc_opt=paths=source_relative \
    $PROTO_PATH/bot.proto

protoc --proto_path=$PROTO_PATH --go_out=./proto --go_opt=paths=source_relative \
    --go-grpc_out=./proto --go-grpc_opt=paths=source_relative \
    $PROTO_PATH/botUser.proto

protoc --proto_path=$PROTO_PATH --go_out=./proto --go_opt=paths=source_relative \
    --go-grpc_out=./proto --go-grpc_opt=paths=source_relative \
    $PROTO_PATH/event.proto

protoc --proto_path=$PROTO_PATH --go_out=./proto --go_opt=paths=source_relative \
    --go-grpc_out=./proto --go-grpc_opt=paths=source_relative \
    $PROTO_PATH/scenario.proto

protoc --proto_path=$PROTO_PATH --go_out=./proto --go_opt=paths=source_relative \
    --go-grpc_out=./proto --go-grpc_opt=paths=source_relative \
    $PROTO_PATH/scenarioAction.proto

protoc --proto_path=$PROTO_PATH --go_out=./proto --go_opt=paths=source_relative \
    --go-grpc_out=./proto --go-grpc_opt=paths=source_relative \
    $PROTO_PATH/connector.proto

protoc --proto_path=$PROTO_PATH --go_out=./proto --go_opt=paths=source_relative \
    --go-grpc_out=./proto --go-grpc_opt=paths=source_relative \
    $PROTO_PATH/scenarioItem.proto

protoc --proto_path=$PROTO_PATH --go_out=./proto --go_opt=paths=source_relative \
    --go-grpc_out=./proto --go-grpc_opt=paths=source_relative \
    $PROTO_PATH/stateMachine.proto

protoc --proto_path=$PROTO_PATH --go_out=./proto --go_opt=paths=source_relative \
    --go-grpc_out=./proto --go-grpc_opt=paths=source_relative \
    $PROTO_PATH/stateMachineServices.proto
