v2.13.2
* Fixed http request with refresh content type

v2.13.1
* Fixed http request with refresh headers and error

v2.13.0
* Added http request with refresh

v2.12.3
* Sender pass to execute-in-sm

v2.12.2
* Fixed styles
* Fixed context menu for sm

v2.12.1
* Rollback state list method in repositories

v2.12.0
* Added possibility to run scenario in SM by HTTP

v2.11.0
* Improved UI, optimized components

v2.10.5
* Added redirect to bots

v2.10.4
* Removed soft delete for bot user

v2.10.3
* Fixed slow triggers deletion

v2.10.2
* Added request status error

v2.10.1
* Fixed ports styles

v2.10.0
* Added import/export for state machine
* Added MIT License file

v2.9.1
* Fixed bot init with environment 

v2.9.0
* Added env for bots
* Reorganized structure of the files in the project

v2.8.6
* Added removeAuthByMetaHandler and removeAuthByHashHandler handlers

v2.8.5
* Disabled soft delete for auth request model

v2.7.9
* Prevent run default state if command was emitted from group

v2.7.8
* Added  authorization for httpRequest action

v2.7.7
* Fixed trigger execution

v2.7.6
* Added botId to botEvent

v2.7.5
* Added command to botEvent action

v2.7.4
* Added remove saved message action

v2.7.3
* Added edit message action

v2.7.2
* Implemented UC for common message

v2.7.1
* Fixed common message

v2.7.0
* Added action saveMessage
* Added action loadMessage
* Added action added replayTo field for botEvent action
* Removed ExecuteReplay method from bot interface

v2.6.3
* Added count method for events

v2.6.2
* Fixed 'executorHandler' field for bot list method

v2.6.1
* Check if connector exist and don't create existed 

v2.6.0
* Updated docker golang versions 
* Added count methods 

v2.5.3
* Changed init with gorutine 

v2.5.2
* Changed method of starting bots 

v2.5.1
* Fixed state list with transitions 

v2.5.0
* Added batch update

v2.4.1
* Fixed key type on scenario action execute

v2.4.0
* Fixed bot executor on default value
* Added possibility to pass data for state based executor

v2.3.3
* Fixed bot executor data update

v2.3.2
* Added base64 encode/decode handlers

v2.3.1
* Added template string migration

v2.3.0
* Added template string
* Updated common-handlers lib to v1.4.0

v2.2.15
* Added buttons splitting for inline keyboard 

v2.2.14
* Fixed parsing if value is not recognized as declared in type

v2.2.13
* Extended data parsing

v2.2.12
* Extended state duration time

v2.2.11
* Added split buttons

v2.2.10
* Added socket markdown style for send message nodes

v2.2.9
* Removed character escape

v2.2.8
* Test markdown

v2.2.7
* Fixed manual registration

v2.2.6
* Added debug info

v2.2.5
* Fixed manual registration handler

v2.2.4
* Fixed character replacer

v2.2.3
* Added escape characters for MD

v2.2.2
* Send method test

v2.2.1
* Fixed messages with buttons and styles

v2.2.0
* Added parse mode for messages

v2.1.6
* Added manual registration

v2.1.5
* Fixed remove keyboard handler

v2.1.4
* Added remove keyboard

v2.1.3
* Scenario list opts method as optional
* Possibility to clear initial state in SM when the state was deleted
* Possibility to clear initial action when the action was deleted

v2.1.2
* Fixed bot executor field

v2.1.1
* Fixed default state appearing

v2.1.0
* Added pagination
* Fixed deinit method

v2.0.5
* Added bot executor command handler

v2.0.4
* Fixed init bot. Dont exit from the app on the bot init error.
* Updated go to 1.8.1

v2.0.3
* Fixed state delete

v2.0.2
* Fixed getting transitions  list

v2.0.1
* Change ToStateID to optional

v2.0.0
* Added bot command executor
* Added event and state command handlers
* Refactoring bot handler

v1.8.0
* Added state machine methods
* Added scenario items

v1.7.0
* Added variables
* Added default variables fo eventBot handler

v1.6.2
* Update auth hash if exist

v1.6.1
* Updated core handlers
* Disabled backward execution of inline keyboard
* Selective socket value

v1.6.0
* Added async operations
* Added indexes to models
* Refactor core engine

v1.5.6
* Fixed grpc bot delivery fields

v1.5.5
* Updated grpc methods
* Added blocked property to users

v1.5.4
* Updated common handlers

v1.5.3
* Added auth field for http block

v1.5.2
* Added nickname

v1.5.1
* Added remove message handler

v1.5.0
* Added replaceable property to send message

v1.4.5
* added "set typing" action

v1.4.4
* Fixed run trigger when bot has been allowed plain commands

v1.4.3
* Added AllowPlainCommand to bot
* Added DisableTriggers to bot
* Updated proto
* Added paginator to bot users
* Fixed reply keyboard params

v1.4.2
* Added text to reply keyboard
* Added flexible size for keyboard

v1.4.1
* Fixed tests

v1.4.0
* Added reply keyboard

v1.3.1
* Fixed seeders

v1.3.0
* Updated module loader

v1.2.6
* Added check exist user in bot http endpoint

v1.2.5
* Added addIntToObject handler

v1.2.4
* Added handlers

v1.2.3
* Trigger can pass data to execution

v1.2.2
* Fixed trigger params execution

v1.2.1
* Fixed trigger execution

v1.2.0
* Updated core handlers
* Added input handler

v1.1.7
* Fixed get external ID return
* Added precision convert action

v1.1.6
* Fixed get external ID object

v1.1.5
* Changed sort for lists
* Added handler "GetExternalIdBySender"

v1.1.4
* Fixed http request
 
v1.1.2
* Added bot to response
 
v1.1.1
* Changed botId to botSlug in auth user request
 
v1.1.0
* Removed .so modules
 
v1.0.13
* Updated dockerfile
 
v1.0.12
* Updated dockerfile
 
v1.0.11
* Updated dockerfile
 
v1.0.10
* Changed docker file to scratch build
 
v1.0.9
* Changed docker file to alpine build
 
v1.0.8
* Added reason display for module loading
 
v1.0.7
* Added CI/CD cache
 
v1.0.6
* Updated docker file
 
v1.0.5
* Fixed empty action data
 
v1.0.4
* Added debug to test
 
v1.0.3
* Fixed sendMessage and fixed docker file

v1.0.2
* Updated docker files for copy modules

v1.0.1
* Added amd and arm modules
* Fixed module loader
* Fixed seed loader

v1.0.0
* Added params and data to core module
* Added modules, implemented through plugins

v0.6.0
* Added method for bots to get sender id by external id

* v0.5.9
* Added bot (re)start on add
* Stop bot on remove it from DB
* Fixed execution scenarios without initial action

v0.5.8
* Some migrations

v0.5.7
* Removed debug

v0.5.6
* Added debug

v0.5.5
* Fixed telegram handler getId 

v0.5.4
* Fixed string values 

v0.5.3
* Made tg notification after build has been uploaded 

v0.5.2
* Updated CI/CD params

v0.5.1
* Added changelog file