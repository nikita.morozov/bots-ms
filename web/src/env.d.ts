/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_PUBLIC_API_DOMAIN_HTTP: string
  readonly VITE_PUBLIC_SERVER_API_DOMAIN_HTTP: string
  readonly VITE_PUBLIC_SERVER_API_DOMAIN: string
  readonly VITE_PUBLIC_API_DOMAIN: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}