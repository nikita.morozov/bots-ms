import { StateMachine } from '@local/modules/proto/stateMachine';
import React, { FC, useEffect, useState } from 'react';
import { useInjection } from 'inversify-react';
import StateMachineEngineMV from '@local/modules/engine/stateMachineEngine';
import StateMachineEditorVM from '@components/stateMachineEditor/VM';
import StateMachineEditorView from '@components/stateMachineEditor/Component';
import { observer } from 'mobx-react-lite';

interface StateMachineEditorProps {
  stateMachine?: StateMachine
}

const StateMachineEditor: FC<StateMachineEditorProps> = ({ stateMachine }) => {
  const engineMV = useInjection<StateMachineEngineMV>(StateMachineEngineMV.diKey);

  const [vm] = useState(new StateMachineEditorVM(engineMV));

  useEffect(() => {
    engineMV.clear();
    if (stateMachine) {
      engineMV.loadLayerModel(stateMachine);
      vm.load();
      return () => vm.unload();
    }
  }, [stateMachine]);

  return <StateMachineEditorView
    engine={engineMV.engine}
  />;
};

export default observer(StateMachineEditor);