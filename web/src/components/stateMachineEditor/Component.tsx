import React, { FC } from 'react';
import { DiagramEngine } from '@projectstorm/react-diagrams';
import { CanvasWidget } from '@projectstorm/react-canvas-core';
import ContextMenuComponent from '@components/stateMachineEditor/contextMenu';

interface IStateMachineEditorView {
  engine: DiagramEngine,
}

const StateMachineEditorView: FC<IStateMachineEditorView> = (props) => {
  const {
    engine,
  } = props;

  return (
    <div style={{ position: 'relative', height: '100%' }}>
      <ContextMenuComponent>
        <CanvasWidget
          className="diagram-container"
          engine={engine}
        />
      </ContextMenuComponent>
    </div>
  )
}

export default StateMachineEditorView;