import { FC, PropsWithChildren, useCallback, useState } from 'react';
import React from 'react';
import { useInjection } from 'inversify-react';
import ContextMenuComponentView from '@components/stateMachineEditor/contextMenu/Component';
import StateMachineContextMenuVM from '@components/stateMachineEditor/contextMenu/VM';
import useLoadableVM from '@sharedModules/Stream/hooks';
import { ScenarioModel } from '@local/modules/proto/botCommon';
import StateMachineEngineMV from '@local/modules/engine/stateMachineEngine';
import { Point } from '@projectstorm/geometry';
import { observer } from 'mobx-react-lite';

const ContextMenuComponent: FC<PropsWithChildren<{}>> = (props) => {
  const {
    children,
  } = props;

  const engineMV = useInjection<StateMachineEngineMV>(StateMachineEngineMV.diKey);

  const [vm] = useState(new StateMachineContextMenuVM(engineMV));
  useLoadableVM(vm);

  const addState = useCallback((scenario: ScenarioModel, position: Point) => {
    vm.addState(scenario, position);
  }, []);

  return <ContextMenuComponentView
    add={addState}
    scenarios={engineMV.scenarioModule.list.items}
    children={children}
  />;
};

export default observer(ContextMenuComponent);