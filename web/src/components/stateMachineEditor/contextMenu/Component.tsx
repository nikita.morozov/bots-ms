import React, { FC, PropsWithChildren, useState } from 'react';
import { Dropdown, Input, Menu } from 'antd';
import { ScenarioModel } from '@local/modules/proto/botCommon';
import { Point } from '@projectstorm/geometry';

interface IContextMenuComponentView {
  scenarios: ScenarioModel[],
  add: (value: ScenarioModel, position: Point) => void,
}

const { Search } = Input;

const ContextMenuComponentView: FC<PropsWithChildren<IContextMenuComponentView>> = (props) => {
  const {
    scenarios,
    add,
    children,
  } = props;

  const [searchQuery, setSearchQuery] = useState('');

  return (
    <Dropdown overlay={<Menu
      style={{ height: 300, overflow: 'scroll' }}
      items={[
        { key: 'search', label: <Search placeholder="Search..." onKeyDown={(e) => e.stopPropagation()} onChange={(e) => setSearchQuery(e.target.value)} />, type: 'group'},
        ...scenarios.filter(i => i.title.search(new RegExp(searchQuery, 'i')) !== -1).map(i => ({
          key: `${i.id}_scenario`,
          label: i.title,
          onClick: (info: any) => {
            const event = info.domEvent as React.MouseEvent<HTMLElement>;
            add(i, new Point(event.clientX, event.clientY));
          }
        }))
      ]}
    />} trigger={['contextMenu']}>
      <div className="diagram-container">
        {children}
      </div>
    </Dropdown>
  );
};

export default ContextMenuComponentView;