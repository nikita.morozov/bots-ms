import * as React from 'react';
import { PortWidget } from '@projectstorm/react-diagrams-core';
import EngineMV from '@local/modules/engine/model';
import { DefaultPortModel } from '@projectstorm/react-diagrams-defaults';
import { ObjectWithId } from '@sharedModules/types';

export interface ICustomDefaultPortLabel<LAYER_MODEL extends ObjectWithId, ENGINE extends EngineMV<LAYER_MODEL>> {
  port: DefaultPortModel,
  engineMV: ENGINE,
}

export class CustomDefaultPortLabel<LAYER_MODEL extends ObjectWithId, ENGINE extends EngineMV<LAYER_MODEL>> extends React.Component<ICustomDefaultPortLabel<LAYER_MODEL, ENGINE>> {
  render() {
    const port = (
      <PortWidget engine={this.props.engineMV.engine} port={this.props.port}>
        <div style={{ width: 15, height: 15, background: 'rgba(255, 255, 255, 0.1)' }} />
      </PortWidget>
    );
    const label = <div style={{ padding: '0 5px', flexGrow: 1 }}>{this.props.port.getOptions().label}</div>;

    return (
      <div style={{ display: 'flex', marginTop: 1, alignItems: 'center' }}>
        {this.props.port.getOptions().in ? port : label}
        {this.props.port.getOptions().in ? label : port}
      </div>
    );
  }
}