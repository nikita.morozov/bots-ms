import * as React from 'react';
import { PortWidget } from '@projectstorm/react-diagrams-core';
import { CustomDefaultPortLabel } from '@components/labels/CustomDefaultPortLabel';
import { StateMachine } from '@local/modules/proto/stateMachine';
import StateMachineEngineMV from '@local/modules/engine/stateMachineEngine';
import { observer } from 'mobx-react';

@observer
class CustomPortLabel extends CustomDefaultPortLabel<StateMachine, StateMachineEngineMV> {
  render() {
    return (
      <>
        <div
          style={{
            whiteSpace: 'nowrap',
            height: 18,
            display: 'flex',
            alignItems: 'center'
          }}
        >
          <div className='node__port_bottom_block'>
            <PortWidget engine={this.props.engineMV.engine} port={this.props.port}>
              <div className={`node__ports_port`} />
            </PortWidget>
          </div>
        </div>
      </>
    )
  }
}

export default CustomPortLabel;