import { PortWidget } from '@projectstorm/react-diagrams-core';
import * as React from 'react';
import { observer } from 'mobx-react';
import SocketNode from '@local/modules/socket/node';
import ScenarioActionNode from '@local/modules/scenarioAction/node';
import { ScenarioActionBatch, ScenarioActionModel, ScenarioModel } from '@local/modules/proto/botCommon';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import { CustomDefaultPortLabel } from '@components/labels/CustomDefaultPortLabel';

@observer
class CustomPortLabel extends CustomDefaultPortLabel<ScenarioModel, ScenarioEngineMV> {
  typeHandler(type: string) {
    return (
      <div key={this.props.port.getID()} className={`node__port_type node__port__type_${(this.props.port as SocketNode).model.type}`}>
        {type}
      </div>
    );
  }

  getParent(): ScenarioActionNode  {
    return this.props.port?.getParent() as ScenarioActionNode;
  }

  inputHandler(): JSX.Element[] {
    const { type, direction, id } = (this.props.port as SocketNode).model;

    const defaultValue = () => {
      const parent = this.getParent();
      return parent.model().data ? JSON.parse(parent.model().data)[`${(this.props.port as SocketNode).model.slug}`] || '' : '';
    };

    const setDefaultValue = (value) => {
      const parent = this.getParent();
      // TODO: Parse it in constructor
      const parsedData = JSON.parse(parent.model().data);
      parsedData[`${(this.props.port as SocketNode).model.slug}`] = value;
      parent.model().data = JSON.stringify(parsedData);
    };

    const onKeyDown = (e) => e.stopPropagation();

    const onBlur = (value: number | string | boolean) => {
      const parent = this.getParent();

      let parentData = { ...JSON.parse(parent.model().data as string || '{}') };
      if (value.toString().length) {
        parentData = {
          ...parentData,
          [`${(this.props.port as SocketNode).model.slug}`]: value,
        };
      } else {
        delete parentData[`${(this.props.port as SocketNode).model.slug}`]
      }

      const allLinks = Object.entries(this.props.port.getLinks());

      const scenarioActionsNodeForUpdate: ScenarioActionNode[] = [];

      if (allLinks.length) {
        for (const item of allLinks) {
          const targetPort = item[1].getTargetPort();

          if (!!targetPort) {
            const targetParent = targetPort.getParent() as ScenarioActionNode;

            let data = { ...JSON.parse(targetParent.model().data || '{}') }

            if (value.toString().length) {
              data = {
                ...data,
                [`${(item[1].getTargetPort() as SocketNode).model.slug}`]: value,
              };
            } else {
              delete data[`${(item[1].getTargetPort() as SocketNode).model.slug}`];
            }

            targetParent.setModel({ ...targetParent.model(), data: JSON.stringify(data), meta: targetParent.model().meta});

            scenarioActionsNodeForUpdate.push(targetParent);
          }
        }
      }

      this.props.engineMV.scenarioActionsModule.batchUpdate(ScenarioActionBatch.fromPartial({ items: scenarioActionsNodeForUpdate.map(i => ScenarioActionModel.fromPartial({ ...i.model(), meta: JSON.stringify(i.model().meta) })) }));

      this.props.engineMV.scenarioActionsModule.update(ScenarioActionModel.fromPartial({ ...parent.model(), data: JSON.stringify(parentData), meta: JSON.stringify(parent.model().meta) }));
    };

    switch (type) {
      case 0:
        return [<textarea
          value={defaultValue()}
          onBlur={e => {
            this.getParent().setLocked(false);
            e.currentTarget.className = "textarea_converted";
            onBlur(e.target.value);
          }}
          onKeyDown={onKeyDown}
          onClick={e => {
            this.getParent().setLocked(true);
            e.currentTarget.className = "textarea_expanded";
          }}
          onChange={e => setDefaultValue(e.target.value)}
          key={`${id}_${direction}`}
          disabled={!(this.props.port as SocketNode).model.manual}
          wrap='hard'
        />,
          this.typeHandler('S'),
        ];
      case 1:
        return [<input key={`${id}_${direction}`} disabled />, this.typeHandler('O')];
      case 2:
        return [<input
          onChange={e => setDefaultValue(e.target.value)}
          value={defaultValue()}
          onBlur={({ target: { value } }) => onBlur(value)}
          disabled={!(this.props.port as SocketNode).model.manual}
          onKeyDown={onKeyDown}
          key={`${id}_${direction}`}
          type="number"
        />,
          this.typeHandler('N'),
        ];
      case 3:
        return [<input type="text" key={`${id}_${direction}`} disabled />, this.typeHandler('EM')];
      case 4:
        return [<span key={`${id}_${direction}`}/>, this.typeHandler('E')];
      case 5:
        return [
          <div
            style={{ width: 20, margin: '0 10px' }}
            key={`${id}_check_block`}
          >
            <input
              type="checkbox"
              key={`${id}_${direction}`}
              onChange={e => onBlur(e.target.checked)}
              disabled={!(this.props.port as SocketNode).model.manual}
              checked={defaultValue() || false}
            />
          </div>,
          this.typeHandler('B')];
      default:
        return [<input type="text" key='default_input' disabled />, this.typeHandler('EM')];
    }
  }

  render() {
    const { links } = this.props.port as SocketNode;
    const { type, manual } = (this.props.port as SocketNode).model;

    const portBlock = (
      <PortWidget engine={this.props.engineMV.engine} port={this.props.port}>
        <div className={'node__ports_port_wrap'}>
          <div className={`node__ports_port ${Object.entries(links).length ? `node__port__type_${type}` : ''}`} />
        </div>
      </PortWidget>
    );

    const label = <div className={`node__port_label`}>{this.props.port.getOptions().label}</div>;

    const input = (
      <div className={`${manual ? 'node__port_input_block' : 'node__port_input_block_disabled'}`} style={{ display: 'flex', alignItems: 'center' }}>
        {this.props.port.getOptions().in ?
          this.inputHandler().reverse() :
          this.inputHandler()}
      </div>
    );

    return (
      <>
        <div style={{ whiteSpace: 'nowrap' }} className={this.props.port.getOptions().in ? 'port_in' : 'port_out'}>
          {label}
          <div className='node__port_bottom_block'>
            {this.props.port.getOptions().in ? portBlock : input}
            {this.props.port.getOptions().in ? input : portBlock}
          </div>
        </div>
      </>
    );
  }
}

export default CustomPortLabel;