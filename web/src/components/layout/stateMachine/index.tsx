import React from 'react';
import { FunctionComponent } from 'react';
import StateMachineServiceTable, { IStateMachineTableProps } from '../../tables/stateMachineService';

const StateMachineLayout: FunctionComponent<IStateMachineTableProps> = (props) => {
  const {
    beforeOpenEditor,
  } = props;

  return (
      <StateMachineServiceTable
        beforeOpenEditor={beforeOpenEditor}
      />
  );
}

export default StateMachineLayout;