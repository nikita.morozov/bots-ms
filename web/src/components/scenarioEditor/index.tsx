import React, { FC, useEffect, useState } from 'react';
import { useInjection } from 'inversify-react';
import ScenarioEditorVM from '@components/scenarioEditor/VM';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import ScenarioEditorView from '@components/scenarioEditor/Component';

const ScenarioEditor: FC = () => {
  const engineMV = useInjection<ScenarioEngineMV>(ScenarioEngineMV.diKey);

  const [vm] = useState(new ScenarioEditorVM(engineMV));

  useEffect(() => {
    if (!!engineMV.layerModel.getItem()?.id) {
      vm.load();
      return () => vm.unload();
    }
  }, [engineMV.layerModel.getItem()]);

  return <ScenarioEditorView engine={engineMV.engine} />;
};

export default ScenarioEditor;