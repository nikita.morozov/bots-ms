import { BaseViewModel } from '@sharedModules/Stream/model/BaseVM';
import { ScenarioActionModel, ScenarioItemModel } from '@local/modules/proto/botCommon';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import { Point } from '@projectstorm/geometry';
import { CommentBoxNodeModel } from '@local/modules/scenarioItem/nodes/commentBox/node';

class ScenarioContextMenuVM extends BaseViewModel {
  private engineMV: ScenarioEngineMV;

  constructor(engineMV: ScenarioEngineMV) {
    super();

    this.engineMV = engineMV;
  }

  private getMouseLocation = (position: Point) => this.engineMV.engine.getRelativeMousePoint({ clientX: position.x, clientY: position.y }).clone();

  addAction = (actionId: number, position: Point) => {
    this.engineMV.scenarioActionsModule.add(ScenarioActionModel.fromPartial({
      scenarioId: this.engineMV.layerModel.getItem()!.id,
      actionId: actionId,
      meta: JSON.stringify({ position: this.getMouseLocation(position) }),
      data: '{}',
    }));
  }

  addTool = (slug: string, position: Point) => {
    const toolHandler = {
      "commentBox": () => this.addCommentBox(position),
    }

    toolHandler[slug]();
  }

  private addCommentBox = (position: Point) => {
    this.engineMV.scenarioItemsModule.add(ScenarioItemModel.fromPartial({
      scenarioId: this.engineMV.layerModel.getItem()!.id,
      type: CommentBoxNodeModel.type,
      meta: JSON.stringify({
        position: this.getMouseLocation(position),
        color: 'background:rgba(255,255,255, 0.5)',
        description: 'New description',
        isLocked: false,
        width: 300,
        height: 300,
      })
    }), CommentBoxNodeModel.type);
  }
}

export default ScenarioContextMenuVM;