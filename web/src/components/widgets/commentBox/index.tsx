import * as React from 'react';
import { FC, useCallback, useState } from 'react';
import { CommentBoxNodeWidgetView } from '@components/widgets/commentBox/NodeComponent';
import { CommentBoxNodeModel } from '@local/modules/scenarioItem/nodes/commentBox/node';
import CommentBoxNodeWidgetVM from '@components/widgets/commentBox/ViewModel';
import useLoadableVM from '@sharedModules/Stream/hooks';
import EngineMV from '@local/modules/engine/model';
import { ObjectWithId } from '@sharedModules/types';

interface ICommentBoxWidget<ENGINE_MODULE extends ObjectWithId> {
  node: CommentBoxNodeModel<ENGINE_MODULE>;
  engineMV: EngineMV<ENGINE_MODULE>;
}

const CommentBoxNodeWidget = <ENGINE_MODULE extends ObjectWithId>(props: ICommentBoxWidget<ENGINE_MODULE>) => {
  const {
    node,
    engineMV,
  } = props;

  const [pickerVisible, setPickerVisible] = useState(false);

  const [vm] = useState(new CommentBoxNodeWidgetVM(engineMV, node));
  useLoadableVM(vm);

  const changeBackgroundColor = useCallback((color: string) => {
    vm.changeBackgroundColor(color);
    togglePickerVisible();
  }, []);

  const togglePickerVisible = useCallback(() => {
    setPickerVisible(!pickerVisible);
  }, [pickerVisible]);

  return <CommentBoxNodeWidgetView<ENGINE_MODULE>
    node={node}
    engineMV={engineMV}
    changeBackgroundColor={changeBackgroundColor}
    pickerVisible={pickerVisible}
    togglePickerVisible={togglePickerVisible}
    onUpdateResize={vm.onUpdateResize}
    onLockNode={vm.onLockNode}
    onUnlockNode={vm.onUnlockNode}
    onBlur={vm.onBlur}
  />
};

export default CommentBoxNodeWidget;