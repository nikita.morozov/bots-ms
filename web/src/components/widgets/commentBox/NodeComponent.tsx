import React from 'react';
import { ResizableBox } from 'react-resizable';
import { GithubPicker } from 'react-color';
import { BgColorsOutlined, LockOutlined, UnlockOutlined } from '@ant-design/icons';
import { CommentBoxNodeModel } from '@local/modules/scenarioItem/nodes/commentBox/node';
import { CustomDefaultNodeWidget } from '@components/widgets/CustomNodeWidget';
import EngineMV from '@local/modules/engine/model';
import { ObjectWithId } from '@sharedModules/types';
import { observer } from 'mobx-react';

interface ICommentBoxNodeWidgetView {
  pickerVisible: boolean,
  changeBackgroundColor: (value: string) => void,
  togglePickerVisible: () => void,
  onUpdateResize: (width: number, height: number) => void,
  onLockNode: () => void,
  onUnlockNode: () => void,
  onBlur: (value: string) => void,
}

@observer
export class CommentBoxNodeWidgetView<ENGINE_MODULE extends ObjectWithId> extends CustomDefaultNodeWidget<ENGINE_MODULE, CommentBoxNodeModel<ENGINE_MODULE>, EngineMV<ENGINE_MODULE>, ICommentBoxNodeWidgetView> {
  baseStyle = {
    height: '100%',
  };
  
  render(): JSX.Element {
    return (
      <div className={`resizable_commentBox ${this.props.node.isSelected() ? 'node_block_active' : ''}`}>
        <ResizableBox
          handle={!this.props.node.isLocked() ? <span className="react-resizable-handle react-resizable-handle-se" /> :
            <span />}
          height={this.props.node.height}
          width={this.props.node.width}
          onResizeStart={() => {
            this.props.node.setLocked(true);
          }}
          onResizeStop={(e, data) => {
            this.props.onUpdateResize(data.size.width, data.size.height);
            this.props.node.setLocked(false);
            this.props.node.setSelected(true);
          }}
        >
          <div
            className={`node_block`}
            data-default-node-name={this.props.node.getOptions().name}
            style={{
              ...this.baseStyle,
              maxWidth: 'initial',
              backgroundColor: this.props.node.extraModel().meta.color,
            }}
          >
            <div
              style={this.baseStyle}
              className={`node_block_wrapper`}
            >
              <div
                className={`node_block_header`}
                style={{
                  backgroundColor: this.props.node.extraModel().meta.color
                }}
              >
                <input
                  className='transparent_input'
                  style={{
                    width: '100%',
                  }}
                  onKeyDown={e => e.stopPropagation()}
                  defaultValue={this.props.node.extraModel().meta.description}
                  onBlur={({ target: { value } }) => {
                    this.props.onBlur(value);
                  }}
                  key="1" type="text"
                />
                <div
                  className="node_block_header_pin_block"
                >
                  <a
                    className="node_block_header_pin"
                    onMouseDown={e => e.stopPropagation()}
                  >
                    <BgColorsOutlined
                      onClick={() => this.props.togglePickerVisible()}
                    />
                    {this.props.pickerVisible &&
                    <div className="color_picker_popover">
                      <div className="color_picker_cover" onClick={() => this.props.togglePickerVisible()} />
                      <GithubPicker onChange={(color) => {
                        this.props.changeBackgroundColor(`rgba(${color.rgb.r}, ${color.rgb.g}, ${color.rgb.b}, 0.5)`);
                      }} />
                    </div>}
                  </a>
                  <a
                    className="node_block_header_pin"
                  >
                    {this.props.node.isLocked() ?
                      <LockOutlined
                        onMouseDown={() => this.props.onUnlockNode()}
                      /> :
                      <UnlockOutlined
                        onMouseDown={() => this.props.onLockNode()}
                      />}
                  </a>
                </div>
              </div>
            </div>
          </div>
        </ResizableBox>
      </div>
    );
  }
}