import { StateServiceExtraModel } from '@local/modules/stateService/models';
import * as _ from 'lodash';
import * as React from 'react';
import { observer } from 'mobx-react';
import StateServiceNodeModel from '@local/modules/stateService/node';
import TransitionNode from '@local/modules/transition/node';
import { StateMachine, Transition } from '@local/modules/proto/stateMachine';
import { TransitionPortNode } from '@local/modules/statePort/node';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import { DeleteTwoTone, PlusCircleTwoTone } from '@ant-design/icons';
import StateMachineEngineMV from '@local/modules/engine/stateMachineEngine';
import { CustomDefaultNodeWidget } from '@components/widgets/CustomNodeWidget';
import CustomPortLabel from '@components/labels/stateMachine/CustomPortLabel';

interface IStateMachineNodeWidgetView {
  getIn: (node: StateServiceExtraModel) => void,
  setInitial: () => void,
}

@observer
class StateMachineNodeWidgetView extends CustomDefaultNodeWidget<StateMachine, StateServiceNodeModel, StateMachineEngineMV, IStateMachineNodeWidgetView> {
  generatePort = (port) => {
    return <CustomPortLabel engineMV={this.props.engineMV} port={port} key={port.getID()} />;
  };

  getModule = () => this.props.node as StateServiceNodeModel;

  onBlur = (value: string, transition: TransitionNode) => {
    const model = transition.model();
    model.trigger = value;
    this.props.engineMV.stateServiceModule.transitionsModule.update(Transition.fromPartial({
      id: transition.model().id,
      stateId: model.stateId!,
      trigger: model.trigger,
      toStateId: model.toState ? model.toState!.model().id : undefined,
      meta: JSON.stringify(model.meta),
    }))
  };

  onCreateTransition() {
    this.props.engineMV.stateServiceModule.transitionsModule.add(Transition.fromPartial({
      trigger: 'New trigger',
      stateId: this.getModule().model().id,
      meta: '{}',
    }));
  }

  onRemoveTrigger(port: TransitionPortNode) {
    this.props.engineMV.stateServiceModule.transitionsModule.remove(IdRequest.fromPartial({ id: port.transition.model().id }));
    this.props.node.removePort(port);
  }

  render(): JSX.Element {
    return (
      <div
        className={`node_block node_block_expanded ${this.props.node.isSelected() ? 'node_block_active' : ''}`}
        data-default-node-name={this.props.node.getOptions().name}
      >
        <div className={`node_block_wrapper ${(this.props.node as StateServiceNodeModel).model().initial ? 'initial' : ''}`}>
          <div className={`node_block_header`}>
            <div className="node_block_header_title">{this.props.node.getOptions().name}</div>
            <div
              className="node_block_header_pin_block"
            >
              <a
                className="node_block_header_pin"
                onMouseDown={e => e.stopPropagation()}
                onClick={() => this.props.getIn(this.getModule().model())}
              >
                &#10162;
              </a>
              <a
                className="node_block_header_pin"
                onMouseDown={e => e.stopPropagation()}
                onClick={() => this.props.setInitial()}
              >
                &#9737;
              </a>
            </div>
          </div>
          <div
            className={'node__ports sm_node__ports'}
          >
            <div
              style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-around', position: 'relative', left: '-9px' }}
            >{_.map(this.props.node.getInPorts(), this.generatePort)}</div>
            <div
              style={{ width: '100%' }}
              onClick={() => this.getModule().setSelected(false)}
            >
              {_.map(this.props.node.getOutPorts(), (i: TransitionPortNode) => (
                  <div
                    key={i.transition.model().id}
                    className="sm_input_container"
                  >
                    <input
                      className="transparent_input sm_input"
                      onBlur={({ target: { value } }) => this.onBlur(value, i.transition)}
                      defaultValue={i.transition.model().trigger || ''}
                    />
                    <DeleteTwoTone
                      onMouseDown={e => e.stopPropagation()}
                      onClick={() => this.onRemoveTrigger(i)}
                      color='primary'
                      className="sm_remove_icon"
                    />
                  </div>
                ))}
            </div>
            <div
              style={{ marginRight: -5, display: 'flex', flexDirection: 'column', justifyContent: 'space-around' }}
            >{_.map(this.props.node.getOutPorts(), this.generatePort)}</div>
          </div>
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              fontSize: 20,
            }}
          >
            <PlusCircleTwoTone
              color='primary'
              onMouseDown={e => e.stopPropagation()}
              onClick={() => this.onCreateTransition()}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default StateMachineNodeWidgetView;