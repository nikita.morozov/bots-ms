import * as React from 'react';
import * as _ from 'lodash';
import { DefaultPortLabel } from '@projectstorm/react-diagrams-defaults';
import { DefaultNodeModel } from '@projectstorm/react-diagrams';
import EngineMV from '@local/modules/engine/model';
import { ObjectWithId } from '@sharedModules/types';


export interface CustomDefaultNodeProps<LAYER_MODEL extends ObjectWithId, NODE extends DefaultNodeModel, ENGINE extends EngineMV<LAYER_MODEL>> {
  node: NODE;
  engineMV: ENGINE;
}

export class CustomDefaultNodeWidget<LAYER_MODEL extends ObjectWithId, NODE extends DefaultNodeModel, ENGINE extends EngineMV<LAYER_MODEL>, PROPS = {}> extends React.Component<CustomDefaultNodeProps<LAYER_MODEL, NODE, ENGINE> & PROPS> {
  generatePort = (port) => {
    return <DefaultPortLabel engine={this.props.engineMV.engine} port={port} key={port.getID()} />;
  };

  render() {
    const { node } = this.props;
    
    return (
      <div
        style={{
          backgroundColor: `solid 2px ${node.isSelected() ? 'rgb(0,192,255)' : 'black'}`,
          borderRadius: '5px',
          fontFamily: 'sans-serif',
          color: 'white',
          overflow: 'visible',
          fontSize: '11px',
          border: `solid 2px ${node.isSelected() ? 'rgb(0,192,255)' : 'black'}`,
        }}
        >
        <div style={{
          background: 'rgba(0, 0, 0, 0.3)',
          display: 'flex',
          whiteSpace: 'nowrap',
          justifyItems: 'center',
        }}>
          <div style={{
            flexGrow: 1,
            padding: '5px 5px',
          }}>{this.props.node.getOptions().name}</div>
        </div>
        <div style={{
          display: 'flex',
          backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.2))`,
        }}>
          <div style={{
            flexGrow: 1,
            display: 'flex',
            flexDirection: 'column',
          }}>{_.map(this.props.node.getInPorts(), this.generatePort)}</div>
          <div style={{
            flexGrow: 1,
            display: 'flex',
            flexDirection: 'column',
          }}>{_.map(this.props.node.getOutPorts(), this.generatePort)}</div>
        </div>
      </div>
    );
  }
}