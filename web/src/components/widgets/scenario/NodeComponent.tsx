import * as React from 'react';
import * as _ from 'lodash';
import { observer } from 'mobx-react';
import CustomPortLabel from '@components/labels/scenario/CustomPortLabel';
import { CustomDefaultNodeWidget } from '@components/widgets/CustomNodeWidget';
import ScenarioActionNode from '@local/modules/scenarioAction/node';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import { ScenarioModel } from '@local/modules/proto/botCommon';

interface INodeWidgetView {
  setInitial: () => void,
}

@observer
class NodeWidgetView extends CustomDefaultNodeWidget<ScenarioModel, ScenarioActionNode, ScenarioEngineMV, INodeWidgetView> {
  generatePort = (port) => {
    return <CustomPortLabel engineMV={this.props.engineMV} port={port} key={port.getID()} />;
  };

  render(): JSX.Element {
    return (
      <div
        className={`node_block ${this.props.node.isSelected() ? 'node_block_active' : ''}`}
        data-default-node-name={this.props.node.getOptions().name}
      >
        <div className={`node_block_wrapper ${this.props.node.model().initial ? 'initial' : ''}`}>
          <div className={`node_block_header`}>
            <div className="node_block_header_title">{this.props.node.getOptions().name}</div>
            <div
              className="node_block_header_pin_block"
            >
              <a
                className="node_block_header_pin"
                onMouseDown={e => e.stopPropagation()}
                onClick={() => this.props.setInitial()}
              >
                &#9737;
              </a>
            </div>
          </div>
          <div className={'node__ports'}>
            <div className="node__ports__container">{_.map(this.props.node.getInPorts(), this.generatePort)}</div>
            <div className="node__ports__container">{_.map(this.props.node.getOutPorts(), this.generatePort)}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default NodeWidgetView;