import { BaseViewModel } from '@sharedModules/Stream/model/BaseVM';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import ScenarioActionNode from '@local/modules/scenarioAction/node';

class ScenarioNodeWidgetVM extends BaseViewModel {
  private engineMV: ScenarioEngineMV;
  private node: ScenarioActionNode;

  constructor(engineMV: ScenarioEngineMV, node: ScenarioActionNode) {
    super();
    this.engineMV = engineMV;
    this.node = node;
  }

  setInitial = () => {
    this.engineMV.setInitialAction(this.node.model().id);
  }
}

export default ScenarioNodeWidgetVM;