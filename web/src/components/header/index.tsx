import {
  CodeOutlined,
  OneToOneOutlined,
  PartitionOutlined,
  RobotOutlined,
  UserSwitchOutlined,
} from '@ant-design/icons';
import routers from '@configs/routers';
import { Menu, MenuProps } from 'antd';
import React, { FC, useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

const items: MenuProps['items'] = [
  {
    label: 'Bots',
    key: routers.bots,
    icon: <RobotOutlined />,
  },
  {
    label: 'User',
    key: routers.botUsers,
    icon: <UserSwitchOutlined />,
  },
  {
    label: 'Events',
    key: routers.events,
    icon: <CodeOutlined />,
  },
  {
    label: 'Scenarios',
    key: routers.scenarios,
    icon: <PartitionOutlined />,
  },
  {
    label: 'State Machine',
    key: routers.stateMachine,
    icon: <OneToOneOutlined />,
  },
];

const MenuComponent: FC = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const [current, setCurrent] = useState<string>('');

  useEffect(() => {
    const i = items.find((item) => location.pathname.includes(item!.key as string));
    setCurrent(i ? i.key as string : location.pathname);
  }, [location]);

  const onClick: MenuProps['onClick'] = (e) => {
    navigate(e.key);
  };

  return (
    <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" items={items} />
  );
};

export default MenuComponent;