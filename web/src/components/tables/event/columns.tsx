import React from 'react';
import { BotModel, EventModel, ScenarioModel } from '@local/modules/proto/botCommon';
import { ITableMethods } from '@sharedModules/Stream/components/types';
import { ColumnsType } from 'antd/es/table';
import { Button, Divider } from 'antd';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';

export default (onRemove: (id: number) => void, onEdit: (obj: EventModel) => void, bots: BotModel[], scenarios: ScenarioModel[], methods: ITableMethods<EventModel>): ColumnsType<EventModel> => ([
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Bot',
      dataIndex: 'bot',
      key: 'bot',
      render: (value, record) => {
        const bot = bots.find(bot => bot.id === record.botId);
        if (bot) {
          return <span>{bot.slug}</span>
        } else {
          return <span/>;
        }
      },
    },
    {
      title: 'Scenario',
      dataIndex: 'scenario',
      key: 'scenario',
      render: (value, record) => {
        const scenario = scenarios.find(scenario => scenario.id === record.scenarioId);
        if (scenario) {
          return  <span>{scenario.title}</span>
        } else {
          return <span/>;
        }
      },
    },
    {
      title: 'Command',
      dataIndex: 'command',
      key: 'command',
    },
    {
      title: 'Actions',
      dataIndex: 'actions',
      key: 'actions',
      render: (value, record) => (
        <>
          <Button onClick={() => onEdit(record)}>
            <EditOutlined />
          </Button>
          <Divider type="vertical" />
          <Button onClick={() => {
            onRemove(record.id);
            methods.removeById(record.id);
          }} >
            <DeleteOutlined />
          </Button>
        </>
      ),
    },
  ]
);