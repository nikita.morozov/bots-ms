import EventUsecase from '@local/modules/event/usecase';
import { BotModel, EventModel, ScenarioModel } from '@local/modules/proto/botCommon';
import TableStream from '@local/TableStream/TableStream';
import { Button, Layout, Space } from 'antd';
import React, { FC } from 'react';
import { IBaseFormProps, IBaseTableViewProps } from '../types';
import EventForm from './forms';

interface IEventTableView extends IBaseTableViewProps<EventModel> {
  scenarioList: ScenarioModel[],
  botList: BotModel[],
}

const EventTableView: FC<IEventTableView> = (props) => {
  const {
    columns,
    add,
    closeForm,
    formVisible,
    formItem,
    scenarioList,
    botList,
  } = props;

  const Form: FC<IBaseFormProps<EventModel>> = (props) => {
    return <EventForm {...props} botList={botList} scenarioList={scenarioList} values={formItem} />;
  };

  return (
    <div>
      <Layout.Content className="top-bar">
        <Space direction="horizontal">
          <Button type="primary" onClick={() => add()}>
            Add
          </Button>
        </Space>
      </Layout.Content>
      <TableStream
        idKey="EventList"
        columns={columns}
        diKey={EventUsecase.diKey}
        showSizeChanger={false}
        form={Form}
        modalTitle="Event edit"
        formVisible={formVisible}
        closeForm={closeForm}
      />
    </div>
  );
};

export default EventTableView;