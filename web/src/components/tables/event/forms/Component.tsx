import React, { FC } from 'react';
import { IBaseFormViewProps } from '../../types';
import { BotModel, EventModel, ScenarioModel } from '@local/modules/proto/botCommon';
import { Button, Form, Input, Select } from 'antd';

interface IEventFormView extends IBaseFormViewProps<EventModel> {
  botsList: BotModel[],
  scenarioList: ScenarioModel[],
}

const EventFormView: FC<IEventFormView> = (props) => {
  const {
    form,
    onSubmit,
    botsList,
    scenarioList,
  } = props;

  return (
    <Form<EventModel>
      form={form}
      onFinish={onSubmit}
    >
      <Form.Item
        label="Bot"
        name="botId"
        initialValue={!!botsList.length && botsList[0].id}
      >
        <Select>
          {botsList.map(i => (<Select.Option key={i.id} value={i.id}>{i.slug}</Select.Option>))}
        </Select>
      </Form.Item>
      <Form.Item
        name="command"
        label="Command"
        required
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Scenario"
        name="scenarioId"
        initialValue={!!scenarioList.length && scenarioList[0].id}
      >
        <Select>
          {scenarioList.map(i => (<Select.Option key={i.id} value={i.id}>{i.title}</Select.Option>))}
        </Select>
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">Save</Button>
      </Form.Item>
    </Form>
  )
}

export default EventFormView;