import React, { FC, useCallback, useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { useInjection } from 'inversify-react';
import EventFormView from './Component';
import { IBaseFormProps } from '../../types';
import { BotModel, EventModel, ScenarioModel } from '@local/modules/proto/botCommon';
import { Form } from 'antd';
import EventUsecase, { IEventUsecase } from '@local/modules/event/usecase';

interface IEventForm extends IBaseFormProps<EventModel> {
  scenarioList: ScenarioModel[],
  botList: BotModel[],
}

const EventForm: FC<IEventForm> = observer((props) => {
  const {
    onCloseForm,
    values,
    botList,
    scenarioList,
    add,
    update,
  } = props;

  const eventUsecase = useInjection<IEventUsecase>(EventUsecase.diKey);

  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue(values);
  }, [values]);

  const submitHandler = useCallback((formValues: EventModel) => {
    const model = EventModel.fromPartial({
      botId: formValues.botId,
      scenarioId: formValues.scenarioId,
      command: formValues.command,
    });

    if (values.id) {
      model.id = values.id;
      eventUsecase.update(model).subscribe();
      update(model);
    } else {
      eventUsecase.create(model).subscribe(res => {
        add({ ...model, id: res.id }, true);
      });
    }
    onCloseForm();
  }, []);

  return <EventFormView onSubmit={submitHandler} scenarioList={scenarioList} botsList={botList} form={form} />;
});

export default EventForm;