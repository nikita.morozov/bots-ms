import BotUserUsecase from '@local/modules/botUser/usecase';
import { BotModel, BotUserModel } from '@local/modules/proto/botCommon';
import TableStream from '@local/TableStream/TableStream';
import { Button, Layout, Select, Space } from 'antd';
import React, { FC, useState } from 'react';
import { IBaseFormProps, IBaseTableViewProps } from '../types';
import BotUserForm from './forms';

interface IBotUserTableView extends IBaseTableViewProps<BotUserModel> {
  botsList: BotModel[],
}

const { Option } = Select;

const BotUserTableView: FC<IBotUserTableView> = (props) => {
  const {
    columns,
    add,
    closeForm,
    formVisible,
    formItem,
    botsList,
  } = props;

  const Form: FC<IBaseFormProps<BotUserModel>> = (props) => {
    return <BotUserForm {...props} botsList={botsList} values={formItem} />;
  };

  if (!botsList.length) {
    return <div>Bots list is empty</div>;
  }

  const [bot, setBot] = useState(botsList[0].id);

  return (
    <div>
      <Layout.Content className="top-bar">
        <Space direction="horizontal">
          <Select
            style={{ width: 200 }}
            placeholder="Select bot"
            onChange={setBot}
            defaultValue={bot}
          >
            {botsList.map(i => (<Option key={i.id} value={i.id}>{i.slug}</Option>))}
          </Select>
          <Button type="primary" onClick={() => add()}>
            Add
          </Button>
        </Space>
      </Layout.Content>
      <TableStream
        idKey="BotUserList"
        columns={columns}
        diKey={BotUserUsecase.diKey}
        showSizeChanger={false}
        form={Form}
        modalTitle="Bot user edit"
        formVisible={formVisible}
        closeForm={closeForm}
        query={{ handler: 'telegram' }}
      />
    </div>
  );
};

export default BotUserTableView;