import React from 'react';
import { BotModel, BotUserModel } from '@local/modules/proto/botCommon';
import { ColumnsType } from 'antd/es/table';
import { ITableMethods } from '@sharedModules/Stream/components/types';
import { Button, Divider } from 'antd';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';

export default (onRemove: (id: number) => void, onEdit: (obj: BotUserModel) => void, bots: BotModel[], methods: ITableMethods<BotUserModel>): ColumnsType<BotUserModel> => ([
    {
      title: "ID",
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: "Bot",
      dataIndex: 'bot',
      key: 'bot',
      render: (value, record) => {
        const bot = bots.find(bot => bot.id === record.botId);
        if (bot) {
          return <span>{bot.slug}</span>
        } else {
          return <span/>;
        }
      }
    },
    {
      title: "User",
      dataIndex: 'userId',
      key: 'userId',
    },
    {
      title: "Meta",
      dataIndex: 'meta',
      key: 'meta',
    },
    {
      title: "Actions",
      dataIndex: 'actions',
      key: 'actions',
      render: (value, record) => (
        <>
          <Button onClick={() => onEdit(record)}>
            <EditOutlined />
          </Button>
          <Divider type="vertical" />
          <Button onClick={() => {
            onRemove(record.id);
            methods.removeById(record.id);
          }} >
            <DeleteOutlined />
          </Button>
        </>
      )
    },
  ]
)