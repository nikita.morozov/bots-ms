import React, { FC, useCallback, useEffect, useState } from 'react';
import columns from './columns';
import { observer } from 'mobx-react-lite';
import { useInjection } from 'inversify-react';
import BotUserTableView from './Component';
import { ListOptions } from '@sharedModules/shared/paginatior';
import { BotModel, BotUserModel } from '@local/modules/proto/botCommon';
import { ITableMethods } from '@sharedModules/Stream/components/types';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import BotUsecase, { IBotUsecase } from '@local/modules/bot/usecase';
import BotUserUsecase, { IBotUserUsecase } from '@local/modules/botUser/usecase';

const BotUserTable: FC = observer(() => {
  const botUserUsecase = useInjection<IBotUserUsecase>(BotUserUsecase.diKey);
  const botUsecase = useInjection<IBotUsecase>(BotUsecase.diKey);

  const [botsList, setBotsList] = useState<BotModel[]>([]);
  const [formItem, setFormItem] = useState<BotUserModel>({} as BotUserModel);
  const [formVisible, setFormVisible] = useState(false);

  const onEdit = useCallback((values: BotUserModel) => {
    setFormItem(values);
    setFormVisible(true);
  }, []);

  const onRemove = useCallback((id: number) => {
    botUserUsecase.remove(IdRequest.fromPartial({ id })).subscribe();
  }, []);

  useEffect(() => {
    const sub = botUsecase.load(ListOptions.fromPartial({ limit: 1000, offset: 0, order: 'id asc' })).subscribe(res => {
     setBotsList(res.items);
    });

    return () => sub.unsubscribe();
  }, []);


  const columnsData = useCallback((methods: ITableMethods<BotUserModel>) => columns(onRemove, onEdit, botsList, methods), [botsList]);

  return <BotUserTableView
    add={() => onEdit({} as BotUserModel)}
    columns={columnsData}
    closeForm={() => setFormVisible(false)}
    formVisible={formVisible}
    botsList={botsList}
    formItem={formItem}
  />;
});

export default BotUserTable;