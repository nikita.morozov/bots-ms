import React, { FC, useCallback, useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { useInjection } from 'inversify-react';
import { IBaseFormProps } from '../../types';
import BotUserFormView from './Component';
import { BotModel, BotUserModel } from '@local/modules/proto/botCommon';
import { Form } from 'antd';
import BotUserUsecase, { IBotUserUsecase } from '@local/modules/botUser/usecase';

interface IBotUserForm extends IBaseFormProps<BotUserModel> {
  botsList: BotModel[],
}

const BotUserForm: FC<IBotUserForm> = observer((props) => {
  const {
    onCloseForm,
    botsList,
    values,
    add,
    update,
  } = props;

  const botUserUsecase = useInjection<IBotUserUsecase>(BotUserUsecase.diKey);

  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue(values);
  }, [values]);

  const submitHandler = useCallback((formValues) => {
    const model = BotUserModel.fromPartial({
      botId: formValues.botId,
      userId: formValues.userId,
      meta: formValues.meta,
    });

    if (values.id) {
      model.id = values.id;
      botUserUsecase.update(model).subscribe();
      update(model);
    } else {
      botUserUsecase.create(model).subscribe(res => {
        add({ ...model, id: res.id }, true);
      });
    }
    onCloseForm();
  }, []);

  return <BotUserFormView botsList={botsList} onSubmit={submitHandler} form={form} />;
});

export default BotUserForm;