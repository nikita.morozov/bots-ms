import React, { FC } from 'react';
import { Button, Form, Input, Select } from 'antd';
import { BotModel, BotUserModel } from '@local/modules/proto/botCommon';
import { IBaseFormViewProps } from '../../types';

interface IBotUserFormView extends IBaseFormViewProps<BotUserModel> {
  botsList: BotModel[],
}

const BotUserFormView: FC<IBotUserFormView> = (props) => {
  const {
    form,
    onSubmit,
    botsList,
  } = props;

  return (
    <Form<BotUserModel>
      form={form}
      onFinish={onSubmit}
    >
      <Form.Item
        label="Bot"
        name="botId"
        initialValue={!!botsList.length && botsList[0].id}
      >
        <Select>
          {botsList.map(i => (<Select.Option key={i.id} value={i.id}>{i.slug}</Select.Option>))}
        </Select>
      </Form.Item>
      <Form.Item
        label="User"
        name="userId"
        required
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="meta"
        label="Meta"
        required
      >
        <Input />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">Save</Button>
      </Form.Item>
    </Form>
  )
}

export default BotUserFormView;