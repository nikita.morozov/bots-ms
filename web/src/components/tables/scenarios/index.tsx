import routers from '@configs/routers';
import React, { FC, useCallback, useEffect, useState } from 'react';
import columns from './columns';
import { useInjection } from 'inversify-react';
import { observer } from 'mobx-react-lite';
import ScenariosTableView from './Component';
import { ITableMethods } from '@sharedModules/Stream/components/types';
import { ScenarioModel } from '@local/modules/proto/botCommon';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import ScenarioUsecase, { IScenarioUsecase } from '@local/modules/scenario/usecase';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import { useNavigate } from 'react-router-dom';

const ScenarioTable: FC = observer(() => {
  const engine = useInjection<ScenarioEngineMV>(ScenarioEngineMV.diKey);
  const navigate = useNavigate();

  const scenarioUsecase = useInjection<IScenarioUsecase>(ScenarioUsecase.diKey);

  const [formItem, setFormItem] = useState<ScenarioModel>({} as ScenarioModel);
  const [formVisible, setFormVisible] = useState(false);

  const onEdit = useCallback((values: ScenarioModel) => {
    setFormItem(values);
    setFormVisible(true);
  }, []);

  const onSetLayoutVisible = useCallback((values: ScenarioModel) => {
    navigate(routers.scenarioItem.replace(':id', values.id.toString()));
  }, []);

  const onRemoveScenario = useCallback((id: number) => {
    scenarioUsecase.delete(IdRequest.fromPartial({ id })).subscribe();
  }, []);

  const columnsData = useCallback((methods: ITableMethods<ScenarioModel>) => columns(onRemoveScenario, onEdit, onSetLayoutVisible, methods), []);

  useEffect(() => {
    engine.clear();
  }, []);

  return <ScenariosTableView
    add={() => onEdit({} as ScenarioModel)}
    columns={columnsData}
    closeForm={() => setFormVisible(false)}
    formVisible={formVisible}
    formItem={formItem}
  />;
});

export default ScenarioTable;