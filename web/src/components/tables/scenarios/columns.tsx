import routers from '@configs/routers';
import React from 'react';
import { ScenarioModel } from '@local/modules/proto/botCommon';
import { ColumnsType } from 'antd/es/table';
import { ITableMethods } from '@sharedModules/Stream/components/types';
import { Button, Divider, Popconfirm, Space } from 'antd';
import { DeleteOutlined, EditOutlined, LinkOutlined, SettingOutlined } from '@ant-design/icons';
import CopyToClipboard from '@modules/copyToClipboard/CopyToClipboard';
import ToasterMessageHandler from '@modules/toaster/handler';

export default (onRemove: (id: number) => void, onEdit: (obj: ScenarioModel) => void, onView: (obj: ScenarioModel) => void, methods: ITableMethods<ScenarioModel>): ColumnsType<ScenarioModel> => ([
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Title',
    dataIndex: 'title',
    key: 'title',
    render: (value, record) => (
      <Space direction="horizontal">
        {record.title}
        <LinkOutlined onClick={() => CopyToClipboard(`${window.location.host}${routers.scenarioItem.replace(':id', record.id.toString())}`, new ToasterMessageHandler())} />
      </Space>
    )
  },
  {
    title: 'Actions',
    dataIndex: 'actions',
    key: 'actions',
    render: (value, record) => (
      <>
        <Button type="primary" onClick={() => onView(record)}>
          <EditOutlined />
        </Button>
        <Divider type="vertical" />
        <Button onClick={() => onEdit(record)}>
          <SettingOutlined />
        </Button>
        <Divider type="vertical" />
        <Popconfirm
          title="Delete it"
          description="Are you sure to delete this item?"
          onConfirm={() => {
            onRemove(record.id);
            methods.removeById(record.id);
          }}
          okText="Yes"
          cancelText="No"
        >
          <Button type="primary" danger>
            <DeleteOutlined />
          </Button>
        </Popconfirm>
      </>
    ),
  },
]);