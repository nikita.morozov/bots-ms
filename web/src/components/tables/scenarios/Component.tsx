import { ScenarioModel } from '@local/modules/proto/botCommon';
import ScenarioUsecase from '@local/modules/scenario/usecase';
import TableStream from '@local/TableStream/TableStream';
import { Button, Layout, Space } from 'antd';
import React, { FC } from 'react';
import { IBaseFormProps, IBaseTableViewProps } from '../types';
import ScenarioForm from './forms';

const ScenariosTableView: FC<IBaseTableViewProps<ScenarioModel>> = (props) => {
  const {
    add,
    columns,
    formVisible,
    closeForm,
    formItem,
  } = props;

  const Form: FC<IBaseFormProps<ScenarioModel>> = (props) => {
    return <ScenarioForm {...props} values={formItem} />;
  };

  return (
    <div>
      <Layout.Content className="top-bar">
        <Space direction="horizontal">
          <Button type="primary" onClick={() => add()}>
            Add
          </Button>
        </Space>
      </Layout.Content>
      <TableStream
        idKey="ScenariosList"
        columns={columns}
        diKey={ScenarioUsecase.diKey}
        showSizeChanger={false}
        form={Form}
        modalTitle="Scenario edit"
        formVisible={formVisible}
        closeForm={closeForm}
      />
    </div>
  );
};

export default ScenariosTableView;