import React, { FC, useCallback, useEffect } from 'react';
import { useInjection } from 'inversify-react';
import { observer } from 'mobx-react-lite';
import ScenariosFormView from './Component';
import { IBaseFormProps } from '../../types';
import { ScenarioModel } from '@local/modules/proto/botCommon';
import ScenarioUsecase, { IScenarioUsecase } from '@local/modules/scenario/usecase';
import { Form } from 'antd';

const ScenarioForm: FC<IBaseFormProps<ScenarioModel>> = observer((props) => {
  const {
    values,
    onCloseForm,
    add,
    update,
  } = props;

  const scenarioUsecase = useInjection<IScenarioUsecase>(ScenarioUsecase.diKey);

  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue(values);
  }, [values]);

  const submitHandler = useCallback((formValues: ScenarioModel) => {
    const model = ScenarioModel.fromPartial({
      title: formValues.title,
      category: formValues.category,
    });

    if (values.id) {
      model.id = values.id;
      scenarioUsecase.update(model).subscribe();
      update(model);
    } else {
      scenarioUsecase.create(model).subscribe(res => {
        add({ ...model, id: res.id }, true);
      });
    }
    onCloseForm();
  }, []);

  return <ScenariosFormView onSubmit={submitHandler} form={form} />
});

export default ScenarioForm;