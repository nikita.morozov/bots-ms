import React, { FC } from 'react';
import { IBaseFormViewProps } from '../../types';
import { ScenarioModel } from '@local/modules/proto/botCommon';
import { Button, Form, Input } from 'antd';

const ScenariosFormView: FC<IBaseFormViewProps<ScenarioModel>> = (props) => {
  const {
    form,
    onSubmit,
  } = props;

  return (
    <Form<ScenarioModel>
      form={form}
      onFinish={onSubmit}
    >
      <Form.Item
        name="title"
        label="Title"
        required
      >
        <Input />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">Save</Button>
      </Form.Item>
    </Form>
  )
}

export default ScenariosFormView;