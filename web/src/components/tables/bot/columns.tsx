import React from 'react';
import { BotModel } from '@local/modules/proto/botCommon';
import { ColumnsType } from 'antd/es/table';
import { Button, Divider } from 'antd';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { ITableMethods } from '@sharedModules/Stream/components/types';

export default (onRemove: (id: number) => void, onEdit: (obj: BotModel) => void, methods: ITableMethods<BotModel>,): ColumnsType<BotModel> => ([
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Token',
    dataIndex: 'token',
    key: 'token',
  },
  {
    title: 'Slug',
    dataIndex: 'slug',
    key: 'slug',
    render: (value, record) => <a target='_blank' href={`https://t.me/${record.slug}`}>{record.slug}</a>,
  },
  {
    title: 'Handler',
    dataIndex: 'handler',
    key: 'handler',
  },
  {
    title: 'Actions',
    dataIndex: 'actions',
    key: 'actions',
    render: (value, record) => (
      <>
        <Button onClick={() => onEdit(record)}>
          <EditOutlined />
        </Button>
        <Divider type="vertical" />
        <Button onClick={() => {
          onRemove(record.id);
          methods.removeById(record.id);
        }} >
          <DeleteOutlined />
        </Button>
      </>
    )
  },
]);