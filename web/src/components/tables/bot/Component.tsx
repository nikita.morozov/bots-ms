import BotUsecase from '@local/modules/bot/usecase';
import { BotModel } from '@local/modules/proto/botCommon';
import TableStream from '@local/TableStream/TableStream';
import { Button, Space, Layout } from 'antd';
import React, { FC, useCallback } from 'react';
import { IBaseFormProps, IBaseTableViewProps } from '../types';
import BotForm from './forms';

const Content = Layout.Content;

const BotTableView: FC<IBaseTableViewProps<BotModel>> = (props) => {
  const {
    add,
    columns,
    formItem,
    closeForm,
    formVisible,
  } = props;

  const Form: FC<IBaseFormProps<BotModel>> = useCallback((props) => {
    return <BotForm {...props} values={formItem} />;
  }, [formItem]);

  return (
    <div>
      <Content className="top-bar">
        <Space direction="horizontal">
          <Button type="primary" onClick={() => add()}>
            Add
          </Button>
        </Space>
      </Content>
      <TableStream<BotModel>
        columns={columns}
        idKey="BotList"
        diKey={BotUsecase.diKey}
        showSizeChanger={false}
        form={Form}
        modalTitle="Bot edit"
        formVisible={formVisible}
        closeForm={closeForm}
      />
    </div>
  );
};

export default BotTableView;