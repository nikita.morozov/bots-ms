import React, { FC, useCallback, useState } from 'react';
import columns from './columns';
import { observer } from 'mobx-react-lite';
import { useInjection } from 'inversify-react';
import BotTableView from './Component';
import { ITableMethods } from '@sharedModules/Stream/components/types';
import { BotModel } from '@local/modules/proto/botCommon';
import { IdRequest } from '@local/modules/proto/commonTypes';
import BotUsecase, { IBotUsecase } from '@local/modules/bot/usecase';

const BotTable: FC = observer(() => {
  const botsUsecase = useInjection<IBotUsecase>(BotUsecase.diKey);

  const [formItem, setFormItem] = useState<BotModel>({} as BotModel);
  const [formVisible, setFormVisible] = useState(false);

  const onEdit = useCallback((values: BotModel) => {
    setFormItem(values);
    setFormVisible(true);
  }, []);

  const onRemoveBot = useCallback((id: number) => {
    botsUsecase.remove(IdRequest.fromPartial({ id })).subscribe();
  }, []);

  const columnsData = useCallback((methods: ITableMethods<BotModel>) => columns(onRemoveBot, onEdit, methods), []);

  const addHandler = useCallback(() => onEdit({} as BotModel), []);
  const closeHandler = useCallback(() => setFormVisible(false), []);

  return <BotTableView
    add={addHandler}
    columns={columnsData}
    formItem={formItem}
    closeForm={closeHandler}
    formVisible={formVisible}
  />;
});

export default BotTable;