import React, { FunctionComponent, useCallback, useEffect } from 'react';
import { useInjection } from 'inversify-react';
import { observer } from 'mobx-react-lite';
import { BotModel } from '@local/modules/proto/botCommon';
import BotUsecase, { IBotUsecase } from '@local/modules/bot/usecase';
import BotFormView from './Component';
import { Form } from 'antd';
import { IBaseFormProps } from '../../types';

const BotForm: FunctionComponent<IBaseFormProps<BotModel>> = observer((props) => {
  const {
    onCloseForm,
    values,
    add,
    update,
  } = props;

  const botsUsecase = useInjection<IBotUsecase>(BotUsecase.diKey);

  const [form] = Form.useForm();

  useEffect(() => {
    form.resetFields();
    form.setFieldsValue(values);
  }, [values]);

  const submitHandler = useCallback((formValues: BotModel) => {
    const model = BotModel.fromPartial({
      token: formValues.token,
      env: formValues.env,
      slug: formValues.slug,
      handler: formValues.handler,
      disableTriggers: formValues.disableTriggers,
      allowPlainCommand: formValues.allowPlainCommand,
      autoRemoveMessages: formValues.autoRemoveMessages,
      executorHandler: formValues.executorHandler,
    });

    if (values.id) {
      model.id = values.id;
      botsUsecase.update(model).subscribe();
      update(model);
    } else {
      botsUsecase.create(model).subscribe(res => {
        add({ ...model, id: res.id }, true);
      });
    }
    onCloseForm();
  }, []);

  return <BotFormView form={form} onSubmit={submitHandler} />;
});

export default BotForm;