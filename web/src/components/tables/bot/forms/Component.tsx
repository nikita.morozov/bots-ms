import React, { FC } from 'react';
import { BotModel } from '@local/modules/proto/botCommon';
import { Button, Form, Input, Select, Switch } from 'antd';
import { IBaseFormViewProps } from '../../types';

const BotFormView: FC<IBaseFormViewProps<BotModel>> = (props) => {
  const {
    form,
    onSubmit,
  } = props;

  return (
    <Form<BotModel>
      form={form}
      onFinish={onSubmit}
    >
      <Form.Item
        name="token"
        label="Token"
        required
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="slug"
        label="Slug"
        required
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="handler"
        label="Handler"
        required
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Disable Triggers"
        name="disableTriggers"
        valuePropName="checked"
      >
        <Switch />
      </Form.Item>
      <Form.Item
        label="Allow Plain Command"
        name="allowPlainCommand"
        valuePropName="checked"
      >
        <Switch />
      </Form.Item>
      <Form.Item
        label="Auto Remove Messages"
        name="autoRemoveMessages"
        valuePropName="checked"
      >
        <Switch />
      </Form.Item>
      <Form.Item
        label="Enviroment"
        name="env"
        initialValue='production'
      >
        <Select>
          <Select.Option key='production' value="production">Production</Select.Option>
          <Select.Option key='development' value="development">Development</Select.Option>
          <Select.Option key='local' value="development">Local</Select.Option>
        </Select>
      </Form.Item>
      <Form.Item
        label="Executor"
        name="executorHandler"
        initialValue='state'
      >
        <Select>
          <Select.Option key='state' value="state">State</Select.Option>
          <Select.Option key='event' value="event">Event</Select.Option>
        </Select>
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">Save</Button>
      </Form.Item>
    </Form>
  )
}

export default BotFormView;