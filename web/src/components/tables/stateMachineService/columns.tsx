import { DeleteOutlined, EditOutlined, ExportOutlined, EyeOutlined, SettingOutlined } from '@ant-design/icons';
import { BotModel } from '@local/modules/proto/botCommon';
import { StateMachine } from '@local/modules/proto/stateMachine';
import { ITableMethods } from '@sharedModules/Stream/components/types';
import { Button, Divider, Popconfirm } from 'antd';
import { ColumnsType } from 'antd/es/table';
import React from 'react';

export default (onRemove: (id: number) => void, onEdit: (obj: StateMachine) => void, onView: (obj: StateMachine) => void, bots: BotModel[], methods: ITableMethods<StateMachine>): ColumnsType<StateMachine> => ([
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Bot',
    dataIndex: 'bot',
    key: 'bot',
    render: (value, record) => {
      const bot = bots.find(bot => bot.id === record.botId);
      if (bot) {
        return <span>{bot.slug}</span>;
      } else {
        return <span />;
      }
    },
  },
  {
    title: 'Meta',
    dataIndex: 'meta',
    key: 'meta',
  },
  {
    title: 'Actions',
    dataIndex: 'actions',
    key: 'actions',
    render: (value, record) => (
      <>
        <Button type="primary" onClick={() => onView(record)}>
          <EditOutlined />
        </Button>
        <Divider type="vertical" />
        <Button onClick={() => onEdit(record)}>
          <SettingOutlined />
        </Button>
        <Divider type="vertical" />
        <Button
          onClick={() => window.location.href = `${process.env.VITE_PUBLIC_API_DOMAIN_HTTP}/api/v1/serialization/export/${record.id}`}>
          <ExportOutlined />
        </Button>
        <Divider type="vertical" />
        <Popconfirm
          title="Delete it"
          description="Are you sure to delete this item?"
          onConfirm={() => {
            onRemove(record.id);
            methods.removeById(record.id);
          }}
          okText="Yes"
          cancelText="No"
        >
          <Button type="primary" danger>
            <DeleteOutlined />
          </Button>
        </Popconfirm>
      </>
    ),
  },
]);