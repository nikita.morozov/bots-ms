import { InboxOutlined } from '@ant-design/icons';
import { BotModel } from '@local/modules/proto/botCommon';
import { ImportObject } from '@local/modules/stateMachineService/types';
import { Button, Form, Select, Upload, UploadProps, message } from 'antd';
import { RcFile } from 'antd/es/upload';
import React, { FC } from 'react';

interface ImportFormProps {
  botsList: BotModel[],
  onSubmit: (values: ImportObject) => boolean,
}

const uploaderProps: UploadProps = {
  name: 'file',
  accept: '.json',
  multiple: false,
  maxCount: 1,
  action: ((file: RcFile) => ""),
  beforeUpload: () => false,
};

const ImportForm: FC<ImportFormProps> = ({ botsList, onSubmit }) => {
  const [form] = Form.useForm();
  return (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      style={{ maxWidth: 600 }}
      initialValues={{ remember: true }}
      onFinish={({botId, file }) => {
        onSubmit({ file: file.file, botId });
        form.resetFields();
      }}
      // onFinish={onFinish}
      // onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="Bot"
        name="botId"
        initialValue={!!botsList.length && botsList[0].id}
      >
        <Select>
          {botsList.map(i => (<Select.Option key={i.id} value={i.id}>{i.slug}</Select.Option>))}
        </Select>
      </Form.Item>
      <Form.Item label="Dragger">
        <Form.Item name="file" valuePropName="file" noStyle>
          <Upload.Dragger name="file" {...uploaderProps}>
            <p className="ant-upload-drag-icon">
              <InboxOutlined />
            </p>
            <p className="ant-upload-text">Click or drag file to this area to upload</p>
          </Upload.Dragger>
        </Form.Item>
      </Form.Item>
      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  )
}

export default ImportForm;