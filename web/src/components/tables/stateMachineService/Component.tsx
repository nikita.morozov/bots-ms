import { ImportOutlined } from '@ant-design/icons';
import { BotModel } from '@local/modules/proto/botCommon';
import { StateMachine } from '@local/modules/proto/stateMachine';
import { ImportObject } from '@local/modules/stateMachineService/types';
import StateMachineServiceUsecase from '@local/modules/stateMachineService/usecase';
import TableStream from '@local/TableStream/TableStream';
import { Button, Layout, Modal, Space, message } from 'antd';
import React, { FC, useCallback, useState } from 'react';
import { IBaseFormProps, IBaseTableViewProps } from '../types';
import ScenarioMachineForm from './forms';
import ImportForm from './ImportForm';

interface IStateMachineTableView extends IBaseTableViewProps<StateMachine> {
  botsList: BotModel[],
  importJson: (values: ImportObject) => void,
}

const normFile = (e: any) => {
  console.log('Upload event:', e);
  if (Array.isArray(e)) {
    return e;
  }
  return e?.fileList;
};

const StateMachineTableView: FC<IStateMachineTableView> = (props) => {
  const {
    add,
    columns,
    formVisible,
    closeForm,
    botsList,
    formItem,
  } = props;

  const [showImport, setShowImport] = useState(false);

  const ScenarioForm: FC<IBaseFormProps<StateMachine>> = (props) => {
    return <ScenarioMachineForm {...props} botsList={botsList} values={formItem} />;
  };

  const onImportHandler = useCallback((values: ImportObject) => {
    props.importJson(values);
    setShowImport(false);
    message.success('Import complete');
    return true;
  }, [props.importJson]);

  return (
    <div>
      <Layout.Content className="top-bar">
        <Space direction="horizontal">
          <Button type="primary" onClick={() => add()}>
            Add
          </Button>
          <Button icon={<ImportOutlined />} onClick={() => setShowImport(true)}>
            Import
          </Button>
        </Space>
      </Layout.Content>
      <TableStream
        idKey="StateMachineList"
        columns={columns}
        diKey={StateMachineServiceUsecase.diKey}
        form={ScenarioForm}
        modalTitle="State machine edit"
        formVisible={formVisible}
        closeForm={closeForm}
      />
      <Modal
        open={showImport}
        title="Import JSON of state machine"
        footer={null}
        onCancel={() => setShowImport(false)}
      >
        <ImportForm botsList={botsList} onSubmit={onImportHandler} />
      </Modal>
    </div>
  );
};

export default StateMachineTableView;