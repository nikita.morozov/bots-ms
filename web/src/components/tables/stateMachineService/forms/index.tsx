import React, { FC, useCallback, useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { useInjection } from 'inversify-react';
import StateMachineFormView from './Component';
import { IBaseFormProps } from '../../types';
import { StateMachine } from '@local/modules/proto/stateMachine';
import { BotModel } from '@local/modules/proto/botCommon';
import { Form } from 'antd';
import StateMachineServiceUsecase, { IStateMachineServiceUsecase } from '@local/modules/stateMachineService/usecase';

interface IStateMachineServiceForm extends IBaseFormProps<StateMachine> {
  botsList: BotModel[],
}

const ScenarioMachineForm: FC<IStateMachineServiceForm> = observer((props) => {
  const {
    botsList,
    values,
    onCloseForm,
    add,
    update,
  } = props;

  const stateMachineServiceUsecase = useInjection<IStateMachineServiceUsecase>(StateMachineServiceUsecase.diKey);

  const [form] = Form.useForm();

  useEffect(() => {
    form.setFieldsValue(values);
  }, [values]);

  const submitHandler = useCallback((formValues: StateMachine) => {
    const model = StateMachine.fromPartial({
      botId: formValues.botId,
      name: formValues.name,
    });

    if (values.id) {
      model.id = values.id;
      stateMachineServiceUsecase.update(model).subscribe();
      update(model);
    } else {
      stateMachineServiceUsecase.create(model).subscribe(res => {
        add({ ...model, id: res.id }, true);
      });
    }
    onCloseForm();
  }, []);

  return <StateMachineFormView form={form} onSubmit={submitHandler} botsList={botsList} />;
});

export default ScenarioMachineForm;