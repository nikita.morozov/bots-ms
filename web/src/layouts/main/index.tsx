import { Layout } from 'antd';
import { FC, PropsWithChildren } from 'react';
import Header from '@components/header';
import React from 'react';

const MainLayout: FC<PropsWithChildren> = ({ children }) => (
  <Layout className="layout">
      <Header />
      <Layout>
        {children}
      </Layout>
  </Layout>
);

export default MainLayout;