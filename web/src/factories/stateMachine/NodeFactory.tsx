import { DefaultNodeFactory } from '@projectstorm/react-diagrams-defaults';
import * as React from 'react';
import StateMachineEngineMV from '@local/modules/engine/stateMachineEngine';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import StateMachineNodeWidget from '@components/widgets/stateMachine';

export class StateNodeFactory extends DefaultNodeFactory {
  private readonly stateMachineEngineVM: StateMachineEngineMV;
  private readonly scenarioEngineVM: ScenarioEngineMV;

  constructor(stateMachineEngineVM: StateMachineEngineMV, scenarioEngineVM: ScenarioEngineMV) {
    super();
    this.type = 'node_factory';
    this.stateMachineEngineVM = stateMachineEngineVM;
    this.scenarioEngineVM = scenarioEngineVM;
  }

  generateReactWidget(event: any): JSX.Element {
    return <StateMachineNodeWidget scenarioEngineVM={this.scenarioEngineVM} engineMV={this.stateMachineEngineVM} node={event.model} />
  }
}