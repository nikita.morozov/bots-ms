import { DefaultNodeFactory } from '@projectstorm/react-diagrams';
import * as React from 'react';
import EngineMV from '@local/modules/engine/model';
import CommentBoxNodeWidget from '@components/widgets/commentBox';
import { ObjectWithId } from '@sharedModules/types';

export class CommentBoxFactory<ENGINE_MODULE extends ObjectWithId> extends DefaultNodeFactory {
  private readonly engineVM: EngineMV<ENGINE_MODULE>;

  constructor(engineVM: EngineMV<ENGINE_MODULE>) {
    super();
    this.type = 'comment_box_factory';
    this.engineVM = engineVM;
  }

  generateReactWidget(event: any): JSX.Element {
    return <CommentBoxNodeWidget engineMV={this.engineVM} node={event.model} />
  }
}