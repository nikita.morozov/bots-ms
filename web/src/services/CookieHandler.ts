import { injectable } from 'inversify';
import Cookies from "ts-cookies";

const isBrowser = typeof window !== 'undefined';

@injectable()
class CookieHandler {
	delete(key: string): void {
		if (!isBrowser) {
			return;
		}

		if (!document) {
			return;
		}

		document.cookie = `${key}=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT`;
	}

	get(key: string): string {
    return Cookies.get(key);
	}

	set(key: string, value: string, duration: number | undefined): void {
		if (!isBrowser || !document || !value) {
			return;
		}

		document.cookie = `${key}=${value}; path=/;Max-Age=${parseInt((duration && `${duration}`) || import.meta.env.PUBLIC_TOKEN_DURATION || '2629743', 10)}`;
	}
}

export default CookieHandler;