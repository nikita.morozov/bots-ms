#!/bin/sh

echo "Compiling proto file(s)..."

BOT_SOURCE_PROTO_PATH=../../../proto
BOT_PROTO_PATH=../local/modules/proto

protoc \
  $BOT_SOURCE_PROTO_PATH/action.proto \
  --plugin=./node_modules/.bin/protoc-gen-ts_proto \
  --ts_proto_out=$BOT_PROTO_PATH \
	--proto_path=$BOT_SOURCE_PROTO_PATH \
  --ts_proto_opt=outputClientImpl=grpc-web \
  --ts_proto_opt=useOptionals=true \
  --ts_proto_opt=outputTypeRegistry=true \
  --ts_proto_opt=esModuleInterop=true \
  --ts_proto_opt=outputClientImpl=grpc-web

protoc \
  $BOT_SOURCE_PROTO_PATH/bot.proto \
  --plugin=./node_modules/.bin/protoc-gen-ts_proto \
  --ts_proto_out=$BOT_PROTO_PATH \
	--proto_path=$BOT_SOURCE_PROTO_PATH \
  --ts_proto_opt=outputClientImpl=grpc-web \
  --ts_proto_opt=useOptionals=true \
  --ts_proto_opt=outputTypeRegistry=true \
  --ts_proto_opt=esModuleInterop=true \
  --ts_proto_opt=outputClientImpl=grpc-web

protoc \
  $BOT_SOURCE_PROTO_PATH/botCommon.proto \
  --plugin=./node_modules/.bin/protoc-gen-ts_proto \
  --ts_proto_out=$BOT_PROTO_PATH \
	--proto_path=$BOT_SOURCE_PROTO_PATH \
  --ts_proto_opt=outputClientImpl=grpc-web \
  --ts_proto_opt=useOptionals=true \
  --ts_proto_opt=outputTypeRegistry=true \
  --ts_proto_opt=esModuleInterop=true \
  --ts_proto_opt=outputClientImpl=grpc-web

protoc \
  $BOT_SOURCE_PROTO_PATH/botUser.proto \
  --plugin=./node_modules/.bin/protoc-gen-ts_proto \
  --ts_proto_out=$BOT_PROTO_PATH \
	--proto_path=$BOT_SOURCE_PROTO_PATH \
  --ts_proto_opt=outputClientImpl=grpc-web \
  --ts_proto_opt=useOptionals=true \
  --ts_proto_opt=outputTypeRegistry=true \
  --ts_proto_opt=esModuleInterop=true \
  --ts_proto_opt=outputClientImpl=grpc-web

protoc \
  $BOT_SOURCE_PROTO_PATH/connector.proto \
  --plugin=./node_modules/.bin/protoc-gen-ts_proto \
  --ts_proto_out=$BOT_PROTO_PATH \
	--proto_path=$BOT_SOURCE_PROTO_PATH \
  --ts_proto_opt=outputClientImpl=grpc-web \
  --ts_proto_opt=useOptionals=true \
  --ts_proto_opt=outputTypeRegistry=true \
  --ts_proto_opt=esModuleInterop=true \
  --ts_proto_opt=outputClientImpl=grpc-web

protoc \
  $BOT_SOURCE_PROTO_PATH/event.proto \
  --plugin=./node_modules/.bin/protoc-gen-ts_proto \
  --ts_proto_out=$BOT_PROTO_PATH \
	--proto_path=$BOT_SOURCE_PROTO_PATH \
  --ts_proto_opt=outputClientImpl=grpc-web \
  --ts_proto_opt=useOptionals=true \
  --ts_proto_opt=outputTypeRegistry=true \
  --ts_proto_opt=esModuleInterop=true \
  --ts_proto_opt=outputClientImpl=grpc-web

protoc \
  $BOT_SOURCE_PROTO_PATH/scenario.proto \
  --plugin=./node_modules/.bin/protoc-gen-ts_proto \
  --ts_proto_out=$BOT_PROTO_PATH \
	--proto_path=$BOT_SOURCE_PROTO_PATH \
  --ts_proto_opt=outputClientImpl=grpc-web \
  --ts_proto_opt=useOptionals=true \
  --ts_proto_opt=outputTypeRegistry=true \
  --ts_proto_opt=esModuleInterop=true \
  --ts_proto_opt=outputClientImpl=grpc-web

protoc \
  $BOT_SOURCE_PROTO_PATH/scenarioAction.proto \
  --plugin=./node_modules/.bin/protoc-gen-ts_proto \
  --ts_proto_out=$BOT_PROTO_PATH \
	--proto_path=$BOT_SOURCE_PROTO_PATH \
  --ts_proto_opt=outputClientImpl=grpc-web \
  --ts_proto_opt=useOptionals=true \
  --ts_proto_opt=outputTypeRegistry=true \
  --ts_proto_opt=esModuleInterop=true \
  --ts_proto_opt=outputClientImpl=grpc-web

protoc \
  $BOT_SOURCE_PROTO_PATH/scenarioItem.proto \
  --plugin=./node_modules/.bin/protoc-gen-ts_proto \
  --ts_proto_out=$BOT_PROTO_PATH \
	--proto_path=$BOT_SOURCE_PROTO_PATH \
  --ts_proto_opt=outputClientImpl=grpc-web \
  --ts_proto_opt=useOptionals=true \
  --ts_proto_opt=outputTypeRegistry=true \
  --ts_proto_opt=esModuleInterop=true \
  --ts_proto_opt=outputClientImpl=grpc-web

protoc \
  $BOT_SOURCE_PROTO_PATH/stateMachine.proto \
  --plugin=./node_modules/.bin/protoc-gen-ts_proto \
  --ts_proto_out=$BOT_PROTO_PATH \
	--proto_path=$BOT_SOURCE_PROTO_PATH \
  --ts_proto_opt=outputClientImpl=grpc-web \
  --ts_proto_opt=useOptionals=true \
  --ts_proto_opt=outputTypeRegistry=true \
  --ts_proto_opt=esModuleInterop=true \
  --ts_proto_opt=outputClientImpl=grpc-web

protoc \
  $BOT_SOURCE_PROTO_PATH/stateMachineServices.proto \
  --plugin=./node_modules/.bin/protoc-gen-ts_proto \
  --ts_proto_out=$BOT_PROTO_PATH \
	--proto_path=$BOT_SOURCE_PROTO_PATH \
  --ts_proto_opt=outputClientImpl=grpc-web \
  --ts_proto_opt=useOptionals=true \
  --ts_proto_opt=outputTypeRegistry=true \
  --ts_proto_opt=esModuleInterop=true \
  --ts_proto_opt=outputClientImpl=grpc-web