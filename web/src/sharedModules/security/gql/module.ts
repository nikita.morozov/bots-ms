import { ContainerModule, interfaces } from 'inversify';
import SecurityGqlService, { ISecurityGqlService } from '@modules/security/gql/service';
import SecurityGqlUsecase, { ISecurityGqlUsecase } from '@modules/security/gql/usecase';

export const SecurityGqlModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<ISecurityGqlService>(SecurityGqlService.diKey).to(SecurityGqlService);

  bind<ISecurityGqlUsecase>(SecurityGqlUsecase.diKey).to(SecurityGqlUsecase).inSingletonScope();
});