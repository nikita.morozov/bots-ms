import { Container, interfaces } from 'inversify';
import { SecurityGqlModule } from '@modules/security/gql/module';

const gqlSecurityContainer: interfaces.Container = new Container();

gqlSecurityContainer.load(SecurityGqlModule);

export default gqlSecurityContainer;