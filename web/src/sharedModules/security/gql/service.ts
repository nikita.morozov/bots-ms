import { map, Observable } from 'rxjs';
import { inject, injectable } from 'inversify';
import GqlClient from '@modules/gqlClient';
import { ApiGqlClientKey } from '@modules/gqlClient/types';
import {
  SecurityCreateDocument,
  SecurityCreateInput,
  SecurityCreateMutation,
  SecurityCreateMutationVariables,
} from '@modules/graphql/security/security-gql';


export interface ISecurityGqlService {
  create(input: SecurityCreateInput): Observable<void>,
}

@injectable()
class SecurityGqlService implements ISecurityGqlService {
  public static diKey = Symbol.for('SecurityGqlServiceDiKey');
  private api: GqlClient;

  constructor(
    @inject(ApiGqlClientKey) api: GqlClient,
  ) {
    this.api = api;
  }

  create(input: SecurityCreateInput): Observable<void> {
    return this.api.mutation$<SecurityCreateMutation, SecurityCreateMutationVariables>(
      SecurityCreateDocument,
      { input },
      {
        noCache: true,
      }
    ).pipe(map(() => {}));
  }
}

export default SecurityGqlService;