import { SecurityCreateInput } from '@modules/graphql/security/security-gql';
import { Observable } from 'rxjs';
import { inject, injectable } from 'inversify';
import SecurityGqlService from '@modules/security/gql/service';

export interface ISecurityGqlUsecase {
  create(input: SecurityCreateInput): Observable<void>,
}

@injectable()
class SecurityGqlUsecase implements ISecurityGqlUsecase {
  public static diKey = Symbol.for('SecurityGqlUsecaseDiKey');

  private service: ISecurityGqlUsecase;

  constructor(
    @inject(SecurityGqlService.diKey) service: ISecurityGqlUsecase,
  ) {
    this.service = service;
  }

  create(input: SecurityCreateInput): Observable<void> {
    return this.service.create(input);
  }
}

export default SecurityGqlUsecase;