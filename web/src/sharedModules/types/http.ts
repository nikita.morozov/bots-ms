import { Paginator } from '@modules/paginatior';
import { BaseCommandProps } from '../../src/types/CommonTypes';

export interface ListResp<T> {
  list: T[],
}

export interface ItemResp<T> {
  item: T,
}

export interface ListPaginatorResp<T> {
  list: T[],
  paginator: Paginator,
}

export interface ResultIdRes extends BaseCommandProps {
  id: number,
}

export interface ResultCountRes extends BaseCommandProps {
  count: number,
}

export interface ResultListRes<P> extends BaseCommandProps {
  list: P[],
}

export interface ResultItemRes<P> extends BaseCommandProps {
  item: P
}