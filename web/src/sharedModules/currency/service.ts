import { inject, injectable } from 'inversify';
import GqlClient from '@modules/gqlClient';
import { map, Observable } from 'rxjs';
import { ApiGqlClientKey } from '@modules/gqlClient/types';
import {
  CurrencyGetRateDocument,
  CurrencyGetRateQuery,
  CurrencyGetRateQueryVariables, CurrencyRateSubscriberDocument,
  CurrencyRateSubscriberSubscription,
  CurrencyRateSubscriberSubscriptionVariables,
} from '@modules/graphql/currency/currency-gql';

export interface ICurrencyService {
  getRate(currency: string): Observable<number>
  subscriber(currency: string): Observable<number>
}

@injectable()
class CurrencyService implements ICurrencyService {
  public static diKey = Symbol.for('CurrencyServiceKey');
  private api: GqlClient;

  public constructor(
    @inject(ApiGqlClientKey) api: GqlClient,
  ) {
    this.api = api;
  }


  getRate(currency: string): Observable<number> {
    return this.api.query$<CurrencyGetRateQuery, CurrencyGetRateQueryVariables>(
      CurrencyGetRateDocument,
      { input: currency.toUpperCase() },
      { noCache: true },
    ).pipe(map(res => res.currencyGetRate));
  }

  subscriber(currency: string): Observable<number> {
    return this.api.subscribe<CurrencyRateSubscriberSubscription, CurrencyRateSubscriberSubscriptionVariables>(
      CurrencyRateSubscriberDocument,
      { input: currency.toUpperCase() },
      { noCache: true },
    ).pipe(map(res => res.currencyRateSubscriber));
  }
}

export default CurrencyService;
