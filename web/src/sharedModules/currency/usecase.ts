import { inject, injectable } from 'inversify';
import { Observable } from 'rxjs';
import CurrencyService from '@modules/currency/service';

interface ICurrencyUsecase {
  getRate(currency: string): Observable<number>
}

@injectable()
class CurrencyUsecase implements ICurrencyUsecase {
  public static diKey = Symbol.for('CurrencyUsecaseDiKey');

  private currencyService: CurrencyService;

  constructor(
    @inject(CurrencyService.diKey) currencyService: CurrencyService,
  ) {
    this.currencyService = currencyService;
  }

  getRate(currency: string): Observable<number> {
    return this.currencyService.getRate(currency);
  }

  subscriber(currency: string): Observable<number> {
    return this.currencyService.subscriber(currency)
  }
}

export default CurrencyUsecase;
