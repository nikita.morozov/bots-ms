import { AsyncContainerModule, interfaces } from 'inversify';
import CurrencyService from '@modules/currency/service';
import CurrencyUsecase from '@modules/currency/usecase';

export const CurrencyModule = new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<CurrencyService>(CurrencyService.diKey).to(CurrencyService);
  bind<CurrencyUsecase>(CurrencyUsecase.diKey).to(CurrencyUsecase).inSingletonScope();
});
