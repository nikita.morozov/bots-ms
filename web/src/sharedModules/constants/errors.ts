export const unauthorizedCode = 16;
export const permissionDeniedCode = 7;
export const unknownErrorCode = 2;
export const deadlineExceeded = 4;