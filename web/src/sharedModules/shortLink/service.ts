import { IExternalService } from '@modules/Stream/types';
import { from, map, Observable } from 'rxjs';
import { inject, injectable } from 'inversify';
import StreamHandler from '@modules/Stream/service/StreamHandler';
import GqlClient from '@modules/gqlClient';
import { ApiGqlClientKey } from '@modules/gqlClient/types';
import { mergeMap } from 'rxjs/operators';
import ShortLink from '@modules/shortLink/models';
import {
  ListOptions,
  ShortLinkCountDocument,
  ShortLinkCountQuery,
  ShortLinkCountQueryVariables,
  ShortLinkCreateDocument,
  ShortLinkCreateMutation,
  ShortLinkCreateMutationVariables,
  ShortLinkDeleteDocument,
  ShortLinkDeleteMutation,
  ShortLinkDeleteMutationVariables, ShortLinkGetByIdDocument, ShortLinkGetByIdQuery, ShortLinkGetByIdQueryVariables,
  ShortLinkInput,
  ShortLinkListDocument,
  ShortLinkListQuery,
  ShortLinkListQueryVariables,
  ShortLinkRequestList,
  ShortLinkUpdateDocument,
  ShortLinkUpdateMutation,
  ShortLinkUpdateMutationVariables,
} from '@modules/graphql/shortLink/shortLink-gql';

export interface IShortLinkService extends IExternalService<ShortLink> {
  delete(id: number): Observable<void>,

  getById(id: number): Observable<ShortLink>,

  create(input: ShortLinkInput): Observable<ShortLink>,

  update(input: ShortLinkInput): Observable<ShortLink>,

  list(opts: ListOptions, query?: ShortLinkRequestList): Observable<ShortLink>

  count(query?: ShortLinkRequestList): Observable<number>
}

@injectable()
export class ShortLinkService extends StreamHandler<ShortLink> implements IShortLinkService {
  public static diKey = Symbol.for('ShortLinkServiceDiKey');
  private api: GqlClient;

  public constructor(
    @inject(ApiGqlClientKey) api: GqlClient,
  ) {
    super();
    this.add('default', this.streamDefault)
    this.api = api
  }


  private streamDefault = {
    stream: (opts: ListOptions, method?: string, query?: ShortLinkRequestList): Observable<ShortLink> => {
      const input: ShortLinkRequestList = {
        code: query?.code,
        extract: query?.extract,
        opts,
      }

      return this.api.query$<ShortLinkListQuery, ShortLinkListQueryVariables>(
        ShortLinkListDocument,
        { in: input },
        {
          noCache: true,
        },
      ).pipe(mergeMap(res => {
        const value = (res.shortLinkList || []).map(i => new ShortLink(i as ShortLink))
        return from(value)
      }))
    },
    loadCount: (method: string, query?: ShortLinkRequestList): Observable<number> => {
      const input: ShortLinkRequestList = {
        code: query?.code,
        extract: query?.extract,
      }
      return this.api.query$<ShortLinkCountQuery, ShortLinkCountQueryVariables>(
        ShortLinkCountDocument,
        { in: input },
      ).pipe(
        map(res => res.shortLinkCount as number),
      )
    },
  }

  list(opts: ListOptions, query?: ShortLinkRequestList): Observable<ShortLink> {
    return this.streamDefault.stream(opts, '', query)
  }

  count(query?: ShortLinkRequestList): Observable<number> {
    return this.streamDefault.loadCount('', query)
  }

  create(input: ShortLinkInput): Observable<ShortLink> {
    return this.api.mutation$<ShortLinkCreateMutation, ShortLinkCreateMutationVariables>(
      ShortLinkCreateDocument,
      { input },
    ).pipe(map(res => new ShortLink(res.shortLinkCreate as ShortLink)));
  }

  update(input: ShortLinkInput): Observable<ShortLink> {
    return this.api.mutation$<ShortLinkUpdateMutation, ShortLinkUpdateMutationVariables>(
      ShortLinkUpdateDocument,
      { input },
    ).pipe(map(res => new ShortLink(res.shortLinkUpdate as ShortLink)));
  }

  delete(id: number): Observable<void> {
    return this.api.mutation$<ShortLinkDeleteMutation, ShortLinkDeleteMutationVariables>(
      ShortLinkDeleteDocument,
      { id },
    ).pipe(map(res => res.shortLinkDelete));
  }

  getById(id: number): Observable<ShortLink> {
    return this.api.query$<ShortLinkGetByIdQuery, ShortLinkGetByIdQueryVariables>(
      ShortLinkGetByIdDocument,
      { in: id },
    ).pipe(map(res => new ShortLink(res.shortLinkGetById as ShortLink)));
  }
}