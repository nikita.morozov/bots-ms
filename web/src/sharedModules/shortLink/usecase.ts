import { inject, injectable } from 'inversify';
import { PageService } from '@modules/Stream/service/ListService';
import { IBaseListUsecase, IPageService } from '@modules/Stream/types';
import { Observable, Subscription } from 'rxjs';
import ListMV from '@modules/Stream/model/ListMV';
import { ListOptions } from '@modules/paginatior';
import ShortLink from '@modules/shortLink/models';
import { ShortLinkInput, ShortLinkRequestList } from '@modules/graphql/shortLink/shortLink-gql';
import { IShortLinkService, ShortLinkService } from '@modules/shortLink/service';

export interface IShortLinkUsecase {
  getById(id: number): Observable<ShortLink>,

  delete(id: number): Observable<void>

  create(input: ShortLinkInput): Observable<ShortLink>

  update(input: ShortLinkInput): Observable<ShortLink>

  list(opts: ListOptions, query?: ShortLinkRequestList): Observable<ShortLink>

  count(query?: ShortLinkRequestList): Observable<number>
}

@injectable()
export class ShortLinkUsecase implements IShortLinkUsecase, IBaseListUsecase<ShortLink> {
  public static diKey = Symbol.for('ShortLinkUsecaseDiKey');
  private shortLinkService: IShortLinkService;
  private pageService: IPageService<ShortLink>

  constructor(
    @inject(ShortLinkService.diKey) shortLinkService: IShortLinkService,
  ) {
    this.shortLinkService = shortLinkService;
    this.pageService = new PageService(this.shortLinkService);
  }

  loadItems(model: ListMV<ShortLink>, opts: ListOptions, method?: string, query?: object): [Observable<ShortLink>, Subscription] {
    return this.pageService.loadPage(model, opts, method, query);
  }

  loadTotalCount(model: ListMV<ShortLink>, method?: string, query?: object): [Observable<number>, Subscription] {
    return this.pageService.loadTotalCount(model, method, query);
  }

  create(input: ShortLinkInput): Observable<ShortLink> {
    return this.shortLinkService.create(input);
  }

  update(input: ShortLinkInput): Observable<ShortLink> {
    return this.shortLinkService.update(input);
  }

  delete(id: number): Observable<void> {
    return this.shortLinkService.delete(id);
  }

  getById(id: number): Observable<ShortLink> {
    return this.shortLinkService.getById(id);
  }

  count(query?: ShortLinkRequestList): Observable<number> {
    return this.shortLinkService.count(query)
  }

  list(opts: ListOptions, query?: ShortLinkRequestList): Observable<ShortLink> {
    return this.shortLinkService.list(opts, query)
  }
}

export default ShortLinkUsecase;