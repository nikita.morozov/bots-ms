import { action, computed, makeAutoObservable, observable } from 'mobx';

class Pagination {
  @observable
  public totalCount: number = 0;

  @observable
  public page = 0;

  @observable
  public pageNeighbours = 2;

  @computed
  public get visible() {
    return !!this.normalizedCount && this.normalizedCount > this.itemsPerPage;
  }

  @computed
  public get pagesCount() {
    if (this.normalizedCount) {
      const pages = this.normalizedCount / this.itemsPerPage;
      return pages < 1 ? 1 : Math.ceil(pages);
    }
    return 1;
  }

  @computed
  public get normalizedCount() {
    return this.totalCount;
  }

  public padding = 1;

  public itemsPerPage = 10;

  public tableCountRequestId?: number;

  @observable
  public isExactCount = false;

  public constructor() {
    makeAutoObservable(this);
  }

  @action
  public changeTotalCount(value: number) {
    this.totalCount = value;
  }

  @action
  public setPage(value: number) {
    this.page = value;
  }

  @action.bound
  public clear() {
    this.page = 0;
    this.totalCount = 0;
    this.tableCountRequestId = undefined;
    this.isExactCount = false;
  }
}

export default Pagination;
