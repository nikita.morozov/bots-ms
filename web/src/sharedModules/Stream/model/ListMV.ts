import { injectable } from 'inversify';
import { action, makeAutoObservable, observable } from 'mobx';

import Pagination from './Pagination';
import { ObjectWithId } from '@modules/types';

@injectable()
class ListMV<T extends ObjectWithId> {
  public static diKey = Symbol.for('ListModelKey');

  @observable
  public items: T[] = [];
  public pagination: Pagination;

  public constructor() {
    makeAutoObservable(this);
    this.pagination = new Pagination();
  }

  @action
  public clear() {
    this.items = [];
    // this.pagination.clear();
  }

  @action
  public add(item: T, toTop?: boolean) {
    if (toTop) {
      return this.items.unshift(item);
    }
    
    return this.items.push(item);
  }

  @action
  public update(item: T) {
    const index = this.items.findIndex(i => i.id === item.id);

    if (!!~index) {
      this.updateByIndex(item, index);
    }
  }

  @action
  public updateByIndex(item: T, index: number) {
    this.items = [
      ...this.items.slice(0, index),
      item,
      ...this.items.slice(index +1)
    ]
  }

  @action
  public getById(id: number) {
    const index = this.items.findIndex(i => i.id === id);

    if (!!~index) {
      return this.items[index];
    }
  }

  @action
  public removeById(id: number) {
    const index = this.items.findIndex(i => i.id === id);

    this.removeByIndex(index)
  }

  @action
  public removeByIndex(index: number) {
    if (!!~index) {
      this.items.splice(index, 1);
    }
  }
}

export default ListMV;
