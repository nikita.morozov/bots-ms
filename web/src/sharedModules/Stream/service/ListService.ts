import { inject, injectable } from 'inversify';

import ListMV from '../model/ListMV';
import { IExternalService, IPageService } from '../types';
import { ObjectWithId } from '@modules/types';
import { Observable, Subscription } from 'rxjs';
import { ListOptions } from '@modules/paginatior';
import { observer } from 'mobx-react-lite';

export const ListServiceExternalServiceKey = Symbol.for('StreamListServiceExternalServiceKey');
export const ListVMExternalServiceKey = Symbol.for('StreamListVMExternalServiceKey');

//this use please
export class PageService<T extends ObjectWithId, QueryType extends object = object> implements IPageService<T, QueryType> {
  private externalService: IExternalService<T, QueryType>;

  public constructor(externalService: IExternalService<T, QueryType>) {
    this.externalService = externalService;
  }

  public loadTotalCount(list: ListMV<T>, method?: string, query?: QueryType): [Observable<number>, Subscription] {
    const observable = this.externalService.loadCount(method, query)
    const sub = observable.subscribe(count => list.pagination.changeTotalCount(count));

    return [observable, sub];
  }

  public loadPage(list: ListMV<T>, opts: ListOptions, method?: string, query?: QueryType): [Observable<T>, Subscription] {
    const page = (opts.offset / opts.limit)

    list.clear()
    list.pagination.setPage(page);
    const observable = this.externalService.stream(opts, method, query);
    const sub = observable.subscribe(item => list.add(item));
    return [observable, sub];
  }
}


//Not use
@injectable()
class ListService<T extends ObjectWithId> {
  public static diKey = Symbol.for('ListServiceKey');

  private externalService: IExternalService<T>;

  public constructor(
    @inject(ListServiceExternalServiceKey) externalService: IExternalService<T>,
  ) {
    this.externalService = externalService;
  }

  public loadCount(list: ListMV<T>): Observable<number> {
    const observable = this.externalService.loadCount()
    observable.subscribe(count => list.pagination.totalCount = count);

    return observable;
  }

  public loadPage(list: ListMV<T>, page: number): Observable<T> {
    const { itemsPerPage } = list.pagination;

    list.clear()
    list.pagination.setPage(page);
    // @ts-ignore
    const observable = this.externalService.stream(page, itemsPerPage);
    observable.subscribe(item => list.add(item));

    return observable;
  }
}

export default ListService;
