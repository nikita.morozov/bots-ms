import { Observable } from 'rxjs';
import { ObjectWithId } from '@modules/types';
import { IExternalService } from '@modules/Stream/types';
import { ListOptions } from '@modules/paginatior';
import { injectable } from 'inversify';

interface IHandlers<T extends ObjectWithId> {
  [key: string]: IExternalService<T>
}

@injectable()
class StreamHandler<T extends ObjectWithId> implements IExternalService<T>{
  private handlers: IHandlers<T>

  constructor() {
    this.handlers = {}
  }

  protected add(method: string, handler: IExternalService<T>) {
    this.handlers[method] = handler;
  }

  loadCount(method?: string, query?: object): Observable<number> {
    const handler = this.get(method || 'default');
    return handler.loadCount(method, query);
  }

  stream(opts: ListOptions, method?: string, query?: object): Observable<T> {
    const handler = this.get(method || 'default');
    return handler.stream(opts, method, query);
  }

  private get(method: string): IExternalService<T> {
    const handler = this.handlers[method]
    if (!handler) {
      throw new Error("stream-handler.not-found")
    }

    return handler;
  }
}

export default StreamHandler;