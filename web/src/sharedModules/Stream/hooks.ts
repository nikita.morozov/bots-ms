import { useEffect } from 'react';
import { IBaseViewModel } from '@modules/Stream/model/BaseVM';

const useLoadableVM = <LoadParamsType = void>(vm: IBaseViewModel<LoadParamsType>, params?: LoadParamsType) => {
  useEffect(() => {
    vm.load(params);
    return () => vm.unload();
  })
}

export default useLoadableVM;
