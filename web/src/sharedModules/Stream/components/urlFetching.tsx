import isEmpty from 'lodash/isEmpty';

function makeid(length: number) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  let counter = 0;
  while (counter < length) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
    counter += 1;
  }
  return result;
}

class UrlParamsFetching<QueryType extends object = object> {
  private limit: number
  private page: number
  private order?: string
  private query?: QueryType
  private url?: URLSearchParams
  private rate: number

  private limitKey = 'limit'
  private queryKey = 'query' + makeid(5)
  private pageKey = 'page'
  private orderKey = 'order'

  constructor(limit: number = 10, order?: string, query?: QueryType, rate?: number) {
    this.page = 0
    this.rate = rate || 0
    this.limit = limit
    this.url = new URLSearchParams(window.location.search)
    // this.setQuery(query)
    // this.setOrder(order)
  }

  private urlParam(key: string): string | null | undefined {
    return this.url?.get(key)
  }

  // setUrl(url: URLSearchParams) {
  //   this.url = url
  // }

  updateState() {
    if (this.url) {
      // @ts-ignore
      history.pushState(null, null, '?' + this.url.toString());
    }
  }

  getPage(): number {
    // const val = this.urlParam(this.pageKey)
    // if (!!val) {
    //   this.page = parseInt(val, 10) - this.rate
    // }

    return this.page
  }

  setPage(page: number) {
    this.url?.set(this.pageKey, (page + this.rate).toString())
    this.updateState()
  }

  getOrder(): string | undefined {
    const val = this.urlParam(this.orderKey)
    if (!!val) {
      return val
    }

    return this.order
  }

  setOrder(order?: string) {
    if (!order) {
      return
    }
    this.order = order
    this.url?.set(this.orderKey, order)
    this.updateState()
  }

  setLimit(limit?: number): number {
    if (!limit) {
      return this.getLimit()
    }
    this.url?.set(this.limitKey, limit.toString())
    this.updateState()
    return this.getLimit()
  }

  getLimit(): number {
    const val = this.urlParam(this.limitKey)
    if (!!val) {
      this.limit = parseInt(val, 10)
    }

    return this.limit
  }

  getQueryParams(): QueryType | undefined {
    const val = this.urlParam(this.queryKey)
    try {
      if (!!val) {
        return JSON.parse(val)
      }

      return this.query
    } catch (e) {
      console.error('Failed json parse filter', e)
      return undefined
    }
  }

  getQuery(): QueryType | undefined {
    if (!this.query && !this.getQueryParams()) {
      return undefined
    }

    const val = { ...(this.query || {}), ...(this.getQueryParams() || {}) } as QueryType
    this.setQuery(val)

    return val
  }

  setQuery(query?: QueryType) {
    if (!!query && !isEmpty(query)) {
      try {
        this.query = query
        const queryValue = JSON.stringify(query)
        this.url?.set(this.queryKey, queryValue);
        this.updateState()
      } catch (e) {
        console.error('Error filter stringify', e)
      }
    }
  }
}

export default UrlParamsFetching