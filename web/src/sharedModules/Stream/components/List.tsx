import { BaseListContentProps } from './types';
import useStream from './useStream';
import { observer } from 'mobx-react';
import React, { useMemo } from 'react';
import { ObjectWithId } from '../../types';
import { ItemIndexer } from '../types';
import PaginatorComponent, { IPaginatorComponent } from './Paginator';

// TODO: remove it
// Use it local, not in module
function List<T extends ItemIndexer & ObjectWithId, QueryType extends object>(props: BaseListContentProps<T, QueryType>) {
  const {
    diKey,
    idKey,
    method,
    countPerPage,
    order,
    rowRenderComponent,
    beforeComponent,
    wrapperListClass,
    classNamePagination,
    activeClassName,
    hidePaginator,
    query,
    wrapper,
    paginator,
    EmptyComponent,
    onChangePaginate,
  } = props;

  const RowComponent = rowRenderComponent;

  const [items, pagination, onPageChanged, methods] = useStream<T, QueryType>(
    diKey,
    query,
    method,
    countPerPage,
    order,
    undefined,
    onChangePaginate,
  );

  const Wrapper = useMemo(() => wrapper || (({
    children,
    beforeComponent,
  }: React.PropsWithChildren<{ beforeComponent: React.ReactNode }>) => (
    <div className={wrapperListClass}>{beforeComponent}{children}</div>
  )), []);

  const Paginator = useMemo(() => paginator || ((params: IPaginatorComponent) =>
    <PaginatorComponent {...params} />), [paginator]);

  if (EmptyComponent && !items.length) {
    return (
      <>
        {EmptyComponent}
      </>
    );
  }

  return (
    <>
      <Wrapper beforeComponent={beforeComponent}>
        {items.map((item, index) => (
            <RowComponent
              key={`ListItem-${idKey}-${item[idKey] || index}`}
              index={index}
              item={item}
              update={(value: T) => methods.update(value)}
              remove={() => methods.removeByIndex(index)}
            />
          ),
        )}
      </Wrapper>
      {!hidePaginator && (
        <Paginator
          paginator={pagination}
          onPageChanged={onPageChanged}
          classNamePagination={classNamePagination}
          activeClassName={activeClassName}
        />
      )}
    </>
  );
}

export default observer(List);
