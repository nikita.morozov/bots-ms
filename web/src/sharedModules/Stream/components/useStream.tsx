import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { toJS } from 'mobx';

import { useInjection } from 'inversify-react';
import { ObjectWithId } from '@modules/types';
import { ListOptions } from '@modules/paginatior';

import ListMV from '../model/ListMV';
import { ITableMethods } from './types';
import { IBaseListUsecase } from '../types';
import Pagination from '../model/Pagination';
import UrlParamsFetching from './urlFetching';

const defaultCountPerPage = 10;

export type OnPageChange = (page: number, size?: number) => void;

export type IUseStream<T extends ObjectWithId> = [T[], Pagination, OnPageChange, ITableMethods<T>];

const useStream = <T extends ObjectWithId, QueryType extends object = object>(diKey: symbol, query?: QueryType, method?: string, countPerPage?: number, order?: string, currentPage?: number, onChangePaginate?: () => void): IUseStream<T> => {
  const listUsecase = useInjection<IBaseListUsecase<T, QueryType>>(diKey);
  const [model] = useState(() => new ListMV<T>());
  const urlFetching = useMemo(() => new UrlParamsFetching<QueryType>((countPerPage || defaultCountPerPage), order, query, 1), [query, order, method]);

  useEffect(() => {
    const limit = model.pagination.itemsPerPage = urlFetching.getLimit()

    const opts: ListOptions = {
      limit,
      offset: urlFetching.getPage() * (limit),
      order: urlFetching.getOrder(),
    };

    const [, subCount] = listUsecase.loadTotalCount(model, method, urlFetching.getQuery() || query);
    const [, subList] = listUsecase.loadItems(model, opts, method, urlFetching.getQuery() || query);

    const subscriber = listUsecase.subscriber?.()
      .subscribe(e => model.update(e));

    return () => {
      subList.unsubscribe()
      subCount.unsubscribe()
      subscriber?.unsubscribe();
    }
  }, [query, method, order, countPerPage]);

  const handler = useCallback((page: number, size?: number) => {
    if (!!onChangePaginate) {
      onChangePaginate();
    }

    let limit = (size || countPerPage || defaultCountPerPage)
    if (size) {
      limit = model.pagination.itemsPerPage = urlFetching.setLimit(size)
    }

    const opts: ListOptions = {
      limit,
      offset: page * limit,
      order: urlFetching.getOrder(),
    };

    urlFetching.setPage(page)

    listUsecase.loadItems(model, opts, method, urlFetching.getQuery() || query);
  }, [urlFetching]);

  const methods = useMemo(() => ({
    add: (item: T, toTop?: boolean) => model.add(item, toTop),
    removeById: (id: number) => model.removeById(id),
    removeByIndex: (index: number) => model.removeByIndex(index),
    update: (item: T) => model.update(item),
  }), [model])

  return [
    toJS(model.items),
    model.pagination,
    handler,
    methods as ITableMethods<T>,
  ]
}

export default useStream;
