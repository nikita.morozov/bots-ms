import gql from 'graphql-tag';
export type Maybe<T> = T | undefined;
export type InputMaybe<T> = T | undefined;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: number;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Int32: number;
  Int64: number;
  Uint32: number;
  Uint64: number;
  Unix: number;
  Void: void;
};

export type ListOptions = {
  limit: Scalars['Int32'];
  offset: Scalars['Int32'];
  order?: InputMaybe<Scalars['String']>;
};

export type Mutation = {
  root: Scalars['Boolean'];
  userChangePassword?: Maybe<Scalars['Void']>;
};


export type MutationUserChangePasswordArgs = {
  in: UserChangePassInput;
};

export type Paginator = {
  countPerPage: Scalars['Int32'];
  offset: Scalars['Int32'];
  totalCount: Scalars['Int64'];
};

export type Query = {
  root: Scalars['Boolean'];
};

export type Subscription = {
  root: Scalars['Boolean'];
};

export type UserChangePassInput = {
  code: Scalars['String'];
  oldPass: Scalars['String'];
  pass: Scalars['String'];
  type: Scalars['String'];
};

export type UserModel = {
  alias?: Maybe<Scalars['String']>;
  blocked: Scalars['Boolean'];
  createdAt: Scalars['Unix'];
  deleted: Scalars['Boolean'];
  email: Scalars['String'];
  firstName?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  lang: Scalars['String'];
  middleName?: Maybe<Scalars['String']>;
  organizationId: Scalars['ID'];
  parentId?: Maybe<Scalars['ID']>;
  phone?: Maybe<Scalars['String']>;
  role?: Maybe<Scalars['String']>;
  surname?: Maybe<Scalars['String']>;
  verified: Scalars['Boolean'];
};

export type UserShortModel = {
  alias?: Maybe<Scalars['String']>;
  blocked: Scalars['Boolean'];
  email: Scalars['String'];
  firstName?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  lang: Scalars['String'];
  middleName?: Maybe<Scalars['String']>;
  parentId?: Maybe<Scalars['ID']>;
  phone?: Maybe<Scalars['String']>;
  surname?: Maybe<Scalars['String']>;
};

export type UserShorterModel = {
  alias?: Maybe<Scalars['String']>;
  blocked: Scalars['Boolean'];
  firstName?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  lang: Scalars['String'];
  middleName?: Maybe<Scalars['String']>;
  parentId?: Maybe<Scalars['ID']>;
  surname?: Maybe<Scalars['String']>;
};

export type UserShorterModelRespAllFragment = { id: number, blocked: boolean, firstName?: string | undefined, middleName?: string | undefined, surname?: string | undefined, parentId?: number | undefined, alias?: string | undefined, lang: string };

export type UserShortModelRespAllFragment = { id: number, email: string, blocked: boolean, phone?: string | undefined, firstName?: string | undefined, middleName?: string | undefined, surname?: string | undefined, parentId?: number | undefined, alias?: string | undefined, lang: string };

export type UserModelRespAllFragment = { id: number, createdAt: number, alias?: string | undefined, parentId?: number | undefined, email: string, verified: boolean, blocked: boolean, deleted: boolean, firstName?: string | undefined, middleName?: string | undefined, surname?: string | undefined, phone?: string | undefined, lang: string, role?: string | undefined, organizationId: number };

export type UserChangePasswordMutationVariables = Exact<{
  input: UserChangePassInput;
}>;


export type UserChangePasswordMutation = { userChangePassword?: void | undefined };

export const UserShorterModelRespAllFragmentDoc = gql`
    fragment UserShorterModelRespAll on UserShorterModel {
  id
  blocked
  firstName
  middleName
  surname
  parentId
  alias
  lang
}
    `;
export const UserShortModelRespAllFragmentDoc = gql`
    fragment UserShortModelRespAll on UserShortModel {
  id
  email
  blocked
  phone
  firstName
  middleName
  surname
  parentId
  alias
  lang
}
    `;
export const UserModelRespAllFragmentDoc = gql`
    fragment UserModelRespAll on UserModel {
  id
  createdAt
  alias
  parentId
  email
  verified
  blocked
  deleted
  firstName
  middleName
  surname
  phone
  lang
  role
  organizationId
}
    `;
export const UserChangePasswordDocument = gql`
    mutation UserChangePassword($input: UserChangePassInput!) {
  userChangePassword(in: $input)
}
    `;