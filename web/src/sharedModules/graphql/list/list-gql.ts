import gql from 'graphql-tag';
export type Maybe<T> = T | undefined;
export type InputMaybe<T> = T | undefined;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: number;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Int32: number;
  Int64: number;
  Uint32: number;
  Uint64: number;
  Unix: number;
  Void: void;
};

export type ListModel = {
  context: Scalars['String'];
  createdAt?: Maybe<Scalars['Unix']>;
  id: Scalars['ID'];
  meta?: Maybe<Scalars['String']>;
  title: Scalars['String'];
};

export type ListModelInput = {
  context: Scalars['String'];
  createdAt?: InputMaybe<Scalars['Unix']>;
  id?: InputMaybe<Scalars['ID']>;
  meta?: InputMaybe<Scalars['String']>;
  title: Scalars['String'];
};

export type ListOptions = {
  limit: Scalars['Int32'];
  offset: Scalars['Int32'];
  order?: InputMaybe<Scalars['String']>;
};

export type Mutation = {
  listAdd?: Maybe<ListModel>;
  listRemove?: Maybe<Scalars['Void']>;
  listRemoveById?: Maybe<Scalars['Void']>;
  listUpdate?: Maybe<ListModel>;
  root: Scalars['Boolean'];
};


export type MutationListAddArgs = {
  in: ListModelInput;
};


export type MutationListRemoveArgs = {
  index: Scalars['Int64'];
};


export type MutationListRemoveByIdArgs = {
  id: Scalars['ID'];
};


export type MutationListUpdateArgs = {
  in: ListModelInput;
};

export type Paginator = {
  countPerPage: Scalars['Int32'];
  offset: Scalars['Int32'];
  totalCount: Scalars['Int64'];
};

export type Query = {
  listGetByContext?: Maybe<ListModel>;
  listListContext: Array<ListModel>;
  listListContexts: Array<ListModel>;
  root: Scalars['Boolean'];
};


export type QueryListGetByContextArgs = {
  context: Scalars['String'];
};


export type QueryListListContextArgs = {
  context: Scalars['String'];
};


export type QueryListListContextsArgs = {
  contexts: Array<Scalars['String']>;
};

export type Subscription = {
  root: Scalars['Boolean'];
};

export type ListModelReqFragment = { id: number, createdAt?: number | undefined, context: string, title: string, meta?: string | undefined };

export type ListAddMutationVariables = Exact<{
  in: ListModelInput;
}>;


export type ListAddMutation = { listAdd?: { id: number, createdAt?: number | undefined, context: string, title: string, meta?: string | undefined } | undefined };

export type ListUpdateMutationVariables = Exact<{
  in: ListModelInput;
}>;


export type ListUpdateMutation = { listUpdate?: { id: number, createdAt?: number | undefined, context: string, title: string, meta?: string | undefined } | undefined };

export type ListRemoveMutationVariables = Exact<{
  index: Scalars['Int64'];
}>;


export type ListRemoveMutation = { listRemove?: void | undefined };

export type ListRemoveByIdMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ListRemoveByIdMutation = { listRemoveById?: void | undefined };

export type ListGetByContextQueryVariables = Exact<{
  context: Scalars['String'];
}>;


export type ListGetByContextQuery = { listGetByContext?: { id: number, createdAt?: number | undefined, context: string, title: string, meta?: string | undefined } | undefined };

export type ListListContextsQueryVariables = Exact<{
  contexts: Array<Scalars['String']> | Scalars['String'];
}>;


export type ListListContextsQuery = { listListContexts: Array<{ id: number, createdAt?: number | undefined, context: string, title: string, meta?: string | undefined }> };

export type ListListContextQueryVariables = Exact<{
  context: Scalars['String'];
}>;


export type ListListContextQuery = { listListContext: Array<{ id: number, createdAt?: number | undefined, context: string, title: string, meta?: string | undefined }> };

export const ListModelReqFragmentDoc = gql`
    fragment ListModelReq on ListModel {
  id
  createdAt
  context
  title
  meta
}
    `;
export const ListAddDocument = gql`
    mutation ListAdd($in: ListModelInput!) {
  listAdd(in: $in) {
    ...ListModelReq
  }
}
    ${ListModelReqFragmentDoc}`;
export const ListUpdateDocument = gql`
    mutation ListUpdate($in: ListModelInput!) {
  listUpdate(in: $in) {
    ...ListModelReq
  }
}
    ${ListModelReqFragmentDoc}`;
export const ListRemoveDocument = gql`
    mutation ListRemove($index: Int64!) {
  listRemove(index: $index)
}
    `;
export const ListRemoveByIdDocument = gql`
    mutation ListRemoveById($id: ID!) {
  listRemoveById(id: $id)
}
    `;
export const ListGetByContextDocument = gql`
    query ListGetByContext($context: String!) {
  listGetByContext(context: $context) {
    ...ListModelReq
  }
}
    ${ListModelReqFragmentDoc}`;
export const ListListContextsDocument = gql`
    query ListListContexts($contexts: [String!]!) {
  listListContexts(contexts: $contexts) {
    ...ListModelReq
  }
}
    ${ListModelReqFragmentDoc}`;
export const ListListContextDocument = gql`
    query listListContext($context: String!) {
  listListContext(context: $context) {
    ...ListModelReq
  }
}
    ${ListModelReqFragmentDoc}`;