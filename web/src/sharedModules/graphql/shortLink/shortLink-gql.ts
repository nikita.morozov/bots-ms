import gql from 'graphql-tag';
export type Maybe<T> = T | undefined;
export type InputMaybe<T> = T | undefined;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: number;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Int32: number;
  Int64: number;
  Uint32: number;
  Uint64: number;
  Unix: number;
  Void: void;
};

export type ListOptions = {
  limit: Scalars['Int32'];
  offset: Scalars['Int32'];
  order?: InputMaybe<Scalars['String']>;
};

export type Mutation = {
  root: Scalars['Boolean'];
  shortLinkCreate?: Maybe<ShortLinkModel>;
  shortLinkDelete?: Maybe<Scalars['Void']>;
  shortLinkUpdate?: Maybe<ShortLinkModel>;
};


export type MutationShortLinkCreateArgs = {
  in: ShortLinkInput;
};


export type MutationShortLinkDeleteArgs = {
  id: Scalars['ID'];
};


export type MutationShortLinkUpdateArgs = {
  in: ShortLinkInput;
};

export type Paginator = {
  countPerPage: Scalars['Int32'];
  offset: Scalars['Int32'];
  totalCount: Scalars['Int64'];
};

export type Query = {
  root: Scalars['Boolean'];
  shortLinkCount?: Maybe<Scalars['Int64']>;
  shortLinkGetById?: Maybe<ShortLinkModel>;
  shortLinkList?: Maybe<Array<Maybe<ShortLinkModel>>>;
};


export type QueryShortLinkCountArgs = {
  in: ShortLinkRequestList;
};


export type QueryShortLinkGetByIdArgs = {
  in: Scalars['ID'];
};


export type QueryShortLinkListArgs = {
  in: ShortLinkRequestList;
};

export type ShortLinkInput = {
  code?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['Unix']>;
  description?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
  link: Scalars['String'];
  openTab: Scalars['Boolean'];
  tags?: InputMaybe<Scalars['String']>;
};

export type ShortLinkModel = {
  code: Scalars['String'];
  createdAt?: Maybe<Scalars['Unix']>;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  link: Scalars['String'];
  openTab?: Maybe<Scalars['Boolean']>;
  tags?: Maybe<Scalars['String']>;
};

export type ShortLinkRequestList = {
  code?: InputMaybe<Scalars['String']>;
  extract?: InputMaybe<Scalars['Boolean']>;
  opts?: InputMaybe<ListOptions>;
};

export type Subscription = {
  root: Scalars['Boolean'];
};

export type ShorLinkRespAllFragment = { id: number, link: string, createdAt?: number | undefined, tags?: string | undefined, code: string, description?: string | undefined, openTab?: boolean | undefined };

export type ShortLinkCreateMutationVariables = Exact<{
  input: ShortLinkInput;
}>;


export type ShortLinkCreateMutation = { shortLinkCreate?: { id: number, link: string, createdAt?: number | undefined, tags?: string | undefined, code: string, description?: string | undefined, openTab?: boolean | undefined } | undefined };

export type ShortLinkUpdateMutationVariables = Exact<{
  input: ShortLinkInput;
}>;


export type ShortLinkUpdateMutation = { shortLinkUpdate?: { id: number, link: string, createdAt?: number | undefined, tags?: string | undefined, code: string, description?: string | undefined, openTab?: boolean | undefined } | undefined };

export type ShortLinkDeleteMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ShortLinkDeleteMutation = { shortLinkDelete?: void | undefined };

export type ShortLinkListQueryVariables = Exact<{
  in: ShortLinkRequestList;
}>;


export type ShortLinkListQuery = { shortLinkList?: Array<{ id: number, link: string, createdAt?: number | undefined, tags?: string | undefined, code: string, description?: string | undefined, openTab?: boolean | undefined } | undefined> | undefined };

export type ShortLinkGetByIdQueryVariables = Exact<{
  in: Scalars['ID'];
}>;


export type ShortLinkGetByIdQuery = { shortLinkGetById?: { id: number, link: string, createdAt?: number | undefined, tags?: string | undefined, code: string, description?: string | undefined, openTab?: boolean | undefined } | undefined };

export type ShortLinkCountQueryVariables = Exact<{
  in: ShortLinkRequestList;
}>;


export type ShortLinkCountQuery = { shortLinkCount?: number | undefined };

export const ShorLinkRespAllFragmentDoc = gql`
    fragment ShorLinkRespAll on ShortLinkModel {
  id
  link
  createdAt
  tags
  code
  description
  openTab
}
    `;
export const ShortLinkCreateDocument = gql`
    mutation ShortLinkCreate($input: ShortLinkInput!) {
  shortLinkCreate(in: $input) {
    ...ShorLinkRespAll
  }
}
    ${ShorLinkRespAllFragmentDoc}`;
export const ShortLinkUpdateDocument = gql`
    mutation ShortLinkUpdate($input: ShortLinkInput!) {
  shortLinkUpdate(in: $input) {
    ...ShorLinkRespAll
  }
}
    ${ShorLinkRespAllFragmentDoc}`;
export const ShortLinkDeleteDocument = gql`
    mutation ShortLinkDelete($id: ID!) {
  shortLinkDelete(id: $id)
}
    `;
export const ShortLinkListDocument = gql`
    query ShortLinkList($in: ShortLinkRequestList!) {
  shortLinkList(in: $in) {
    ...ShorLinkRespAll
  }
}
    ${ShorLinkRespAllFragmentDoc}`;
export const ShortLinkGetByIdDocument = gql`
    query ShortLinkGetById($in: ID!) {
  shortLinkGetById(in: $in) {
    ...ShorLinkRespAll
  }
}
    ${ShorLinkRespAllFragmentDoc}`;
export const ShortLinkCountDocument = gql`
    query ShortLinkCount($in: ShortLinkRequestList!) {
  shortLinkCount(in: $in)
}
    `;