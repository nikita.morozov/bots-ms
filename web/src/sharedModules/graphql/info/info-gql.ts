import gql from 'graphql-tag';
export type Maybe<T> = T | undefined;
export type InputMaybe<T> = T | undefined;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: number;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Int32: number;
  Int64: number;
  Uint32: number;
  Uint64: number;
  Unix: number;
  Void: void;
};

export type ListItemsReq = {
  landingShow?: InputMaybe<Scalars['Boolean']>;
  opts?: InputMaybe<ListOptions>;
  slug?: InputMaybe<Scalars['String']>;
};

export type ListOptions = {
  limit: Scalars['Int32'];
  offset: Scalars['Int32'];
  order?: InputMaybe<Scalars['String']>;
};

export type ListSectionReq = {
  adminShow?: InputMaybe<Scalars['Boolean']>;
  opts?: InputMaybe<ListOptions>;
  slug: Scalars['String'];
};

export type Mutation = {
  infoSectionCreate?: Maybe<SectionModel>;
  infoSectionDelete?: Maybe<Scalars['Void']>;
  infoSectionItemCreate?: Maybe<SectionItemModel>;
  infoSectionItemDelete?: Maybe<Scalars['Void']>;
  infoSectionItemUpdate?: Maybe<SectionItemModel>;
  infoSectionUpdate?: Maybe<SectionModel>;
  root: Scalars['Boolean'];
};


export type MutationInfoSectionCreateArgs = {
  in: SectionModelInput;
};


export type MutationInfoSectionDeleteArgs = {
  id: Scalars['ID'];
};


export type MutationInfoSectionItemCreateArgs = {
  in: SectionItemModelInput;
};


export type MutationInfoSectionItemDeleteArgs = {
  id: Scalars['ID'];
};


export type MutationInfoSectionItemUpdateArgs = {
  in: SectionItemModelInput;
};


export type MutationInfoSectionUpdateArgs = {
  in: SectionModelInput;
};

export type Paginator = {
  countPerPage: Scalars['Int32'];
  offset: Scalars['Int32'];
  totalCount: Scalars['Int64'];
};

export type Query = {
  infoSectionCount?: Maybe<Scalars['Int64']>;
  infoSectionGetById?: Maybe<SectionModel>;
  infoSectionItemCount?: Maybe<Scalars['Int64']>;
  infoSectionItemGetById?: Maybe<SectionItemModel>;
  infoSectionItemList: Array<SectionItemModel>;
  infoSectionItemListPlain: Array<SectionItemModel>;
  infoSectionItemLoadInSection: Array<SectionItemModel>;
  infoSectionList: Array<SectionModel>;
  infoSectionListPlain: Array<SectionModel>;
  root: Scalars['Boolean'];
};


export type QueryInfoSectionGetByIdArgs = {
  id: Scalars['ID'];
};


export type QueryInfoSectionItemCountArgs = {
  in?: InputMaybe<Scalars['String']>;
};


export type QueryInfoSectionItemGetByIdArgs = {
  id: Scalars['ID'];
};


export type QueryInfoSectionItemListArgs = {
  in?: InputMaybe<ListItemsReq>;
};


export type QueryInfoSectionItemListPlainArgs = {
  in?: InputMaybe<ListOptions>;
};


export type QueryInfoSectionItemLoadInSectionArgs = {
  id: Scalars['ID'];
};


export type QueryInfoSectionListArgs = {
  in?: InputMaybe<ListSectionReq>;
};


export type QueryInfoSectionListPlainArgs = {
  in?: InputMaybe<ListOptions>;
};

export type SectionItemModel = {
  content?: Maybe<Scalars['String']>;
  createdAt: Scalars['Unix'];
  date?: Maybe<Scalars['Unix']>;
  displayLanding?: Maybe<Scalars['Boolean']>;
  id: Scalars['ID'];
  image?: Maybe<Scalars['String']>;
  index?: Maybe<Scalars['Int64']>;
  language?: Maybe<Scalars['String']>;
  link?: Maybe<Scalars['String']>;
  sectionId: Scalars['ID'];
  speaker?: Maybe<Scalars['String']>;
  time?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  topic?: Maybe<Scalars['String']>;
};

export type SectionItemModelInput = {
  content?: InputMaybe<Scalars['String']>;
  createdAt?: InputMaybe<Scalars['Unix']>;
  date?: InputMaybe<Scalars['Unix']>;
  displayLanding?: InputMaybe<Scalars['Boolean']>;
  id?: InputMaybe<Scalars['ID']>;
  image?: InputMaybe<Scalars['String']>;
  index?: InputMaybe<Scalars['Int64']>;
  language?: InputMaybe<Scalars['String']>;
  link?: InputMaybe<Scalars['String']>;
  sectionId: Scalars['ID'];
  speaker?: InputMaybe<Scalars['String']>;
  time?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
  topic?: InputMaybe<Scalars['String']>;
};

export type SectionModel = {
  createdAt: Scalars['Unix'];
  id: Scalars['ID'];
  index?: Maybe<Scalars['Int64']>;
  pid?: Maybe<Scalars['ID']>;
  showSupport: Scalars['Boolean'];
  slug: Scalars['String'];
  title: Scalars['String'];
};

export type SectionModelInput = {
  createdAt?: InputMaybe<Scalars['Unix']>;
  id?: InputMaybe<Scalars['ID']>;
  index: Scalars['Int64'];
  pid?: InputMaybe<Scalars['ID']>;
  showSupport: Scalars['Boolean'];
  slug: Scalars['String'];
  title: Scalars['String'];
};

export type Subscription = {
  root: Scalars['Boolean'];
};

export type SectionItemRespAllFragment = { id: number, title?: string | undefined, content?: string | undefined, date?: number | undefined, time?: string | undefined, topic?: string | undefined, speaker?: string | undefined, language?: string | undefined, link?: string | undefined, image?: string | undefined, displayLanding?: boolean | undefined, index?: number | undefined, sectionId: number, createdAt: number };

export type InfoSectionItemGetByIdQueryVariables = Exact<{
  input: Scalars['ID'];
}>;


export type InfoSectionItemGetByIdQuery = { infoSectionItemGetById?: { id: number, title?: string | undefined, content?: string | undefined, date?: number | undefined, time?: string | undefined, topic?: string | undefined, speaker?: string | undefined, language?: string | undefined, link?: string | undefined, image?: string | undefined, displayLanding?: boolean | undefined, index?: number | undefined, sectionId: number, createdAt: number } | undefined };

export type InfoSectionItemLoadInSectionQueryVariables = Exact<{
  input: Scalars['ID'];
}>;


export type InfoSectionItemLoadInSectionQuery = { infoSectionItemLoadInSection: Array<{ id: number, title?: string | undefined, content?: string | undefined, date?: number | undefined, time?: string | undefined, topic?: string | undefined, speaker?: string | undefined, language?: string | undefined, link?: string | undefined, image?: string | undefined, displayLanding?: boolean | undefined, index?: number | undefined, sectionId: number, createdAt: number }> };

export type InfoSectionItemListQueryVariables = Exact<{
  input?: InputMaybe<ListItemsReq>;
}>;


export type InfoSectionItemListQuery = { infoSectionItemList: Array<{ id: number, title?: string | undefined, content?: string | undefined, date?: number | undefined, time?: string | undefined, topic?: string | undefined, speaker?: string | undefined, language?: string | undefined, link?: string | undefined, image?: string | undefined, displayLanding?: boolean | undefined, index?: number | undefined, sectionId: number, createdAt: number }> };

export type InfoSectionItemListPlainQueryVariables = Exact<{
  input?: InputMaybe<ListOptions>;
}>;


export type InfoSectionItemListPlainQuery = { infoSectionItemListPlain: Array<{ id: number, title?: string | undefined, content?: string | undefined, date?: number | undefined, time?: string | undefined, topic?: string | undefined, speaker?: string | undefined, language?: string | undefined, link?: string | undefined, image?: string | undefined, displayLanding?: boolean | undefined, index?: number | undefined, sectionId: number, createdAt: number }> };

export type InfoSectionItemCountQueryVariables = Exact<{
  input?: InputMaybe<Scalars['String']>;
}>;


export type InfoSectionItemCountQuery = { infoSectionItemCount?: number | undefined };

export type InfoSectionItemCreateMutationVariables = Exact<{
  input: SectionItemModelInput;
}>;


export type InfoSectionItemCreateMutation = { infoSectionItemCreate?: { id: number, title?: string | undefined, content?: string | undefined, date?: number | undefined, time?: string | undefined, topic?: string | undefined, speaker?: string | undefined, language?: string | undefined, link?: string | undefined, image?: string | undefined, displayLanding?: boolean | undefined, index?: number | undefined, sectionId: number, createdAt: number } | undefined };

export type InfoSectionItemUpdateMutationVariables = Exact<{
  input: SectionItemModelInput;
}>;


export type InfoSectionItemUpdateMutation = { infoSectionItemUpdate?: { id: number, title?: string | undefined, content?: string | undefined, date?: number | undefined, time?: string | undefined, topic?: string | undefined, speaker?: string | undefined, language?: string | undefined, link?: string | undefined, image?: string | undefined, displayLanding?: boolean | undefined, index?: number | undefined, sectionId: number, createdAt: number } | undefined };

export type InfoSectionItemDeleteMutationVariables = Exact<{
  input: Scalars['ID'];
}>;


export type InfoSectionItemDeleteMutation = { infoSectionItemDelete?: void | undefined };

export type SectionRespAllFragment = { id: number, title: string, slug: string, pid?: number | undefined, showSupport: boolean, index?: number | undefined, createdAt: number };

export type InfoSectionGetByIdQueryVariables = Exact<{
  input: Scalars['ID'];
}>;


export type InfoSectionGetByIdQuery = { infoSectionGetById?: { id: number, title: string, slug: string, pid?: number | undefined, showSupport: boolean, index?: number | undefined, createdAt: number } | undefined };

export type InfoSectionListQueryVariables = Exact<{
  input?: InputMaybe<ListSectionReq>;
}>;


export type InfoSectionListQuery = { infoSectionList: Array<{ id: number, title: string, slug: string, pid?: number | undefined, showSupport: boolean, index?: number | undefined, createdAt: number }> };

export type InfoSectionListPlainQueryVariables = Exact<{
  input?: InputMaybe<ListOptions>;
}>;


export type InfoSectionListPlainQuery = { infoSectionListPlain: Array<{ id: number, title: string, slug: string, pid?: number | undefined, showSupport: boolean, index?: number | undefined, createdAt: number }> };

export type InfoSectionCountQueryVariables = Exact<{ [key: string]: never; }>;


export type InfoSectionCountQuery = { infoSectionCount?: number | undefined };

export type InfoSectionCreateMutationVariables = Exact<{
  input: SectionModelInput;
}>;


export type InfoSectionCreateMutation = { infoSectionCreate?: { id: number, title: string, slug: string, pid?: number | undefined, showSupport: boolean, index?: number | undefined, createdAt: number } | undefined };

export type InfoSectionUpdateMutationVariables = Exact<{
  input: SectionModelInput;
}>;


export type InfoSectionUpdateMutation = { infoSectionUpdate?: { id: number, title: string, slug: string, pid?: number | undefined, showSupport: boolean, index?: number | undefined, createdAt: number } | undefined };

export type InfoSectionDeleteMutationVariables = Exact<{
  input: Scalars['ID'];
}>;


export type InfoSectionDeleteMutation = { infoSectionDelete?: void | undefined };

export const SectionItemRespAllFragmentDoc = gql`
    fragment SectionItemRespAll on SectionItemModel {
  id
  title
  content
  date
  time
  topic
  speaker
  language
  link
  image
  displayLanding
  index
  sectionId
  createdAt
}
    `;
export const SectionRespAllFragmentDoc = gql`
    fragment SectionRespAll on SectionModel {
  id
  title
  slug
  pid
  showSupport
  index
  createdAt
}
    `;
export const InfoSectionItemGetByIdDocument = gql`
    query InfoSectionItemGetById($input: ID!) {
  infoSectionItemGetById(id: $input) {
    ...SectionItemRespAll
  }
}
    ${SectionItemRespAllFragmentDoc}`;
export const InfoSectionItemLoadInSectionDocument = gql`
    query InfoSectionItemLoadInSection($input: ID!) {
  infoSectionItemLoadInSection(id: $input) {
    ...SectionItemRespAll
  }
}
    ${SectionItemRespAllFragmentDoc}`;
export const InfoSectionItemListDocument = gql`
    query InfoSectionItemList($input: ListItemsReq) {
  infoSectionItemList(in: $input) {
    ...SectionItemRespAll
  }
}
    ${SectionItemRespAllFragmentDoc}`;
export const InfoSectionItemListPlainDocument = gql`
    query InfoSectionItemListPlain($input: ListOptions) {
  infoSectionItemListPlain(in: $input) {
    ...SectionItemRespAll
  }
}
    ${SectionItemRespAllFragmentDoc}`;
export const InfoSectionItemCountDocument = gql`
    query InfoSectionItemCount($input: String) {
  infoSectionItemCount(in: $input)
}
    `;
export const InfoSectionItemCreateDocument = gql`
    mutation InfoSectionItemCreate($input: SectionItemModelInput!) {
  infoSectionItemCreate(in: $input) {
    ...SectionItemRespAll
  }
}
    ${SectionItemRespAllFragmentDoc}`;
export const InfoSectionItemUpdateDocument = gql`
    mutation InfoSectionItemUpdate($input: SectionItemModelInput!) {
  infoSectionItemUpdate(in: $input) {
    ...SectionItemRespAll
  }
}
    ${SectionItemRespAllFragmentDoc}`;
export const InfoSectionItemDeleteDocument = gql`
    mutation InfoSectionItemDelete($input: ID!) {
  infoSectionItemDelete(id: $input)
}
    `;
export const InfoSectionGetByIdDocument = gql`
    query InfoSectionGetById($input: ID!) {
  infoSectionGetById(id: $input) {
    ...SectionRespAll
  }
}
    ${SectionRespAllFragmentDoc}`;
export const InfoSectionListDocument = gql`
    query InfoSectionList($input: ListSectionReq) {
  infoSectionList(in: $input) {
    ...SectionRespAll
  }
}
    ${SectionRespAllFragmentDoc}`;
export const InfoSectionListPlainDocument = gql`
    query InfoSectionListPlain($input: ListOptions) {
  infoSectionListPlain(in: $input) {
    ...SectionRespAll
  }
}
    ${SectionRespAllFragmentDoc}`;
export const InfoSectionCountDocument = gql`
    query InfoSectionCount {
  infoSectionCount
}
    `;
export const InfoSectionCreateDocument = gql`
    mutation InfoSectionCreate($input: SectionModelInput!) {
  infoSectionCreate(in: $input) {
    ...SectionRespAll
  }
}
    ${SectionRespAllFragmentDoc}`;
export const InfoSectionUpdateDocument = gql`
    mutation InfoSectionUpdate($input: SectionModelInput!) {
  infoSectionUpdate(in: $input) {
    ...SectionRespAll
  }
}
    ${SectionRespAllFragmentDoc}`;
export const InfoSectionDeleteDocument = gql`
    mutation InfoSectionDelete($input: ID!) {
  infoSectionDelete(id: $input)
}
    `;