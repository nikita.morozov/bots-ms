import gql from 'graphql-tag';
export type Maybe<T> = T | undefined;
export type InputMaybe<T> = T | undefined;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: number;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Int32: number;
  Int64: number;
  Uint32: number;
  Uint64: number;
  Unix: number;
  Void: void;
};

export type ListOptions = {
  limit: Scalars['Int32'];
  offset: Scalars['Int32'];
  order?: InputMaybe<Scalars['String']>;
};

export type Mutation = {
  currencyProviderCreate?: Maybe<ProviderModel>;
  currencyProviderDelete?: Maybe<Scalars['Void']>;
  currencyProviderUpdate?: Maybe<ProviderModel>;
  root: Scalars['Boolean'];
};


export type MutationCurrencyProviderCreateArgs = {
  in: ProviderInput;
};


export type MutationCurrencyProviderDeleteArgs = {
  id?: InputMaybe<Scalars['ID']>;
};


export type MutationCurrencyProviderUpdateArgs = {
  in: ProviderInput;
};

export type Paginator = {
  countPerPage: Scalars['Int32'];
  offset: Scalars['Int32'];
  totalCount: Scalars['Int64'];
};

export type ProviderInput = {
  createdAt?: InputMaybe<Scalars['Unix']>;
  currency: Scalars['String'];
  id?: InputMaybe<Scalars['ID']>;
  path: Scalars['String'];
  sort: Scalars['Int32'];
  url: Scalars['String'];
};

export type ProviderModel = {
  createdAt: Scalars['Unix'];
  currency: Scalars['String'];
  id: Scalars['ID'];
  path: Scalars['String'];
  sort: Scalars['Int32'];
  url: Scalars['String'];
};

export type Query = {
  currencyGetRate: Scalars['Float'];
  currencyProviderCount?: Maybe<Scalars['Int64']>;
  currencyProviderList: Array<ProviderModel>;
  root: Scalars['Boolean'];
};


export type QueryCurrencyGetRateArgs = {
  in: Scalars['String'];
};


export type QueryCurrencyProviderListArgs = {
  in?: InputMaybe<ListOptions>;
};

export type RateModel = {
  currency: Scalars['String'];
  id: Scalars['ID'];
  value: Scalars['Float'];
};

export type Subscription = {
  currencyRateSubscriber: Scalars['Float'];
  root: Scalars['Boolean'];
};


export type SubscriptionCurrencyRateSubscriberArgs = {
  in: Scalars['String'];
};

export type ReapAllProviderFragment = { id: number, path: string, url: string, currency: string, sort: number, createdAt: number };

export type CurrencyProviderCreateMutationVariables = Exact<{
  input: ProviderInput;
}>;


export type CurrencyProviderCreateMutation = { currencyProviderCreate?: { id: number, path: string, url: string, currency: string, sort: number, createdAt: number } | undefined };

export type CurrencyProviderUpdateMutationVariables = Exact<{
  input: ProviderInput;
}>;


export type CurrencyProviderUpdateMutation = { currencyProviderUpdate?: { id: number, path: string, url: string, currency: string, sort: number, createdAt: number } | undefined };

export type CurrencyProviderDeleteMutationVariables = Exact<{
  input: Scalars['ID'];
}>;


export type CurrencyProviderDeleteMutation = { currencyProviderDelete?: void | undefined };

export type CurrencyProviderListQueryVariables = Exact<{
  input: ListOptions;
}>;


export type CurrencyProviderListQuery = { currencyProviderList: Array<{ id: number, path: string, url: string, currency: string, sort: number, createdAt: number }> };

export type CurrencyProviderCountQueryVariables = Exact<{ [key: string]: never; }>;


export type CurrencyProviderCountQuery = { currencyProviderCount?: number | undefined };

export type CurrencyGetRateQueryVariables = Exact<{
  input: Scalars['String'];
}>;


export type CurrencyGetRateQuery = { currencyGetRate: number };

export type CurrencyRateSubscriberSubscriptionVariables = Exact<{
  input: Scalars['String'];
}>;


export type CurrencyRateSubscriberSubscription = { currencyRateSubscriber: number };

export const ReapAllProviderFragmentDoc = gql`
    fragment ReapAllProvider on ProviderModel {
  id
  path
  url
  currency
  sort
  createdAt
}
    `;
export const CurrencyProviderCreateDocument = gql`
    mutation CurrencyProviderCreate($input: ProviderInput!) {
  currencyProviderCreate(in: $input) {
    ...ReapAllProvider
  }
}
    ${ReapAllProviderFragmentDoc}`;
export const CurrencyProviderUpdateDocument = gql`
    mutation CurrencyProviderUpdate($input: ProviderInput!) {
  currencyProviderUpdate(in: $input) {
    ...ReapAllProvider
  }
}
    ${ReapAllProviderFragmentDoc}`;
export const CurrencyProviderDeleteDocument = gql`
    mutation CurrencyProviderDelete($input: ID!) {
  currencyProviderDelete(id: $input)
}
    `;
export const CurrencyProviderListDocument = gql`
    query CurrencyProviderList($input: ListOptions!) {
  currencyProviderList(in: $input) {
    ...ReapAllProvider
  }
}
    ${ReapAllProviderFragmentDoc}`;
export const CurrencyProviderCountDocument = gql`
    query CurrencyProviderCount {
  currencyProviderCount
}
    `;
export const CurrencyGetRateDocument = gql`
    query CurrencyGetRate($input: String!) {
  currencyGetRate(in: $input)
}
    `;
export const CurrencyRateSubscriberDocument = gql`
    subscription CurrencyRateSubscriber($input: String!) {
  currencyRateSubscriber(in: $input)
}
    `;