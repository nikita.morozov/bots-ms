import gql from 'graphql-tag';
export type Maybe<T> = T | undefined;
export type InputMaybe<T> = T | undefined;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: number;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Int32: number;
  Int64: number;
  Uint32: number;
  Uint64: number;
  Unix: number;
  Void: void;
};

export type IRequestModel = {
  amount: Scalars['Uint64'];
  createdAt: Scalars['Unix'];
  id: Scalars['ID'];
  meta?: Maybe<Scalars['String']>;
  status: Scalars['Int32'];
  type: Scalars['String'];
  userId: Scalars['ID'];
};

export type ListOptions = {
  limit: Scalars['Int32'];
  offset: Scalars['Int32'];
  order?: InputMaybe<Scalars['String']>;
};

export type Mutation = {
  requestsChangeStatus?: Maybe<Scalars['Void']>;
  requestsCreate: RequestModel;
  requestsUpdate: RequestModel;
  root: Scalars['Boolean'];
};


export type MutationRequestsChangeStatusArgs = {
  in?: InputMaybe<RequestModelInput>;
};


export type MutationRequestsCreateArgs = {
  in?: InputMaybe<RequestModelInput>;
};


export type MutationRequestsUpdateArgs = {
  in?: InputMaybe<RequestModelInput>;
};

export type Paginator = {
  countPerPage: Scalars['Int32'];
  offset: Scalars['Int32'];
  totalCount: Scalars['Int64'];
};

export type Query = {
  requestsCount: Scalars['Int64'];
  requestsGet: RequestModel;
  requestsList: Array<RequestModel>;
  root: Scalars['Boolean'];
};


export type QueryRequestsGetArgs = {
  in?: InputMaybe<Scalars['ID']>;
};


export type QueryRequestsListArgs = {
  in?: InputMaybe<ListOptions>;
};

export type RequestModel = {
  amount: Scalars['Uint64'];
  createdAt: Scalars['Unix'];
  id: Scalars['ID'];
  meta?: Maybe<Scalars['String']>;
  status: Scalars['Int32'];
  type: Scalars['String'];
  userId: Scalars['ID'];
};

export type RequestModelInput = {
  amount: Scalars['Int64'];
  createdAt?: InputMaybe<Scalars['Unix']>;
  id?: InputMaybe<Scalars['ID']>;
  meta?: InputMaybe<Scalars['String']>;
  status?: InputMaybe<Scalars['Int32']>;
  type: Scalars['String'];
  userId: Scalars['ID'];
};

export type Subscription = {
  root: Scalars['Boolean'];
};

export type RequestModelResFragment = { id: number, createdAt: number, userId: number, amount: number, type: string, status: number, meta?: string | undefined };

export type RequestsListQueryVariables = Exact<{
  input: ListOptions;
}>;


export type RequestsListQuery = { requestsList: Array<{ id: number, createdAt: number, userId: number, amount: number, type: string, status: number, meta?: string | undefined }> };

export type RequestsGetQueryVariables = Exact<{
  input: Scalars['ID'];
}>;


export type RequestsGetQuery = { requestsGet: { id: number, createdAt: number, userId: number, amount: number, type: string, status: number, meta?: string | undefined } };

export type RequestsCountQueryVariables = Exact<{ [key: string]: never; }>;


export type RequestsCountQuery = { requestsCount: number };

export type RequestsCreateMutationVariables = Exact<{
  input: RequestModelInput;
}>;


export type RequestsCreateMutation = { requestsCreate: { id: number, createdAt: number, userId: number, amount: number, type: string, status: number, meta?: string | undefined } };

export type RequestsUpdateMutationVariables = Exact<{
  input: RequestModelInput;
}>;


export type RequestsUpdateMutation = { requestsUpdate: { id: number, createdAt: number, userId: number, amount: number, type: string, status: number, meta?: string | undefined } };

export type RequestsChangeStatusMutationVariables = Exact<{
  input: RequestModelInput;
}>;


export type RequestsChangeStatusMutation = { requestsChangeStatus?: void | undefined };

export const RequestModelResFragmentDoc = gql`
    fragment RequestModelRes on RequestModel {
  id
  createdAt
  userId
  amount
  type
  status
  meta
}
    `;
export const RequestsListDocument = gql`
    query RequestsList($input: ListOptions!) {
  requestsList(in: $input) {
    ...RequestModelRes
  }
}
    ${RequestModelResFragmentDoc}`;
export const RequestsGetDocument = gql`
    query RequestsGet($input: ID!) {
  requestsGet(in: $input) {
    ...RequestModelRes
  }
}
    ${RequestModelResFragmentDoc}`;
export const RequestsCountDocument = gql`
    query RequestsCount {
  requestsCount
}
    `;
export const RequestsCreateDocument = gql`
    mutation RequestsCreate($input: RequestModelInput!) {
  requestsCreate(in: $input) {
    ...RequestModelRes
  }
}
    ${RequestModelResFragmentDoc}`;
export const RequestsUpdateDocument = gql`
    mutation RequestsUpdate($input: RequestModelInput!) {
  requestsUpdate(in: $input) {
    ...RequestModelRes
  }
}
    ${RequestModelResFragmentDoc}`;
export const RequestsChangeStatusDocument = gql`
    mutation RequestsChangeStatus($input: RequestModelInput!) {
  requestsChangeStatus(in: $input)
}
    `;