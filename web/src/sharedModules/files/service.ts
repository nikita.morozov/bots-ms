import { IFilesUploadRes } from '@modules/files/models';
import { inject, injectable } from 'inversify';
import HttpClient from '@modules/httpClient';
import { BaseCommandProps } from '@modules/httpClient/types';
import { ApiHttpClientKey } from '@modules/httpClient/constants';

export interface IFilesService {
  upload: (file: File) => Promise<IFilesUploadRes>,
  get: (key: string) => Promise<IFilesUploadRes>,
  getFileInfo: (key: string) => Promise<IFilesUploadRes>,
}

@injectable()
class FilesService implements IFilesService {
  public static diKey = Symbol.for('FilesServiceKey');
  private api: HttpClient;

  public constructor(
    @inject(ApiHttpClientKey) api: HttpClient,
  ) {
    this.api = api;
  }

  getFileInfo(key: string): Promise<IFilesUploadRes> {
    return this.api.request<object, IFilesUploadRes & BaseCommandProps>(
      `/api/v1/file/info/${key}`, undefined, { method: 'get' },
    );
  }

  upload(file: File): Promise<IFilesUploadRes> {
    const formData = new FormData();

    formData.append('file', file);

    return this.api.request<object, IFilesUploadRes & BaseCommandProps>(
      `/api/v1/files/upload`,
      formData,
      {
        method: 'post',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        isFile: true,
      },
    );
  }

  get(key: string): Promise<IFilesUploadRes> {
    return this.api.request<object, IFilesUploadRes & BaseCommandProps>(
      `/api/v1/file/${key}`, undefined, { method: 'get' },
    );
  }
}

export default FilesService;
