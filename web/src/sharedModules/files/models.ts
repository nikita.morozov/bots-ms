export interface IFilesUploadRes {
  createdAt: Date
  id: number
  key: string,
  filename: string,
  size: number
  type: string
}
