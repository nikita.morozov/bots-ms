import { AsyncContainerModule, ContainerModule, interfaces } from 'inversify';
import FilesUsecase from '@modules/files/usecase';
import FilesService from '@modules/files/service';

export const FilesModule = new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<FilesUsecase>(FilesUsecase.diKey).to(FilesUsecase).inSingletonScope();
  bind<FilesService>(FilesService.diKey).to(FilesService);
});