import { IFilesUploadRes } from '@modules/files/models';
import { inject, injectable } from 'inversify';
import FilesService, { IFilesService } from '@modules/files/service';
import { IAppConfig } from '@modules/app/types';
import { AppConfigDiKey } from '@modules/app/constants';

export interface IFilesUsecase {
  upload: (file: File) => Promise<IFilesUploadRes>,
  get: (key: string) => Promise<IFilesUploadRes>,
  getFileInfo: (key: string) => Promise<IFilesUploadRes>,
}

@injectable()
class FilesUsecase implements IFilesUsecase {
  public static diKey = Symbol.for('FilesUsecaseDiKey');

  private filesService: IFilesService;
  private appConfig: IAppConfig;
  constructor(
    @inject(FilesService.diKey) filesService: IFilesService,
    @inject(AppConfigDiKey) appConfig: IAppConfig,
  ) {
    this.filesService = filesService;
    this.appConfig = appConfig;
  }

  upload(file: File): Promise<IFilesUploadRes> {
    return this.filesService.upload(file);
  }

  get(key: string): Promise<IFilesUploadRes> {
    return this.filesService.get(key);
  }

  getFileInfo(key: string): Promise<IFilesUploadRes> {
    return this.filesService.getFileInfo(key);
  }

  srcByKey(key: string | undefined): string {
    if (!key) {
      return '';
    }

    return `${this.appConfig.imageStorageUrl}/api/v1/file/${key}`;
  }
}

export default FilesUsecase;
