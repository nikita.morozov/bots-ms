import { grpc } from '@improbable-eng/grpc-web';
import { inject, injectable } from 'inversify';
import { ProtobufMessage } from '@improbable-eng/grpc-web/dist/typings/message';
import forEach from 'lodash/forEach';
import { ApiConfigDiKey, UnaryMethodDefinitionish } from '../grpcClient/types';
import { BrowserHeaders } from 'browser-headers';
import { Observable } from 'rxjs';
import { ApiClientOptions, IApiConfig } from '@modules/client/types';
import BaseClient from '@modules/client/model';

interface WriterBuf {
  finish(): Uint8Array,
}

interface Coder<T> {
  encode: (message: T) => WriterBuf;
  decode: (values: Uint8Array, length?: number) => T;
}

export interface StreamRequest<T> {
  obj: T,
  coder: Coder<T>
}

@injectable()
export class ApiClient extends BaseClient {
  public static diKey = Symbol.for('HttpClientKey');

  private readonly host: string;

  constructor(
    @inject(ApiConfigDiKey) config: IApiConfig,
  ) {
    super(config);
    const host = config.getHost();

    if (!host) {
      throw new Error('Host is not undefined');
    }

    this.host = host;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    options?: ApiClientOptions,
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata = new BrowserHeaders({
      ...(options?.metadata || {}),
      ...(this._getExtraMetadata(options) || {}),
    })
    forEach(this.middlewares, (m) => m.OnRequestStart && m.OnRequestStart(options));

    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request: {
          toObject: () => request.obj,
          serializeBinary: () => request.coder.encode(request.obj).finish(),
        },
        host: options?.host ? options.host : this.host,
        metadata: maybeCombinedMetadata,
        onEnd: (response) => {
          if (response.status === grpc.Code.OK) {
            forEach(this.middlewares, (m) => m.OnRequestComplete && m.OnRequestComplete(response, options));

            if (response.message) {
              const { toObject, ...otherObj } = response.message as ProtobufMessage;
              resolve(otherObj);
            } else {
              resolve(response.message);
            }
          } else {
            if (options?.silent) {
              return reject({ message: response.statusMessage, code: response.status, metadata: response.trailers });
            }

            const err = new Error(response.statusMessage);
            forEach(this.middlewares, (m) => m.OnRequestError && m.OnRequestError(err, options));
            reject({ message: response.statusMessage, code: response.status, metadata: response.trailers });
          }
        },
      });
    });
  }

  unary$<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    options?: ApiClientOptions,
  ): Observable<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata = new BrowserHeaders({
      ...(options?.metadata || {}),
      ...(this._getExtraMetadata(options) || {}),
    })
    forEach(this.middlewares, (m) => m.OnRequestStart && m.OnRequestStart(options));

    return new Observable<any>(s => {
      grpc.unary(methodDesc, {
        request: {
          toObject: () => request.obj,
          serializeBinary: () => request.coder.encode(request.obj).finish(),
        },
        host: options?.host ? options.host : this.host,
        metadata: maybeCombinedMetadata,
        onEnd: (response) => {
          if (response.status === grpc.Code.OK) {
            forEach(this.middlewares, (m) => m.OnRequestComplete && m.OnRequestComplete(response, options));

            if (response.message) {
              const { toObject, ...otherObj } = response.message as ProtobufMessage;
              s.next(otherObj);
            } else {
              s.next(response.message);
            }
          } else {
            if (options?.silent) {
              return;
            }

            const err = new Error(response.statusMessage);
            forEach(this.middlewares, (m) => m.OnRequestError && m.OnRequestError(err, options));
            s.error(err);
          }

          s.complete();
        },
      });
    });
  }

  async stream<TRequest extends ProtobufMessage, TResponse extends ProtobufMessage, M extends grpc.MethodDefinition<TRequest, TResponse>, T>(descriptor: M, request: StreamRequest<T>, options?: ApiClientOptions): Promise<any[]> {
    const metadata = {
      ...(options?.metadata || {}),
      ...this._getExtraMetadata(options),
    };

    forEach(this.middlewares, (m) => m.OnRequestStart && m.OnRequestStart(options));

    return new Promise((resolve, reject) => {
      const res: any[] = [];

      // @ts-ignore
      grpc.invoke(descriptor, {
        host: options?.host ? options.host : this.host,
        metadata,
        request: {
          toObject: () => request.obj,
          serializeBinary: () => request.coder.encode(request.obj).finish(),
        },
        onMessage: (message: any) => {
          res.push(message);
        },
        onEnd: (code: grpc.Code, msg: string | undefined, trailers: grpc.Metadata) => {
          if (code == grpc.Code.OK) {
          } else {
          }

          resolve(res);
        },
      })
    });
  }

  stream$<TRequest extends ProtobufMessage, TResponse extends ProtobufMessage, M extends grpc.MethodDefinition<TRequest, TResponse>, T>(descriptor: M, request: StreamRequest<T>, options?: ApiClientOptions): Observable<any> {
    const metadata = {
      ...(options?.metadata || {}),
      ...this._getExtraMetadata(options),
    };

    forEach(this.middlewares, (m) => m.OnRequestStart && m.OnRequestStart(options));

    return new Observable<any>(s => {
      // @ts-ignore
      grpc.invoke(descriptor, {
        host: options?.host ? options.host : this.host,
        metadata,
        request: {
          toObject: () => request.obj,
          serializeBinary: () => request.coder.encode(request.obj).finish(),
        },
        onMessage: (message: any) => {
          s.next(message)
        },
        onEnd: (code: grpc.Code, msg: string | undefined, trailers: grpc.Metadata) => {
          if ((code == grpc.Code.OK) || (code == grpc.Code.Unknown)) {
            s.complete();
            return;
          }

          console.error(msg, descriptor);
          const err = new Error(msg);
          forEach(this.middlewares, (m) => m.OnRequestError && m.OnRequestError(err, options));
          s.error(err);
        },
      })
    })
  }
}
