import { grpc } from '@improbable-eng/grpc-web';

interface UnaryMethodDefinitionishR
	extends grpc.UnaryMethodDefinition<any, any> {
	requestStream: any;
	responseStream: any;
}

export type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;


export const ApiConfigDiKey = Symbol.for("ApiConfigDiKey")