import { AsyncContainerModule, interfaces } from 'inversify';
import LoadingService from '@modules/loading/service';
import LoadingUsecase from '@modules/loading/usecase';

export const LoadingModule = new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<LoadingService>(LoadingService.diKey).to(LoadingService);
  bind<LoadingUsecase>(LoadingUsecase.diKey).to(LoadingUsecase).inSingletonScope();
});
