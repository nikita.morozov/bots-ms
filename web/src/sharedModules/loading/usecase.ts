import { inject, injectable } from 'inversify';
import { Subject, Subscription } from 'rxjs';
import { ILoadingCallbackType, ILoadingEvent } from '@modules/loading/type';
import LoadingService from '@modules/loading/service';

export interface ILoadingUsecase {
  load(context: string, callback: ILoadingCallbackType): Subscription
  getSubject(): Subject<ILoadingEvent>
  getSize(context: string): number
}

@injectable()
class LoadingUsecase implements ILoadingUsecase {
  public static diKey = Symbol.for('LoadingUsecaseKey');

  private loadingService: LoadingService

  public constructor(
    @inject(LoadingService.diKey) loadingService: LoadingService,
  ) {
    this.loadingService = loadingService
  }

  load(context: string, callback: ILoadingCallbackType): Subscription {
    return this.loadingService.load(context, callback)
  }

  getSize(context: string): number {
    return this.loadingService.getSize(context)
  }

  getSubject(): Subject<ILoadingEvent> {
    return this.loadingService.getSubject()
  }
}


export default LoadingUsecase;
