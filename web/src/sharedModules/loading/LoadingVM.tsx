import ObservableVM, { BaseViewModel } from '@modules/Stream/model/BaseVM';
import { ObservableField } from '@modules/Stream/model/decorator';
import LoadingUsecase from '@modules/loading/usecase';
import { ILoadingMetaData } from '@modules/loading/type';

class LoadingVM extends BaseViewModel {
  @ObservableField()
  public visible: ObservableVM<boolean>
  private loadingUC: LoadingUsecase
  private withTimeout?: boolean
  private context: string
  private clearHandler?: (() => void)
  public defaultValue?: boolean
  public changeHandler?: (value: boolean) => void


  constructor(loadingUC: LoadingUsecase, context: string, metadata?: ILoadingMetaData) {
    super();
    this.loadingUC = loadingUC
    this.defaultValue = metadata?.defaultValue || false
    this.visible = new ObservableVM<boolean>(this.defaultValue)
    this.changeHandler = metadata?.changeHandler
    this.changeHandler = metadata?.changeHandler
    this.context = context
    this.withTimeout = metadata?.withTimeout
  }

  changeVisible = (value: boolean) => {
    this.visible.setItem(value)
    this.changeHandler && this.changeHandler(value)
  }

  setVisible(func: () => void, time = 1000) {
    const handler = setTimeout(func, time)
    return () => clearTimeout(handler)
  }

  setLoading() {
    this.clearLoading()
    this.clearHandler = this.setVisible(() => this.changeVisible(!!this.defaultValue))
  }

  clearLoading() {
    if (!!this.clearHandler) {
      this.clearHandler();
      this.clearHandler = undefined;
    }
  }

  remove(size: number) {
    if (!!size) {
      this.clearLoading()
    } else {
      this.setLoading()
    }
  }

  set() {
      this.clearLoading()
    this.changeVisible(!this.defaultValue);
  }

  load = () => {
    const sub = this.loadingUC.load(this.context, ({ kind, size }) => {
      if (kind === 'remove') {
        this.remove(size)
      }
      if (kind === 'set' && !this.visible.getItem()) {
        this.set()
      }
    })

    this.addSubscription(sub)
  }

  private setAction() {
    const size = this.loadingUC.getSize(this.context)
    return {
      kind: 'set',
      context: 'router',
      size: size + 1,
    }
  }

  private removeAction() {
    const size = this.loadingUC.getSize(this.context)
    return {
      kind: 'remove',
      context: 'router',
      size: Math.max(size - 1, 0),
    }
  }

  onStartHandler() {
    this.loadingUC.getSubject().next(this.setAction())
  }

  onCompleteHandler() {
    this.loadingUC.getSubject().next(this.removeAction())
  }

  onErrorHandler() {
    this.loadingUC.getSubject().next(this.removeAction())
  }
}

export default LoadingVM