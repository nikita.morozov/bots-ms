import { injectable } from 'inversify';
import { Subject, Subscription } from 'rxjs';
import { ILoadingCallbackType, ILoadingEvent } from '@modules/loading/type';
import { filter } from 'rxjs/operators';

export interface ILoadingService {
  load(context: string, callback: ILoadingCallbackType): void
  getSize(context: string): number
  getSubject(): Subject<ILoadingEvent>
}

@injectable()
class LoadingService implements ILoadingService {
  public static diKey = Symbol.for('LoadingServiceKey');

  private loadingSubject: Subject<ILoadingEvent>
  private contextSizes: { [context: string]: number }

  public constructor() {
    this.loadingSubject = new Subject<ILoadingEvent>()
    this.contextSizes = {}
  }

  load(context: string, callback: ILoadingCallbackType): Subscription {
    return this.loadingSubject
      .pipe(filter(i => i.context === context))
      .subscribe(callback)
  }

  getSubject(): Subject<ILoadingEvent> {
    return this.loadingSubject
  }


  getSize(context: string): number {
    return this.contextSizes[context] || 0
  }

  incurse(context: string): void {
    if (!this.getSize(context)) {
      this.contextSizes[context] = 0
    }
    this.contextSizes[context] += 1
  }

  reduce(context: string): void {
    if (!this.getSize(context)) {
      this.contextSizes[context] = 0
    }
    this.contextSizes[context] -= 1
  }

}


export default LoadingService;
