import { useState } from 'react';
import { useInjection } from 'inversify-react';
import useLoadableVM from '@modules/Stream/hooks';
import LoadingUsecase from '@modules/loading/usecase';
import LoadingVM from '@modules/loading/LoadingVM';

const useLoading = (context: string, defaultValue?: boolean): [boolean] => {
  const [loading, setLoading] = useState(!!defaultValue)
  const loadingUC = useInjection<LoadingUsecase>(LoadingUsecase.diKey)

  const [vm] = useState(new LoadingVM(loadingUC, context, {
    defaultValue,
    changeHandler: (value) => {
      setLoading(value)
    },
  }))

  useLoadableVM(vm)
  return [loading]
}

export default useLoading;