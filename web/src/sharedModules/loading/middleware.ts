import { ApiClientMiddleware, ApiClientOptions } from '@modules/client/types';
import { ILoadingEvent } from '@modules/loading/type';
import { Subject } from 'rxjs';

export type ContextType = { [key: string]: number }

class LoadingMiddleware implements ApiClientMiddleware {
  private subject?: Subject<ILoadingEvent>
  //TODO: use context size in service
  private contexts: ContextType = {}

  constructor(subject: Subject<ILoadingEvent>) {
    this.subject = subject
  }

  setSize(context: string, size: number) {
    this.contexts = {
      ...this.contexts,
      [context]: size,
    }
  }

  OnRequestStart(options?: ApiClientOptions) {
    if (options?.context) {
      const size = (this.contexts[options?.context] || 0) + 1;
      this.subject?.next({
        kind: 'set',
        context: options?.context,
        size,
      })

      this.setSize(options.context, size)
    }
  }

  OnRequestComplete(err: Error, options?: ApiClientOptions) {
    if (options?.context) {
      const size = Math.max((this.contexts[options?.context] || 0) - 1, 0);
      this.subject?.next({
        kind: 'remove',
        context: options?.context,
        size,
      })
      this.setSize(options.context, size)
    }
  }

  OnRequestError(err: Error, options?: ApiClientOptions) {
    if (options?.context) {
      const size = Math.max((this.contexts[options?.context] || 0) - 1, 0);
      this.subject?.next({
        kind: 'remove',
        context: options?.context,
        size,
      })
      this.setSize(options.context, size)
    }
  }
}

export default LoadingMiddleware;