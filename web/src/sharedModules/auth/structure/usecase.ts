import { inject, injectable } from 'inversify';
import StructService, { IStructService } from '@modules/auth/structure/service';
import { Observable, of } from 'rxjs';
import SessionService from '@modules/auth/session/service';

export interface IStructUsecase {
  getStructCount(): Observable<number>
}

@injectable()
class StructUsecase implements IStructUsecase {
  public static diKey = Symbol.for('StructUsecaseDiKey');

  private structureService: IStructService;
  private sessionService: SessionService;

  constructor(
    @inject(StructService.diKey) structureService: IStructService,
    @inject(SessionService.diKey) sessionService: SessionService,
  ) {
    this.structureService = structureService;
    this.sessionService = sessionService;
  }

  getStructCount(): Observable<number> {
    const user = this.sessionService.getCurrentUser();

    if (user?.id) {
      return this.structureService.getStructCount(user.id)
    }

    return of(0)
  }
}

export default StructUsecase;