import { AsyncContainerModule, interfaces } from 'inversify';
import StructUsecase, { IStructUsecase } from '@modules/auth/structure/usecase';
import StructService, { IStructService } from './service';

export const StructureInfoModule = new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IStructService>(StructService.diKey).to(StructService).inSingletonScope();
  bind<IStructUsecase>(StructUsecase.diKey).to(StructUsecase)
});