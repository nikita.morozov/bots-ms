import { inject, injectable } from 'inversify';
import { StructureGetStructureCountDesc, UserWithDepthReq } from '../proto/structure';
import { ApiClient } from '@modules/grpcClient';
import { AppConfigDiKey } from '@modules/app/constants';
import { IAppConfig } from '@modules/app/types';
import { map, Observable } from 'rxjs';

export interface IStructService {
  getStructCount(userId: number, maxDepth?: number): Observable<number>
}

@injectable()
class StructService implements IStructService {
  public static diKey = Symbol.for('StructServiceKey');
  private api: ApiClient;
  private appConfig: IAppConfig;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
    @inject(AppConfigDiKey) appConfig: IAppConfig,
  ) {
    this.api = api;
    this.appConfig = appConfig;
  }

  getStructCount(userId: number, maxDepth?: number): Observable<number> {
    const req = UserWithDepthReq.fromPartial({
      userId,
      maxDepth
    })
    return this.api.stream$(
      StructureGetStructureCountDesc,
      {
        obj: req,
        coder: UserWithDepthReq,
      },
      { host: this.appConfig.authUrlServer }
    ).pipe(map(i => i.value))
  }
}

export default StructService;