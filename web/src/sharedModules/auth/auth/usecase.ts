import { inject, injectable } from 'inversify';
import SessionService, { ISessionService } from '@modules/auth/session/service';
import { LoginResp, RegAndLoginReq, RegisterResp } from '../proto/auth';
import AuthService, { IAuthService } from '@modules/auth/auth/service';
import SessionCookieStorage from '@modules/auth/session/cookie';

export interface IAuthUsecase {
  login: (req: RegAndLoginReq) => Promise<LoginResp>,
  register: (req: RegAndLoginReq) => Promise<RegisterResp>,
}

@injectable()
class AuthUsecase implements IAuthUsecase {
  public static diKey = Symbol.for('AuthUsecaseDiKey');

  private sessionService: ISessionService;
  private sessionCookieStorage: SessionCookieStorage;
  private authService: IAuthService;

  constructor(
    @inject(AuthService.diKey) authService: IAuthService,
    @inject(SessionService.diKey) sessionService: ISessionService,
    @inject(SessionCookieStorage.diKey) sessionCookieStorage: SessionCookieStorage,
  ) {
    this.sessionService = sessionService;
    this.authService = authService;
    this.sessionCookieStorage = sessionCookieStorage;
  }

  login(req: RegAndLoginReq): Promise<LoginResp> {
    return this.authService.login(req).then((res) => {
      this.sessionCookieStorage.setAccessToken(res.accessToken, req.applicationSlug);
      this.sessionCookieStorage.setRefreshToken(res.refreshToken, req.applicationSlug);

      return res;
    });
  }

  register(req: RegAndLoginReq): Promise<RegisterResp> {
    return this.authService.register(req);
  }
}

export default AuthUsecase;