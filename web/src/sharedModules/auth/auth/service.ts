import { inject, injectable } from 'inversify';
import { ApiClient } from '@modules/grpcClient';
import { AuthLoginDesc, AuthRegisterDesc, LoginResp, RegAndLoginReq, RegisterResp } from '@modules/auth/proto/auth';
import { AppConfigDiKey } from '@modules/app/constants';
import { IAppConfig } from '@modules/app/types';

export interface IAuthService {
  login: (req: RegAndLoginReq) => Promise<LoginResp>,
  register: (req: RegAndLoginReq) => Promise<RegisterResp>,
}

@injectable()
class AuthService implements IAuthService {
  public static diKey = Symbol.for('AuthServiceKey');
  private api: ApiClient;
  private appConfig: IAppConfig;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
    @inject(AppConfigDiKey) appConfig: IAppConfig,
  ) {
    this.api = api;
    this.appConfig = appConfig;
  }

  login(req: RegAndLoginReq): Promise<LoginResp> {
    return this.api.unary(
      AuthLoginDesc,
      {
        obj: req,
        coder: RegAndLoginReq,
      },
      { host: this.appConfig.authUrlServer }
    );
  }

  register(req: RegAndLoginReq): Promise<RegisterResp> {
    return this.api.unary(
      AuthRegisterDesc,
      {
        obj: req,
        coder: RegAndLoginReq,
      },
      { host: this.appConfig.authUrlServer }
    );
  }
}

export default AuthService;