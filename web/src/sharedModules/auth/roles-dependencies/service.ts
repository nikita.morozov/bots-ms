import { inject, injectable } from 'inversify';
import { ApiClient } from '@modules/grpcClient';
import { map, Observable } from 'rxjs';
import { GetGroupingPolicyReq, PolicyGetGroupingPolicyDesc, PolicyModel } from '@modules/auth/proto/policy';
import { AppConfigDiKey } from '@modules/app/constants';
import { IAppConfig } from '@modules/app/types';

export interface IRoleService {
  initialize: () => Observable<PolicyModel>,
}

@injectable()
class RoleService implements IRoleService {
  public static diKey = Symbol.for('RoleService');

  private api: ApiClient;
  private appConfig: IAppConfig;

  constructor(
    @inject(ApiClient.diKey) api: ApiClient,
    @inject(AppConfigDiKey) appConfig: IAppConfig,
  ) {
    this.api = api;
    this.appConfig = appConfig;
  }

  initialize(): Observable<PolicyModel> {
    return this.api.stream$(
      PolicyGetGroupingPolicyDesc,
      {
        obj: GetGroupingPolicyReq.fromPartial({ opts: { offset: 0, limit: 1000, order: 'id asc' } }),
        coder: GetGroupingPolicyReq,
      },
      { host: this.appConfig.authUrlServer }
    )
  }
}

export default RoleService;