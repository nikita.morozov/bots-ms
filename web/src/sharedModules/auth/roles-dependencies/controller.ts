import { PolicyModel } from '@modules/auth/proto/policy';

interface IRoles extends PolicyModel {
  children: IRoles[];
}

class RoleDependenciesController {
  private roles: IRoles[] = [];

  get items() {
    return this.roles;
  }


  load(roles: PolicyModel[]) {
    const _roles = roles.map(i => ({ ...i, children: [] }));

    const insert = (arr: IRoles[] = [], cur: IRoles): boolean => {
      let res = false;
      for (let i = 0; i < arr.length; i++) {
        if ((arr[i].v1 === cur.v0) && (arr[i].v2 === cur.v2)) {
          arr[i].children = [...(arr[i].children || []), cur];

          res = true;
        }

        if (arr[i].children && arr[i].children.length) {
          const r = insert(arr[i].children, cur);
          res = res || r;
        }
      }

      return res;
    }

    let done = false;
    let i = 0;
    while (!done) {
      const item = _roles[i];

      if (!item) {
        break;
      }

      if (item.v0 === null) {
        i++;
        continue;
      }

      const r = insert(_roles, item);

      if (r) {
        _roles.splice(i, 1);
        continue;
      }

      if (i >= _roles.length) {
        done = true;
      }
      i++;
    }

    this.roles = _roles;
  }

  check(checkedRole: string, role: string, domain: string): boolean {
    if (checkedRole.length === 0 || role.length === 0 || domain.length === 0) {
      return false;
    }

    const filteredValues = this.roles.filter(i => i.v2 === domain && i.v1 === checkedRole);

    if (!filteredValues.length) {
      return false;
    }

    if ((filteredValues[0].v1 === role) || (checkedRole === role)) {
      return true;
    }

    return this._check(filteredValues[0].children, role);
  }

  private _check(filteredValues: IRoles[], role: string): boolean {
    let res = false;
    for (const val of filteredValues) {
      if (val.v1 === role) {
        res = true;
      } else {
        if (val.children && val.children.length) {
          res = this._check(val.children, role)
        }
      }
    }

    return res;
  }
}

export default RoleDependenciesController;