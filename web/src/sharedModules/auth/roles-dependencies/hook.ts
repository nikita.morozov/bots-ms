import { useInjection } from 'inversify-react';
import RoleUsecase from '@modules/auth/roles-dependencies/usecase';
import { AppConfigDiKey } from '@modules/app/constants';
import { IAppConfig } from '@modules/app/types';
import { useEffect, useState } from 'react';

// This hook need to check if you inherited role or not
//
// EXAMPLE
// This check if admin inherited from user
// useRoleCheck('user', 'admin');

const useRoleCheck = (checkRole: string, role: string) => {
  const [isInherit, setIsInherit] = useState<boolean>();
  const [initialized, setInitialized] = useState(false);
  const uc = useInjection<RoleUsecase>(RoleUsecase.diKey);
  const app = useInjection<IAppConfig>(AppConfigDiKey);

  useEffect(() => {
    if (!uc.isInitialized()) {
      uc.initialize().then(() => setInitialized(true));
    } else {
      setInitialized(true);
    }
  }, []);

  useEffect(() => {
    if (initialized) {
      setIsInherit(uc.isInherit(checkRole, role, `${app.org}:${app.app}`));
    }
  }, [initialized]);

  return isInherit;
}

export default useRoleCheck;