import {expect, test} from '@jest/globals';
import RoleDependenciesController from '@modules/auth/roles-dependencies/controller';
import { PolicyModel } from '@modules/auth/proto/policy';

const simpleGroup: PolicyModel[] = [
  PolicyModel.fromPartial({ ptype: 'g', v0: 'admin', v1: 'moder', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: 'moder', v1: 'user', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: 'user', v1: 'public', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: 'admin', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: 'user', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: 'public', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: 'moder', v2: 'jetup:sso' }),
];

const reverseSimpleGroup: PolicyModel[] = [
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: 'admin', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: 'user', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: 'public', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: 'moder', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: 'user', v1: 'public', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: 'moder', v1: 'user', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: 'admin', v1: 'moder', v2: 'jetup:sso' }),
];

const mixedSortSimpleGroup: PolicyModel[] = [
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: 'admin', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: 'user', v1: 'public', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: 'user', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: 'moder', v1: 'user', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: 'admin', v1: 'moder', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: 'public', v2: 'jetup:sso' }),
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: 'moder', v2: 'jetup:sso' }),
];

const complexGroup: PolicyModel[] = [
  PolicyModel.fromPartial({ ptype: 'g', v0: "admin", v1: "user", v2: "dao2:fairyapp"}),
  PolicyModel.fromPartial({ ptype: 'g', v0: "user", v1: "public", v2: "dao2:fairyapp"}),
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: "admin", v2: "dao2:fairyapp"}),
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: "api-user", v2: "dao2:fairyapp"}),
  PolicyModel.fromPartial({ ptype: 'g', v0: "fairy_moderator", v1: "user", v2: "dao2:fairyapp"}),
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: "fairy_moderator", v2: "dao2:fairyapp"}),
  PolicyModel.fromPartial({ ptype: 'g', v0: "fairy_location_read", v1: "user", v2: "dao2:fairyapp"}),
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: "fairy_location_read", v2: "dao2:fairyapp"}),
  PolicyModel.fromPartial({ ptype: 'g', v0: "fairy_location_write", v1: "user", v2: "dao2:fairyapp"}),
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: "fairy_location_write", v2: "dao2:fairyapp"}),
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: "fairy_location_moderator", v2: "dao2:fairyapp"}),
  PolicyModel.fromPartial({ ptype: 'g', v0: "fairy_location_moderator", v1: "fairy_location_read", v2: "dao2:fairyapp"}),
  PolicyModel.fromPartial({ ptype: 'g', v0: "fairy_location_moderator", v1: "fairy_location_write", v2: "dao2:fairyapp"}),
  PolicyModel.fromPartial({ ptype: 'g', v0: "fairy_moderator", v1: "fairy_bot_moderator", v2: "dao2:fairyapp"}),
  PolicyModel.fromPartial({ ptype: 'g', v0: "fairy_bot_moderator", v1: "user", v2: "dao2:fairyapp"}),
  PolicyModel.fromPartial({ ptype: 'g', v0: undefined, v1: "fairy_bot_moderator", v2: "dao2:fairyapp"}),
  PolicyModel.fromPartial({ ptype: 'g', v0: "admin", v1: "fairy_moderator", v2: "dao2:fairyapp"}),
];

test('Role dependencies test', () => {
  const controller = new RoleDependenciesController();
  controller.load(simpleGroup);
  expect(controller.check('admin', 'user', 'jetup:sso')).toBe(true);
  expect(controller.check('user', 'admin', 'jetup:sso')).toBe(false);
  expect(controller.check('moder', 'admin', 'jetup:sso')).toBe(false);
  expect(controller.check('admin', 'moder', 'jetup:sso')).toBe(true);
  expect(controller.check('admin', 'public', 'jetup:sso')).toBe(true);
  expect(controller.check('admin', 'admin', 'jetup:sso')).toBe(true);
  expect(controller.check('public', 'public', 'jetup:sso')).toBe(true);
  expect(controller.check('moder', 'moder', 'jetup:sso')).toBe(true);
  expect(controller.check('user', 'user', 'jetup:sso')).toBe(true);
  expect(controller.check('', 'user', 'jetup:sso')).toBe(false);
  expect(controller.check('notexistist', 'user', 'jetup:sso')).toBe(false);

  controller.load(reverseSimpleGroup);
  expect(controller.check('admin', 'user', 'jetup:sso')).toBe(true);
  expect(controller.check('user', 'admin', 'jetup:sso')).toBe(false);
  expect(controller.check('moder', 'admin', 'jetup:sso')).toBe(false);
  expect(controller.check('admin', 'moder', 'jetup:sso')).toBe(true);
  expect(controller.check('admin', 'public', 'jetup:sso')).toBe(true);
  expect(controller.check('admin', 'admin', 'jetup:sso')).toBe(true);
  expect(controller.check('public', 'public', 'jetup:sso')).toBe(true);
  expect(controller.check('moder', 'moder', 'jetup:sso')).toBe(true);
  expect(controller.check('user', 'user', 'jetup:sso')).toBe(true);
  expect(controller.check('', 'user', 'jetup:sso')).toBe(false);
  expect(controller.check('notexistist', 'user', 'jetup:sso')).toBe(false);

  controller.load(mixedSortSimpleGroup);
  expect(controller.check('admin', 'user', 'jetup:sso')).toBe(true);
  expect(controller.check('user', 'admin', 'jetup:sso')).toBe(false);
  expect(controller.check('moder', 'admin', 'jetup:sso')).toBe(false);
  expect(controller.check('admin', 'moder', 'jetup:sso')).toBe(true);
  expect(controller.check('admin', 'public', 'jetup:sso')).toBe(true);
  expect(controller.check('public', 'public', 'jetup:sso')).toBe(true);
  expect(controller.check('moder', 'moder', 'jetup:sso')).toBe(true);
  expect(controller.check('user', 'user', 'jetup:sso')).toBe(true);
  expect(controller.check('', 'user', 'jetup:sso')).toBe(false);
  expect(controller.check('notexistist', 'user', 'jetup:sso')).toBe(false);

  controller.load(complexGroup);
  expect(controller.check('user', 'fairy_moderator', 'dao2:fairyapp')).toBe(false);
  expect(controller.check('admin', 'fairy_moderator', 'dao2:fairyapp')).toBe(true);
  expect(controller.check('fairy_location_moderator', 'fairy_moderator', 'dao2:fairyapp')).toBe(false);
  expect(controller.check('user', 'api-user', 'dao2:fairyapp')).toBe(false);
});