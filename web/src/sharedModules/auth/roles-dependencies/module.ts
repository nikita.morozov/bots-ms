import { AsyncContainerModule, interfaces } from 'inversify';
import RoleUsecase, { IRoleUsecase } from '@modules/auth/roles-dependencies/usecase';
import RoleService, { IRoleService } from '@modules/auth/roles-dependencies/service';

export const RoleDependenciesModule = new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IRoleUsecase>(RoleUsecase.diKey).to(RoleUsecase);
  bind<IRoleService>(RoleService.diKey).to(RoleService);
});