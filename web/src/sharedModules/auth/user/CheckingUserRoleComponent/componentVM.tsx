import SessionUsecase from '@modules/auth/session/usecase';
import ObservableVM, { BaseViewModel } from '@modules/Stream/model/BaseVM';

interface CheckingUserRoleParams {
  role: string;
}

class CheckingUserRoleVM extends BaseViewModel<CheckingUserRoleParams> {
  public checkRole: ObservableVM<boolean>;

  private readonly sessionUC: SessionUsecase;

  constructor(sessionUC: SessionUsecase) {
    super();

    this.sessionUC = sessionUC;
    this.checkRole = new ObservableVM<boolean>(false);
  }

  load = (params?: CheckingUserRoleParams) => {
    const userSub = this.sessionUC.getCurrentUser$().subscribe(e => {
      if (!params?.role || !e?.role || e?.role !== params.role) {
        this.checkRole.setItem(false);
      }
      !!e && this.checkRole.setItem(true);
    });

    this.addSubscription(userSub);
  };
}

export default CheckingUserRoleVM;
