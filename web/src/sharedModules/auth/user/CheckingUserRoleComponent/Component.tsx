import { FunctionComponent } from 'react';
import cObserver from '@modules/Stream/model/connector';
import { ICheckingUserRoleComponentView } from '@modules/auth/user/CheckingUserRoleComponent';

const CheckingUserRoleComponentView: FunctionComponent<ICheckingUserRoleComponentView> = (props) => {
  const { children, checkRole } = props;
  if (!checkRole) {
    return null;
  }
  return (
    <>
      {children}
    </>
  );
};
export default cObserver(CheckingUserRoleComponentView);