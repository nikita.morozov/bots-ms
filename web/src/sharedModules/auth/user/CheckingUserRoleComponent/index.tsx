import { FunctionComponent, ReactElement, useState } from 'react';
import CheckingUserRoleComponentView from './Component';
import { useInjection } from 'inversify-react';
import SessionUsecase from '@modules/auth/session/usecase';
import useLoadableVM from '@modules/Stream/hooks';
import CheckingUserRoleVM from '@modules/auth/user/CheckingUserRoleComponent/componentVM';

interface ICheckingUserRoleComponentChildren {
  children: ReactElement;
}

export interface ICheckingUserRoleComponentView extends ICheckingUserRoleComponentChildren{
  checkRole: boolean;
}

interface ICheckingUserRoleComponent extends ICheckingUserRoleComponentChildren {
  role: string;
}

const CheckingUserRoleComponent: FunctionComponent<ICheckingUserRoleComponent> = (props) => {
  const { role, children } = props;
  if (!children) {
    return null;
  }

  const sessionUC = useInjection<SessionUsecase>(SessionUsecase.diKey);

  const [vm] = useState(() => new CheckingUserRoleVM(sessionUC));
  useLoadableVM(vm, { role: role });

  return (
    <CheckingUserRoleComponentView
      vm={vm}
      children={children}
    />
  );
};
export default CheckingUserRoleComponent;