import { Container, interfaces } from 'inversify';
import { UserModule } from '@modules/auth/user/module';

const userContainer: interfaces.Container = new Container();

userContainer.load(UserModule);

export default userContainer;