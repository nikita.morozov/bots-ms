import { map, Observable, Subscription } from 'rxjs';
import { inject, injectable } from 'inversify';
import UserService, { AdminUserService, IUserService } from '@modules/auth/user/service';
import { ListOptions } from '@modules/paginatior';
import User from '@modules/auth/user/user';
import SessionService, { ISessionService } from '@modules/auth/session/service';
import { IAppConfig } from '@modules/app/types';
import { AppConfigDiKey } from '@modules/app/constants';
import { IBaseListUsecase, IPageService } from '@modules/Stream/types';
import { PageService } from '@modules/Stream/service/ListService';
import ListMV from '@modules/Stream/model/ListMV';
import { UserFilterModel } from '@modules/auth/proto/user';

export interface IBaseUserUsecase<UserType extends User = User> {
  getByAlias(alias: string): Observable<UserType>,

  update(value: UserType): Observable<UserType | undefined>

  verifyEmail: (token: string, app: string) => Observable<boolean>,
  getById: (id: number) => Observable<UserType | undefined>,
}

export interface IAdminUserUsecase<UserType extends User = User> extends IBaseUserUsecase<UserType> {
  updateByAdmin(req: UserType): Observable<void>,
  create: (req: UserType) => Observable<void>,
  list(opts: ListOptions, method: string, filter?: UserFilterModel): Observable<UserType>,
  getFullById: (id: number) => Observable<UserType | undefined>,
}

@injectable()
class BaseUserUsecase<UserType extends User = User> implements IBaseUserUsecase<UserType> {
  public static diKey = Symbol.for('UserUsecaseDiKey');
  protected cfg: IAppConfig;
  protected userService: IUserService<UserType>;
  protected sessionService: ISessionService<UserType>;

  constructor(
    @inject(AppConfigDiKey) cfg: IAppConfig,
    @inject(UserService.diKey) userService: IUserService<UserType>,
    @inject(SessionService.diKey) sessionService: ISessionService<UserType>,
  ) {
    this.cfg = cfg;
    this.userService = userService;
    this.sessionService = sessionService;
  }

  getByAlias(alias: string): Observable<UserType> {
    return this.userService.getByAlias(alias);
  }

  update(value: UserType): Observable<UserType | undefined> {
    value.createdAt = 0;
    value.path = String(value.path);

    return this.userService.update(value)
      .pipe(
        map(token => {
          this.sessionService.update(token);
          return this.sessionService.getCurrentUser();
        }),
      );
  }

  verifyEmail(token: string, app: string): Observable<boolean> {
    return this.userService.verifyEmail(token, app);
  }

  getById(id: number): Observable<UserType | undefined> {
    return this.userService.getById(id);
  }
}

@injectable()
export class AdminUserUsecase<UserType extends User = User> extends BaseUserUsecase<UserType> implements IAdminUserUsecase<UserType>, IBaseListUsecase<UserType> {
  public static diKey = Symbol.for('AdminUserUsecaseDiKey');
  protected adminUserService: AdminUserService<UserType>;
  private pageService: IPageService<UserType>;

  constructor(
    @inject(AppConfigDiKey) cfg: IAppConfig,
    @inject(UserService.diKey) userService: IUserService<UserType>,
    @inject(AdminUserService.diKey) adminUserService: AdminUserService<UserType>,
    @inject(SessionService.diKey) sessionService: ISessionService<UserType>,
  ) {
    super(cfg, userService, sessionService);
    this.adminUserService = adminUserService;
    this.pageService = new PageService(this.adminUserService);
  }

  loadTotalCount(model: ListMV<UserType>, method?: string | undefined, query?: object | undefined): [Observable<number>, Subscription] {
    return this.pageService.loadTotalCount(model, method, query);
  }

  loadItems(model: ListMV<UserType>, opts: ListOptions, method?: string | undefined, query?: object | undefined): [Observable<UserType>, Subscription] {
    return this.pageService.loadPage(model, opts, method, query);
  }

  updateByAdmin(req: UserType): Observable<void> {
    return this.adminUserService.updateByAdmin(req);
  }

  create(req: UserType): Observable<void> {
    return this.adminUserService.create(req);
  }

  list(opts: ListOptions, method: string, filter?: UserFilterModel): Observable<UserType> {
    return  this.adminUserService.list(opts, method, filter);
  }

  getFullById(id: number): Observable<UserType | undefined> {
    return this.adminUserService.getFullById(id);
  }
}

export default BaseUserUsecase;