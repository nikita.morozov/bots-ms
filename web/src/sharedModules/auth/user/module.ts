import { AsyncContainerModule, interfaces } from 'inversify';
import UserService, { AdminUserService, IAdminUserService, IUserService } from '@modules/auth/user/service';
import BaseUserUsecase, { AdminUserUsecase, IAdminUserUsecase, IBaseUserUsecase } from '@modules/auth/user/usecase';

export const UserModule = new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IBaseUserUsecase>(BaseUserUsecase.diKey).to(BaseUserUsecase).inSingletonScope();
  bind<IUserService>(UserService.diKey).to(UserService);
});

export const UserAdminModule = new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<IAdminUserService>(AdminUserService.diKey).to(AdminUserService);
  bind<IAdminUserUsecase>(AdminUserUsecase.diKey).to(AdminUserUsecase).inSingletonScope();

  bind<IBaseUserUsecase>(BaseUserUsecase.diKey).to(BaseUserUsecase).inSingletonScope();
  bind<IUserService>(UserService.diKey).to(UserService);
});