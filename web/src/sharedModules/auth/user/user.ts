import { ShortUserModel, UserModel } from '@modules/auth/proto/authCommon';
import StringBuilder from '@modules/tools/stringBuilder';
import { ObjectWithId } from '@modules/types';
import { UserShorterModel } from '@modules/graphql/auth/auth-gql';

class User<ProfileModel = {}> implements UserModel, ObjectWithId {
  public blocked: boolean = false;
  public createdAt: number = new Date().valueOf();
  public deleted: boolean = false;
  public email: string = '';
  public id: number = 0;
  public lang: string = 'en';
  public path: string = '[]';
  public role: string = '';
  public verified: boolean = false;
  alias: string | undefined;
  firstName: string | undefined;
  middleName: string | undefined;
  parentId: number | undefined;
  phone: string | undefined;
  surname: string | undefined;
  organizationId: number;
  profile?: ProfileModel;
  $type: 'authMs.UserModel' = 'authMs.UserModel';

  constructor(user: UserModel, profileModel?: ProfileModel) {
    this.blocked = user.blocked;
    this.createdAt = user.createdAt;
    this.deleted = user.deleted;
    this.email = user.email;
    this.id = user.id;
    this.lang = user.lang;
    this.path = user.path;
    this.role = user.role;
    this.verified = user.verified;
    this.alias = user.alias;
    this.firstName = user.firstName;
    this.middleName = user.middleName;
    this.parentId = user.parentId;
    this.phone = user.phone;
    this.surname = user.surname;
    this.$type = user.$type;
    this.organizationId = user.organizationId;
    this.profile = profileModel;
  }

  setProfile(profile: ProfileModel) {
    if (!profile) {
      return
    }
    this.profile = profile
  }

  get object(): UserModel {
    return UserModel.fromPartial({
      blocked: this.blocked,
      createdAt: this.createdAt,
      deleted: this.deleted,
      email: this.email,
      id: this.id,
      lang: this.lang,
      path: this.path,
      role: this.role,
      verified: this.verified,
      alias: this.alias,
      firstName: this.firstName,
      middleName: this.middleName,
      parentId: this.parentId,
      phone: this.phone,
      surname: this.surname,
      organizationId: this.organizationId,
    });
  }

  fullName(firstName: boolean = true, middleName: boolean = false, lastName: boolean = true): string {
    if (!this.surname && !this.firstName && !this.middleName) {
      return this.email;
    }

    const sb = new StringBuilder();
    if (firstName) {
      sb.write(this.firstName || '-')
      sb.write(' ')
    }

    if (middleName) {
      sb.write(this.middleName || '-')
      sb.write(' ')
    }

    if (lastName) {
      sb.write(this.surname || '-')
    }

    return sb.toString().trim();
  }
}

export class ShortUser<ProfileModel = {}> extends User<ProfileModel> {
  constructor(user: ShortUserModel, profileModel?: ProfileModel) {
    super({
      id: user.id,
      email: user.email,
      blocked: user.blocked,
      deleted: user.deleted,
      phone: user.phone,
      firstName: user.firstName,
      middleName: user.middleName,
      surname: user.surname,
      parentId: user.parentId,
      alias: user.alias,
      lang: user.lang,
    } as UserModel, profileModel);
  }
}

export class ShorterUser<ProfileModel = {}> extends ShortUser<ProfileModel> {
  constructor(user: UserShorterModel, profileModel?: ProfileModel) {
    super({
      id: user.id,
      blocked: user.blocked,
      firstName: user.firstName,
      middleName: user.middleName,
      surname: user.surname,
      parentId: user.parentId,
      alias: user.alias,
      lang: user.lang,
    } as ShortUserModel, profileModel);
  }
}

export default User;
