import { inject, injectable } from 'inversify';
import { ApiClient } from '@modules/grpcClient';
import {
  GetByStringReq,
  UserCountDesc, UserCountReq, UserCreateByAdminDesc, UserFilterModel,
  UserGetByAliasDesc,
  UserGetDesc, UserGetFullByIdDesc, UserListDesc, UserListReq,
  UserUpdateByAdminDesc,
  UserUpdateDesc,
  UserVerifyByTokenDesc, VerifyByTokenReq,
} from '@modules/auth/proto/user';
import { UserModel } from '@modules/auth/proto/authCommon';
import { Id64Request } from '@modules/commonTypes';
import { ListOptions } from '@modules/paginatior';
import { map, Observable } from 'rxjs';
import { IAppConfig } from '@modules/app/types';
import { AppConfigDiKey } from '@modules/app/constants';
import User from '@modules/auth/user/user';
import StreamHandler from '@modules/Stream/service/StreamHandler';

export interface IUserService<UserType extends User = User> {
  getByAlias(alias: string): Observable<UserType>
  update(value: UserType): Observable<string>
  verifyEmail: (token: string, app: string) => Observable<boolean>,
  getById: (id: number) => Observable<UserType | undefined>,
}

export interface IAdminUserService<UserType extends User = User> extends IUserService<UserType> {
  updateByAdmin: (req: UserType) => Observable<void>,
  create: (req: UserType) => Observable<void>,
  getFullById: (id: number) => Observable<UserType | undefined>,
}

@injectable()
class UserService<UserType extends User = User> extends StreamHandler<UserType> implements IUserService<UserType> {
  public static diKey = Symbol.for('UserServiceKey');
  protected api: ApiClient;
  protected appConfig: IAppConfig;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
    @inject(AppConfigDiKey) appConfig: IAppConfig,
  ) {
    super();
    this.api = api;
    this.appConfig = appConfig;
  }

  getByAlias(alias: string): Observable<UserType> {
    const req = GetByStringReq.fromPartial({
      appSlug: this.appConfig.app,
      value: alias,
    })
    return this.api.unary$(
      UserGetByAliasDesc,
      {
        obj: req,
        coder: GetByStringReq,
      },
      { host: this.appConfig.authUrlServer },
    );
  }

  update(value: UserType): Observable<string> {
    const req = UserModel.fromPartial(value)
    return this.api.unary$(
      UserUpdateDesc,
      {
        obj: req,
        coder: UserModel,
      },
      { host: this.appConfig.authUrlServer },
    ).pipe(map(i => i.value));
  }

  verifyEmail(token: string, app: string): Observable<boolean> {
    return this.api.unary$(
      UserVerifyByTokenDesc,
      {
        obj: VerifyByTokenReq.fromPartial({ token, app }),
        coder: VerifyByTokenReq,
      },
      { host: this.appConfig.authUrlServer },
    );
  }

  getById(id: number): Observable<UserType | undefined> {
    return this.api.unary$(
      UserGetDesc,
      {
        obj: Id64Request.fromPartial({ id }),
        coder: Id64Request,
      },
      { host: this.appConfig.authUrlServer },
    );
  }
}

@injectable()
export class AdminUserService<UserType extends User = User> extends UserService<UserType> implements IAdminUserService<UserType> {
  public static diKey = Symbol.for('AdminUserServiceKey');

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
    @inject(AppConfigDiKey) appConfig: IAppConfig,
  ) {
    super(api, appConfig);
    this.add('default', this.streamDefault);
  }

  private streamDefault = {
    stream: (opts: ListOptions, method: string, filter?: UserFilterModel): Observable<UserType> => {
      return this.api.stream$(
        UserListDesc,
        { obj: UserListReq.fromPartial({
            opts,
            filter,
          }), coder: UserListReq },
        { host: this.appConfig.authUrlServer },
      ).pipe();
    },
    loadCount: (method: string, filter?: UserFilterModel): Observable<number> => {
      return this.api.unary$(
        UserCountDesc,
        {
          obj: UserCountReq.fromPartial({ filter }),
          coder: UserCountReq,
        },
        { host: this.appConfig.authUrlServer },
      ).pipe(map(res => res.value));
    }
  }

  updateByAdmin(req: UserType): Observable<void> {
    return this.api.unary$(
      UserUpdateByAdminDesc,
      {
        obj: UserModel.fromPartial(req.object),
        coder: UserModel,
      },
      { host: this.appConfig.authUrlServer },
    );
  }

  create(req: UserType): Observable<void> {
    return this.api.unary$(
      UserCreateByAdminDesc,
      {
        obj: req,
        coder: UserModel,
      }
    );
  }

  list(opts: ListOptions, method: string, filter?: UserFilterModel): Observable<UserType> {
    return this.streamDefault.stream(opts, method, filter);
  }

  getFullById(id: number): Observable<UserType | undefined> {
    return this.api.unary$(
      UserGetFullByIdDesc,
      {
        obj: Id64Request.fromPartial({ id }),
        coder: Id64Request,
      },
      { host: this.appConfig.authUrlServer },
    );
  }
}

export default UserService;