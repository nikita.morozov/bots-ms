/* eslint-disable */
import { messageTypeRegistry } from "./typeRegistry";
import Long from "long";
import { grpc } from "@improbable-eng/grpc-web";
import _m0 from "protobufjs/minimal";
import { SignUpModel } from "./signup";
import { Observable } from "rxjs";
import { Id64Request, Int64Value } from "./commonTypes";
import { ListOptions } from "./paginatior";
import { Empty } from "./google/protobuf/empty";
import { BrowserHeaders } from "browser-headers";
import { share } from "rxjs/operators";

export const protobufPackage = "authMs";

export interface SignUpItemModel {
  $type: "authMs.SignUpItemModel";
  id: number;
  title: string;
  visible: boolean;
  required: boolean;
  prompted: boolean;
  rule: string;
  applicationId: number;
  signupId: number;
  signup?: SignUpModel;
}

const baseSignUpItemModel: object = {
  $type: "authMs.SignUpItemModel",
  id: 0,
  title: "",
  visible: false,
  required: false,
  prompted: false,
  rule: "",
  applicationId: 0,
  signupId: 0,
};

export const SignUpItemModel = {
  $type: "authMs.SignUpItemModel" as const,

  encode(
    message: SignUpItemModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.title !== "") {
      writer.uint32(18).string(message.title);
    }
    if (message.visible === true) {
      writer.uint32(24).bool(message.visible);
    }
    if (message.required === true) {
      writer.uint32(32).bool(message.required);
    }
    if (message.prompted === true) {
      writer.uint32(40).bool(message.prompted);
    }
    if (message.rule !== "") {
      writer.uint32(50).string(message.rule);
    }
    if (message.applicationId !== 0) {
      writer.uint32(56).uint32(message.applicationId);
    }
    if (message.signupId !== 0) {
      writer.uint32(64).uint32(message.signupId);
    }
    if (message.signup !== undefined) {
      SignUpModel.encode(message.signup, writer.uint32(74).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SignUpItemModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSignUpItemModel } as SignUpItemModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.title = reader.string();
          break;
        case 3:
          message.visible = reader.bool();
          break;
        case 4:
          message.required = reader.bool();
          break;
        case 5:
          message.prompted = reader.bool();
          break;
        case 6:
          message.rule = reader.string();
          break;
        case 7:
          message.applicationId = reader.uint32();
          break;
        case 8:
          message.signupId = reader.uint32();
          break;
        case 9:
          message.signup = SignUpModel.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SignUpItemModel {
    const message = { ...baseSignUpItemModel } as SignUpItemModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.title =
      object.title !== undefined && object.title !== null
        ? String(object.title)
        : "";
    message.visible =
      object.visible !== undefined && object.visible !== null
        ? Boolean(object.visible)
        : false;
    message.required =
      object.required !== undefined && object.required !== null
        ? Boolean(object.required)
        : false;
    message.prompted =
      object.prompted !== undefined && object.prompted !== null
        ? Boolean(object.prompted)
        : false;
    message.rule =
      object.rule !== undefined && object.rule !== null
        ? String(object.rule)
        : "";
    message.applicationId =
      object.applicationId !== undefined && object.applicationId !== null
        ? Number(object.applicationId)
        : 0;
    message.signupId =
      object.signupId !== undefined && object.signupId !== null
        ? Number(object.signupId)
        : 0;
    message.signup =
      object.signup !== undefined && object.signup !== null
        ? SignUpModel.fromJSON(object.signup)
        : undefined;
    return message;
  },

  toJSON(message: SignUpItemModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.title !== undefined && (obj.title = message.title);
    message.visible !== undefined && (obj.visible = message.visible);
    message.required !== undefined && (obj.required = message.required);
    message.prompted !== undefined && (obj.prompted = message.prompted);
    message.rule !== undefined && (obj.rule = message.rule);
    message.applicationId !== undefined &&
      (obj.applicationId = message.applicationId);
    message.signupId !== undefined && (obj.signupId = message.signupId);
    message.signup !== undefined &&
      (obj.signup = message.signup
        ? SignUpModel.toJSON(message.signup)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<SignUpItemModel>): SignUpItemModel {
    const message = { ...baseSignUpItemModel } as SignUpItemModel;
    message.id = object.id ?? 0;
    message.title = object.title ?? "";
    message.visible = object.visible ?? false;
    message.required = object.required ?? false;
    message.prompted = object.prompted ?? false;
    message.rule = object.rule ?? "";
    message.applicationId = object.applicationId ?? 0;
    message.signupId = object.signupId ?? 0;
    message.signup =
      object.signup !== undefined && object.signup !== null
        ? SignUpModel.fromPartial(object.signup)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(SignUpItemModel.$type, SignUpItemModel);

/** Represents SignUpItem methods. */
export interface SignUpItem {
  /** / Create SignUpItem */
  Create(
    request: DeepPartial<SignUpItemModel>,
    metadata?: grpc.Metadata
  ): Promise<SignUpItemModel>;
  /** / Update SignUpItem */
  Update(
    request: DeepPartial<SignUpItemModel>,
    metadata?: grpc.Metadata
  ): Promise<SignUpItemModel>;
  /** / GetById SignUpItem */
  GetById(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<SignUpItemModel>;
  /** / List SignUpItem */
  List(
    request: DeepPartial<ListOptions>,
    metadata?: grpc.Metadata
  ): Observable<SignUpItemModel>;
  /** / Count SignUpItem */
  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value>;
  /** / Remove SignUpItem */
  Remove(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
}

export class SignUpItemClientImpl implements SignUpItem {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Create = this.Create.bind(this);
    this.Update = this.Update.bind(this);
    this.GetById = this.GetById.bind(this);
    this.List = this.List.bind(this);
    this.Count = this.Count.bind(this);
    this.Remove = this.Remove.bind(this);
  }

  Create(
    request: DeepPartial<SignUpItemModel>,
    metadata?: grpc.Metadata
  ): Promise<SignUpItemModel> {
    return this.rpc.unary(
      SignUpItemCreateDesc,
      SignUpItemModel.fromPartial(request),
      metadata
    );
  }

  Update(
    request: DeepPartial<SignUpItemModel>,
    metadata?: grpc.Metadata
  ): Promise<SignUpItemModel> {
    return this.rpc.unary(
      SignUpItemUpdateDesc,
      SignUpItemModel.fromPartial(request),
      metadata
    );
  }

  GetById(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<SignUpItemModel> {
    return this.rpc.unary(
      SignUpItemGetByIdDesc,
      Id64Request.fromPartial(request),
      metadata
    );
  }

  List(
    request: DeepPartial<ListOptions>,
    metadata?: grpc.Metadata
  ): Observable<SignUpItemModel> {
    return this.rpc.invoke(
      SignUpItemListDesc,
      ListOptions.fromPartial(request),
      metadata
    );
  }

  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value> {
    return this.rpc.unary(
      SignUpItemCountDesc,
      Empty.fromPartial(request),
      metadata
    );
  }

  Remove(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      SignUpItemRemoveDesc,
      Id64Request.fromPartial(request),
      metadata
    );
  }
}

export const SignUpItemDesc = {
  serviceName: "authMs.SignUpItem",
};

export const SignUpItemCreateDesc: UnaryMethodDefinitionish = {
  methodName: "Create",
  service: SignUpItemDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return SignUpItemModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...SignUpItemModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const SignUpItemUpdateDesc: UnaryMethodDefinitionish = {
  methodName: "Update",
  service: SignUpItemDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return SignUpItemModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...SignUpItemModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const SignUpItemGetByIdDesc: UnaryMethodDefinitionish = {
  methodName: "GetById",
  service: SignUpItemDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Id64Request.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...SignUpItemModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const SignUpItemListDesc: UnaryMethodDefinitionish = {
  methodName: "List",
  service: SignUpItemDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return ListOptions.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...SignUpItemModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const SignUpItemCountDesc: UnaryMethodDefinitionish = {
  methodName: "Count",
  service: SignUpItemDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Int64Value.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const SignUpItemRemoveDesc: UnaryMethodDefinitionish = {
  methodName: "Remove",
  service: SignUpItemDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Id64Request.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR
  extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
    }
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata,
        transport: this.options.transport,
        debug: this.options.debug,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message);
          } else {
            const err = new Error(response.statusMessage) as any;
            err.code = response.status;
            err.metadata = response.trailers;
            reject(err);
          }
        },
      });
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any> {
    // Status Response Codes (https://developers.google.com/maps-booking/reference/grpc-api/status_codes)
    const upStreamCodes = [2, 4, 8, 9, 10, 13, 14, 15];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          transport: this.options.streamingTransport || this.options.transport,
          metadata: maybeCombinedMetadata,
          debug: this.options.debug,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              observer.error(new Error(`Error ${code} ${message}`));
            }
          },
        });
        observer.add(() => client.close());
      };
      upStream();
    }).pipe(share());
  }
}

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
