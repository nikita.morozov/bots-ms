/* eslint-disable */
import { messageTypeRegistry } from "./typeRegistry";
import Long from "long";
import { grpc } from "@improbable-eng/grpc-web";
import _m0 from "protobufjs/minimal";
import { ProviderModel } from "./provider";
import { Observable } from "rxjs";
import { Id64Request, Int64Value } from "./commonTypes";
import { ListOptions } from "./paginatior";
import { Empty } from "./google/protobuf/empty";
import { BrowserHeaders } from "browser-headers";
import { share } from "rxjs/operators";

export const protobufPackage = "authMs";

export interface ProviderItemModel {
  $type: "authMs.ProviderItemModel";
  id: number;
  canSignUp: boolean;
  canSignIn: boolean;
  canUnlink: boolean;
  prompted: boolean;
  alertType: string;
  providerId: number;
  provider?: ProviderModel;
  applicationId: number;
}

const baseProviderItemModel: object = {
  $type: "authMs.ProviderItemModel",
  id: 0,
  canSignUp: false,
  canSignIn: false,
  canUnlink: false,
  prompted: false,
  alertType: "",
  providerId: 0,
  applicationId: 0,
};

export const ProviderItemModel = {
  $type: "authMs.ProviderItemModel" as const,

  encode(
    message: ProviderItemModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.canSignUp === true) {
      writer.uint32(16).bool(message.canSignUp);
    }
    if (message.canSignIn === true) {
      writer.uint32(24).bool(message.canSignIn);
    }
    if (message.canUnlink === true) {
      writer.uint32(32).bool(message.canUnlink);
    }
    if (message.prompted === true) {
      writer.uint32(40).bool(message.prompted);
    }
    if (message.alertType !== "") {
      writer.uint32(50).string(message.alertType);
    }
    if (message.providerId !== 0) {
      writer.uint32(56).uint32(message.providerId);
    }
    if (message.provider !== undefined) {
      ProviderModel.encode(message.provider, writer.uint32(66).fork()).ldelim();
    }
    if (message.applicationId !== 0) {
      writer.uint32(72).uint32(message.applicationId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ProviderItemModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseProviderItemModel } as ProviderItemModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.canSignUp = reader.bool();
          break;
        case 3:
          message.canSignIn = reader.bool();
          break;
        case 4:
          message.canUnlink = reader.bool();
          break;
        case 5:
          message.prompted = reader.bool();
          break;
        case 6:
          message.alertType = reader.string();
          break;
        case 7:
          message.providerId = reader.uint32();
          break;
        case 8:
          message.provider = ProviderModel.decode(reader, reader.uint32());
          break;
        case 9:
          message.applicationId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ProviderItemModel {
    const message = { ...baseProviderItemModel } as ProviderItemModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.canSignUp =
      object.canSignUp !== undefined && object.canSignUp !== null
        ? Boolean(object.canSignUp)
        : false;
    message.canSignIn =
      object.canSignIn !== undefined && object.canSignIn !== null
        ? Boolean(object.canSignIn)
        : false;
    message.canUnlink =
      object.canUnlink !== undefined && object.canUnlink !== null
        ? Boolean(object.canUnlink)
        : false;
    message.prompted =
      object.prompted !== undefined && object.prompted !== null
        ? Boolean(object.prompted)
        : false;
    message.alertType =
      object.alertType !== undefined && object.alertType !== null
        ? String(object.alertType)
        : "";
    message.providerId =
      object.providerId !== undefined && object.providerId !== null
        ? Number(object.providerId)
        : 0;
    message.provider =
      object.provider !== undefined && object.provider !== null
        ? ProviderModel.fromJSON(object.provider)
        : undefined;
    message.applicationId =
      object.applicationId !== undefined && object.applicationId !== null
        ? Number(object.applicationId)
        : 0;
    return message;
  },

  toJSON(message: ProviderItemModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.canSignUp !== undefined && (obj.canSignUp = message.canSignUp);
    message.canSignIn !== undefined && (obj.canSignIn = message.canSignIn);
    message.canUnlink !== undefined && (obj.canUnlink = message.canUnlink);
    message.prompted !== undefined && (obj.prompted = message.prompted);
    message.alertType !== undefined && (obj.alertType = message.alertType);
    message.providerId !== undefined && (obj.providerId = message.providerId);
    message.provider !== undefined &&
      (obj.provider = message.provider
        ? ProviderModel.toJSON(message.provider)
        : undefined);
    message.applicationId !== undefined &&
      (obj.applicationId = message.applicationId);
    return obj;
  },

  fromPartial(object: DeepPartial<ProviderItemModel>): ProviderItemModel {
    const message = { ...baseProviderItemModel } as ProviderItemModel;
    message.id = object.id ?? 0;
    message.canSignUp = object.canSignUp ?? false;
    message.canSignIn = object.canSignIn ?? false;
    message.canUnlink = object.canUnlink ?? false;
    message.prompted = object.prompted ?? false;
    message.alertType = object.alertType ?? "";
    message.providerId = object.providerId ?? 0;
    message.provider =
      object.provider !== undefined && object.provider !== null
        ? ProviderModel.fromPartial(object.provider)
        : undefined;
    message.applicationId = object.applicationId ?? 0;
    return message;
  },
};

messageTypeRegistry.set(ProviderItemModel.$type, ProviderItemModel);

/** Represents provider methods. */
export interface ProviderItem {
  /** / Create provider */
  Create(
    request: DeepPartial<ProviderItemModel>,
    metadata?: grpc.Metadata
  ): Promise<ProviderItemModel>;
  /** / Update provider */
  Update(
    request: DeepPartial<ProviderItemModel>,
    metadata?: grpc.Metadata
  ): Promise<ProviderItemModel>;
  /** / GetById provider */
  GetById(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<ProviderItemModel>;
  /** / List provider */
  List(
    request: DeepPartial<ListOptions>,
    metadata?: grpc.Metadata
  ): Observable<ProviderItemModel>;
  /** / Count provider */
  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value>;
  /** / Remove provider */
  Remove(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
}

export class ProviderItemClientImpl implements ProviderItem {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Create = this.Create.bind(this);
    this.Update = this.Update.bind(this);
    this.GetById = this.GetById.bind(this);
    this.List = this.List.bind(this);
    this.Count = this.Count.bind(this);
    this.Remove = this.Remove.bind(this);
  }

  Create(
    request: DeepPartial<ProviderItemModel>,
    metadata?: grpc.Metadata
  ): Promise<ProviderItemModel> {
    return this.rpc.unary(
      ProviderItemCreateDesc,
      ProviderItemModel.fromPartial(request),
      metadata
    );
  }

  Update(
    request: DeepPartial<ProviderItemModel>,
    metadata?: grpc.Metadata
  ): Promise<ProviderItemModel> {
    return this.rpc.unary(
      ProviderItemUpdateDesc,
      ProviderItemModel.fromPartial(request),
      metadata
    );
  }

  GetById(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<ProviderItemModel> {
    return this.rpc.unary(
      ProviderItemGetByIdDesc,
      Id64Request.fromPartial(request),
      metadata
    );
  }

  List(
    request: DeepPartial<ListOptions>,
    metadata?: grpc.Metadata
  ): Observable<ProviderItemModel> {
    return this.rpc.invoke(
      ProviderItemListDesc,
      ListOptions.fromPartial(request),
      metadata
    );
  }

  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value> {
    return this.rpc.unary(
      ProviderItemCountDesc,
      Empty.fromPartial(request),
      metadata
    );
  }

  Remove(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      ProviderItemRemoveDesc,
      Id64Request.fromPartial(request),
      metadata
    );
  }
}

export const ProviderItemDesc = {
  serviceName: "authMs.ProviderItem",
};

export const ProviderItemCreateDesc: UnaryMethodDefinitionish = {
  methodName: "Create",
  service: ProviderItemDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ProviderItemModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ProviderItemModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ProviderItemUpdateDesc: UnaryMethodDefinitionish = {
  methodName: "Update",
  service: ProviderItemDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ProviderItemModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ProviderItemModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ProviderItemGetByIdDesc: UnaryMethodDefinitionish = {
  methodName: "GetById",
  service: ProviderItemDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Id64Request.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ProviderItemModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ProviderItemListDesc: UnaryMethodDefinitionish = {
  methodName: "List",
  service: ProviderItemDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return ListOptions.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ProviderItemModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ProviderItemCountDesc: UnaryMethodDefinitionish = {
  methodName: "Count",
  service: ProviderItemDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Int64Value.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ProviderItemRemoveDesc: UnaryMethodDefinitionish = {
  methodName: "Remove",
  service: ProviderItemDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Id64Request.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR
  extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
    }
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata,
        transport: this.options.transport,
        debug: this.options.debug,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message);
          } else {
            const err = new Error(response.statusMessage) as any;
            err.code = response.status;
            err.metadata = response.trailers;
            reject(err);
          }
        },
      });
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any> {
    // Status Response Codes (https://developers.google.com/maps-booking/reference/grpc-api/status_codes)
    const upStreamCodes = [2, 4, 8, 9, 10, 13, 14, 15];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          transport: this.options.streamingTransport || this.options.transport,
          metadata: maybeCombinedMetadata,
          debug: this.options.debug,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              observer.error(new Error(`Error ${code} ${message}`));
            }
          },
        });
        observer.add(() => client.close());
      };
      upStream();
    }).pipe(share());
  }
}

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
