/* eslint-disable */
import { messageTypeRegistry } from "./typeRegistry";
import Long from "long";
import { grpc } from "@improbable-eng/grpc-web";
import _m0 from "protobufjs/minimal";
import { Observable } from "rxjs";
import { Id64Request, Int64Value } from "./commonTypes";
import { ListOptions } from "./paginatior";
import { Empty } from "./google/protobuf/empty";
import { BrowserHeaders } from "browser-headers";
import { share } from "rxjs/operators";

export const protobufPackage = "authMs";

export interface SignUpModel {
  $type: "authMs.SignUpModel";
  id: number;
  title: string;
  name: string;
}

const baseSignUpModel: object = {
  $type: "authMs.SignUpModel",
  id: 0,
  title: "",
  name: "",
};

export const SignUpModel = {
  $type: "authMs.SignUpModel" as const,

  encode(
    message: SignUpModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.title !== "") {
      writer.uint32(18).string(message.title);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SignUpModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSignUpModel } as SignUpModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.title = reader.string();
          break;
        case 3:
          message.name = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SignUpModel {
    const message = { ...baseSignUpModel } as SignUpModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.title =
      object.title !== undefined && object.title !== null
        ? String(object.title)
        : "";
    message.name =
      object.name !== undefined && object.name !== null
        ? String(object.name)
        : "";
    return message;
  },

  toJSON(message: SignUpModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.title !== undefined && (obj.title = message.title);
    message.name !== undefined && (obj.name = message.name);
    return obj;
  },

  fromPartial(object: DeepPartial<SignUpModel>): SignUpModel {
    const message = { ...baseSignUpModel } as SignUpModel;
    message.id = object.id ?? 0;
    message.title = object.title ?? "";
    message.name = object.name ?? "";
    return message;
  },
};

messageTypeRegistry.set(SignUpModel.$type, SignUpModel);

/** Represents SignUpItem methods. */
export interface SignUp {
  /** / Create SignUpItem */
  Create(
    request: DeepPartial<SignUpModel>,
    metadata?: grpc.Metadata
  ): Promise<SignUpModel>;
  /** / Update SignUpItem */
  Update(
    request: DeepPartial<SignUpModel>,
    metadata?: grpc.Metadata
  ): Promise<SignUpModel>;
  /** / GetById SignUpItem */
  GetById(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<SignUpModel>;
  /** / List SignUpItem */
  List(
    request: DeepPartial<ListOptions>,
    metadata?: grpc.Metadata
  ): Observable<SignUpModel>;
  /** / Count SignUpItem */
  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value>;
  /** / Remove SignUpItem */
  Remove(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
}

export class SignUpClientImpl implements SignUp {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Create = this.Create.bind(this);
    this.Update = this.Update.bind(this);
    this.GetById = this.GetById.bind(this);
    this.List = this.List.bind(this);
    this.Count = this.Count.bind(this);
    this.Remove = this.Remove.bind(this);
  }

  Create(
    request: DeepPartial<SignUpModel>,
    metadata?: grpc.Metadata
  ): Promise<SignUpModel> {
    return this.rpc.unary(
      SignUpCreateDesc,
      SignUpModel.fromPartial(request),
      metadata
    );
  }

  Update(
    request: DeepPartial<SignUpModel>,
    metadata?: grpc.Metadata
  ): Promise<SignUpModel> {
    return this.rpc.unary(
      SignUpUpdateDesc,
      SignUpModel.fromPartial(request),
      metadata
    );
  }

  GetById(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<SignUpModel> {
    return this.rpc.unary(
      SignUpGetByIdDesc,
      Id64Request.fromPartial(request),
      metadata
    );
  }

  List(
    request: DeepPartial<ListOptions>,
    metadata?: grpc.Metadata
  ): Observable<SignUpModel> {
    return this.rpc.invoke(
      SignUpListDesc,
      ListOptions.fromPartial(request),
      metadata
    );
  }

  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<Int64Value> {
    return this.rpc.unary(
      SignUpCountDesc,
      Empty.fromPartial(request),
      metadata
    );
  }

  Remove(
    request: DeepPartial<Id64Request>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      SignUpRemoveDesc,
      Id64Request.fromPartial(request),
      metadata
    );
  }
}

export const SignUpDesc = {
  serviceName: "authMs.SignUp",
};

export const SignUpCreateDesc: UnaryMethodDefinitionish = {
  methodName: "Create",
  service: SignUpDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return SignUpModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...SignUpModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const SignUpUpdateDesc: UnaryMethodDefinitionish = {
  methodName: "Update",
  service: SignUpDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return SignUpModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...SignUpModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const SignUpGetByIdDesc: UnaryMethodDefinitionish = {
  methodName: "GetById",
  service: SignUpDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Id64Request.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...SignUpModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const SignUpListDesc: UnaryMethodDefinitionish = {
  methodName: "List",
  service: SignUpDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return ListOptions.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...SignUpModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const SignUpCountDesc: UnaryMethodDefinitionish = {
  methodName: "Count",
  service: SignUpDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Int64Value.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const SignUpRemoveDesc: UnaryMethodDefinitionish = {
  methodName: "Remove",
  service: SignUpDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Id64Request.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR
  extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
    }
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata,
        transport: this.options.transport,
        debug: this.options.debug,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message);
          } else {
            const err = new Error(response.statusMessage) as any;
            err.code = response.status;
            err.metadata = response.trailers;
            reject(err);
          }
        },
      });
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any> {
    // Status Response Codes (https://developers.google.com/maps-booking/reference/grpc-api/status_codes)
    const upStreamCodes = [2, 4, 8, 9, 10, 13, 14, 15];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          transport: this.options.streamingTransport || this.options.transport,
          metadata: maybeCombinedMetadata,
          debug: this.options.debug,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              observer.error(new Error(`Error ${code} ${message}`));
            }
          },
        });
        observer.add(() => client.close());
      };
      upStream();
    }).pipe(share());
  }
}

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
