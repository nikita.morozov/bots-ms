import { inject, injectable } from 'inversify';
import { ApiClient } from '@modules/grpcClient';
import { Id64Request, StringRequest } from '@modules/shared/commonTypes';
import {
  ApplicationCountDesc,
  ApplicationCreateDesc, ApplicationGetByClientIdDesc,
  ApplicationGetByIdDesc, ApplicationListDesc,
  ApplicationModel, ApplicationRemoveDesc, ApplicationUpdateDesc, DeepPartial,
} from './proto/application';
import { Empty } from '@modules/shared/empty';
import { ListOptions } from '@modules/paginatior';
import { AppConfigDiKey } from '@modules/app/constants';
import { IAppConfig } from '@modules/app/types';
import { map, Observable } from 'rxjs';
import StreamHandler from '@modules/Stream/service/StreamHandler';
import CacheRepository, { ICacheRepoHandler } from '@modules/cacheRepository';

export interface IApplicationService {
  create(request: ApplicationModel): Observable<ApplicationModel>,
  update(request: ApplicationModel): Observable<ApplicationModel>,
  getById(request: Id64Request): Observable<ApplicationModel>,
  getByClientId(request: StringRequest): Observable<ApplicationModel>,
  remove(request: Id64Request): Observable<void>,
}

@injectable()
class ApplicationService extends StreamHandler<ApplicationModel> implements IApplicationService, ICacheRepoHandler<ApplicationModel> {
  public static diKey = Symbol.for('ApplicationServiceKey');
  private api: ApiClient;
  private appConfig: IAppConfig;
  private cacheRepo: ICacheRepoHandler<ApplicationModel>;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
    @inject(AppConfigDiKey) appConfig: IAppConfig,
  ) {
    super();
    this.add('default', this.streamDefault)
    this.api = api;
    this.appConfig = appConfig;
    this.cacheRepo = new CacheRepository();
  }

  private streamDefault = {
    stream: (opts: ListOptions): Observable<ApplicationModel> => {
      return this.list(opts);
    },
    loadCount: (): Observable<number> => {
      return this.count();
    },
  }

  count(): Observable<number> {
    return this.api.unary$(
      ApplicationCountDesc,
      {
        obj: {},
        coder: Empty,
      },
      { host: this.appConfig.authUrlServer }
    ).pipe(map(res => res.value as number));
  }

  create(request: ApplicationModel): Observable<ApplicationModel> {
    return this.api.unary$(
      ApplicationCreateDesc,
      {
        obj: request,
        coder: ApplicationModel,
      },
      { host: this.appConfig.authUrlServer }
    );
  }

  getById(request: Id64Request): Observable<ApplicationModel> {
    return this.api.unary$(
      ApplicationGetByIdDesc,
      {
        obj: request,
        coder: Id64Request,
      },
      { host: this.appConfig.authUrlServer }
    );
  }

  list(request: ListOptions): Observable<ApplicationModel> {
    return this.api.stream$(
      ApplicationListDesc,
      { obj: request, coder: ListOptions },
      { host: this.appConfig.authUrlServer, noCache: true },
    ).pipe();
  }

  update(request: ApplicationModel): Observable<ApplicationModel> {
    return this.api.unary$(
      ApplicationUpdateDesc,
      {
        obj: request,
        coder: ApplicationModel,
      },
      { host: this.appConfig.authUrlServer }
    );
  }

  getByClientId(clientId: StringRequest): Observable<ApplicationModel> {
    return this.api.unary$(
      ApplicationGetByClientIdDesc,
      {
        obj: clientId,
        coder: StringRequest,
      },
      { host: this.appConfig.authUrlServer }
    );
  }

  addCache(key: string, value: ApplicationModel): void {
    this.cacheRepo.addCache(key, value);
  }

  getCache(key: string): ApplicationModel {
    return this.cacheRepo.getCache(key);
  }

  remove(request: Id64Request): Observable<void> {
    return this.api.unary$(
      ApplicationRemoveDesc,
      {
        obj: request,
        coder: Id64Request,
      },
      { host: this.appConfig.authUrlServer }
    );
  }
}

export default ApplicationService;