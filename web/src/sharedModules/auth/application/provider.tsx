import { FunctionComponent, PropsWithChildren } from 'react';
import applicationContainer from './container';
import DiProvider from '@modules/tools/di-provider';

export const ApplicationProvider: FunctionComponent<PropsWithChildren<{}>> = ({ children }) => (
  <DiProvider child={applicationContainer}>
    {children}
  </DiProvider>
);