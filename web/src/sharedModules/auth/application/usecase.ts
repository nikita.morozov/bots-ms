import { inject, injectable } from 'inversify';
import { Id64Request, StringRequest } from '@modules/commonTypes';
import { ApplicationModel } from './proto/application';
import { ListOptions } from './proto/paginatior';
import ApplicationService  from './service';
import { Observable, Subscription } from 'rxjs';
import { IBaseListUsecase, IPageService } from '@modules/Stream/types';
import { PageService } from '@modules/Stream/service/ListService';
import ListMV from '@modules/Stream/model/ListMV';
import { ICacheRepoHandler } from '@modules/cacheRepository';

export interface IApplicationUsecase {
  create(request: ApplicationModel): Observable<ApplicationModel>,
  update(request: ApplicationModel): Observable<ApplicationModel>,
  getById(request: Id64Request): Observable<ApplicationModel>,
  getByClientId(request: StringRequest): Observable<ApplicationModel>,
  remove(request: Id64Request): Observable<void>,
  loadItems(model: ListMV<ApplicationModel>, opts: ListOptions, method?: string, query?: object): Observable<ApplicationModel>,
  loadTotalCount(model: ListMV<ApplicationModel>, method?: string, query?: object): Observable<number>,
  getByClientId(request: StringRequest): Observable<ApplicationModel>,
}

@injectable()
class ApplicationUsecase implements IApplicationUsecase, IBaseListUsecase<ApplicationModel>, ICacheRepoHandler<ApplicationModel> {
  public static diKey = Symbol.for('ApplicationUsecaseDiKey');

  private applicationService: ApplicationService;
  private pageService: IPageService<ApplicationModel>

  constructor(
    @inject(ApplicationService.diKey) applicationService: ApplicationService,
  ) {
    this.applicationService = applicationService;
    this.pageService = new PageService(this.applicationService);
  }

  addCache(key: string, value: ApplicationModel): void {
    this.applicationService.addCache(key, value);
  }

  getCache(key: string): ApplicationModel {
    return this.applicationService.getCache(key) || {};
  }

  loadItems(model: ListMV<ApplicationModel>, opts: ListOptions, method?: string, query?: object): [Observable<ApplicationModel>, Subscription] {
    return this.pageService.loadPage(model, opts);
  }

  loadTotalCount(model: ListMV<ApplicationModel>, method?: string, query?: object): [Observable<number>, Subscription] {
    return this.pageService.loadTotalCount(model);
  }

  create(request: ApplicationModel): Observable<ApplicationModel> {
    return this.applicationService.create(request);
  }

  getById(request: Id64Request): Observable<ApplicationModel> {
    return this.applicationService.getById(request);
  }

  update(request: ApplicationModel): Observable<ApplicationModel> {
    return this.applicationService.update(request);
  }

  getByClientId(request: StringRequest): Observable<ApplicationModel> {
    return this.applicationService.getByClientId(request);
  }

  remove(request: Id64Request): Observable<void> {
    return this.applicationService.remove(request);
  }
}

export default ApplicationUsecase;