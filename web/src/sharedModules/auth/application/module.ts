import { ContainerModule, interfaces } from 'inversify';
import ApplicationService, { IApplicationService } from './service';
import ApplicationUsecase, { IApplicationUsecase } from './usecase';

export const ApplicationModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<IApplicationService>(ApplicationService.diKey).to(ApplicationService);

  bind<IApplicationUsecase>(ApplicationUsecase.diKey).to(ApplicationUsecase).inSingletonScope();
});