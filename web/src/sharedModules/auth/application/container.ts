import { Container, interfaces } from 'inversify';
import { ApplicationModule } from './module';

const applicationContainer: interfaces.Container = new Container();

applicationContainer.load(ApplicationModule);

export default applicationContainer;