import { ApiClient } from '@modules/grpcClient';
import { CheckReq, ClearReq, SessionCheckDesc, SessionClearDesc } from '@modules/auth/proto/session';
import { IAppConfig } from '@modules/app/types';
import SessionCookieStorage, { ISessionCookieManager } from '@modules/auth/session/cookie';
import { deadlineExceeded, unauthorizedCode } from '@modules/constants/errors';
import { IAccessCheckResult, IProfileRes } from '@modules/auth/session/types';
import User from '@modules/auth/user/user';
import { JwtParser } from '@modules/tools/jwtParser';
import { inject, injectable } from 'inversify';
import { AppConfigDiKey } from '@modules/app/constants';
import { BehaviorSubject, Observable } from 'rxjs';

export interface ISessionService<UserType extends User = User> {
  check: (path: string, app?: string) => Promise<IAccessCheckResult>,
  clear: (app?: string) => Promise<void>,
  update: (token: string) => void,
  getCurrentUser(): UserType | undefined
  getCurrentUser$(): Observable<UserType | undefined>
}

@injectable()
class SessionService<UserType extends User = User> implements ISessionService<UserType> {
  public static diKey = Symbol.for('SessionServiceKey');

  private api: ApiClient;
  private storage: ISessionCookieManager;
  private readonly cfg: IAppConfig;
  private currentUser$: BehaviorSubject<UserType | undefined>

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
    @inject(SessionCookieStorage.diKey) storage: ISessionCookieManager,
    @inject(AppConfigDiKey) cfg: IAppConfig,
  ) {
    this.storage = storage;
    this.api = api;
    this.cfg = cfg;
    this.currentUser$ = new BehaviorSubject<UserType | undefined>(this.getCurrentUser());
  }

  getCurrentUser$(): Observable<UserType | undefined> {
    return this.currentUser$.asObservable();
  }

  getCurrentUser(): UserType | undefined {
    try {
      const token = this.storage.accessToken(this.cfg.app);
      const parser = new JwtParser<IProfileRes>();
      return new User(parser.parse(token).profile) as UserType;
    } catch (e) {
      return undefined;
    }
  }

  update(token: string): void {
    this.storage.setAccessToken(token, this.cfg.app)
    this.currentUser$.next(this.getCurrentUser());
  }

  check(path: string, app?: string): Promise<IAccessCheckResult> {
    return this.api.unary(
      SessionCheckDesc,
      {
        obj: CheckReq.fromPartial({
          appSlug: app || this.cfg.app,
          url: path,
          refreshToken: this.storage.refreshToken(app || this.cfg.app),
        }),
        coder: CheckReq,
      },
      { host: this.cfg.authUrlServer, silent: true },
    )
      .catch(e => {
      if (e.code === deadlineExceeded || e.code === unauthorizedCode) {
        this.clear(app);
      }

      throw e;
    });
  }

  clear(app?: string): Promise<void> {
    return this.api
      .unary(
        SessionClearDesc,
        {
          obj: ClearReq.fromPartial({
            token: this.storage.refreshToken(app || this.cfg.app),
          }),
          coder: ClearReq,
        },
        { host: this.cfg.authUrlServer, silent: true },
      )
      .finally(() => {
        this.storage.clear(app || this.cfg.app)
        this.currentUser$.next(undefined);
      });
  }
}

export default SessionService;