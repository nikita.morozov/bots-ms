import { inject, injectable } from 'inversify';
import CookieStorage, { ICookieStorage } from '@modules/cookie/storage';

export interface ISessionCookieStorage {
  clear(app: string): void
  setAccessToken(token: string | undefined, app: string): void
  setRefreshToken(token: string | undefined, app: string): void
}

export interface ISessionCookieReader {
  accessToken(app: string): string
  refreshToken(app: string): string
}

export interface ISessionCookieManager extends ISessionCookieReader, ISessionCookieStorage {}

@injectable()
class SessionCookieStorage implements ISessionCookieStorage, ISessionCookieReader {
	public static diKey = Symbol.for('SessionCookieStorageDiKey');
	private storage: ICookieStorage;

	public constructor(
    @inject(CookieStorage.diKey) cookieStorage: ICookieStorage,
	) {
		this.storage = cookieStorage;
	}

  accessToken(app: string): string {
    return this.storage.get(`${app}_accessToken`);
  }

  clear(app: string): void {
    this.storage.delete(`${app}_accessToken`)
    this.storage.delete(`${app}_refreshToken`)
  }

  refreshToken(app: string): string {
    return this.storage.get(`${app}_refreshToken`);
  }

  setAccessToken(token: string | undefined, app: string): void {
    this.storage.set(`${app}_accessToken`, token || '');
  }

  setRefreshToken(token: string | undefined, app: string): void {
    this.storage.set(`${app}_refreshToken`, token || '');
  }
}

export default SessionCookieStorage;