import { UserModel } from '@modules/auth/proto/authCommon';

export interface IProfileRes {
  profile: UserModel,
}

export interface IAccessCheckResult {
  isPublic: boolean,
}

export interface IDispose {
  dispose(): void
}