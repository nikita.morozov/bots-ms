import { inject, injectable } from 'inversify';
import SessionService, { ISessionService } from '@modules/auth/session/service';
import User from '@modules/auth/user/user';
import { AppConfigDiKey } from '@modules/app/constants';
import { IAppConfig } from '@modules/app/types';
import SessionCookieStorage, { ISessionCookieManager } from '@modules/auth/session/cookie';
import { JwtParser } from '@modules/tools/jwtParser';
import { IAccessCheckResult, IProfileRes } from '@modules/auth/session/types';
import { Observable, Subject } from 'rxjs';

export interface ISessionUsecase<UserProfile = {}> {
  check: (path: string, app?: string) => Promise<IAccessCheckResult>,
  clear: (app?: string) => Promise<void>,

  // TODO: Maybe
  isAuthorized(): boolean
  isCurrentUser(id: number | undefined): boolean
  getCurrentUser$(): Observable<User<UserProfile> | undefined>
  getCurrentUser(): User<UserProfile> | undefined

  setAccessToken(token: string | undefined): void
  setRefreshToken(token: string | undefined): void
}

@injectable()
class SessionUsecase<UserProfile = {}> implements ISessionUsecase<UserProfile> {
  public static diKey = Symbol.for('SessionUsecaseDiKey');

  private parser = new JwtParser<IProfileRes>();
  private service: ISessionService<User<UserProfile>>;
  private cfg: IAppConfig;
  private readonly storage: ISessionCookieManager;
  private cachedData: { [key: string]: User<UserProfile> } = {};
  private onChangedToken: Subject<void>;

  constructor(
    @inject(AppConfigDiKey) cfg: IAppConfig,
    @inject(SessionCookieStorage.diKey) storage: ISessionCookieManager,
    @inject(SessionService.diKey) service: ISessionService<User<UserProfile>>,
  ) {
    this.storage = storage;
    this.service = service;
    this.cfg = cfg;

    this.onChangedToken = new Subject<void>();
  }

  isAuthorized(): boolean {
    return !!this.getCurrentUser();
  }

  check(path: string, app?: string): Promise<IAccessCheckResult> {
    return this.service.check(path, app);
  }

  async clear(): Promise<void> {
    this.cachedData = {};
    await this.service.clear();
    this.onChangedToken.next();
  }

  setAccessToken(token: string | undefined): void {
    this.storage.setAccessToken(token, this.cfg.app);
    this.onChangedToken.next();
  }

  setRefreshToken(token: string | undefined): void {
    this.storage.setRefreshToken(token, this.cfg.app);
    this.onChangedToken.next();
  }

  isCurrentUser(id: number | undefined): boolean {
    if (!id) {
      return false;
    }
    return this.getCurrentUser()?.id === id;
  }

  getCurrentUser$(): Observable<User<UserProfile> | undefined> {
    return this.service.getCurrentUser$();
  }

  getCurrentUser(): User<UserProfile> | undefined {
    const token = this.storage.accessToken(this.cfg.app);

    if (!token) {
      return undefined;
    }

    if (!this.cachedData[token]) {
      this.cachedData[token] = new User(this.parser.parse(token).profile);
    }

    return this.cachedData[token];
  }

  get onChangeToken$(): Observable<void> {
    return this.onChangedToken.asObservable();
  }
}

export default SessionUsecase;
