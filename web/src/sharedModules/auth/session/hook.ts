import { useInjection } from 'inversify-react';
import SessionUsecase from '@modules/auth/session/usecase';
import { useEffect, useState } from 'react';
import User from '@modules/auth/user/user';

const useSession = () => {
  const sessionUsecase = useInjection<SessionUsecase>(SessionUsecase.diKey);
  const [user, setUser] = useState<User>();

  useEffect(() => {
    const sub = sessionUsecase.getCurrentUser$().subscribe((user) => setUser(user));
    return () => sub.unsubscribe();
  }, []);

  return user;
};

export default useSession;