import useRoleCheck from '@modules/auth/roles-dependencies/hook';
import useSession from '@modules/auth/session/hook';
import User from '@modules/auth/user/user';

const useSessionWithRoleCheck = (role: string): [user: User | undefined, isInherit: boolean | undefined] => {
  const session = useSession();
  const isInherit = useRoleCheck(session?.role || "", role);

  if (!isInherit) {
    return [undefined, undefined]
  }

  return [session, isInherit];
}

export default useSessionWithRoleCheck;