import { inject, injectable } from 'inversify';
import HttpClient from '@modules/httpClient';
import { ApiHttpClientKey } from '@modules/httpClient/constants';
import { ListOptions } from '@modules/paginatior';
import { IInfoSectionService } from '@modules/info/section/service';
import { ResultCountRes, ResultItemRes, ResultListRes } from '@modules/types/http';
import {
  ListSectionReq,
  SectionModel,
  SectionModelInput,
} from '@modules/graphql/info/info-gql';
import { from, map, mergeMap, Observable } from 'rxjs';
import { BaseCommandProps } from '@modules/httpClient/types';
import StreamHandler from '@modules/Stream/service/StreamHandler';
import { ObjectWithId } from '@modules/types';

@injectable()
class InfoSectionHttpService extends StreamHandler<SectionModel> implements IInfoSectionService {
  private api: HttpClient;

  public constructor(
    @inject(ApiHttpClientKey) api: HttpClient,
  ) {
    super();
    this.add("default", this.streamDefault)
    this.add("plain", this.plainStream)
    this.api = api;
  }

  private streamDefault = {
    stream: (opts: ListOptions, method?: string, query?: ListSectionReq): Observable<SectionModel & ObjectWithId> => {
      const input: ListSectionReq = {
        adminShow: query?.adminShow,
        slug: query?.slug as string,
        opts
      }

      return this.list(input);
    },
    loadCount: (): Observable<number> => {
      return this.count();
    },
  }

  private plainStream = {
    stream: (opts: ListOptions, method?: string, query?: object): Observable<SectionModel & ObjectWithId> => {
      return this.plainList(opts);
    },
    loadCount: (): Observable<number> => this.streamDefault.loadCount(),
  }

  count(): Observable<number> {
    return this.api.$request<object, ResultCountRes>(
      `/v1/info/section/count`, {}, { method: 'POST' }
    ).pipe(map(i => i.count));
  }

  create(value: SectionModelInput): Observable<SectionModel> {
    return this.api.$request<object, ResultItemRes<SectionModel>>(
      `/v1/info/section`, value, { method: 'POST' }
    ).pipe(map(i => i.item));
  }

  delete(id: number): Observable<void> {
    return this.api.$request<object, void & BaseCommandProps>(
      `/v1/info/section/${id}`, {}, { method: 'DELETE' }
    );
  }

  getById(id: number): Observable<SectionModel> {
    return this.api.$request<object, ResultItemRes<SectionModel>>(
      `/v1/info/section/${id}`, {}, { method: 'GET' }
    ).pipe(map(i => i.item));
  }

  plainList(value: ListOptions): Observable<SectionModel> {
    return this.api.$request<object, ResultListRes<SectionModel>>(
      `/v1/info/section/plain-list`, value, { method: 'POST' }
    ).pipe(mergeMap(i => from(i.list)));
  }

  list(value: ListSectionReq): Observable<SectionModel> {
    return this.api.$request<object, ResultListRes<SectionModel>>(
      `/v1/info/section/list`, value, { method: 'POST' }
    ).pipe(mergeMap(i => from(i.list)));
  }

  update(value: SectionModelInput): Observable<SectionModel> {
    return this.api.$request<object, ResultItemRes<SectionModel>>(
      `/v1/info/section`, value, { method: 'PUT' }
    ).pipe(map(i => i.item));
  }
}

export default InfoSectionHttpService;