import { inject, injectable } from 'inversify';
import StreamHandler from '@modules/Stream/service/StreamHandler';
import { ApiGqlClientKey } from '@modules/gqlClient/types';
import GqlClient from '@modules/gqlClient';
import {
  InfoSectionCountDocument,
  InfoSectionCountQuery,
  InfoSectionCountQueryVariables,
  InfoSectionCreateDocument,
  InfoSectionCreateMutation,
  InfoSectionCreateMutationVariables,
  InfoSectionDeleteDocument,
  InfoSectionDeleteMutation,
  InfoSectionDeleteMutationVariables,
  InfoSectionGetByIdDocument,
  InfoSectionGetByIdQuery,
  InfoSectionGetByIdQueryVariables,
  InfoSectionListDocument,
  InfoSectionListPlainDocument, InfoSectionListPlainQuery, InfoSectionListPlainQueryVariables,
  InfoSectionListQuery,
  InfoSectionListQueryVariables,
  InfoSectionUpdateDocument,
  InfoSectionUpdateMutation,
  InfoSectionUpdateMutationVariables,
  ListOptions,
  ListSectionReq,
  SectionModel,
  SectionModelInput,
} from '@modules/graphql/info/info-gql';
import { from, map, Observable } from 'rxjs';
import { ObjectWithId } from '@modules/types';
import { mergeMap } from 'rxjs/operators';
import { IExternalService } from '@modules/Stream/types';

export interface IInfoSectionService extends IExternalService<SectionModel> {
  delete(id: number): Observable<void>,
  getById(id: number): Observable<SectionModel>,
  create(input: SectionModelInput): Observable<SectionModel>,
  update(input: SectionModelInput): Observable<SectionModel>,
  count(): Observable<number>,
  plainList(opts: ListOptions): Observable<SectionModel>,
  list(value: ListSectionReq): Observable<SectionModel>,
}

@injectable()
class InfoSectionService extends StreamHandler<SectionModel> implements IInfoSectionService {
  public static diKey = Symbol.for('InfoSectionServiceKey');
  private api: GqlClient;

  public constructor(
    @inject(ApiGqlClientKey) api: GqlClient,
  ) {
    super();
    this.add("default", this.streamDefault)
    this.add("plain", this.plainStream)
    this.api = api
  }

  count(): Observable<number> {
    throw new Error('Method not implemented.');
  }

  plainList(opts: ListOptions): Observable<SectionModel> {
    throw new Error('Method not implemented.');
  }

  list(value: ListSectionReq): Observable<SectionModel> {
    throw new Error('Method not implemented.');
  }

  private streamDefault = {
    stream: (opts: ListOptions, method?: string, query?: ListSectionReq): Observable<SectionModel & ObjectWithId> => {
      const input: ListSectionReq = {
        adminShow: query?.adminShow,
        slug: query?.slug as string,
        opts
      }

      return this.api.query$<InfoSectionListQuery, InfoSectionListQueryVariables>(
        InfoSectionListDocument,
        { input },
        {
          noCache: true,
        },
      ).pipe(mergeMap(res => from(res.infoSectionList)))
    },
    loadCount: (): Observable<number> => {
      return this.api.query$<InfoSectionCountQuery, InfoSectionCountQueryVariables>(
        InfoSectionCountDocument,
      ).pipe(
        map(res => res.infoSectionCount as number),
      )
    },
  }

  private plainStream = {
    stream: (opts: ListOptions, method?: string, query?: object): Observable<SectionModel & ObjectWithId> => {
      return this.api.query$<InfoSectionListPlainQuery, InfoSectionListPlainQueryVariables>(
        InfoSectionListPlainDocument,
        { input: opts },
        {
          noCache: true,
        },
      ).pipe(mergeMap(res => from(res.infoSectionListPlain)));
    },
    loadCount: (): Observable<number> => this.streamDefault.loadCount(),
  }

  create(input: SectionModelInput): Observable<SectionModel> {
    return this.api.mutation$<InfoSectionCreateMutation, InfoSectionCreateMutationVariables>(
      InfoSectionCreateDocument,
      { input },
    ).pipe(map(res => res.infoSectionCreate as SectionModel));
  }

  update(input: SectionModelInput): Observable<SectionModel> {
    return this.api.mutation$<InfoSectionUpdateMutation, InfoSectionUpdateMutationVariables>(
      InfoSectionUpdateDocument,
      { input },
    ).pipe(map(res => res.infoSectionUpdate as SectionModel));
  }

  getById(id: number): Observable<SectionModel> {
    return this.api.query$<InfoSectionGetByIdQuery, InfoSectionGetByIdQueryVariables>(
      InfoSectionGetByIdDocument,
      { input: id },
    ).pipe(map(res => res.infoSectionGetById as SectionModel));
  }

  delete(id: number): Observable<void> {
    return this.api.mutation$<InfoSectionDeleteMutation, InfoSectionDeleteMutationVariables>(
      InfoSectionDeleteDocument,
      { input: id },
    ).pipe(map(res => res.infoSectionDelete));
  }
}

export default InfoSectionService;