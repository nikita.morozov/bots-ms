import { inject, injectable } from 'inversify';
import { PageService } from '@modules/Stream/service/ListService';
import { IBaseListUsecase, IPageService } from '@modules/Stream/types';
import { SectionModel, SectionModelInput } from '@modules/graphql/info/info-gql';
import InfoSectionService, { IInfoSectionService } from '@modules/info/section/service';
import { Observable, Subscription } from 'rxjs';
import ListMV from '@modules/Stream/model/ListMV';
import { ListOptions } from '@modules/paginatior';

export interface IInfoSectionUsecase {
  delete(id: number): Observable<void>

  getById(id: number): Observable<SectionModel>

  create(input: SectionModelInput): Observable<SectionModel>

  update(input: SectionModelInput): Observable<SectionModel>
}

@injectable()
class InfoSectionUsecase implements IInfoSectionUsecase, IBaseListUsecase<SectionModel> {
  public static diKey = Symbol.for('InfoSectionUsecaseDiKey');
  private infoSectionService: IInfoSectionService;
  private pageService: IPageService<SectionModel>

  constructor(
    @inject(InfoSectionService.diKey) infoSectionService: IInfoSectionService,
  ) {
    this.infoSectionService = infoSectionService;
    this.pageService = new PageService(this.infoSectionService);
  }

  loadItems(model: ListMV<SectionModel>, opts: ListOptions, method?: string, query?: object): [Observable<SectionModel>, Subscription] {
    return this.pageService.loadPage(model, opts, method, query);
  }

  loadTotalCount(model: ListMV<SectionModel>, method?: string, query?: object): [Observable<number>, Subscription] {
    return this.pageService.loadTotalCount(model, method, query);
  }

  create(input: SectionModelInput): Observable<SectionModel> {
    return this.infoSectionService.create(input);
  }

  update(input: SectionModelInput): Observable<SectionModel> {
    return this.infoSectionService.update(input);
  }

  delete(id: number): Observable<void> {
    return this.infoSectionService.delete(id);
  }

  getById(id: number): Observable<SectionModel> {
    return this.infoSectionService.getById(id);
  }
}

export default InfoSectionUsecase;