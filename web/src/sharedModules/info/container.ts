import { Container, interfaces } from 'inversify';
import { InfoModule } from '@modules/info/module';
import { ImageModule } from '@modules/image/module';

const infoContainer: interfaces.Container = new Container();
infoContainer.load(InfoModule, ImageModule);
export default infoContainer;
