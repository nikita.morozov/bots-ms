import { inject, injectable } from 'inversify';
import StreamHandler from '@modules/Stream/service/StreamHandler';
import {
  InfoSectionItemCountDocument,
  InfoSectionItemCountQuery,
  InfoSectionItemCountQueryVariables,
  InfoSectionItemCreateDocument,
  InfoSectionItemCreateMutation,
  InfoSectionItemCreateMutationVariables,
  InfoSectionItemDeleteDocument,
  InfoSectionItemDeleteMutation,
  InfoSectionItemDeleteMutationVariables,
  InfoSectionItemGetByIdDocument,
  InfoSectionItemGetByIdQuery,
  InfoSectionItemGetByIdQueryVariables,
  InfoSectionItemListDocument,
  InfoSectionItemListPlainDocument,
  InfoSectionItemListPlainQuery,
  InfoSectionItemListPlainQueryVariables,
  InfoSectionItemListQuery,
  InfoSectionItemListQueryVariables,
  InfoSectionItemUpdateDocument,
  InfoSectionItemUpdateMutation,
  InfoSectionItemUpdateMutationVariables,
  ListItemsReq,
  ListOptions,
  SectionItemModelInput,
} from '@modules/graphql/info/info-gql';
import { ApiGqlClientKey } from '@modules/gqlClient/types';
import GqlClient from '@modules/gqlClient';
import { from, map, Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import InfoItem from '@modules/info/item/model';
import { IExternalService } from '@modules/Stream/types';

export interface IInfoSectionItemService extends IExternalService<InfoItem> {
  create(input: SectionItemModelInput): Observable<InfoItem>,
  update(input: SectionItemModelInput): Observable<InfoItem>,
  delete(id: number): Observable<void>,
  getById(id: number): Observable<InfoItem>,
  loadItems(query: ListItemsReq): Observable<InfoItem>,
  list(opts: ListOptions): Observable<InfoItem>,
  bySection(sectionId: number): Observable<InfoItem>,
  count(slug: string): Observable<number>,
}

@injectable()
class InfoSectionItemService extends StreamHandler<InfoItem> implements IInfoSectionItemService {
  public static diKey = Symbol.for('InfoItemsServiceKey');

  private api: GqlClient;


  public constructor(
    @inject(ApiGqlClientKey) api: GqlClient,
  ) {
    super();
    this.add('default', this.streamDefault);
    this.add('plain', this.plainStream);
    this.api = api;
  }

  list(opts: ListOptions): Observable<InfoItem> {
    throw new Error('Method not implemented.');
  }

  bySection(sectionId: number): Observable<InfoItem> {
    throw new Error('Method not implemented.');
  }

  count(slug: string): Observable<number> {
    throw new Error('Method not implemented.');
  }

  private streamDefault = {
    stream: (opts?: ListOptions, method?: string, query?: ListItemsReq): Observable<InfoItem> => {
      const input: ListItemsReq = {
        landingShow: query?.landingShow,
        slug: query?.slug as string,
        opts: query?.opts,
      };

      if (query?.opts) {
        input.opts = query.opts;
      } else {
        input.opts = opts;
      }

      return this.api.query$<InfoSectionItemListQuery, InfoSectionItemListQueryVariables>(
        InfoSectionItemListDocument,
        { input },
        {
          noCache: true,
        },
      ).pipe(mergeMap(res => res.infoSectionItemList));
    },
    loadCount: (method?: string, query?: object): Observable<number> => {
      return this.api.query$<InfoSectionItemCountQuery, InfoSectionItemCountQueryVariables>(
        InfoSectionItemCountDocument,
        {},
      ).pipe(
        map(res => res.infoSectionItemCount as number),
      );
    },
  };

  private plainStream = {
    stream: (opts: ListOptions, method?: string, query?: object): Observable<InfoItem> => {
      return this.api.query$<InfoSectionItemListPlainQuery, InfoSectionItemListPlainQueryVariables>(
        InfoSectionItemListPlainDocument,
        { input: opts },
        {
          noCache: true,
        },
      ).pipe(mergeMap(res => {
        const items = res.infoSectionItemListPlain.map(i => new InfoItem(i));
        return from(items);
      }));
    },
    loadCount: (method?: string, query?: object): Observable<number> => this.streamDefault.loadCount(),
  };

  create(input: SectionItemModelInput): Observable<InfoItem> {
    return this.api.mutation$<InfoSectionItemCreateMutation, InfoSectionItemCreateMutationVariables>(
      InfoSectionItemCreateDocument,
      { input },
    ).pipe(map(res => new InfoItem(res.infoSectionItemCreate as InfoItem)));
  }

  update(input: SectionItemModelInput): Observable<InfoItem> {
    return this.api.mutation$<InfoSectionItemUpdateMutation, InfoSectionItemUpdateMutationVariables>(
      InfoSectionItemUpdateDocument,
      { input },
    ).pipe(map(res => new InfoItem(res.infoSectionItemUpdate as InfoItem)));
  }

  getById(id: number): Observable<InfoItem> {
    return this.api.query$<InfoSectionItemGetByIdQuery, InfoSectionItemGetByIdQueryVariables>(
      InfoSectionItemGetByIdDocument,
      { input: id },
    ).pipe(map(res => new InfoItem(res.infoSectionItemGetById as InfoItem)));
  }

  delete(id: number): Observable<void> {
    return this.api.mutation$<InfoSectionItemDeleteMutation, InfoSectionItemDeleteMutationVariables>(
      InfoSectionItemDeleteDocument,
      { input: id },
    ).pipe(map(res => res.infoSectionItemDelete));
  }

  loadItems(query: ListItemsReq): Observable<InfoItem> {
    return this.streamDefault.stream(query.opts, '', query);
  }
}

export default InfoSectionItemService;