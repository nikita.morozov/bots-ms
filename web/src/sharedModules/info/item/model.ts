import { SectionItemModel } from '@modules/graphql/info/info-gql';

class InfoItem implements SectionItemModel {
  content?: string;
  createdAt: number;
  date?: number;
  displayLanding?: boolean;
  id: number;
  image?: string;
  index?: number;
  language?: string;
  link?: string;
  sectionId: number;
  speaker?: string;
  time?: string;
  title?: string;
  topic?: string;

  constructor(obj: SectionItemModel) {
    this.content = obj.content;
    this.createdAt = obj.createdAt;
    this.date = obj.date;
    this.displayLanding = obj.displayLanding;
    this.id = obj.id;
    this.image = obj.image;
    this.index = obj.index;
    this.language = obj.language;
    this.link = obj.link;
    this.sectionId = obj.sectionId;
    this.speaker = obj.speaker;
    this.time = obj.time;
    this.title = obj.title;
    this.topic = obj.topic;
  }
}

export default InfoItem;