import { inject, injectable } from 'inversify';
import InfoSectionItemService, { IInfoSectionItemService } from '@modules/info/item/service';
import { IBaseListUsecase, IPageService } from '@modules/Stream/types';
import { PageService } from '@modules/Stream/service/ListService';
import ListMV from '@modules/Stream/model/ListMV';
import { Observable, Subscription } from 'rxjs';
import { ListItemsReq, ListOptions, SectionItemModelInput } from '@modules/graphql/info/info-gql';
import InfoItem from './model';

export interface IInfoItemUsecase {
  create(input: SectionItemModelInput): Observable<InfoItem>
  update(input: SectionItemModelInput): Observable<InfoItem>
  delete(id: number): Observable<void>
  getById(id: number): Observable<InfoItem>
  loadDefaultItems(query: ListItemsReq): Observable<InfoItem>
}

@injectable()
class InfoSectionItemUsecase implements IInfoItemUsecase, IBaseListUsecase<InfoItem> {
  public static diKey = Symbol.for('InfoItemUsecaseDiKey');
  private infoSectionItemService: IInfoSectionItemService;
  private pageService: IPageService<InfoItem>;

  constructor(
    @inject(InfoSectionItemService.diKey) infoSectionItemService: IInfoSectionItemService,
  ) {
    this.infoSectionItemService = infoSectionItemService;
    this.pageService = new PageService(this.infoSectionItemService);
  }

  loadItems(model: ListMV<InfoItem>, opts: ListOptions, method?: string, query?: object): [Observable<InfoItem>, Subscription] {
    return this.pageService.loadPage(model, opts, method, query);
  }

  loadTotalCount(model: ListMV<InfoItem>, method?: string, query?: object): [Observable<number>, Subscription] {
    return this.pageService.loadTotalCount(model, method, query);
  }

  create(input: SectionItemModelInput): Observable<InfoItem> {
    return this.infoSectionItemService.create(input)
  }

  update(input: SectionItemModelInput): Observable<InfoItem> {
    return this.infoSectionItemService.update(input)
  }

  delete(id: number): Observable<void> {
    return this.infoSectionItemService.delete(id);
  }

  getById(id: number): Observable<InfoItem> {
    return this.infoSectionItemService.getById(id)
  }

  loadDefaultItems(query: ListItemsReq): Observable<InfoItem> {
    return this.infoSectionItemService.loadItems(query);
  }
}

export default InfoSectionItemUsecase;