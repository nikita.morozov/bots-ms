import { inject, injectable } from 'inversify';
import { IInfoSectionItemService } from '@modules/info/item/service';
import HttpClient from '@modules/httpClient';
import { ApiHttpClientKey } from '@modules/httpClient/constants';
import { ListOptions } from '@modules/paginatior';
import { ResultCountRes, ResultItemRes, ResultListRes } from '@modules/types/http';
import { from, map, mergeMap, Observable } from 'rxjs';
import {
  ListItemsReq, SectionItemModel,
  SectionItemModelInput,
} from '@modules/graphql/info/info-gql';
import InfoItem from '@modules/info/item/model';
import { BaseCommandProps } from '@modules/httpClient/types';
import StreamHandler from '@modules/Stream/service/StreamHandler';

@injectable()
class InfoSectionItemHttpService extends StreamHandler<InfoItem> implements IInfoSectionItemService {
  private api: HttpClient;

  public constructor(
    @inject(ApiHttpClientKey) api: HttpClient,
  ) {
    super();
    this.api = api;
    this.add('default', this.streamDefault);
    this.add('plain', this.plainStream);
  }

  private streamDefault = {
    stream: (opts?: ListOptions, method?: string, query?: ListItemsReq): Observable<InfoItem> => {
      const input: ListItemsReq = {
        landingShow: query?.landingShow,
        slug: query?.slug as string,
        opts: query?.opts,
      };

      if (query?.opts) {
        input.opts = query.opts;
      } else {
        input.opts = opts;
      }

      return this.loadItems(input);
    },
    loadCount: (method?: string, query?: { slug: string }): Observable<number> => {
      return this.count(query?.slug || "");
    },
  };

  private plainStream = {
    stream: (opts: ListOptions, method?: string, query?: object): Observable<InfoItem> => {
      return this.list(opts);
    },
    loadCount: (method?: string, query?: object): Observable<number> => this.streamDefault.loadCount(),
  };

  count(slug: string): Observable<number> {
    return this.api.$request<object, ResultCountRes>(
      `/v1/info/item/count/${slug}`, {}, { method: 'GET' }
    ).pipe(map(i => i.count));
  }

  create(value: SectionItemModelInput): Observable<InfoItem> {
    return this.api.$request<object, ResultItemRes<SectionItemModel>>(
      `/v1/info/item`, value, { method: 'POST' }
    ).pipe(map(i => new InfoItem(i.item)));
  }

  delete(id: number): Observable<void> {
    return this.api.$request<object, void & BaseCommandProps>(
      `/v1/info/item/${id}`, {}, { method: 'DELETE' }
    );
  }

  getById(id: number): Observable<InfoItem> {
    return this.api.$request<object, ResultItemRes<SectionItemModel>>(
      `/v1/info/item/${id}`, {}, { method: 'GET' }
    ).pipe(map(i => new InfoItem(i.item)));
  }

  loadItems(query: ListItemsReq): Observable<InfoItem> {
    return this.api.$request<object, ResultListRes<SectionItemModel>>(
      `/v1/info/item/list`, query, { method: 'POST' }
    ).pipe(mergeMap(i => from(i.list.map(x => new InfoItem(x)))));
  }

  bySection(sectionId: number): Observable<InfoItem> {
    return this.api.$request<object, ResultListRes<SectionItemModel>>(
      `/v1/info/item/load-in-section/${sectionId}`, {}, { method: 'GET' }
    ).pipe(mergeMap(i => from(i.list.map(x => new InfoItem(x)))));
  }

  list(opts: ListOptions): Observable<InfoItem> {
    return this.api.$request<object, ResultListRes<SectionItemModel>>(
      `/v1/info/item/plain-list`, opts, { method: 'POST' }
    ).pipe(mergeMap(i => from(i.list.map(x => new InfoItem(x)))));
  }

  update(input: SectionItemModelInput): Observable<InfoItem> {
    return this.api.$request<object, ResultItemRes<SectionItemModel>>(
      `/v1/info/item`, input, { method: 'PUT' }
    ).pipe(map(i => new InfoItem(i.item)));
  }
}

export default InfoSectionItemHttpService;