import { AsyncContainerModule, interfaces } from 'inversify';
import InfoSectionUsecase from '@modules/info/section/usecase';
import InfoSectionItemUsecase from './item/usecase';
import InfoSectionService from '@modules/info/section/service';
import InfoSectionItemService from '@modules/info/item/service';

export const InfoModule = new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<InfoSectionService>(InfoSectionService.diKey).to(InfoSectionService);
  bind<InfoSectionItemService>(InfoSectionItemService.diKey).to(InfoSectionItemService);
  bind<InfoSectionUsecase>(InfoSectionUsecase.diKey).to(InfoSectionUsecase);
  bind<InfoSectionItemUsecase>(InfoSectionItemUsecase.diKey).to(InfoSectionItemUsecase);
});