import { ApiClientMiddleware, ApiClientOptions, ApiMetadata, IApiConfig } from '@modules/client/types';
import {injectable} from "inversify";

@injectable()
abstract class BaseClient {
  protected readonly debug: boolean;
  protected middlewares: ApiClientMiddleware[] = [];

  constructor(config: IApiConfig) {
    this.debug = !!config.getDebug;
    this.middlewares = config.getMiddlewares();
  }

  addMiddleware = (...middleware: ApiClientMiddleware[]) => {
    this.middlewares.push(...middleware);
  };

  _getExtraMetadata = (options?: ApiClientOptions | undefined): ApiMetadata => {
    return (this.middlewares || []).reduce<ApiMetadata>((acc, cur) => {
      acc = {
        ...acc,
        ...(cur.ExtraMetadata && cur.ExtraMetadata(options) || {}),
      };
      return acc;
    }, {});
  };
}

export default BaseClient;