import { inject, injectable } from 'inversify';
import { pipe, subscribe } from 'wonka';
import { GqlApiConfigDiKey } from '@modules/gqlClient/types';
import forEach from 'lodash/forEach';
import { Observable } from 'rxjs';
import { createClient as createWSClient } from 'graphql-ws';
import { Client as ClientWS } from 'graphql-ws/lib/client';
import { ApiClientOptions, IApiConfig } from '@modules/client/types';
import BaseClient from '@modules/client/model';
import IGqlClient from '@modules/gqlClient/interfaces';
import CookieStorage, { ICookieStorage } from '@modules/cookie/storage';
import { AppConfigDiKey } from '@modules/app/constants';
import { IAppConfig } from '@modules/app/types';
import { createClient, defaultExchanges, subscriptionExchange } from 'urql';
import { Client } from '@urql/core/dist/types/client';
import { DocumentNode } from 'graphql/language/ast';
import { AnyVariables, RequestPolicy } from '@urql/core/dist/types/types';

@injectable()
export class GqlClient extends BaseClient implements IGqlClient<DocumentNode> {
  private client!: Client;
  private wsClient!: ClientWS;

  private readonly config: IApiConfig;
  private readonly appConfig: IAppConfig;
  private readonly cookieStorage: ICookieStorage;

  constructor(
    @inject(GqlApiConfigDiKey) config: IApiConfig,
    @inject(AppConfigDiKey) appConfig: IAppConfig,
    @inject(CookieStorage.diKey) cookieStorage: ICookieStorage,
  ) {
    super(config);
    this.config = config;
    this.appConfig = appConfig;
    this.cookieStorage = cookieStorage;

    this.connect()
  }

  private connect = () => {
    const host = this.config.getHost();
    const hostWs = this.config.getHostWs && this.config.getHostWs() || '';
    if (!host) {
      throw new Error('Host is not undefined');
    }

    this.wsClient = createWSClient({
        url: hostWs + '/subscriptions',
        keepAlive: 10_000,
        lazy: true,
        lazyCloseTimeout: 10_000,
        connectionParams: () => new Promise<Record<string, unknown>>((resolve) => {
          const handler = setInterval(() => {
            const token = this.cookieStorage.get(this.appConfig.app + '_refreshToken');
            if (token) {
              resolve({
                app: this.appConfig.app,
                token,
              })

              clearInterval(handler);
            }
          }, 500);
        }),
      },
    )

    this.client = createClient({
      url: host + '/query',
      exchanges: [
        ...defaultExchanges,
        subscriptionExchange({
          forwardSubscription: (operation) => ({
            subscribe: (sink) => ({
              unsubscribe: this.wsClient.subscribe(operation, sink),
            }),
          }),
        }),
      ],
    })
  }

  dispose = () => {
    this.wsClient.terminate();
    this.connect();
  }

  getRequestPolicy(options?: ApiClientOptions): RequestPolicy {
    if (!options?.noCache) {
      switch (options?.cacheType) {
        case 'cache-only':
          return 'cache-only'
        case 'network-only':
          return 'network-only'
        case 'cache-and-network':
          return 'cache-and-network'
        case 'cache-first':
          return 'cache-first'
        default:
          return 'network-only'
      }
    }

    return 'network-only'
  }

  mutation = async <TData, TVariables extends AnyVariables>(mutation: DocumentNode, variables?: TVariables & AnyVariables, options?: ApiClientOptions) => {
    const headers = {
      ...(options?.metadata || {}),
      ...this._getExtraMetadata(options),
    };

    forEach(this.middlewares, (m) => m.OnRequestStart && m.OnRequestStart(options));

    try {
      const resp = await this.client.mutation<TData, TVariables>(mutation, variables || {} as TVariables, {
        fetchOptions: {
          // @ts-ignore
          headers,
        },
        requestPolicy: this.getRequestPolicy(options),
      }).toPromise();

      if (resp.error) {
        forEach(this.middlewares, (m) => m.OnRequestError && m.OnRequestError(resp.error as Error, options))
        return
      }

      forEach(this.middlewares, (m) => m.OnRequestComplete && m.OnRequestComplete(resp, options));
      return resp.data;
    } catch (e) {
      forEach(this.middlewares, (m) => m.OnRequestError && m.OnRequestError(e as Error, options));
      throw e;
    }
  };

  subscribe = <TData, TVariables extends AnyVariables>(query: DocumentNode, variables?: TVariables, options?: ApiClientOptions): Observable<TData> => {
    const headers = {
      ...(options?.metadata || {}),
      ...this._getExtraMetadata(options),
    };
    return new Observable<TData>(sub => {
      // forEach(this.middlewares, (m) => m.OnRequestStart && m.OnRequestStart(options));
      const subcr = pipe(
        this.client.subscription<TData, TVariables>(query, variables || {} as TVariables, {
          fetchOptions: {
            // @ts-ignore
            headers,
          },
        }),
        subscribe(
          (result) => {
            if (result.error) {
              forEach(this.middlewares, (m) => m.OnRequestError && m.OnRequestError(result.error as Error, options));
              return;
            }

            sub.next(result.data)
          },
        ),
      );

      return () => subcr.unsubscribe();
    });
  };

  query = async <TData, TVariables extends AnyVariables>(query: DocumentNode, variables?: TVariables, options?: ApiClientOptions) => {
    const headers = {
      ...(options?.metadata || {}),
      ...this._getExtraMetadata(options),
    };
    forEach(this.middlewares, (m) => m.OnRequestStart && m.OnRequestStart(options));

    try {
      const resp = await this.client.query<TData, TVariables>(query, variables || {} as TVariables, {
        fetchOptions: {
          // @ts-ignore
          headers,
        },
        requestPolicy: this.getRequestPolicy(options),
      }).toPromise();

      if (resp.error) {
        forEach(this.middlewares, (m) => m.OnRequestError && m.OnRequestError(resp.error as Error, options))
        return
      }

      forEach(this.middlewares, (m) => m.OnRequestComplete && m.OnRequestComplete(resp, options));
      return resp.data;
    } catch (e) {
      forEach(this.middlewares, (m) => m.OnRequestError && m.OnRequestError(e as Error, options));
      throw e;
    }
  };

  query$ = <TData, TVariables extends AnyVariables>(query: DocumentNode, variables?: TVariables, options?: ApiClientOptions): Observable<TData> => {
    const headers = {
      ...(options?.metadata || {}),
      ...this._getExtraMetadata(options),
    };

    return new Observable<TData>(sub => {
      forEach(this.middlewares, (m) => m.OnRequestStart && m.OnRequestStart(options));
      this.client.query<TData, TVariables>(query, variables || {} as TVariables, {
        fetchOptions: {
          // @ts-ignore
          headers,
        },
        requestPolicy: this.getRequestPolicy(options),
      }).toPromise().then((item) => {
        if (item.error) {
          sub.error(item.error);
          forEach(this.middlewares, (m) => m.OnRequestError && m.OnRequestError(item.error as Error, options))
          return
        }
        sub.next(item.data);

        forEach(this.middlewares, (m) => m.OnRequestComplete && m.OnRequestComplete({}, options));
        sub.complete();
      })
        .catch(e => forEach(this.middlewares, (m) => m.OnRequestError && m.OnRequestError(e as Error, options)));
    });
  };


  mutation$ = <TData, TVariables extends AnyVariables>(mutation: DocumentNode, variables?: TVariables, options?: ApiClientOptions): Observable<TData> => {
    const headers = {
      ...(options?.metadata || {}),
      ...this._getExtraMetadata(options),
    };

    return new Observable<TData>(sub => {
      forEach(this.middlewares, (m) => m.OnRequestStart && m.OnRequestStart(options));
      this.client.mutation<TData, TVariables>(mutation, variables || {} as TVariables, {
        fetchOptions: {
          // @ts-ignore
          headers,
        },
        requestPolicy: this.getRequestPolicy(options),
      }).toPromise().then((item) => {
        if (item.error) {
          forEach(this.middlewares, (m) => m.OnRequestError && m.OnRequestError(item.error as Error, options))
          return
        }

        sub.next(item.data);

        forEach(this.middlewares, (m) => m.OnRequestComplete && m.OnRequestComplete({}, options));
        sub.complete();
      })
        .catch(e => forEach(this.middlewares, (m) => m.OnRequestError && m.OnRequestError(e as Error, options)));
    });
  };
}

export default GqlClient;