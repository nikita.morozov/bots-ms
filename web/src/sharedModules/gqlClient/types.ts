export interface GqlClientOptions {
  headers?: object;
}

export const ApiGqlClientKey = Symbol.for("ApiGqlClientKey")
export const GqlApiConfigDiKey = Symbol.for("GqlApiConfigDiKey")