import { Method } from 'axios';

export interface BaseCommandProps {
  success: string
  statusCode?: number
  message?: string
}

export interface ApiClientOptions {
  method?: Method;
  headers?: object;
  asData?: boolean;
  silent?: boolean;
  context?: string;
  passError?: boolean;
  isFile?: boolean;
  baseURL?: string;
  transformResponse?: ((data: any) => any)[];
  asImpersonate?: boolean;
}

export class StatusError extends Error {
  public status: number;

  constructor(message: string, status: number) {
    super(message);
    this.status = status;
  }
}