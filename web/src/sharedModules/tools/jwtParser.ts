import jwtDecode from 'jwt-decode';

export interface IJwtParser<T> {
  parse(token: string): T;
}

export class JwtParser<T> implements IJwtParser<T> {
  parse(token: string): T {
    return jwtDecode<T>(token);
  }
}