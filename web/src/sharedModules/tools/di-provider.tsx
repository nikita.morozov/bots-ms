import React, { FC, PropsWithChildren, useEffect, useState } from 'react';
import { interfaces } from 'inversify';
import { Provider } from 'inversify-react';

export interface DiProviderProps {
  child: interfaces.Container;
}

const DiProvider: FC<PropsWithChildren<DiProviderProps>> = ({child, children}) => {
  const [ready, setReady] = useState(false);
  useEffect(() => {
    child.parent = null;
    setReady(true);
  }, [child])

  if (!ready) {
    // TODO: Loading
    return null;
  }

  return (
    <Provider
      key={child.id}
      container={child}
    >
      {children}
    </Provider>
  );
};

export default DiProvider;