import { useEffect, useState } from 'react';
import { interfaces } from 'inversify';

const useContainer = (_container: () => Promise<interfaces.Container>) => {
	const [cont, setContainer] = useState<interfaces.Container | undefined>()
	useEffect(() => {
		const init = async () => {
			const c = await _container();
			setContainer(c);
		}

		init();
	}, [_container]);

	return cont;
}

export const useChildContainer = (child: interfaces.Container, _container: () => Promise<interfaces.Container>) => {
	const cont = useContainer(_container);
	useEffect(() => {
		if (!cont) {
			return;
		}

		child.parent = cont;
	}, [cont]);

	if (!cont) {
		return null;
	}

	return child;
}

export default useContainer;