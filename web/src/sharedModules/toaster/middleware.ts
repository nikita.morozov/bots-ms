import { ApiClientMiddleware, ApiClientOptions } from '@modules/client/types';
import { IMessageHandler, MessageItem } from '@modules/message/handler';

class MessageMiddleware implements ApiClientMiddleware {
  private handler: IMessageHandler<string, MessageItem>;

  constructor(handler: IMessageHandler<string, MessageItem>) {
    this.handler = handler;
  }

  OnRequestError(err: Error, options?: ApiClientOptions) {
    this.handler.sendError({ title: err.message })
  }
}

export default MessageMiddleware;