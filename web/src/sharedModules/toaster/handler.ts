import { message } from 'antd';
import { injectable } from 'inversify';
import { IMessageHandler, MessageItem } from '@modules/message/handler';

@injectable()
class ToasterMessageHandler implements IMessageHandler<string, MessageItem> {
  constructor() {
  }

  send(type: string, data: MessageItem): void {
    switch (type) {
      case 'error':
        this.sendError(data);
        break;
      case 'info':
        this.sendInfo(data);
        break;
      case 'success':
        this.sendSuccess(data);
        break;
      default:
        message.info(data.title)
    }
  }

  getText(data: MessageItem): string {
    if (!data.description) {
      return data.title;
    }

    return `${data.title}: ${data.description}`;
  }

  sendError(data: MessageItem): void {
    message.error(this.getText(data))
  }

  sendInfo(data: MessageItem): void {
    message.info(this.getText(data))
  }

  sendSuccess(data: MessageItem): void {
    message.success(this.getText(data))
  }
}

export default ToasterMessageHandler;