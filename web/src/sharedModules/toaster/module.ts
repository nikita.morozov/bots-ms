import { AsyncContainerModule, interfaces } from 'inversify';
import { MessageExternalHandlerKey } from '@modules/message/constants';
import MessageModule from '@modules/message/usecase';
import ToasterMessageHandler from '@modules/toaster/handler';

export const ToastMessageModule = new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<ToasterMessageHandler>(MessageExternalHandlerKey).to(ToasterMessageHandler);
  bind<MessageModule>(MessageModule.diKey).to(MessageModule);
});
