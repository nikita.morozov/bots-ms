import { inject, injectable } from 'inversify';
import { IMessageHandler, MessageItem } from '@modules/message/handler';
import { MessageExternalHandlerKey } from '@modules/message/constants';

@injectable()
class MessageModule implements IMessageHandler<string, MessageItem> {
  static diKey = Symbol.for('MessageModuleDiKey');

  private handler: IMessageHandler<string, MessageItem>;

  constructor(
    @inject(MessageExternalHandlerKey) handler: IMessageHandler<string, MessageItem>
  ) {
    this.handler = handler;
  }

  send(type: string, data: MessageItem): void {
    this.handler.send(type, data)
  }

  sendError(data: MessageItem): void {
    this.handler.sendError(data)
  }

  sendInfo(data: MessageItem): void {
    this.handler.sendInfo(data)
  }

  sendSuccess(data: MessageItem): void {
    this.handler.sendSuccess(data)
  }
}

export default MessageModule;