import { ListModel } from '@modules/graphql/list/list-gql';

export interface IMeta {
  id: number,
  item: string
}

class List<MetaData = {}> implements ListModel {
  context: string;
  createdAt?: number;
  id: number;
  meta?: string;
  title: string;

  constructor(obj: ListModel) {
    this.context = obj.context;
    this.createdAt = obj.createdAt;
    this.id = obj.id;
    this.meta = obj.meta;
    this.title = obj.title;
  }

  get getMeta(): MetaData | undefined {
    if (!this.meta) {
      return
    }

    try {
      return JSON.parse(this.meta)
    } catch (e) {
      throw Error("get meta parsing failed")
    }
  }
}

export default List
