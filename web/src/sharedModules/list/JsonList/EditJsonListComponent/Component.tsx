import { FunctionComponent, useEffect, useState } from 'react';
import { IEditJsonList, IListEditData } from '@modules/list/JsonList/types';
import EditJsonList from '@modules/list/JsonList/EditJsonList';
import cObserver from '@modules/Stream/model/connector';
import { Modal } from 'antd';

const EditJsonListComponentView: FunctionComponent<IEditJsonList> = (props) => {
  const { getSubject, getVisible, setVisible } = props;
  const [visible, setVisibleHandle] = useState(false);
  const [data, setData] = useState<IListEditData | undefined>();
  const paramsModal = data?.paramsModal;
  const titleModal = paramsModal?.title;

  useEffect(() => {
    const sub = getSubject().subscribe(i => {
      setData(i);
      setVisible(true);
    });

    const subVisible = getVisible().subscribe(e => {
      setVisibleHandle(e);
    });

    return () => {
      sub.unsubscribe();
      subVisible.unsubscribe();
    };
  }, []);

  if (!visible || !data || !data.jsonSchema) {
    return null;
  }

  return (
    <Modal
      open={true}
      title={titleModal || 'common.edit'}
      onCancel={() => setVisible(false)}
    >
      <div className='json-list-wrapper'>
        <EditJsonList />
      </div>
    </Modal>
  );
};

export default cObserver(EditJsonListComponentView);