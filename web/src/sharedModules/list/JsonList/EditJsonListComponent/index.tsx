import { FunctionComponent, useState } from 'react';
import { useInjection } from 'inversify-react';
import ListUsecase from '@modules/list/usecase';
import List from '@modules/list/model';
import JsonListVM from '@modules/list/JsonList/JsonListVM';
import EditJsonListComponentView from '@modules/list/JsonList/EditJsonListComponent/Component';

const EditJsonListComponent: FunctionComponent = () => {
  const listUC = useInjection<ListUsecase<List>>(ListUsecase.diKey);
  const [vm] = useState(new JsonListVM(listUC));

  return (
    <EditJsonListComponentView
      vm={vm}
    />
  );
};

export default EditJsonListComponent;