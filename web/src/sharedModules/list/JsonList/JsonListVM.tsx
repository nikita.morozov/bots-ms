import ObservableVM, { BaseViewModel } from '@modules/Stream/model/BaseVM';
import ListUsecase from '@modules/list/usecase';
import List from '@modules/list/model';
import { ListModelInput } from '@modules/graphql/list/list-gql';
import { map } from 'rxjs';
import { ObservableField } from '@modules/Stream/model/decorator';

interface SearchVMLoadParams {
  context: string;
}

class JsonListVM extends BaseViewModel<SearchVMLoadParams> {
  private listUC: ListUsecase<List>;
  @ObservableField()
  public data?: ObservableVM<List>;

  constructor(listUC: ListUsecase<List>) {
    super();
    this.listUC = listUC;
    this.data = new ObservableVM<List>({} as List);
  }

  load = (params?: SearchVMLoadParams) => {
    if (!params?.context) {
      return;
    }

    const sub = this.listUC.getByContext(params.context)
      .subscribe(e => this.data?.setItem(e));

    const subscriberSubject = this.listUC.getSubscriberSubject()
      .subscribe(e => this.data?.setItem(e));

    this.addSubscription(sub);
    this.addSubscription(subscriberSubject);
  };


  getSubject = () => {
    return this.listUC.getSubject();
  };

  getVisible = () => {
    return this.listUC.getVisible();
  };

  setVisible = (value: boolean) => {
    this.listUC.setVisible(value);
  };

  save = (value: ListModelInput) => {
    return this.listUC.save(value).pipe(
      map(i => {
        if (i) {
          this.listUC.getSubscriberSubject().next(i);
        }
        return i;
      }),
    );
  };
}


export default JsonListVM;
