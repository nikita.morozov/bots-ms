import JsonDataView from '@modules/list/JsonList/JsonData/Component';
import { JsonDataProps } from '@modules/list/JsonList/JsonData/types';
import useLoadableVM from '@modules/Stream/hooks';
import { useInjection } from 'inversify-react';
import ListUsecase from '@modules/list/usecase';
import List from '@modules/list/model';
import { useState } from 'react';
import JsonListVM from '@modules/list/JsonList/JsonListVM';

const JsonData = <T extends object>(props: JsonDataProps<T>) => {
  const listUC = useInjection<ListUsecase<List>>(ListUsecase.diKey);

  const [vm] = useState(new JsonListVM(listUC));
  useLoadableVM(vm, { context: props.context });

  return (
    <JsonDataView vm={vm} {...props} />
  );
};

export default JsonData;
