import { ReactElement, ReactNode } from 'react';
import { IListEditData, paramsModalList } from '../types';
import { Subject } from 'rxjs';
import List from '@modules/list/model';

type TypePos = 'before' | 'after'

export interface JsonDataViewProps<T> extends JsonDataProps<T> {
  editSubject: Subject<IListEditData>,
  getSubject(): Subject<IListEditData>,
  data: List<T>
}

export interface JsonDataProps<T> {
  context: string,
  schema: object,
  editable: boolean,
  children: (items: T) => ReactNode;
  uiSchema?: object,
  wrapperClassName?: string
  rootClass?: string
  classEditButton?: string
  iconEditButton?: ReactElement
  paramsModal?: paramsModalList
  parentId?: string
  editModal?: boolean
  editablePos?: TypePos
  alwaysShow?: boolean
  activeEditButton?: string
}
