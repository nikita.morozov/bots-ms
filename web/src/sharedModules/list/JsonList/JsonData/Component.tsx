import React, { useEffect, useState } from 'react';
import { IListEditData } from '@modules/list/JsonList/types';
import { JsonDataViewProps } from '@modules/list/JsonList/JsonData/types';
import cObserver from '@modules/Stream/model/connector';
import classNames from 'classnames';

const JsonDataView = <T extends object>(props: JsonDataViewProps<T>) => {
  const {
    context,
    schema,
    uiSchema,
    classEditButton,
    iconEditButton,
    editable,
    wrapperClassName,
    rootClass,
    paramsModal,
    editablePos = 'before',
    editModal = true,
    parentId,
    alwaysShow,
    activeEditButton,
    getSubject,
    children,
    data,
  } = props;

  const [buttonVisible, setButtonVisible] = useState(false);
  const [editMode, setEditMode] = useState(false);

  useEffect(() => {
    setEditMode(false);
  }, [parentId]);

  const value = (!!parentId && Array.isArray(data.getMeta)) ? data.getMeta.filter(i => i.parentId == parentId) as T : data.getMeta;
  const activeClass = activeEditButton ? { [activeEditButton]: !!value && !!activeEditButton } : {};

  const EditButton = (
    <div
      className={classNames(
        'list-ms_edit-cont',
        {
          ['edit-button_hover']: buttonVisible || !data || alwaysShow,
          ...activeClass,
        },
        classEditButton,
      )}
      onClick={editModal ? () => getSubject().next({
        context: context,
        jsonSchema: schema,
        uiSchema: uiSchema,
        paramsModal: paramsModal,
        parentId,
        data,
      } as IListEditData) : () => setEditMode(true)}
      onMouseEnter={() => setButtonVisible(true)}
      onMouseLeave={() => setButtonVisible(false)}
    >
      {iconEditButton ? (
        <>
          {iconEditButton}
        </>
      ) : 'Edit'}
    </div>
  );

  if (editable) {
    return (
      <>
        <div
          className={classNames('list-ms-content', rootClass)}
          style={{ width: editMode ? '100%' : '' }}
        >
          {!editMode && editablePos === 'before' && EditButton}
          <div
            className={wrapperClassName}
            onMouseEnter={() => setButtonVisible(true)}
            onMouseLeave={() => setButtonVisible(false)}
          >
            {value ? children(value) : children([] as T)}
          </div>
          {!editMode && editablePos === 'after' && EditButton}
        </div>
      </>
    );
  }

  return value ? children(value) : children([] as T);
};

export default cObserver(JsonDataView);
