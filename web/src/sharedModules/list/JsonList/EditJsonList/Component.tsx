import React, { FunctionComponent, useEffect, useState } from 'react';
import FormEdit from './form';
import { IEditJsonList, IListEditData } from '@modules/list/JsonList/types';
import cObserver from '@modules/Stream/model/connector';


const EditJsonListView: FunctionComponent<IEditJsonList> = (props) => {
  const { save, getSubject, setVisible } = props;
  const [data, setData] = useState<IListEditData | undefined>();

  useEffect(() => {
    const sub = getSubject().subscribe(i => {
      setData(i);
    });
    return () => sub.unsubscribe();
  }, []);

  if (!data || !data.jsonSchema) {
    return null;
  }

  return (
    <div className="edit-json-form__wrapper">
      <FormEdit
        jsonSchema={data.jsonSchema}
        uiSchema={data.uiSchema}
        data={data.data.getMeta}
        onSubmit={(value) => {
          save({ ...data.data, context: data?.context || data?.data.context, meta: JSON.stringify(value) }).subscribe();
          setVisible && setVisible(false)
        }}
        parentId={data.parentId}
        context={data.context}
      />
    </div>
  );
};

export default cObserver(EditJsonListView);
