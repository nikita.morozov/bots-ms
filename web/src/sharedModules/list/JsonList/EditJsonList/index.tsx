import EditJsonListView from './Component';
import { FunctionComponent, useState } from 'react';
import { useInjection } from 'inversify-react';
import ListUsecase from '@modules/list/usecase';
import List from '@modules/list/model';
import JsonListVM from '@modules/list/JsonList/JsonListVM';

const EditJsonList: FunctionComponent = () => {
  const listUC = useInjection<ListUsecase<List>>(ListUsecase.diKey);
  const [vm] = useState(new JsonListVM(listUC));

  return (
    <EditJsonListView vm={vm} />
  );
};

export default EditJsonList;
