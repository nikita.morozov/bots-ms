import React from 'react';
import FormUploadImage from '../../../../widgets/FormUploadImage';
import { useInjection } from 'inversify-react';
import ImageUsecase from '@modules/image/usecase';

const UploadImageWidget = (props: any) => {
  const imageUC = useInjection<ImageUsecase>(ImageUsecase.diKey);

  return (
    <div style={{ maxWidth: '300px' }}>
      <FormUploadImage
        accept={{ 'image/jpeg': ['.jpg', '.jpeg'], 'image/png': ['.png'] }}
        title="form-upload-image.title.drag"
        isImage
        fullWidth
        fullHeight
        image={imageUC.srcByKey(props.value, 825, 465, '')}
        sendUpload={(file: File) => imageUC.upload(file).then((res) => props.onChange(res.key))}
        handleClose={() => props.onChange('')}
      />
    </div>
  );
};

export default UploadImageWidget;
