import React from 'react';
import ColorPicker from 'react-best-gradient-color-picker'


const GradientField = (props: any) => {
  return (
    <ColorPicker value={props.value} onChange={props.onChange} />
  );
};

export default GradientField;
