import React from 'react';
import { TransField } from '@locus/translate-react';
import { Input } from 'antd';


const TransFieldWidget = (props: any) => {
  return (
    <TransField
      required={true}
      name={props.schema.name}
      value={props.value}
      locKey={props.schema.locKey}
      onChange={props.onChange}
      single={true}
      showButton={<></>}
    >
      <Input type="text" {...props} label={props.schema.label} />
    </TransField>
  );
};

export default TransFieldWidget;
