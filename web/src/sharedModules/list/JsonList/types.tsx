import { Observable, Subject } from 'rxjs';
import List, { IMeta } from '@modules/list/model';
import { ListModelInput } from '@modules/graphql/list/list-gql';

export interface paramsModalList {
  width: number
  title: string
}

export interface IListEditData {
  context: string,
  jsonSchema: any,
  uiSchema: any,
  data: List
  paramsModal?: paramsModalList
  parentId?: string
}

export interface IEditJsonList {
  getVisible(): Subject<boolean>
  getSubject(): Subject<IListEditData>
  setVisible(value: boolean): void
  save(values: ListModelInput): Observable<List<IMeta[]> | undefined>
}
