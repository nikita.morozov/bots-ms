import { inject, injectable } from 'inversify';
import { mergeMap } from 'rxjs/operators';
import { from, map, Observable, ReplaySubject, Subject } from 'rxjs';
import StreamHandler from '@modules/Stream/service/StreamHandler';
import { ApiGqlClientKey } from '@modules/gqlClient/types';
import GqlClient from '@modules/gqlClient';
import List from '@modules/list/model';
import {
  ListAddDocument,
  ListAddMutation,
  ListAddMutationVariables,
  ListGetByContextDocument,
  ListGetByContextQuery,
  ListGetByContextQueryVariables,
  ListListContextDocument,
  ListListContextQuery,
  ListListContextQueryVariables,
  ListListContextsDocument,
  ListListContextsQuery,
  ListListContextsQueryVariables,
  ListModelInput,
  ListRemoveByIdDocument,
  ListRemoveByIdMutation,
  ListRemoveByIdMutationVariables,
  ListUpdateDocument,
  ListUpdateMutation,
  ListUpdateMutationVariables,
} from '@modules/graphql/list/list-gql';
import { IListEditData } from '@modules/list/JsonList/types';

export interface IListService<MetaData> {
  update(input: ListModelInput): Observable<List<MetaData> | undefined>;
  create(input: ListModelInput): Observable<List<MetaData> | undefined>;
  listByContext(context: string): Observable<List<MetaData>>;
  getByContext(context: string): Observable<List<MetaData> | undefined>;
  listByContexts(contexts: string[]): Observable<List<MetaData>>;
  removeById(id: number): Observable<void>;
  getSubject(): Subject<IListEditData>;
  getSubscriberSubject(): Subject<List<MetaData>>;
  getVisible(): Subject<boolean>;
  setVisible(value: boolean): void;
}

@injectable()
class ListService<MetaData = {}> extends StreamHandler<List> implements IListService<MetaData> {
  public static diKey = Symbol.for('ListMsServiceKey');
  private api: GqlClient;
  private editSubject: Subject<IListEditData>;
  private visible: Subject<boolean>;
  private subscribeSubject: Subject<List<MetaData>>;

  public constructor(
    @inject(ApiGqlClientKey) api: GqlClient,
  ) {
    super();
    this.api = api;
    this.editSubject = new ReplaySubject<IListEditData>(1);
    this.subscribeSubject = new Subject<List<MetaData>>();
    this.visible = new Subject<boolean>();
  }

  //subject
  getSubject(): Subject<IListEditData> {
    return this.editSubject;
  }

  getVisible(): Subject<boolean> {
    return this.visible;
  }

  setVisible(value: boolean) {
    this.visible.next(value);
  }

  getSubscriberSubject(): Subject<List<MetaData>> {
    return this.subscribeSubject;
  }

  getByContext(context: string): Observable<List<MetaData> | undefined> {
    return this.api.query$<ListGetByContextQuery, ListGetByContextQueryVariables>(
      ListGetByContextDocument,
      { context },
      { noCache: true },
    ).pipe(map(i => i.listGetByContext && new List(i.listGetByContext)));
  }

  listByContext(context: string): Observable<List<MetaData>> {
    return this.api.query$<ListListContextQuery, ListListContextQueryVariables>(
      ListListContextDocument,
      { context },
      { noCache: true },
    ).pipe(
      mergeMap(i => from(i.listListContext?.map(i => {
        return new List<MetaData>(i);
      }))),
    );
  }

  listByContexts(contexts: string[]): Observable<List<MetaData>> {
    return this.api.query$<ListListContextsQuery, ListListContextsQueryVariables>(
      ListListContextsDocument,
      { contexts },
      { noCache: true },
    ).pipe(
      mergeMap(i => from(i.listListContexts?.map(i => {
        return new List<MetaData>(i);
      }))),
    );
  }

  update(input: ListModelInput): Observable<List<MetaData> | undefined> {
    return this.api.mutation$<ListUpdateMutation, ListUpdateMutationVariables>(
      ListUpdateDocument,
      { in: input },
      { context: 'common' },
    ).pipe(map(i => {
      const item = i.listUpdate;
      if (!item) {
        return;
      }

      this.visible.next(false);
      return new List<MetaData>(item);
    }));
  }

  create(input: ListModelInput): Observable<List<MetaData> | undefined> {
    return this.api.mutation$<ListAddMutation, ListAddMutationVariables>(
      ListAddDocument,
      { in: input },
    ).pipe(map(i => {
      const item = i.listAdd;
      if (!item) {
        return;
      }

      return new List<MetaData>(item);
    }));
  }

  removeById(id: number): Observable<void> {
    return this.api.mutation$<ListRemoveByIdMutation, ListRemoveByIdMutationVariables>(
      ListRemoveByIdDocument,
      { id },
    ).pipe(map(i => undefined));
  }
}


export default ListService;
