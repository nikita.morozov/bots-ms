import { inject, injectable } from 'inversify';
import { Observable, Subject } from 'rxjs';
import { ListModelInput } from '@modules/graphql/list/list-gql';
import List from '@modules/list/model';
import ListService, { IListService } from '@modules/list/service';
import { IListEditData } from '@modules/list/JsonList/types';

export interface IListUsecase<MetaData> {
  save(input: ListModelInput): Observable<List<MetaData> | undefined>;
  listByContext(context: string): Observable<List<MetaData>>;
  getByContext(context: string): Observable<List<MetaData> | undefined>;
  listByContexts(contexts: string[]): Observable<List<MetaData>>;
  getSubject(): Subject<IListEditData>;
  getSubscriberSubject(): Subject<List<MetaData>>
  getVisible(): Subject<boolean>;
  setVisible(value: boolean): void
}

@injectable()
class ListUsecase<MetaData = {}> implements IListUsecase<MetaData> {
  public static diKey = Symbol.for('ListServiceDiKey');

  private listService: IListService<MetaData>;

  constructor(
    @inject(ListService.diKey) listService: IListService<MetaData>,
  ) {
    this.listService = listService;
  }

  getSubject(): Subject<IListEditData> {
    return this.listService.getSubject();
  }

  getVisible(): Subject<boolean> {
    return this.listService.getVisible();
  }

  setVisible(value: boolean) {
    this.listService.setVisible(value);
  }

  getSubscriberSubject(): Subject<List<MetaData>> {
    return this.listService.getSubscriberSubject();
  }

  save(input: ListModelInput): Observable<List<MetaData> | undefined> {
    if (!!input.id) {
      return this.listService.update(input);
    } else {
      return this.listService.create(input)
    }
  }

  getByContext(context: string): Observable<List<MetaData> | undefined> {
    return this.listService.getByContext(context);
  }

  listByContexts(contexts: string[]): Observable<List<MetaData>> {
    return this.listService.listByContexts(contexts);
  }

  listByContext(context: string): Observable<List<MetaData>> {
    return this.listService.listByContext(context);
  }
}

export default ListUsecase;
