import StreamHandler from '@modules/Stream/service/StreamHandler';
import List from '@modules/list/model';
import { IListService } from '@modules/list/service';
import HttpClient from '@modules/httpClient';
import { inject } from 'inversify';
import { ApiHttpClientKey } from '@modules/httpClient/constants';
import { ListModelInput } from '@modules/graphql/list/list-gql';
import { from, map, mergeMap, Observable, ReplaySubject, Subject } from 'rxjs';
import { IListEditData } from '@modules/list/JsonList/types';
import { ResultItemRes, ResultListRes } from '@modules/types/http';
import { BaseCommandProps } from '@modules/httpClient/types';

class ListHttpService<MetaData = {}> extends StreamHandler<List> implements IListService<MetaData> {
  private api: HttpClient;
  private editSubject: Subject<IListEditData>;
  private visible: Subject<boolean>;
  private subscribeSubject: Subject<List<MetaData>>;

  public constructor(
    @inject(ApiHttpClientKey) api: HttpClient,
  ) {
    super();
    this.api = api;
    this.editSubject = new ReplaySubject<IListEditData>(1);
    this.subscribeSubject = new Subject<List<MetaData>>();
    this.visible = new Subject<boolean>();
  }

  create(input: ListModelInput): Observable<List<MetaData> | undefined> {
    return this.api.$request<object, ResultItemRes<List<MetaData>>>(
      `/v1/list/admin/create`, input, { method: 'POST' }
    ).pipe(map(i => i.item));
  }

  getByContext(context: string): Observable<List<MetaData> | undefined> {
    return this.api.$request<object, ResultItemRes<List<MetaData>>>(
      `/v1/list/item/${context}`, {}, { method: 'GET' },
    ).pipe(map(i => new List<MetaData>(i.item)));
  }

  getSubject(): Subject<IListEditData> {
    return this.editSubject;
  }

  getSubscriberSubject(): Subject<List<MetaData>> {
    return this.subscribeSubject;
  }

  getVisible(): Subject<boolean> {
    return this.visible;
  }

  listByContext(context: string): Observable<List<MetaData>> {
    return this.api.$request<object, ResultListRes<List<MetaData>>>(
      `/v1/list/by-context`, { context }, { method: 'POST' },
    ).pipe(mergeMap(i => from(i.list.map(x => new List<MetaData>(x)))));
  }

  listByContexts(contexts: string[]): Observable<List<MetaData>> {
    return this.api.$request<object, ResultListRes<List<MetaData>>>(
      `/v1/list/by-contexts`, { contexts }, { method: 'POST' }
    ).pipe(mergeMap(i => from(i.list.map(x => new List<MetaData>(x)))));
  }

  removeById(id: number): Observable<void> {
    return this.api.$request<object, void & BaseCommandProps>(
      `/v1/list`, { id }, { method: 'DELETE' },
    );
  }

  setVisible(value: boolean): void {
    this.visible.next(value);
  }

  update(input: ListModelInput): Observable<List<MetaData> | undefined> {
    return this.api.$request<object, ResultItemRes<List<MetaData>>>(
      `/v1/list`, input, { method: 'PUT' },
    ).pipe(map(i => new List<MetaData>(i.item)));
  }
}

export default ListHttpService;