import { AsyncContainerModule, interfaces } from 'inversify';
import ListUsecase from '@modules/list/usecase';

export const ListModule = new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<ListUsecase>(ListUsecase.diKey).to(ListUsecase).inSingletonScope();
});