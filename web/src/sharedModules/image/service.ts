import { inject, injectable } from 'inversify';
import { IImageUploadRes } from './models';
import HttpClient from '@modules/httpClient';
import { BaseCommandProps } from '@modules/httpClient/types';
import { ApiHttpClientKey } from '@modules/httpClient/constants';

export interface IImageService {
  upload: (file: File) => Promise<IImageUploadRes>,
  get: (width: number, height: number, key: string) => Promise<File>
}

@injectable()
class ImageService implements IImageService {
  public static diKey = Symbol.for('ImageServiceKey');
  private api: HttpClient;

  public constructor(
    @inject(ApiHttpClientKey) api: HttpClient,
  ) {
    this.api = api;
  }

  get(width: number, height: number, key: string): Promise<File> {
    if (key) {
      return this.api.request<object, File & BaseCommandProps>(
        `/api/v1/image/${width}/${height}/${key}`,
        undefined,
        {
          method: 'get',
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          isFile: true,
        },
      ).then((res) => {
        // const blob = new Blob([res], {type: 'application/json'});
        // const reader = new FileReader();
        // const file = new File([res.data!], 'image', { lastModified: new Date().getTime(), type: res.data!.type })
        //
        // return file;
        return res;
      })
    } else {
      return Promise.reject();
    }
  }

  upload(file: File): Promise<IImageUploadRes> {
    const formData = new FormData();

    formData.append("file", file);

    return this.api.request<object, IImageUploadRes & BaseCommandProps>(
      `/api/v1/image/image-upload`,
      formData,
      {
        method: 'post',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        isFile: true,
      },
    )
  }
}

export default ImageService;