import { AsyncContainerModule, interfaces } from 'inversify';
import ImageUsecase from './usecase';
import ImageService from './service';

export const ImageModule = new AsyncContainerModule(async (bind: interfaces.Bind, unbind: interfaces.Unbind) => {
  bind<ImageService>(ImageService.diKey).to(ImageService);
  bind<ImageUsecase>(ImageUsecase.diKey).to(ImageUsecase).inSingletonScope();
});