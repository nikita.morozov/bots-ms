import copy from 'copy-to-clipboard';
import ToasterMessageHandler from '@modules/toaster/handler';

const CopyToClipboard = (text: string, toaster: ToasterMessageHandler) => {
  copy(text);
  toaster.sendSuccess({
    title: "Success copied!",
  })
};

export default CopyToClipboard
