import validateEmail from '../../tools/validateEmail';

const falseEmails = ["lia93094", "@lia93094", "lia93094@gmail", "test@test.co.in."];
const trueEmails = ["a@a.a", "test@test.test", "test@test.co.in"];

describe('validateEmail ', () => {
  it('test true email', async () => {
    for (let email of trueEmails) {
      const result = validateEmail(email);
      expect(result).toBe(true);
    }
  });

  it('test false email', async () => {
    for (let email of falseEmails) {
      const result = validateEmail(email);
      expect(result).toBe(false);
    }
  });
});
