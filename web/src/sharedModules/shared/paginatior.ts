/* eslint-disable */
import Long from "long";
import _m0 from "protobufjs/minimal";

export const protobufPackage = "aquix.shared";

export interface Paginator {
  offset: number;
  totalCount: number;
  countPerPage: number;
}

export interface ListOptions {
  offset: number;
  limit: number;
  order?: string | undefined;
}

function createBasePaginator(): Paginator {
  return { offset: 0, totalCount: 0, countPerPage: 0 };
}

export const Paginator = {
  encode(
    message: Paginator,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.offset !== 0) {
      writer.uint32(8).int64(message.offset);
    }
    if (message.totalCount !== 0) {
      writer.uint32(16).int64(message.totalCount);
    }
    if (message.countPerPage !== 0) {
      writer.uint32(24).int32(message.countPerPage);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Paginator {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBasePaginator();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.offset = longToNumber(reader.int64() as Long);
          break;
        case 2:
          message.totalCount = longToNumber(reader.int64() as Long);
          break;
        case 3:
          message.countPerPage = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Paginator {
    return {
      offset: isSet(object.offset) ? Number(object.offset) : 0,
      totalCount: isSet(object.totalCount) ? Number(object.totalCount) : 0,
      countPerPage: isSet(object.countPerPage)
        ? Number(object.countPerPage)
        : 0,
    };
  },

  toJSON(message: Paginator): unknown {
    const obj: any = {};
    message.offset !== undefined && (obj.offset = Math.round(message.offset));
    message.totalCount !== undefined &&
      (obj.totalCount = Math.round(message.totalCount));
    message.countPerPage !== undefined &&
      (obj.countPerPage = Math.round(message.countPerPage));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Paginator>, I>>(
    object: I
  ): Paginator {
    const message = createBasePaginator();
    message.offset = object.offset ?? 0;
    message.totalCount = object.totalCount ?? 0;
    message.countPerPage = object.countPerPage ?? 0;
    return message;
  },
};

function createBaseListOptions(): ListOptions {
  return { offset: 0, limit: 0, order: undefined };
}

export const ListOptions = {
  encode(
    message: ListOptions,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.offset !== 0) {
      writer.uint32(8).int32(message.offset);
    }
    if (message.limit !== 0) {
      writer.uint32(16).int32(message.limit);
    }
    if (message.order !== undefined) {
      writer.uint32(26).string(message.order);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListOptions {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseListOptions();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.offset = reader.int32();
          break;
        case 2:
          message.limit = reader.int32();
          break;
        case 3:
          message.order = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListOptions {
    return {
      offset: isSet(object.offset) ? Number(object.offset) : 0,
      limit: isSet(object.limit) ? Number(object.limit) : 0,
      order: isSet(object.order) ? String(object.order) : undefined,
    };
  },

  toJSON(message: ListOptions): unknown {
    const obj: any = {};
    message.offset !== undefined && (obj.offset = Math.round(message.offset));
    message.limit !== undefined && (obj.limit = Math.round(message.limit));
    message.order !== undefined && (obj.order = message.order);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ListOptions>, I>>(
    object: I
  ): ListOptions {
    const message = createBaseListOptions();
    message.offset = object.offset ?? 0;
    message.limit = object.limit ?? 0;
    message.order = object.order ?? undefined;
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;

export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin
  ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & Record<
        Exclude<keyof I, KeysOfUnion<P>>,
        never
      >;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
