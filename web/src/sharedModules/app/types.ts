export interface IAppConfig {
  authUrlServer: string,
  imageUrlServer: string,
  authSSOClient: string,
  app: string,
  org: string,
  imageStorageUrl: string
}