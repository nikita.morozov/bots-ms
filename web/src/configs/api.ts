import {injectable} from 'inversify';
import { ApiClientMiddleware, IApiConfig } from '@sharedModules/client/types';

@injectable()
abstract class BaseConfig implements IApiConfig {
    private middlewares: ApiClientMiddleware[] = [];

    constructor() {
    }

    getMiddlewares(): ApiClientMiddleware[] {
        return this.middlewares;
    }

    getDebug(): boolean {
        return import.meta.env.NODE_ENV === 'development';
    }

    getHost(): string {
        return '';
    }
}

@injectable()
export class ServerApiConfigImpl extends BaseConfig {
    constructor() {
        super();
    }

    getHost(): string {
        return <string>import.meta.env.VITE_PUBLIC_SERVER_API_DOMAIN;
    }
}

@injectable()
export class ClientApiConfigImpl extends BaseConfig {
    constructor() {
        super();
    }

    getHost(): string {
        return <string>import.meta.env.VITE_PUBLIC_API_DOMAIN;
    }
}