class Routers {
  public static bots = '/private/bots';
  public static botUsers = '/private/bot-user';
  public static events = '/private/events';
  public static scenarios = '/private/scenarios';
  public static scenarioItem = '/private/scenarios/:id';
  public static stateMachine = '/private/state-machine';
  public static stateMachineItem = '/private/state-machine/:id';

  public static getValue(key: keyof Routers) {
    return this[key];
  }
}

export default Routers;