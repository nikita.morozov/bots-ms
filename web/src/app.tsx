import 'reflect-metadata';

import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import DiProvider from '@di/provider';
import routers from '@configs/routers';
import AppProvider from '@local/app/provider';

import '@assets/styles/resizable.css';
import '@assets/styles/main.css';

const Bots = React.lazy(() => import('@containers/bots'));
const BotsUser = React.lazy(() => import('@containers/bot-user'));
const Events = React.lazy(() => import('@containers/events'));
const Scenarios = React.lazy(() => import('@containers/scenarios'));
const ScenarioItem = React.lazy(() => import('@containers/scenarioItem'));
const StateMachine = React.lazy(() => import('@containers/state-machine'));
const StateMachineItem = React.lazy(() => import('@containers/stateMachineItem'));

const cc = document.getElementById('app');
if (!cc) throw new Error('Failed to find the root element');
const root = ReactDOM.createRoot(cc);

root.render(
  <DiProvider>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Navigate to={routers.bots} />} />
        <Route path={routers.bots} element={<React.Suspense fallback={<>...</>}><Bots /></React.Suspense>} />
        <Route path={routers.botUsers} element={<React.Suspense fallback={<>...</>}><BotsUser /></React.Suspense>} />
        <Route path={routers.events} element={<React.Suspense fallback={<>...</>}><Events /></React.Suspense>} />
        <Route path={routers.scenarios} element={<React.Suspense fallback={<>...</>}><Scenarios /></React.Suspense>} />
        <Route path={routers.scenarioItem} element={<React.Suspense fallback={<>...</>}><ScenarioItem /></React.Suspense>} />
        <Route path={routers.stateMachine}
               element={<React.Suspense fallback={<>...</>}><StateMachine /></React.Suspense>} />
        <Route path={routers.stateMachineItem}
               element={<React.Suspense fallback={<>...</>}><StateMachineItem /></React.Suspense>} />
      </Routes>
    </BrowserRouter>
    <AppProvider />
  </DiProvider>,
);