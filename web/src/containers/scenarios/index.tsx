import ScenarioTable from '@components/tables/scenarios/index';
import MainLayout from '@layouts/main';
import React from 'react';

const Page = () => {
  return (
    <MainLayout>
      <ScenarioTable />
    </MainLayout>
  );
};

export default Page;