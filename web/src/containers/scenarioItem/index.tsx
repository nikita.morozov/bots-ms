import EditorLayout from '@components/layout/editor';
import routers from '@configs/routers';
import MainLayout from '@layouts/main';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import ScenarioUsecase, { IScenarioUsecase } from '@local/modules/scenario/usecase';
import { IdRequest } from '@modules/commonTypes';
import { Breadcrumb, Layout } from 'antd';
import { useInjection } from 'inversify-react';
import React, { CSSProperties, useEffect, useMemo, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

const Page = () => {
  let { id } = useParams();
  const navigate = useNavigate();
  const engine = useInjection<ScenarioEngineMV>(ScenarioEngineMV.diKey);
  const scenarioUsecase = useInjection<IScenarioUsecase>(ScenarioUsecase.diKey);
  const [scenarioModel, setScenarioModel] = useState<{ title: string }>();

  useEffect(() => {
    if (id) {
      scenarioUsecase
        .getById(IdRequest.fromPartial({ id: parseInt(id, 10) }))
        .subscribe(values => {
          engine.loadLayerModel(values);
          setScenarioModel(values);
        });
    }
  }, [id]);

  const [fullScreen, setFullScreen] = useState(false);

  const editorStyles = useMemo(() => {
    const fullScreenProps = fullScreen && {
      position: fullScreen ? 'absolute' : 'initial',
      left: 0,
      top: 0,
      zIndex: 2000,
    } || {};

    return { width: '100%', height: '100%', ...fullScreenProps as CSSProperties };
  }, [fullScreen]);

  return (
    <>
      <style jsx global>{`#main-content { z-index: auto; }`}</style>
      <MainLayout>
        <Layout.Content className="top-bar">
          <Breadcrumb>
            <Breadcrumb.Item>
              <a href="#" onClick={e => {
                e.preventDefault();
                navigate(routers.scenarios);
              }}>Scenarios</a>
            </Breadcrumb.Item>
            <Breadcrumb.Item>{scenarioModel ? scenarioModel.title : ''}</Breadcrumb.Item>
          </Breadcrumb>
        </Layout.Content>
        <EditorLayout />
      </MainLayout>
    </>
  );
};

export default Page;

