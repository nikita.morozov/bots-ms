import React from 'react';
import MainLayout from '@layouts/main';
import BotLayout from '@components/layout/bot';

const Page = () => (
  <MainLayout>
    <React.Suspense fallback="Loading...">
      <BotLayout />
    </React.Suspense>
  </MainLayout>
);

export default Page;