import StateMachineServiceTable from '@components/tables/stateMachineService/index';
import MainLayout from '@layouts/main';
import React from 'react';

const Page = () => {
  return (
    <MainLayout>
      <StateMachineServiceTable />
    </MainLayout>
  );
};

export default Page;