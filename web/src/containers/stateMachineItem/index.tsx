import MachineEditorLayout from '@components/layout/stateMachineEditor';
import routers from '@configs/routers';
import MainLayout from '@layouts/main';
import { StateMachine } from '@local/modules/proto/stateMachine';
import StateMachineServiceUsecase, { IStateMachineServiceUsecase } from '@local/modules/stateMachineService/usecase';
import { IdRequest } from '@modules/commonTypes';
import { Breadcrumb, Layout } from 'antd';
import { useInjection } from 'inversify-react';
import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

const Page = () => {
  let { id } = useParams();
  const navigate = useNavigate();
  const stateMachineServiceUsecase = useInjection<IStateMachineServiceUsecase>(StateMachineServiceUsecase.diKey);
  const [stateMachineModel, setStateMachineModel] = useState<StateMachine>();

  useEffect(() => {
    if (id) {
      stateMachineServiceUsecase
        .getById(IdRequest.fromPartial({ id: parseInt(id, 10) }))
        .subscribe(values => {
          setStateMachineModel(values);
        });
    }
  }, [id]);

  return (
    <>
      <style jsx global>{`#main-content { z-index: auto; }`}</style>
      <MainLayout>
        <Layout.Content className="top-bar">
          <Breadcrumb>
            <Breadcrumb.Item>
              <a href="#" onClick={e => {
                e.preventDefault();
                navigate(routers.stateMachine);
              }}>State Machines</a>
            </Breadcrumb.Item>
            <Breadcrumb.Item>{stateMachineModel ? stateMachineModel.name : ''}</Breadcrumb.Item>
          </Breadcrumb>
        </Layout.Content>
        <MachineEditorLayout stateMachine={stateMachineModel} />
      </MainLayout>
    </>
  );
};

export default Page;