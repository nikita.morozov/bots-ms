import React from 'react';
import MainLayout from '@layouts/main';
import EventLayout from '@components/layout/event';

const Page = () => (
  <MainLayout>
    <EventLayout />
  </MainLayout>
);

export default Page;