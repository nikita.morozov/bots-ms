import React from 'react';
import BotUserLayout from '@components/layout/botUser';
import MainLayout from '@layouts/main';

const Page = () => (
  <MainLayout>
    <BotUserLayout />
  </MainLayout>
);

export default Page;