import React, { FC } from 'react';
import { Pagination } from 'antd';
import { observer } from 'mobx-react-lite';
import { IPaginatorComponent } from '@sharedModules/Stream/components/Paginator';

interface ILocalPaginatorComponentProps extends IPaginatorComponent {
  classNamePagination?: string,
  showSizeChanger?: boolean

  onChangeSize?(page: number, size: number): void
}

const PaginatorComponent: FC<ILocalPaginatorComponentProps> = (props) => {
  const {
    paginator,
    onPageChanged,
    onChangeSize,
    showSizeChanger = true,
    classNamePagination,
  } = props;

  if (!paginator.visible) {
    return null;
  }

  const current = paginator.page;

  return (
    <Pagination
      className={classNamePagination}
      style={{ display: 'flex', justifyContent: 'flex-end' }}
      current={current + 1}
      pageSize={paginator.itemsPerPage}
      onChange={(page) => {
        onPageChanged(page - 1)
      }}
      total={paginator.totalCount}
      showSizeChanger={showSizeChanger}
      onShowSizeChange={(page, size) => showSizeChanger && onChangeSize && onChangeSize(page, size)}
    />
  );
};


export default observer(PaginatorComponent)