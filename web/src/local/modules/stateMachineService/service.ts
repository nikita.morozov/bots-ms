import { UInt64Value } from '@local/modules/proto/commonTypes';
import { ImportObject } from '@local/modules/stateMachineService/types';
import { ApiHttpClientKey } from '@modules/httpClient/constants';
import HttpClient from '@modules/httpClient/index';
import { ApiClient } from '@sharedModules/grpcClient';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import { Empty } from '@sharedModules/shared/empty';
import { ListOptions } from '@sharedModules/shared/paginatior';
import StreamHandler from '@sharedModules/Stream/service/StreamHandler';
import { inject, injectable } from 'inversify';
import { map, mergeMap, Observable } from 'rxjs';
import { StateMachine, StateMachineResp } from '../proto/stateMachine';
import {
  StateMachineServiceCountDesc,
  StateMachineServiceCreateDesc,
  StateMachineServiceDeleteDesc,
  StateMachineServiceListDesc,
  StateMachineServiceUpdateDesc,
  StateMachineServiceGetDesc,
} from '../proto/stateMachineServices';

export interface IStateMachineService {
  create(request: StateMachine): Observable<IdRequest>,
  update(request: StateMachine): Observable<void>,
  delete(request: IdRequest): Observable<void>,
  importJson(values: ImportObject): void,
  list(request: ListOptions): Observable<StateMachineResp>,
  getById(request: IdRequest): Observable<StateMachine>,
  count(): Observable<UInt64Value>,
}

@injectable()
class StateMachineService extends StreamHandler<StateMachine> implements IStateMachineService {
  public static diKey = Symbol.for('StateMachineService');
  private api: ApiClient;
  private http: HttpClient;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
    @inject(ApiHttpClientKey) http: HttpClient,
  ) {
    super();
    this.api = api;
    this.http = http;
    this.add('default', this.streamDefault);
  }

  private streamDefault = {
    stream: (opts: ListOptions): Observable<StateMachine> => {
      return this.list(opts).pipe(mergeMap(e => e.items));
    },
    loadCount: (): Observable<number> => {
      return this.count().pipe(map(res => res.value));
    },
  };

  create(request: StateMachine): Observable<IdRequest> {
    return this.api.unary$(
      StateMachineServiceCreateDesc,
      {
        obj: request,
        coder: StateMachine,
      },
    );
  }

  delete(request: IdRequest): Observable<void> {
    return this.api.unary$(
      StateMachineServiceDeleteDesc,
      {
        obj: request,
        coder: IdRequest,
      },
    );
  }

  export(request: IdRequest): Observable<void> {
    return this.api.unary$(
      StateMachineServiceDeleteDesc,
      {
        obj: request,
        coder: IdRequest,
      },
    );
  }

  importJson(values: ImportObject): void {
    const form = new FormData();
    form.append('botId', values.botId.toString());
    form.append('file', values.file);

    this.http.request(
      '/api/v1/serialization/import',
      form,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
  }

  list(request: ListOptions): Observable<StateMachineResp> {
    return this.api.stream$(
      StateMachineServiceListDesc,
      {
        obj: request,
        coder: ListOptions,
      },
    );
  }

  getById(request: IdRequest): Observable<StateMachine> {
    return this.api.unary$(
      StateMachineServiceGetDesc,
      {
        obj: request,
        coder: IdRequest,
      },
    );
  }

  update(request: StateMachine): Observable<void> {
    return this.api.unary$(
      StateMachineServiceUpdateDesc,
      {
        obj: request,
        coder: StateMachine,
      },
    );
  }

  count(): Observable<UInt64Value> {
    return this.api.unary$(
      StateMachineServiceCountDesc,
      {
        obj: {},
        coder: Empty,
      },
    );
  }
}

export default StateMachineService;