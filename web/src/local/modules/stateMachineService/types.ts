export interface ImportObject {
  botId: number,
  file: File,
}