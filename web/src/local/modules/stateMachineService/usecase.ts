import { ImportObject } from '@local/modules/stateMachineService/types';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import { ListOptions } from '@sharedModules/shared/paginatior';
import ListMV from '@sharedModules/Stream/model/ListMV';
import { PageService } from '@sharedModules/Stream/service/ListService';
import { IBaseListUsecase, IPageService } from '@sharedModules/Stream/types';
import { inject, injectable } from 'inversify';
import { Observable } from 'rxjs';
import { StateMachine, StateMachineResp } from '../proto/stateMachine';
import StateMachineService from './service';

export interface IStateMachineServiceUsecase {
  create(request: StateMachine): Observable<IdRequest>,

  update(request: StateMachine): Observable<void>,

  delete(request: IdRequest): Observable<void>,

  importJson(values: ImportObject): void,

  list(request: ListOptions): Observable<StateMachineResp>,
  getById(request: IdRequest): Observable<StateMachine>,
}

@injectable()
class StateMachineServiceUsecase implements IStateMachineServiceUsecase, IBaseListUsecase<StateMachine> {
  public static diKey = Symbol.for('StateMachineServiceUsecaseDiKey');

  private service: StateMachineService;
  private pageService: IPageService<StateMachine>;

  constructor(
    @inject(StateMachineService.diKey) service: StateMachineService,
  ) {
    this.service = service;
    this.pageService = new PageService(this.service);
  }

  create(request: StateMachine): Observable<IdRequest> {
    return this.service.create(request);
  }

  delete(request: IdRequest): Observable<void> {
    return this.service.delete(request);
  }

  importJson(values: ImportObject): void {
    this.service.importJson(values);
  }

  list(request: ListOptions): Observable<StateMachineResp> {
    return this.service.list(request);
  }

  update(request: StateMachine): Observable<void> {
    return this.service.update(request);
  }

  getById(request: IdRequest): Observable<StateMachine> {
    return this.service.getById(request);
  }

  loadItems(model: ListMV<StateMachine>, opts: ListOptions, method?: string, query?: object): Observable<StateMachine> {
    return this.pageService.loadPage(model, opts, method, query);
  }

  loadTotalCount(model: ListMV<StateMachine>, method?: string, query?: object): Observable<number> {
    return this.pageService.loadTotalCount(model, method, query);
  }
}

export default StateMachineServiceUsecase;