import { inject, injectable } from 'inversify';
import { ApiClient } from '@sharedModules/grpcClient';
import { ConnectorModel, ConnectorReq } from '@local/modules/proto/botCommon';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import { Observable } from 'rxjs';
import { ConnectorCreateDesc, ConnectorDeleteDesc, ConnectorListDesc } from '@local/modules/proto/connector';

export interface IConnectorService {
  create(request: ConnectorModel): Observable<IdRequest>;
  delete(request: IdRequest): Observable<void>;
  list(request: ConnectorReq): Observable<ConnectorModel>;
}

@injectable()
class ConnectorService implements IConnectorService {
  public static diKey = Symbol.for('ConnectorServiceKey');
  private api: ApiClient;

  constructor(
    @inject(ApiClient.diKey) api: ApiClient,
  ) {
    this.api = api;
  }

  create(request: ConnectorModel): Observable<IdRequest> {
    return this.api.unary$(
      ConnectorCreateDesc,
      {
        obj: request,
        coder: ConnectorModel,
      }
    )
  }

  delete(request: IdRequest): Observable<void> {
    return this.api.unary$(
      ConnectorDeleteDesc,
      {
        obj: request,
        coder: IdRequest,
      }
    );
  }

  list(request: ConnectorReq): Observable<ConnectorModel> {
    return this.api.stream$(ConnectorListDesc, { obj: request, coder: ConnectorReq });
  }
}

export default ConnectorService;