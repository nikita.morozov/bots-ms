import { ContainerModule, interfaces } from 'inversify';
import ConnectorService, { IConnectorService } from './service';
import ConnectorUsecase, { IConnectorUsecase } from './usecase';

export const ConnectorModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<IConnectorService>(ConnectorService.diKey).to(ConnectorService);

  bind<IConnectorUsecase>(ConnectorUsecase.diKey).to(ConnectorUsecase).inSingletonScope();
});