import { ConnectorModel, ConnectorReq } from '../proto/botCommon';
import { inject, injectable } from 'inversify';
import ConnectorService, { IConnectorService } from './service';
import { Observable } from 'rxjs';
import { IdRequest } from '@sharedModules/shared/commonTypes';

export interface IConnectorUsecase {
  create(request: ConnectorModel): Observable<IdRequest>;

  delete(request: IdRequest): Observable<void>;

  list(request: ConnectorReq): Observable<ConnectorModel>;
}

@injectable()
class ConnectorUsecase implements IConnectorUsecase {
  public static diKey = Symbol.for('ConnectorUsecaseDiKey');

  private connectorService: IConnectorService;

  constructor(
    @inject(ConnectorService.diKey) connectorService: IConnectorService,
  ) {
    this.connectorService = connectorService;
  }

  create(request: ConnectorModel): Observable<IdRequest> {
    return this.connectorService.create(request);
  }

  delete(request: IdRequest): Observable<void> {
    return this.connectorService.delete(request);
  }

  list(request: ConnectorReq): Observable<ConnectorModel> {
    return this.connectorService.list(request);
  }
}

export default ConnectorUsecase;