import { ScenarioActionBatch, ScenarioActionModel, ScenarioActionReq } from '../proto/botCommon';
import { inject, injectable } from 'inversify';
import ScenarioActionService, { IScenarioActionService } from './service';
import { Observable } from 'rxjs';
import { IdRequest } from '@sharedModules/shared/commonTypes';

export interface IScenarioActionUsecase {
  create(request: ScenarioActionModel): Observable<IdRequest>,
  delete(request: IdRequest): Observable<void>,
  list(request: ScenarioActionReq): Observable<ScenarioActionModel>,
  batchUpdate(request: ScenarioActionBatch): Observable<void>,
  update(request: ScenarioActionModel): Observable<void>,
}

@injectable()
class ScenarioActionUsecase implements IScenarioActionUsecase {
  public static diKey = Symbol.for('ScenarioActionUsecaseDiKey');

  private scenarioActionService: IScenarioActionService;

  constructor(
    @inject(ScenarioActionService.diKey) scenarioActionService: IScenarioActionService,
  ) {
    this.scenarioActionService = scenarioActionService;
  }

  create(request: ScenarioActionModel): Observable<IdRequest> {
    return this.scenarioActionService.create(request);
  }

  delete(request: IdRequest): Observable<void> {
    return this.scenarioActionService.delete(request);
  }

  list(request: ScenarioActionReq): Observable<ScenarioActionModel> {
    return this.scenarioActionService.list(request);
  }

  batchUpdate(request: ScenarioActionBatch): Observable<void> {
    return this.scenarioActionService.batchUpdate(request);
  }

  update(request: ScenarioActionModel): Observable<void> {
    return this.scenarioActionService.update(request);
  }
}

export default ScenarioActionUsecase;