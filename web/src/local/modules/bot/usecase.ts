import { inject, injectable } from 'inversify';
import BotService from './service';
import { BotListResp, BotModel } from '@local/modules/proto/botCommon';
import { Observable, Subscription } from 'rxjs';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import { ListOptions } from '@sharedModules/shared/paginatior';
import { IBaseListUsecase, IPageService } from '@sharedModules/Stream/types';
import { PageService } from '@sharedModules/Stream/service/ListService';
import ListMV from '@sharedModules/Stream/model/ListMV';

export interface IBotUsecase {
  create: (req: BotModel) => Observable<BotModel>,
  update: (req: BotModel) => Observable<void>,
  remove: (req: IdRequest) => Observable<void>,
  load: (req: ListOptions) => Observable<BotListResp>,
}

@injectable()
class BotUsecase implements IBotUsecase, IBaseListUsecase<BotModel> {
  public static diKey = Symbol.for('BotUsecaseDiKey');

  private botService: BotService;
  private pageService: IPageService<BotModel>;

  constructor(
    @inject(BotService.diKey) botService: BotService,
  ) {
    this.botService = botService;
    this.pageService = new PageService(this.botService);
  }

  create(req: BotModel): Observable<BotModel> {
    return this.botService.create(req);
  }

  load(req: ListOptions): Observable<BotListResp> {
    return this.botService.load(req);
  }

  remove(req: IdRequest): Observable<void> {
    return this.botService.remove(req)
  }

  update(req: BotModel): Observable<void> {
    return this.botService.update(req);
  }

  loadItems(model: ListMV<BotModel>, opts: ListOptions, method?: string, query?: object): [Observable<BotModel>, Subscription] {
    return this.pageService.loadPage(model, opts, method, query);
  }

  loadTotalCount(model: ListMV<BotModel>, method?: string, query?: object): [Observable<number>, Subscription] {
    return this.pageService.loadTotalCount(model, method, query);
  }
}

export default BotUsecase;