import { ContainerModule, interfaces } from 'inversify';
import BotService, { IBotService } from './service';
import BotUsecase, { IBotUsecase } from './usecase';

export const BotModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<IBotService>(BotService.diKey).to(BotService);

  bind<IBotUsecase>(BotUsecase.diKey).to(BotUsecase).inSingletonScope();
});