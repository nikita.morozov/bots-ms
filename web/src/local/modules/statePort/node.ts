import { DefaultPortModel } from '@projectstorm/react-diagrams-defaults';
import { PortModel } from '@projectstorm/react-diagrams-core';
import TransitionNode from '../transition/node';
import { action, makeObservable, observable } from 'mobx';

export class TransitionPortNode extends DefaultPortModel {
  @observable
  public transition: TransitionNode;

  constructor(transition: TransitionNode) {
    super(false, `transition_${transition.model().id}_out` );

    this.transition = transition;
    makeObservable(this);
  }

  @action
  createLinkModel(): TransitionNode {
    super.createLinkModel();
    this.transition.setSourcePort(null);
    this.transition.setTargetPort(null);
    return this.transition;
  }
}

export class StatePortNode extends DefaultPortModel {
  constructor(stateId: number) {
    super(true, `state_${stateId}_in`);
  }

  canLinkToPort(port: PortModel): boolean {
    return false;
  }
}