import { ContainerModule, interfaces } from 'inversify';
import TransitionService, { ITransitionService } from './service';
import TransitionUsecase, { ITransitionUsecase } from './usecase';

export const transitionModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<ITransitionService>(TransitionService.diKey).to(TransitionService);

  bind<ITransitionUsecase>(TransitionUsecase.diKey).to(TransitionUsecase).inSingletonScope();
});