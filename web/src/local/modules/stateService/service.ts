import { State, StateBatch } from '../proto/stateMachine';
import { inject, injectable } from 'inversify';
import {
  StateServiceBatchUpdateDesc,
  StateServiceCreateDesc,
  StateServiceDeleteDesc,
  StateServiceListDesc, StateServiceUpdateDesc,
} from '../proto/stateMachineServices';
import { ApiClient } from '@sharedModules/grpcClient';
import { Observable } from 'rxjs';
import { IdRequest } from '@sharedModules/shared/commonTypes';

export interface IStateService {
  create(request: State): Observable<IdRequest>,
  delete(request: IdRequest): Observable<void>,
  list(request: IdRequest): Observable<State>,
  batchUpdate(request: StateBatch): Observable<void>,
  update(request: State): Observable<void>
}

@injectable()
class StateService implements IStateService {
  public static diKey = Symbol.for('StateService');
  private api: ApiClient;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
  ) {
    this.api = api;
  }

  create(request: State): Observable<IdRequest> {
    return this.api.unary$(
      StateServiceCreateDesc,
      {
        obj: request,
        coder: State,
      }
    );
  }

  delete(request: IdRequest): Observable<void> {
    return this.api.unary$(
      StateServiceDeleteDesc,
      {
        obj: request,
        coder: IdRequest,
      }
    );
  }

  list(request: IdRequest): Observable<State> {
    return this.api.stream$(
      StateServiceListDesc,
      {
        obj: request,
        coder: IdRequest,
      }
    );
  }

  batchUpdate(request: StateBatch): Observable<void> {
    return this.api.unary$(
      StateServiceBatchUpdateDesc,
      {
        obj: request,
        coder: StateBatch,
      }
    )
  }

  update(request: State): Observable<void> {
    return this.api.unary$(
      StateServiceUpdateDesc,
      {
        obj: request,
        coder: State,
      }
    )
  }
}

export default StateService;