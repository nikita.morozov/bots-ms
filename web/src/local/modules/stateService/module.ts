import { ContainerModule, interfaces } from 'inversify';
import StateService, { IStateService } from './service';
import StateUsecase, { IStateUsecase } from './usecase';

export const stateServiceModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<IStateService>(StateService.diKey).to(StateService);

  bind<IStateUsecase>(StateUsecase.diKey).to(StateUsecase).inSingletonScope();
});