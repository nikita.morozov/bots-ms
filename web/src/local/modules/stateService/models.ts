import { Point } from '@projectstorm/geometry';
import TransitionNode from '../transition/node';

interface Meta {
  position: Point,
}

export interface StateServiceExtraModel {
  id: number;
  name: string;
  scenarioId: number;
  stateMachineId: number;
  meta: Meta;
  initial: boolean;
  transitions: TransitionNode[],
}