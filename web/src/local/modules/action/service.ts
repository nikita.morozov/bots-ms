import { inject, injectable } from 'inversify';
import { ApiClient } from '@sharedModules/grpcClient';
import { Empty } from '@sharedModules/shared/empty';
import { ActionModel } from '@local/modules/proto/botCommon';
import { ActionListDesc } from '@local/modules/proto/action';
import { Observable } from 'rxjs';

export interface IActionService {
  load: () => Observable<ActionModel>
}

@injectable()
class ActionService implements IActionService{
  public static diKey = Symbol.for('ActionServiceKey');
  private api: ApiClient;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
  ) {
    this.api = api;
  }

  load(): Observable<ActionModel> {
    return this.api.stream$(
      ActionListDesc,
      { obj: {}, coder: Empty },
      { noCache: true }
    );
  }
}

export default ActionService;