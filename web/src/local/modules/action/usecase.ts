import { inject, injectable } from 'inversify';
import ActionService, { IActionService } from './service';
import { Observable } from 'rxjs';
import { ActionModel } from '@local/modules/proto/botCommon';

export interface IActionUsecase {
  load: () => Observable<ActionModel>,
}

@injectable()
class ActionUsecase implements IActionUsecase {
  public static diKey = Symbol.for('ActionUsecaseDiKey');

  private actionService: IActionService;

  constructor(
    @inject(ActionService.diKey) actionService: IActionService,
  ) {
    this.actionService = actionService;
  }

  load(): Observable<ActionModel> {
    return this.actionService.load();
  }
}

export default ActionUsecase;