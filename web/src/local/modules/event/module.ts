import { ContainerModule, interfaces } from 'inversify';
import EventService, { IEventService } from './service';
import EventUsecase, { IEventUsecase } from './usecase';

export const EventModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<IEventService>(EventService.diKey).to(EventService);

  bind<IEventUsecase>(EventUsecase.diKey).to(EventUsecase).inSingletonScope();
});