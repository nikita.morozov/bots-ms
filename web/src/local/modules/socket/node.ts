import { DefaultPortModel } from '@projectstorm/react-diagrams-defaults';
import { SocketModel } from '../proto/botCommon';
import ConnectorLink from '../connector/node';
import { action, makeObservable, observable } from 'mobx';
import ConnectorsModulesVM from '@local/modules/engine/modelViews/connectors';

class SocketNode extends DefaultPortModel {
  @observable
  public readonly id;
  @observable
  private readonly socket: SocketModel = {} as SocketModel;
  @observable
  links = {};

  private connectorsModule: ConnectorsModulesVM;

  constructor(props: SocketModel, connectorsModule: ConnectorsModulesVM) {
    super({
      in: !props.direction,
      name: String(props.id),
      label: props.title,
    });

    this.connectorsModule = connectorsModule;

    this.id = props.id;
    this.socket = props;
    this.socket.manual = props.manual || (this.IsEditable || !props.direction);
    makeObservable(this);
  }

  get model() {
    return this.socket;
  }

  @action
  setIsEditable(val: boolean) {
    this.socket.manual = val;
  }

  private get IsEditable(): boolean {
    return this.socket.type === 0 || this.socket.type === 2;
  }

  @action
  createLinkModel(): ConnectorLink {
    return new ConnectorLink(this.connectorsModule);
  }

  @action
  addLink(link: ConnectorLink) {
    if (link.getID()) {
      this.links[String(link.getID())] = link;
    }
  }

  @action
  removeLink(link: ConnectorLink) {
    if (link.getID()) delete this.links[link.getID()];
  }
}

export default SocketNode;
