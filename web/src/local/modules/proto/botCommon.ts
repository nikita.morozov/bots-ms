/* eslint-disable */
import { messageTypeRegistry } from "./typeRegistry";
import Long from "long";
import _m0 from "protobufjs/minimal";
import { Paginator, ListOptions } from "./paginatior";

export const protobufPackage = "botMs";

export interface ScenarioInitialRequest {
  $type: "botMs.ScenarioInitialRequest";
  scenarioId: number;
  actionId: number;
}

export interface BotModel {
  $type: "botMs.BotModel";
  id: number;
  createdAt: number;
  updatedAt: number;
  token: string;
  slug: string;
  env: string;
  handler: string;
  executorHandler: string;
  allowPlainCommand: boolean;
  disableTriggers: boolean;
  autoRemoveMessages: boolean;
  deletedAt?: number | undefined;
}

export interface BotListResp {
  $type: "botMs.BotListResp";
  items: BotModel[];
  paginator?: Paginator;
}

export interface BotUserModel {
  $type: "botMs.BotUserModel";
  id: number;
  createdAt: number;
  updatedAt: number;
  botId: number;
  userId: string;
  meta: string;
  deletedAt?: number | undefined;
}

export interface BotUserListResp {
  $type: "botMs.BotUserListResp";
  items: BotUserModel[];
  paginator?: Paginator;
}

export interface BotUserList {
  $type: "botMs.BotUserList";
  handler: string;
  opts?: ListOptions | undefined;
}

export interface ActionModel {
  $type: "botMs.ActionModel";
  id: number;
  name: string;
  handler: string;
  scheme: string;
  sockets: SocketModel[];
}

export interface ExecuteReq {
  $type: "botMs.ExecuteReq";
  name: number;
  botSlug: string;
  data: string;
}

export interface EventModel {
  $type: "botMs.EventModel";
  id: number;
  createdAt: number;
  updatedAt: number;
  botId: number;
  scenarioId: number;
  command: string;
  deletedAt?: number | undefined;
}

export interface EvenListResp {
  $type: "botMs.EvenListResp";
  items: EventModel[];
  paginator?: Paginator;
}

export interface ScenarioActionModel {
  $type: "botMs.ScenarioActionModel";
  id: number;
  meta: string;
  scenarioId: number;
  actionId: number;
  data: string;
}

export interface ScenarioActionBatch {
  $type: "botMs.ScenarioActionBatch";
  items: ScenarioActionModel[];
}

export interface ScenarioItemModel {
  $type: "botMs.ScenarioItemModel";
  id: number;
  type: string;
  meta: string;
  scenarioId: number;
}

export interface ScenarioItemBatch {
  $type: "botMs.ScenarioItemBatch";
  items: ScenarioItemModel[];
}

export interface ScenarioActionReq {
  $type: "botMs.ScenarioActionReq";
  scenarioId: number;
  opt?: ListOptions | undefined;
}

export interface ScenarioModel {
  $type: "botMs.ScenarioModel";
  id: number;
  createdAt: number;
  updatedAt: number;
  initial?: number | undefined;
  title: string;
  category: string;
  actions: ScenarioActionModel[];
  deletedAt?: number | undefined;
}

export interface ScenarioModelListResp {
  $type: "botMs.ScenarioModelListResp";
  items: ScenarioModel[];
  paginator?: Paginator;
}

export interface SocketModel {
  $type: "botMs.SocketModel";
  id: number;
  direction: number;
  type: number;
  title: string;
  slug: string;
  actionId: number;
  manual: boolean;
}

export interface ConnectorModel {
  $type: "botMs.ConnectorModel";
  id: number;
  fromScenarioAction: number;
  fromSocket: number;
  toScenarioAction: number;
  toSocket: number;
  scenarioId: number;
}

export interface ConnectorReq {
  $type: "botMs.ConnectorReq";
  scenarioId: number;
}

export interface AuthRequestModel {
  $type: "botMs.AuthRequestModel";
  botId: number;
  meta: string;
  hash: string;
}

export interface ScenarioListReq {
  $type: "botMs.ScenarioListReq";
  opts?: ListOptions | undefined;
}

const baseScenarioInitialRequest: object = {
  $type: "botMs.ScenarioInitialRequest",
  scenarioId: 0,
  actionId: 0,
};

export const ScenarioInitialRequest = {
  $type: "botMs.ScenarioInitialRequest" as const,

  encode(
    message: ScenarioInitialRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.scenarioId !== 0) {
      writer.uint32(8).uint32(message.scenarioId);
    }
    if (message.actionId !== 0) {
      writer.uint32(16).uint32(message.actionId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ScenarioInitialRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseScenarioInitialRequest } as ScenarioInitialRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.scenarioId = reader.uint32();
          break;
        case 2:
          message.actionId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ScenarioInitialRequest {
    const message = { ...baseScenarioInitialRequest } as ScenarioInitialRequest;
    message.scenarioId =
      object.scenarioId !== undefined && object.scenarioId !== null
        ? Number(object.scenarioId)
        : 0;
    message.actionId =
      object.actionId !== undefined && object.actionId !== null
        ? Number(object.actionId)
        : 0;
    return message;
  },

  toJSON(message: ScenarioInitialRequest): unknown {
    const obj: any = {};
    message.scenarioId !== undefined && (obj.scenarioId = message.scenarioId);
    message.actionId !== undefined && (obj.actionId = message.actionId);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ScenarioInitialRequest>
  ): ScenarioInitialRequest {
    const message = { ...baseScenarioInitialRequest } as ScenarioInitialRequest;
    message.scenarioId = object.scenarioId ?? 0;
    message.actionId = object.actionId ?? 0;
    return message;
  },
};

messageTypeRegistry.set(ScenarioInitialRequest.$type, ScenarioInitialRequest);

const baseBotModel: object = {
  $type: "botMs.BotModel",
  id: 0,
  createdAt: 0,
  updatedAt: 0,
  token: "",
  slug: "",
  env: "",
  handler: "",
  executorHandler: "",
  allowPlainCommand: false,
  disableTriggers: false,
  autoRemoveMessages: false,
};

export const BotModel = {
  $type: "botMs.BotModel" as const,

  encode(
    message: BotModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.createdAt !== 0) {
      writer.uint32(16).int64(message.createdAt);
    }
    if (message.updatedAt !== 0) {
      writer.uint32(24).int64(message.updatedAt);
    }
    if (message.token !== "") {
      writer.uint32(34).string(message.token);
    }
    if (message.slug !== "") {
      writer.uint32(42).string(message.slug);
    }
    if (message.env !== "") {
      writer.uint32(50).string(message.env);
    }
    if (message.handler !== "") {
      writer.uint32(58).string(message.handler);
    }
    if (message.executorHandler !== "") {
      writer.uint32(66).string(message.executorHandler);
    }
    if (message.allowPlainCommand === true) {
      writer.uint32(72).bool(message.allowPlainCommand);
    }
    if (message.disableTriggers === true) {
      writer.uint32(80).bool(message.disableTriggers);
    }
    if (message.autoRemoveMessages === true) {
      writer.uint32(88).bool(message.autoRemoveMessages);
    }
    if (message.deletedAt !== undefined) {
      writer.uint32(96).int64(message.deletedAt);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BotModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseBotModel } as BotModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.createdAt = longToNumber(reader.int64() as Long);
          break;
        case 3:
          message.updatedAt = longToNumber(reader.int64() as Long);
          break;
        case 4:
          message.token = reader.string();
          break;
        case 5:
          message.slug = reader.string();
          break;
        case 6:
          message.env = reader.string();
          break;
        case 7:
          message.handler = reader.string();
          break;
        case 8:
          message.executorHandler = reader.string();
          break;
        case 9:
          message.allowPlainCommand = reader.bool();
          break;
        case 10:
          message.disableTriggers = reader.bool();
          break;
        case 11:
          message.autoRemoveMessages = reader.bool();
          break;
        case 12:
          message.deletedAt = longToNumber(reader.int64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): BotModel {
    const message = { ...baseBotModel } as BotModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.createdAt =
      object.createdAt !== undefined && object.createdAt !== null
        ? Number(object.createdAt)
        : 0;
    message.updatedAt =
      object.updatedAt !== undefined && object.updatedAt !== null
        ? Number(object.updatedAt)
        : 0;
    message.token =
      object.token !== undefined && object.token !== null
        ? String(object.token)
        : "";
    message.slug =
      object.slug !== undefined && object.slug !== null
        ? String(object.slug)
        : "";
    message.env =
      object.env !== undefined && object.env !== null ? String(object.env) : "";
    message.handler =
      object.handler !== undefined && object.handler !== null
        ? String(object.handler)
        : "";
    message.executorHandler =
      object.executorHandler !== undefined && object.executorHandler !== null
        ? String(object.executorHandler)
        : "";
    message.allowPlainCommand =
      object.allowPlainCommand !== undefined &&
      object.allowPlainCommand !== null
        ? Boolean(object.allowPlainCommand)
        : false;
    message.disableTriggers =
      object.disableTriggers !== undefined && object.disableTriggers !== null
        ? Boolean(object.disableTriggers)
        : false;
    message.autoRemoveMessages =
      object.autoRemoveMessages !== undefined &&
      object.autoRemoveMessages !== null
        ? Boolean(object.autoRemoveMessages)
        : false;
    message.deletedAt =
      object.deletedAt !== undefined && object.deletedAt !== null
        ? Number(object.deletedAt)
        : undefined;
    return message;
  },

  toJSON(message: BotModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.createdAt !== undefined && (obj.createdAt = message.createdAt);
    message.updatedAt !== undefined && (obj.updatedAt = message.updatedAt);
    message.token !== undefined && (obj.token = message.token);
    message.slug !== undefined && (obj.slug = message.slug);
    message.env !== undefined && (obj.env = message.env);
    message.handler !== undefined && (obj.handler = message.handler);
    message.executorHandler !== undefined &&
      (obj.executorHandler = message.executorHandler);
    message.allowPlainCommand !== undefined &&
      (obj.allowPlainCommand = message.allowPlainCommand);
    message.disableTriggers !== undefined &&
      (obj.disableTriggers = message.disableTriggers);
    message.autoRemoveMessages !== undefined &&
      (obj.autoRemoveMessages = message.autoRemoveMessages);
    message.deletedAt !== undefined && (obj.deletedAt = message.deletedAt);
    return obj;
  },

  fromPartial(object: DeepPartial<BotModel>): BotModel {
    const message = { ...baseBotModel } as BotModel;
    message.id = object.id ?? 0;
    message.createdAt = object.createdAt ?? 0;
    message.updatedAt = object.updatedAt ?? 0;
    message.token = object.token ?? "";
    message.slug = object.slug ?? "";
    message.env = object.env ?? "";
    message.handler = object.handler ?? "";
    message.executorHandler = object.executorHandler ?? "";
    message.allowPlainCommand = object.allowPlainCommand ?? false;
    message.disableTriggers = object.disableTriggers ?? false;
    message.autoRemoveMessages = object.autoRemoveMessages ?? false;
    message.deletedAt = object.deletedAt ?? undefined;
    return message;
  },
};

messageTypeRegistry.set(BotModel.$type, BotModel);

const baseBotListResp: object = { $type: "botMs.BotListResp" };

export const BotListResp = {
  $type: "botMs.BotListResp" as const,

  encode(
    message: BotListResp,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.items) {
      BotModel.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.paginator !== undefined) {
      Paginator.encode(message.paginator, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BotListResp {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseBotListResp } as BotListResp;
    message.items = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.items.push(BotModel.decode(reader, reader.uint32()));
          break;
        case 2:
          message.paginator = Paginator.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): BotListResp {
    const message = { ...baseBotListResp } as BotListResp;
    message.items = (object.items ?? []).map((e: any) => BotModel.fromJSON(e));
    message.paginator =
      object.paginator !== undefined && object.paginator !== null
        ? Paginator.fromJSON(object.paginator)
        : undefined;
    return message;
  },

  toJSON(message: BotListResp): unknown {
    const obj: any = {};
    if (message.items) {
      obj.items = message.items.map((e) =>
        e ? BotModel.toJSON(e) : undefined
      );
    } else {
      obj.items = [];
    }
    message.paginator !== undefined &&
      (obj.paginator = message.paginator
        ? Paginator.toJSON(message.paginator)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<BotListResp>): BotListResp {
    const message = { ...baseBotListResp } as BotListResp;
    message.items = (object.items ?? []).map((e) => BotModel.fromPartial(e));
    message.paginator =
      object.paginator !== undefined && object.paginator !== null
        ? Paginator.fromPartial(object.paginator)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(BotListResp.$type, BotListResp);

const baseBotUserModel: object = {
  $type: "botMs.BotUserModel",
  id: 0,
  createdAt: 0,
  updatedAt: 0,
  botId: 0,
  userId: "",
  meta: "",
};

export const BotUserModel = {
  $type: "botMs.BotUserModel" as const,

  encode(
    message: BotUserModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.createdAt !== 0) {
      writer.uint32(16).int64(message.createdAt);
    }
    if (message.updatedAt !== 0) {
      writer.uint32(24).int64(message.updatedAt);
    }
    if (message.botId !== 0) {
      writer.uint32(32).uint32(message.botId);
    }
    if (message.userId !== "") {
      writer.uint32(42).string(message.userId);
    }
    if (message.meta !== "") {
      writer.uint32(50).string(message.meta);
    }
    if (message.deletedAt !== undefined) {
      writer.uint32(56).int64(message.deletedAt);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BotUserModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseBotUserModel } as BotUserModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.createdAt = longToNumber(reader.int64() as Long);
          break;
        case 3:
          message.updatedAt = longToNumber(reader.int64() as Long);
          break;
        case 4:
          message.botId = reader.uint32();
          break;
        case 5:
          message.userId = reader.string();
          break;
        case 6:
          message.meta = reader.string();
          break;
        case 7:
          message.deletedAt = longToNumber(reader.int64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): BotUserModel {
    const message = { ...baseBotUserModel } as BotUserModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.createdAt =
      object.createdAt !== undefined && object.createdAt !== null
        ? Number(object.createdAt)
        : 0;
    message.updatedAt =
      object.updatedAt !== undefined && object.updatedAt !== null
        ? Number(object.updatedAt)
        : 0;
    message.botId =
      object.botId !== undefined && object.botId !== null
        ? Number(object.botId)
        : 0;
    message.userId =
      object.userId !== undefined && object.userId !== null
        ? String(object.userId)
        : "";
    message.meta =
      object.meta !== undefined && object.meta !== null
        ? String(object.meta)
        : "";
    message.deletedAt =
      object.deletedAt !== undefined && object.deletedAt !== null
        ? Number(object.deletedAt)
        : undefined;
    return message;
  },

  toJSON(message: BotUserModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.createdAt !== undefined && (obj.createdAt = message.createdAt);
    message.updatedAt !== undefined && (obj.updatedAt = message.updatedAt);
    message.botId !== undefined && (obj.botId = message.botId);
    message.userId !== undefined && (obj.userId = message.userId);
    message.meta !== undefined && (obj.meta = message.meta);
    message.deletedAt !== undefined && (obj.deletedAt = message.deletedAt);
    return obj;
  },

  fromPartial(object: DeepPartial<BotUserModel>): BotUserModel {
    const message = { ...baseBotUserModel } as BotUserModel;
    message.id = object.id ?? 0;
    message.createdAt = object.createdAt ?? 0;
    message.updatedAt = object.updatedAt ?? 0;
    message.botId = object.botId ?? 0;
    message.userId = object.userId ?? "";
    message.meta = object.meta ?? "";
    message.deletedAt = object.deletedAt ?? undefined;
    return message;
  },
};

messageTypeRegistry.set(BotUserModel.$type, BotUserModel);

const baseBotUserListResp: object = { $type: "botMs.BotUserListResp" };

export const BotUserListResp = {
  $type: "botMs.BotUserListResp" as const,

  encode(
    message: BotUserListResp,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.items) {
      BotUserModel.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.paginator !== undefined) {
      Paginator.encode(message.paginator, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BotUserListResp {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseBotUserListResp } as BotUserListResp;
    message.items = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.items.push(BotUserModel.decode(reader, reader.uint32()));
          break;
        case 2:
          message.paginator = Paginator.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): BotUserListResp {
    const message = { ...baseBotUserListResp } as BotUserListResp;
    message.items = (object.items ?? []).map((e: any) =>
      BotUserModel.fromJSON(e)
    );
    message.paginator =
      object.paginator !== undefined && object.paginator !== null
        ? Paginator.fromJSON(object.paginator)
        : undefined;
    return message;
  },

  toJSON(message: BotUserListResp): unknown {
    const obj: any = {};
    if (message.items) {
      obj.items = message.items.map((e) =>
        e ? BotUserModel.toJSON(e) : undefined
      );
    } else {
      obj.items = [];
    }
    message.paginator !== undefined &&
      (obj.paginator = message.paginator
        ? Paginator.toJSON(message.paginator)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<BotUserListResp>): BotUserListResp {
    const message = { ...baseBotUserListResp } as BotUserListResp;
    message.items = (object.items ?? []).map((e) =>
      BotUserModel.fromPartial(e)
    );
    message.paginator =
      object.paginator !== undefined && object.paginator !== null
        ? Paginator.fromPartial(object.paginator)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(BotUserListResp.$type, BotUserListResp);

const baseBotUserList: object = { $type: "botMs.BotUserList", handler: "" };

export const BotUserList = {
  $type: "botMs.BotUserList" as const,

  encode(
    message: BotUserList,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.handler !== "") {
      writer.uint32(10).string(message.handler);
    }
    if (message.opts !== undefined) {
      ListOptions.encode(message.opts, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): BotUserList {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseBotUserList } as BotUserList;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.handler = reader.string();
          break;
        case 2:
          message.opts = ListOptions.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): BotUserList {
    const message = { ...baseBotUserList } as BotUserList;
    message.handler =
      object.handler !== undefined && object.handler !== null
        ? String(object.handler)
        : "";
    message.opts =
      object.opts !== undefined && object.opts !== null
        ? ListOptions.fromJSON(object.opts)
        : undefined;
    return message;
  },

  toJSON(message: BotUserList): unknown {
    const obj: any = {};
    message.handler !== undefined && (obj.handler = message.handler);
    message.opts !== undefined &&
      (obj.opts = message.opts ? ListOptions.toJSON(message.opts) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<BotUserList>): BotUserList {
    const message = { ...baseBotUserList } as BotUserList;
    message.handler = object.handler ?? "";
    message.opts =
      object.opts !== undefined && object.opts !== null
        ? ListOptions.fromPartial(object.opts)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(BotUserList.$type, BotUserList);

const baseActionModel: object = {
  $type: "botMs.ActionModel",
  id: 0,
  name: "",
  handler: "",
  scheme: "",
};

export const ActionModel = {
  $type: "botMs.ActionModel" as const,

  encode(
    message: ActionModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    if (message.handler !== "") {
      writer.uint32(26).string(message.handler);
    }
    if (message.scheme !== "") {
      writer.uint32(34).string(message.scheme);
    }
    for (const v of message.sockets) {
      SocketModel.encode(v!, writer.uint32(42).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ActionModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseActionModel } as ActionModel;
    message.sockets = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.name = reader.string();
          break;
        case 3:
          message.handler = reader.string();
          break;
        case 4:
          message.scheme = reader.string();
          break;
        case 5:
          message.sockets.push(SocketModel.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ActionModel {
    const message = { ...baseActionModel } as ActionModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.name =
      object.name !== undefined && object.name !== null
        ? String(object.name)
        : "";
    message.handler =
      object.handler !== undefined && object.handler !== null
        ? String(object.handler)
        : "";
    message.scheme =
      object.scheme !== undefined && object.scheme !== null
        ? String(object.scheme)
        : "";
    message.sockets = (object.sockets ?? []).map((e: any) =>
      SocketModel.fromJSON(e)
    );
    return message;
  },

  toJSON(message: ActionModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    message.handler !== undefined && (obj.handler = message.handler);
    message.scheme !== undefined && (obj.scheme = message.scheme);
    if (message.sockets) {
      obj.sockets = message.sockets.map((e) =>
        e ? SocketModel.toJSON(e) : undefined
      );
    } else {
      obj.sockets = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<ActionModel>): ActionModel {
    const message = { ...baseActionModel } as ActionModel;
    message.id = object.id ?? 0;
    message.name = object.name ?? "";
    message.handler = object.handler ?? "";
    message.scheme = object.scheme ?? "";
    message.sockets = (object.sockets ?? []).map((e) =>
      SocketModel.fromPartial(e)
    );
    return message;
  },
};

messageTypeRegistry.set(ActionModel.$type, ActionModel);

const baseExecuteReq: object = {
  $type: "botMs.ExecuteReq",
  name: 0,
  botSlug: "",
  data: "",
};

export const ExecuteReq = {
  $type: "botMs.ExecuteReq" as const,

  encode(
    message: ExecuteReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.name !== 0) {
      writer.uint32(8).uint32(message.name);
    }
    if (message.botSlug !== "") {
      writer.uint32(18).string(message.botSlug);
    }
    if (message.data !== "") {
      writer.uint32(26).string(message.data);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ExecuteReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseExecuteReq } as ExecuteReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.name = reader.uint32();
          break;
        case 2:
          message.botSlug = reader.string();
          break;
        case 3:
          message.data = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ExecuteReq {
    const message = { ...baseExecuteReq } as ExecuteReq;
    message.name =
      object.name !== undefined && object.name !== null
        ? Number(object.name)
        : 0;
    message.botSlug =
      object.botSlug !== undefined && object.botSlug !== null
        ? String(object.botSlug)
        : "";
    message.data =
      object.data !== undefined && object.data !== null
        ? String(object.data)
        : "";
    return message;
  },

  toJSON(message: ExecuteReq): unknown {
    const obj: any = {};
    message.name !== undefined && (obj.name = message.name);
    message.botSlug !== undefined && (obj.botSlug = message.botSlug);
    message.data !== undefined && (obj.data = message.data);
    return obj;
  },

  fromPartial(object: DeepPartial<ExecuteReq>): ExecuteReq {
    const message = { ...baseExecuteReq } as ExecuteReq;
    message.name = object.name ?? 0;
    message.botSlug = object.botSlug ?? "";
    message.data = object.data ?? "";
    return message;
  },
};

messageTypeRegistry.set(ExecuteReq.$type, ExecuteReq);

const baseEventModel: object = {
  $type: "botMs.EventModel",
  id: 0,
  createdAt: 0,
  updatedAt: 0,
  botId: 0,
  scenarioId: 0,
  command: "",
};

export const EventModel = {
  $type: "botMs.EventModel" as const,

  encode(
    message: EventModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.createdAt !== 0) {
      writer.uint32(16).int64(message.createdAt);
    }
    if (message.updatedAt !== 0) {
      writer.uint32(24).int64(message.updatedAt);
    }
    if (message.botId !== 0) {
      writer.uint32(32).uint32(message.botId);
    }
    if (message.scenarioId !== 0) {
      writer.uint32(40).uint32(message.scenarioId);
    }
    if (message.command !== "") {
      writer.uint32(50).string(message.command);
    }
    if (message.deletedAt !== undefined) {
      writer.uint32(56).int64(message.deletedAt);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): EventModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseEventModel } as EventModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.createdAt = longToNumber(reader.int64() as Long);
          break;
        case 3:
          message.updatedAt = longToNumber(reader.int64() as Long);
          break;
        case 4:
          message.botId = reader.uint32();
          break;
        case 5:
          message.scenarioId = reader.uint32();
          break;
        case 6:
          message.command = reader.string();
          break;
        case 7:
          message.deletedAt = longToNumber(reader.int64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): EventModel {
    const message = { ...baseEventModel } as EventModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.createdAt =
      object.createdAt !== undefined && object.createdAt !== null
        ? Number(object.createdAt)
        : 0;
    message.updatedAt =
      object.updatedAt !== undefined && object.updatedAt !== null
        ? Number(object.updatedAt)
        : 0;
    message.botId =
      object.botId !== undefined && object.botId !== null
        ? Number(object.botId)
        : 0;
    message.scenarioId =
      object.scenarioId !== undefined && object.scenarioId !== null
        ? Number(object.scenarioId)
        : 0;
    message.command =
      object.command !== undefined && object.command !== null
        ? String(object.command)
        : "";
    message.deletedAt =
      object.deletedAt !== undefined && object.deletedAt !== null
        ? Number(object.deletedAt)
        : undefined;
    return message;
  },

  toJSON(message: EventModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.createdAt !== undefined && (obj.createdAt = message.createdAt);
    message.updatedAt !== undefined && (obj.updatedAt = message.updatedAt);
    message.botId !== undefined && (obj.botId = message.botId);
    message.scenarioId !== undefined && (obj.scenarioId = message.scenarioId);
    message.command !== undefined && (obj.command = message.command);
    message.deletedAt !== undefined && (obj.deletedAt = message.deletedAt);
    return obj;
  },

  fromPartial(object: DeepPartial<EventModel>): EventModel {
    const message = { ...baseEventModel } as EventModel;
    message.id = object.id ?? 0;
    message.createdAt = object.createdAt ?? 0;
    message.updatedAt = object.updatedAt ?? 0;
    message.botId = object.botId ?? 0;
    message.scenarioId = object.scenarioId ?? 0;
    message.command = object.command ?? "";
    message.deletedAt = object.deletedAt ?? undefined;
    return message;
  },
};

messageTypeRegistry.set(EventModel.$type, EventModel);

const baseEvenListResp: object = { $type: "botMs.EvenListResp" };

export const EvenListResp = {
  $type: "botMs.EvenListResp" as const,

  encode(
    message: EvenListResp,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.items) {
      EventModel.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.paginator !== undefined) {
      Paginator.encode(message.paginator, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): EvenListResp {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseEvenListResp } as EvenListResp;
    message.items = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.items.push(EventModel.decode(reader, reader.uint32()));
          break;
        case 2:
          message.paginator = Paginator.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): EvenListResp {
    const message = { ...baseEvenListResp } as EvenListResp;
    message.items = (object.items ?? []).map((e: any) =>
      EventModel.fromJSON(e)
    );
    message.paginator =
      object.paginator !== undefined && object.paginator !== null
        ? Paginator.fromJSON(object.paginator)
        : undefined;
    return message;
  },

  toJSON(message: EvenListResp): unknown {
    const obj: any = {};
    if (message.items) {
      obj.items = message.items.map((e) =>
        e ? EventModel.toJSON(e) : undefined
      );
    } else {
      obj.items = [];
    }
    message.paginator !== undefined &&
      (obj.paginator = message.paginator
        ? Paginator.toJSON(message.paginator)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<EvenListResp>): EvenListResp {
    const message = { ...baseEvenListResp } as EvenListResp;
    message.items = (object.items ?? []).map((e) => EventModel.fromPartial(e));
    message.paginator =
      object.paginator !== undefined && object.paginator !== null
        ? Paginator.fromPartial(object.paginator)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(EvenListResp.$type, EvenListResp);

const baseScenarioActionModel: object = {
  $type: "botMs.ScenarioActionModel",
  id: 0,
  meta: "",
  scenarioId: 0,
  actionId: 0,
  data: "",
};

export const ScenarioActionModel = {
  $type: "botMs.ScenarioActionModel" as const,

  encode(
    message: ScenarioActionModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.meta !== "") {
      writer.uint32(18).string(message.meta);
    }
    if (message.scenarioId !== 0) {
      writer.uint32(24).uint32(message.scenarioId);
    }
    if (message.actionId !== 0) {
      writer.uint32(32).uint32(message.actionId);
    }
    if (message.data !== "") {
      writer.uint32(42).string(message.data);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ScenarioActionModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseScenarioActionModel } as ScenarioActionModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.meta = reader.string();
          break;
        case 3:
          message.scenarioId = reader.uint32();
          break;
        case 4:
          message.actionId = reader.uint32();
          break;
        case 5:
          message.data = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ScenarioActionModel {
    const message = { ...baseScenarioActionModel } as ScenarioActionModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.meta =
      object.meta !== undefined && object.meta !== null
        ? String(object.meta)
        : "";
    message.scenarioId =
      object.scenarioId !== undefined && object.scenarioId !== null
        ? Number(object.scenarioId)
        : 0;
    message.actionId =
      object.actionId !== undefined && object.actionId !== null
        ? Number(object.actionId)
        : 0;
    message.data =
      object.data !== undefined && object.data !== null
        ? String(object.data)
        : "";
    return message;
  },

  toJSON(message: ScenarioActionModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.meta !== undefined && (obj.meta = message.meta);
    message.scenarioId !== undefined && (obj.scenarioId = message.scenarioId);
    message.actionId !== undefined && (obj.actionId = message.actionId);
    message.data !== undefined && (obj.data = message.data);
    return obj;
  },

  fromPartial(object: DeepPartial<ScenarioActionModel>): ScenarioActionModel {
    const message = { ...baseScenarioActionModel } as ScenarioActionModel;
    message.id = object.id ?? 0;
    message.meta = object.meta ?? "";
    message.scenarioId = object.scenarioId ?? 0;
    message.actionId = object.actionId ?? 0;
    message.data = object.data ?? "";
    return message;
  },
};

messageTypeRegistry.set(ScenarioActionModel.$type, ScenarioActionModel);

const baseScenarioActionBatch: object = { $type: "botMs.ScenarioActionBatch" };

export const ScenarioActionBatch = {
  $type: "botMs.ScenarioActionBatch" as const,

  encode(
    message: ScenarioActionBatch,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.items) {
      ScenarioActionModel.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ScenarioActionBatch {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseScenarioActionBatch } as ScenarioActionBatch;
    message.items = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.items.push(
            ScenarioActionModel.decode(reader, reader.uint32())
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ScenarioActionBatch {
    const message = { ...baseScenarioActionBatch } as ScenarioActionBatch;
    message.items = (object.items ?? []).map((e: any) =>
      ScenarioActionModel.fromJSON(e)
    );
    return message;
  },

  toJSON(message: ScenarioActionBatch): unknown {
    const obj: any = {};
    if (message.items) {
      obj.items = message.items.map((e) =>
        e ? ScenarioActionModel.toJSON(e) : undefined
      );
    } else {
      obj.items = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<ScenarioActionBatch>): ScenarioActionBatch {
    const message = { ...baseScenarioActionBatch } as ScenarioActionBatch;
    message.items = (object.items ?? []).map((e) =>
      ScenarioActionModel.fromPartial(e)
    );
    return message;
  },
};

messageTypeRegistry.set(ScenarioActionBatch.$type, ScenarioActionBatch);

const baseScenarioItemModel: object = {
  $type: "botMs.ScenarioItemModel",
  id: 0,
  type: "",
  meta: "",
  scenarioId: 0,
};

export const ScenarioItemModel = {
  $type: "botMs.ScenarioItemModel" as const,

  encode(
    message: ScenarioItemModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.type !== "") {
      writer.uint32(18).string(message.type);
    }
    if (message.meta !== "") {
      writer.uint32(26).string(message.meta);
    }
    if (message.scenarioId !== 0) {
      writer.uint32(32).uint32(message.scenarioId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ScenarioItemModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseScenarioItemModel } as ScenarioItemModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.type = reader.string();
          break;
        case 3:
          message.meta = reader.string();
          break;
        case 4:
          message.scenarioId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ScenarioItemModel {
    const message = { ...baseScenarioItemModel } as ScenarioItemModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.type =
      object.type !== undefined && object.type !== null
        ? String(object.type)
        : "";
    message.meta =
      object.meta !== undefined && object.meta !== null
        ? String(object.meta)
        : "";
    message.scenarioId =
      object.scenarioId !== undefined && object.scenarioId !== null
        ? Number(object.scenarioId)
        : 0;
    return message;
  },

  toJSON(message: ScenarioItemModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.type !== undefined && (obj.type = message.type);
    message.meta !== undefined && (obj.meta = message.meta);
    message.scenarioId !== undefined && (obj.scenarioId = message.scenarioId);
    return obj;
  },

  fromPartial(object: DeepPartial<ScenarioItemModel>): ScenarioItemModel {
    const message = { ...baseScenarioItemModel } as ScenarioItemModel;
    message.id = object.id ?? 0;
    message.type = object.type ?? "";
    message.meta = object.meta ?? "";
    message.scenarioId = object.scenarioId ?? 0;
    return message;
  },
};

messageTypeRegistry.set(ScenarioItemModel.$type, ScenarioItemModel);

const baseScenarioItemBatch: object = { $type: "botMs.ScenarioItemBatch" };

export const ScenarioItemBatch = {
  $type: "botMs.ScenarioItemBatch" as const,

  encode(
    message: ScenarioItemBatch,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.items) {
      ScenarioItemModel.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ScenarioItemBatch {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseScenarioItemBatch } as ScenarioItemBatch;
    message.items = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.items.push(ScenarioItemModel.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ScenarioItemBatch {
    const message = { ...baseScenarioItemBatch } as ScenarioItemBatch;
    message.items = (object.items ?? []).map((e: any) =>
      ScenarioItemModel.fromJSON(e)
    );
    return message;
  },

  toJSON(message: ScenarioItemBatch): unknown {
    const obj: any = {};
    if (message.items) {
      obj.items = message.items.map((e) =>
        e ? ScenarioItemModel.toJSON(e) : undefined
      );
    } else {
      obj.items = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<ScenarioItemBatch>): ScenarioItemBatch {
    const message = { ...baseScenarioItemBatch } as ScenarioItemBatch;
    message.items = (object.items ?? []).map((e) =>
      ScenarioItemModel.fromPartial(e)
    );
    return message;
  },
};

messageTypeRegistry.set(ScenarioItemBatch.$type, ScenarioItemBatch);

const baseScenarioActionReq: object = {
  $type: "botMs.ScenarioActionReq",
  scenarioId: 0,
};

export const ScenarioActionReq = {
  $type: "botMs.ScenarioActionReq" as const,

  encode(
    message: ScenarioActionReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.scenarioId !== 0) {
      writer.uint32(8).uint32(message.scenarioId);
    }
    if (message.opt !== undefined) {
      ListOptions.encode(message.opt, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ScenarioActionReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseScenarioActionReq } as ScenarioActionReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.scenarioId = reader.uint32();
          break;
        case 2:
          message.opt = ListOptions.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ScenarioActionReq {
    const message = { ...baseScenarioActionReq } as ScenarioActionReq;
    message.scenarioId =
      object.scenarioId !== undefined && object.scenarioId !== null
        ? Number(object.scenarioId)
        : 0;
    message.opt =
      object.opt !== undefined && object.opt !== null
        ? ListOptions.fromJSON(object.opt)
        : undefined;
    return message;
  },

  toJSON(message: ScenarioActionReq): unknown {
    const obj: any = {};
    message.scenarioId !== undefined && (obj.scenarioId = message.scenarioId);
    message.opt !== undefined &&
      (obj.opt = message.opt ? ListOptions.toJSON(message.opt) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<ScenarioActionReq>): ScenarioActionReq {
    const message = { ...baseScenarioActionReq } as ScenarioActionReq;
    message.scenarioId = object.scenarioId ?? 0;
    message.opt =
      object.opt !== undefined && object.opt !== null
        ? ListOptions.fromPartial(object.opt)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(ScenarioActionReq.$type, ScenarioActionReq);

const baseScenarioModel: object = {
  $type: "botMs.ScenarioModel",
  id: 0,
  createdAt: 0,
  updatedAt: 0,
  title: "",
  category: "",
};

export const ScenarioModel = {
  $type: "botMs.ScenarioModel" as const,

  encode(
    message: ScenarioModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.createdAt !== 0) {
      writer.uint32(16).int64(message.createdAt);
    }
    if (message.updatedAt !== 0) {
      writer.uint32(24).int64(message.updatedAt);
    }
    if (message.initial !== undefined) {
      writer.uint32(32).uint32(message.initial);
    }
    if (message.title !== "") {
      writer.uint32(42).string(message.title);
    }
    if (message.category !== "") {
      writer.uint32(50).string(message.category);
    }
    for (const v of message.actions) {
      ScenarioActionModel.encode(v!, writer.uint32(58).fork()).ldelim();
    }
    if (message.deletedAt !== undefined) {
      writer.uint32(64).int64(message.deletedAt);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ScenarioModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseScenarioModel } as ScenarioModel;
    message.actions = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.createdAt = longToNumber(reader.int64() as Long);
          break;
        case 3:
          message.updatedAt = longToNumber(reader.int64() as Long);
          break;
        case 4:
          message.initial = reader.uint32();
          break;
        case 5:
          message.title = reader.string();
          break;
        case 6:
          message.category = reader.string();
          break;
        case 7:
          message.actions.push(
            ScenarioActionModel.decode(reader, reader.uint32())
          );
          break;
        case 8:
          message.deletedAt = longToNumber(reader.int64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ScenarioModel {
    const message = { ...baseScenarioModel } as ScenarioModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.createdAt =
      object.createdAt !== undefined && object.createdAt !== null
        ? Number(object.createdAt)
        : 0;
    message.updatedAt =
      object.updatedAt !== undefined && object.updatedAt !== null
        ? Number(object.updatedAt)
        : 0;
    message.initial =
      object.initial !== undefined && object.initial !== null
        ? Number(object.initial)
        : undefined;
    message.title =
      object.title !== undefined && object.title !== null
        ? String(object.title)
        : "";
    message.category =
      object.category !== undefined && object.category !== null
        ? String(object.category)
        : "";
    message.actions = (object.actions ?? []).map((e: any) =>
      ScenarioActionModel.fromJSON(e)
    );
    message.deletedAt =
      object.deletedAt !== undefined && object.deletedAt !== null
        ? Number(object.deletedAt)
        : undefined;
    return message;
  },

  toJSON(message: ScenarioModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.createdAt !== undefined && (obj.createdAt = message.createdAt);
    message.updatedAt !== undefined && (obj.updatedAt = message.updatedAt);
    message.initial !== undefined && (obj.initial = message.initial);
    message.title !== undefined && (obj.title = message.title);
    message.category !== undefined && (obj.category = message.category);
    if (message.actions) {
      obj.actions = message.actions.map((e) =>
        e ? ScenarioActionModel.toJSON(e) : undefined
      );
    } else {
      obj.actions = [];
    }
    message.deletedAt !== undefined && (obj.deletedAt = message.deletedAt);
    return obj;
  },

  fromPartial(object: DeepPartial<ScenarioModel>): ScenarioModel {
    const message = { ...baseScenarioModel } as ScenarioModel;
    message.id = object.id ?? 0;
    message.createdAt = object.createdAt ?? 0;
    message.updatedAt = object.updatedAt ?? 0;
    message.initial = object.initial ?? undefined;
    message.title = object.title ?? "";
    message.category = object.category ?? "";
    message.actions = (object.actions ?? []).map((e) =>
      ScenarioActionModel.fromPartial(e)
    );
    message.deletedAt = object.deletedAt ?? undefined;
    return message;
  },
};

messageTypeRegistry.set(ScenarioModel.$type, ScenarioModel);

const baseScenarioModelListResp: object = {
  $type: "botMs.ScenarioModelListResp",
};

export const ScenarioModelListResp = {
  $type: "botMs.ScenarioModelListResp" as const,

  encode(
    message: ScenarioModelListResp,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.items) {
      ScenarioModel.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.paginator !== undefined) {
      Paginator.encode(message.paginator, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ScenarioModelListResp {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseScenarioModelListResp } as ScenarioModelListResp;
    message.items = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.items.push(ScenarioModel.decode(reader, reader.uint32()));
          break;
        case 2:
          message.paginator = Paginator.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ScenarioModelListResp {
    const message = { ...baseScenarioModelListResp } as ScenarioModelListResp;
    message.items = (object.items ?? []).map((e: any) =>
      ScenarioModel.fromJSON(e)
    );
    message.paginator =
      object.paginator !== undefined && object.paginator !== null
        ? Paginator.fromJSON(object.paginator)
        : undefined;
    return message;
  },

  toJSON(message: ScenarioModelListResp): unknown {
    const obj: any = {};
    if (message.items) {
      obj.items = message.items.map((e) =>
        e ? ScenarioModel.toJSON(e) : undefined
      );
    } else {
      obj.items = [];
    }
    message.paginator !== undefined &&
      (obj.paginator = message.paginator
        ? Paginator.toJSON(message.paginator)
        : undefined);
    return obj;
  },

  fromPartial(
    object: DeepPartial<ScenarioModelListResp>
  ): ScenarioModelListResp {
    const message = { ...baseScenarioModelListResp } as ScenarioModelListResp;
    message.items = (object.items ?? []).map((e) =>
      ScenarioModel.fromPartial(e)
    );
    message.paginator =
      object.paginator !== undefined && object.paginator !== null
        ? Paginator.fromPartial(object.paginator)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(ScenarioModelListResp.$type, ScenarioModelListResp);

const baseSocketModel: object = {
  $type: "botMs.SocketModel",
  id: 0,
  direction: 0,
  type: 0,
  title: "",
  slug: "",
  actionId: 0,
  manual: false,
};

export const SocketModel = {
  $type: "botMs.SocketModel" as const,

  encode(
    message: SocketModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.direction !== 0) {
      writer.uint32(16).uint32(message.direction);
    }
    if (message.type !== 0) {
      writer.uint32(24).uint32(message.type);
    }
    if (message.title !== "") {
      writer.uint32(34).string(message.title);
    }
    if (message.slug !== "") {
      writer.uint32(42).string(message.slug);
    }
    if (message.actionId !== 0) {
      writer.uint32(48).uint32(message.actionId);
    }
    if (message.manual === true) {
      writer.uint32(56).bool(message.manual);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SocketModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSocketModel } as SocketModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.direction = reader.uint32();
          break;
        case 3:
          message.type = reader.uint32();
          break;
        case 4:
          message.title = reader.string();
          break;
        case 5:
          message.slug = reader.string();
          break;
        case 6:
          message.actionId = reader.uint32();
          break;
        case 7:
          message.manual = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): SocketModel {
    const message = { ...baseSocketModel } as SocketModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.direction =
      object.direction !== undefined && object.direction !== null
        ? Number(object.direction)
        : 0;
    message.type =
      object.type !== undefined && object.type !== null
        ? Number(object.type)
        : 0;
    message.title =
      object.title !== undefined && object.title !== null
        ? String(object.title)
        : "";
    message.slug =
      object.slug !== undefined && object.slug !== null
        ? String(object.slug)
        : "";
    message.actionId =
      object.actionId !== undefined && object.actionId !== null
        ? Number(object.actionId)
        : 0;
    message.manual =
      object.manual !== undefined && object.manual !== null
        ? Boolean(object.manual)
        : false;
    return message;
  },

  toJSON(message: SocketModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.direction !== undefined && (obj.direction = message.direction);
    message.type !== undefined && (obj.type = message.type);
    message.title !== undefined && (obj.title = message.title);
    message.slug !== undefined && (obj.slug = message.slug);
    message.actionId !== undefined && (obj.actionId = message.actionId);
    message.manual !== undefined && (obj.manual = message.manual);
    return obj;
  },

  fromPartial(object: DeepPartial<SocketModel>): SocketModel {
    const message = { ...baseSocketModel } as SocketModel;
    message.id = object.id ?? 0;
    message.direction = object.direction ?? 0;
    message.type = object.type ?? 0;
    message.title = object.title ?? "";
    message.slug = object.slug ?? "";
    message.actionId = object.actionId ?? 0;
    message.manual = object.manual ?? false;
    return message;
  },
};

messageTypeRegistry.set(SocketModel.$type, SocketModel);

const baseConnectorModel: object = {
  $type: "botMs.ConnectorModel",
  id: 0,
  fromScenarioAction: 0,
  fromSocket: 0,
  toScenarioAction: 0,
  toSocket: 0,
  scenarioId: 0,
};

export const ConnectorModel = {
  $type: "botMs.ConnectorModel" as const,

  encode(
    message: ConnectorModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.fromScenarioAction !== 0) {
      writer.uint32(16).uint32(message.fromScenarioAction);
    }
    if (message.fromSocket !== 0) {
      writer.uint32(24).uint32(message.fromSocket);
    }
    if (message.toScenarioAction !== 0) {
      writer.uint32(32).uint32(message.toScenarioAction);
    }
    if (message.toSocket !== 0) {
      writer.uint32(40).uint32(message.toSocket);
    }
    if (message.scenarioId !== 0) {
      writer.uint32(48).uint32(message.scenarioId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ConnectorModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseConnectorModel } as ConnectorModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.fromScenarioAction = reader.uint32();
          break;
        case 3:
          message.fromSocket = reader.uint32();
          break;
        case 4:
          message.toScenarioAction = reader.uint32();
          break;
        case 5:
          message.toSocket = reader.uint32();
          break;
        case 6:
          message.scenarioId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ConnectorModel {
    const message = { ...baseConnectorModel } as ConnectorModel;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.fromScenarioAction =
      object.fromScenarioAction !== undefined &&
      object.fromScenarioAction !== null
        ? Number(object.fromScenarioAction)
        : 0;
    message.fromSocket =
      object.fromSocket !== undefined && object.fromSocket !== null
        ? Number(object.fromSocket)
        : 0;
    message.toScenarioAction =
      object.toScenarioAction !== undefined && object.toScenarioAction !== null
        ? Number(object.toScenarioAction)
        : 0;
    message.toSocket =
      object.toSocket !== undefined && object.toSocket !== null
        ? Number(object.toSocket)
        : 0;
    message.scenarioId =
      object.scenarioId !== undefined && object.scenarioId !== null
        ? Number(object.scenarioId)
        : 0;
    return message;
  },

  toJSON(message: ConnectorModel): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.fromScenarioAction !== undefined &&
      (obj.fromScenarioAction = message.fromScenarioAction);
    message.fromSocket !== undefined && (obj.fromSocket = message.fromSocket);
    message.toScenarioAction !== undefined &&
      (obj.toScenarioAction = message.toScenarioAction);
    message.toSocket !== undefined && (obj.toSocket = message.toSocket);
    message.scenarioId !== undefined && (obj.scenarioId = message.scenarioId);
    return obj;
  },

  fromPartial(object: DeepPartial<ConnectorModel>): ConnectorModel {
    const message = { ...baseConnectorModel } as ConnectorModel;
    message.id = object.id ?? 0;
    message.fromScenarioAction = object.fromScenarioAction ?? 0;
    message.fromSocket = object.fromSocket ?? 0;
    message.toScenarioAction = object.toScenarioAction ?? 0;
    message.toSocket = object.toSocket ?? 0;
    message.scenarioId = object.scenarioId ?? 0;
    return message;
  },
};

messageTypeRegistry.set(ConnectorModel.$type, ConnectorModel);

const baseConnectorReq: object = { $type: "botMs.ConnectorReq", scenarioId: 0 };

export const ConnectorReq = {
  $type: "botMs.ConnectorReq" as const,

  encode(
    message: ConnectorReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.scenarioId !== 0) {
      writer.uint32(8).uint32(message.scenarioId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ConnectorReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseConnectorReq } as ConnectorReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.scenarioId = reader.uint32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ConnectorReq {
    const message = { ...baseConnectorReq } as ConnectorReq;
    message.scenarioId =
      object.scenarioId !== undefined && object.scenarioId !== null
        ? Number(object.scenarioId)
        : 0;
    return message;
  },

  toJSON(message: ConnectorReq): unknown {
    const obj: any = {};
    message.scenarioId !== undefined && (obj.scenarioId = message.scenarioId);
    return obj;
  },

  fromPartial(object: DeepPartial<ConnectorReq>): ConnectorReq {
    const message = { ...baseConnectorReq } as ConnectorReq;
    message.scenarioId = object.scenarioId ?? 0;
    return message;
  },
};

messageTypeRegistry.set(ConnectorReq.$type, ConnectorReq);

const baseAuthRequestModel: object = {
  $type: "botMs.AuthRequestModel",
  botId: 0,
  meta: "",
  hash: "",
};

export const AuthRequestModel = {
  $type: "botMs.AuthRequestModel" as const,

  encode(
    message: AuthRequestModel,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.botId !== 0) {
      writer.uint32(8).uint32(message.botId);
    }
    if (message.meta !== "") {
      writer.uint32(18).string(message.meta);
    }
    if (message.hash !== "") {
      writer.uint32(26).string(message.hash);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): AuthRequestModel {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseAuthRequestModel } as AuthRequestModel;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.botId = reader.uint32();
          break;
        case 2:
          message.meta = reader.string();
          break;
        case 3:
          message.hash = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): AuthRequestModel {
    const message = { ...baseAuthRequestModel } as AuthRequestModel;
    message.botId =
      object.botId !== undefined && object.botId !== null
        ? Number(object.botId)
        : 0;
    message.meta =
      object.meta !== undefined && object.meta !== null
        ? String(object.meta)
        : "";
    message.hash =
      object.hash !== undefined && object.hash !== null
        ? String(object.hash)
        : "";
    return message;
  },

  toJSON(message: AuthRequestModel): unknown {
    const obj: any = {};
    message.botId !== undefined && (obj.botId = message.botId);
    message.meta !== undefined && (obj.meta = message.meta);
    message.hash !== undefined && (obj.hash = message.hash);
    return obj;
  },

  fromPartial(object: DeepPartial<AuthRequestModel>): AuthRequestModel {
    const message = { ...baseAuthRequestModel } as AuthRequestModel;
    message.botId = object.botId ?? 0;
    message.meta = object.meta ?? "";
    message.hash = object.hash ?? "";
    return message;
  },
};

messageTypeRegistry.set(AuthRequestModel.$type, AuthRequestModel);

const baseScenarioListReq: object = { $type: "botMs.ScenarioListReq" };

export const ScenarioListReq = {
  $type: "botMs.ScenarioListReq" as const,

  encode(
    message: ScenarioListReq,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.opts !== undefined) {
      ListOptions.encode(message.opts, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ScenarioListReq {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseScenarioListReq } as ScenarioListReq;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.opts = ListOptions.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ScenarioListReq {
    const message = { ...baseScenarioListReq } as ScenarioListReq;
    message.opts =
      object.opts !== undefined && object.opts !== null
        ? ListOptions.fromJSON(object.opts)
        : undefined;
    return message;
  },

  toJSON(message: ScenarioListReq): unknown {
    const obj: any = {};
    message.opts !== undefined &&
      (obj.opts = message.opts ? ListOptions.toJSON(message.opts) : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<ScenarioListReq>): ScenarioListReq {
    const message = { ...baseScenarioListReq } as ScenarioListReq;
    message.opts =
      object.opts !== undefined && object.opts !== null
        ? ListOptions.fromPartial(object.opts)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(ScenarioListReq.$type, ScenarioListReq);

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
