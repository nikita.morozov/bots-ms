/* eslint-disable */
import Long from "long";
import { grpc } from "@improbable-eng/grpc-web";
import _m0 from "protobufjs/minimal";
import { BotModel, BotListResp } from "./botCommon";
import { IdRequest, BoolValue, UInt64Value } from "./commonTypes";
import { ListOptions } from "./paginatior";
import { Empty } from "./google/protobuf/empty";
import { BrowserHeaders } from "browser-headers";

export const protobufPackage = "botMs";

export interface Bot {
  Create(
    request: DeepPartial<BotModel>,
    metadata?: grpc.Metadata
  ): Promise<IdRequest>;
  Update(
    request: DeepPartial<BotModel>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  Delete(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue>;
  Get(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BotModel>;
  List(
    request: DeepPartial<ListOptions>,
    metadata?: grpc.Metadata
  ): Promise<BotListResp>;
  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<UInt64Value>;
}

export class BotClientImpl implements Bot {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Create = this.Create.bind(this);
    this.Update = this.Update.bind(this);
    this.Delete = this.Delete.bind(this);
    this.Get = this.Get.bind(this);
    this.List = this.List.bind(this);
    this.Count = this.Count.bind(this);
  }

  Create(
    request: DeepPartial<BotModel>,
    metadata?: grpc.Metadata
  ): Promise<IdRequest> {
    return this.rpc.unary(
      BotCreateDesc,
      BotModel.fromPartial(request),
      metadata
    );
  }

  Update(
    request: DeepPartial<BotModel>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      BotUpdateDesc,
      BotModel.fromPartial(request),
      metadata
    );
  }

  Delete(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue> {
    return this.rpc.unary(
      BotDeleteDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }

  Get(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BotModel> {
    return this.rpc.unary(BotGetDesc, IdRequest.fromPartial(request), metadata);
  }

  List(
    request: DeepPartial<ListOptions>,
    metadata?: grpc.Metadata
  ): Promise<BotListResp> {
    return this.rpc.unary(
      BotListDesc,
      ListOptions.fromPartial(request),
      metadata
    );
  }

  Count(
    request: DeepPartial<Empty>,
    metadata?: grpc.Metadata
  ): Promise<UInt64Value> {
    return this.rpc.unary(BotCountDesc, Empty.fromPartial(request), metadata);
  }
}

export const BotDesc = {
  serviceName: "botMs.Bot",
};

export const BotCreateDesc: UnaryMethodDefinitionish = {
  methodName: "Create",
  service: BotDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return BotModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...IdRequest.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const BotUpdateDesc: UnaryMethodDefinitionish = {
  methodName: "Update",
  service: BotDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return BotModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const BotDeleteDesc: UnaryMethodDefinitionish = {
  methodName: "Delete",
  service: BotDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...BoolValue.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const BotGetDesc: UnaryMethodDefinitionish = {
  methodName: "Get",
  service: BotDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...BotModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const BotListDesc: UnaryMethodDefinitionish = {
  methodName: "List",
  service: BotDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ListOptions.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...BotListResp.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const BotCountDesc: UnaryMethodDefinitionish = {
  methodName: "Count",
  service: BotDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return Empty.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...UInt64Value.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR
  extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;

    debug?: boolean;
    metadata?: grpc.Metadata;
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;

      debug?: boolean;
      metadata?: grpc.Metadata;
    }
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata,
        transport: this.options.transport,
        debug: this.options.debug,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message);
          } else {
            const err = new Error(response.statusMessage) as any;
            err.code = response.status;
            err.metadata = response.trailers;
            reject(err);
          }
        },
      });
    });
  }
}

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
