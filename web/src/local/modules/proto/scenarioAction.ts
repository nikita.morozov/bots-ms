/* eslint-disable */
import Long from "long";
import { grpc } from "@improbable-eng/grpc-web";
import _m0 from "protobufjs/minimal";
import { Observable } from "rxjs";
import {
  ScenarioActionModel,
  ScenarioActionBatch,
  ScenarioActionReq,
} from "./botCommon";
import { IdRequest, BoolValue } from "./commonTypes";
import { Empty } from "./google/protobuf/empty";
import { BrowserHeaders } from "browser-headers";
import { share } from "rxjs/operators";

export const protobufPackage = "botMs";

export interface ScenarioAction {
  Create(
    request: DeepPartial<ScenarioActionModel>,
    metadata?: grpc.Metadata
  ): Promise<IdRequest>;
  Update(
    request: DeepPartial<ScenarioActionModel>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  BatchUpdate(
    request: DeepPartial<ScenarioActionBatch>,
    metadata?: grpc.Metadata
  ): Promise<Empty>;
  Delete(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue>;
  Get(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<ScenarioActionModel>;
  List(
    request: DeepPartial<ScenarioActionReq>,
    metadata?: grpc.Metadata
  ): Observable<ScenarioActionModel>;
}

export class ScenarioActionClientImpl implements ScenarioAction {
  private readonly rpc: Rpc;

  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.Create = this.Create.bind(this);
    this.Update = this.Update.bind(this);
    this.BatchUpdate = this.BatchUpdate.bind(this);
    this.Delete = this.Delete.bind(this);
    this.Get = this.Get.bind(this);
    this.List = this.List.bind(this);
  }

  Create(
    request: DeepPartial<ScenarioActionModel>,
    metadata?: grpc.Metadata
  ): Promise<IdRequest> {
    return this.rpc.unary(
      ScenarioActionCreateDesc,
      ScenarioActionModel.fromPartial(request),
      metadata
    );
  }

  Update(
    request: DeepPartial<ScenarioActionModel>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      ScenarioActionUpdateDesc,
      ScenarioActionModel.fromPartial(request),
      metadata
    );
  }

  BatchUpdate(
    request: DeepPartial<ScenarioActionBatch>,
    metadata?: grpc.Metadata
  ): Promise<Empty> {
    return this.rpc.unary(
      ScenarioActionBatchUpdateDesc,
      ScenarioActionBatch.fromPartial(request),
      metadata
    );
  }

  Delete(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<BoolValue> {
    return this.rpc.unary(
      ScenarioActionDeleteDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }

  Get(
    request: DeepPartial<IdRequest>,
    metadata?: grpc.Metadata
  ): Promise<ScenarioActionModel> {
    return this.rpc.unary(
      ScenarioActionGetDesc,
      IdRequest.fromPartial(request),
      metadata
    );
  }

  List(
    request: DeepPartial<ScenarioActionReq>,
    metadata?: grpc.Metadata
  ): Observable<ScenarioActionModel> {
    return this.rpc.invoke(
      ScenarioActionListDesc,
      ScenarioActionReq.fromPartial(request),
      metadata
    );
  }
}

export const ScenarioActionDesc = {
  serviceName: "botMs.ScenarioAction",
};

export const ScenarioActionCreateDesc: UnaryMethodDefinitionish = {
  methodName: "Create",
  service: ScenarioActionDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ScenarioActionModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...IdRequest.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ScenarioActionUpdateDesc: UnaryMethodDefinitionish = {
  methodName: "Update",
  service: ScenarioActionDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ScenarioActionModel.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ScenarioActionBatchUpdateDesc: UnaryMethodDefinitionish = {
  methodName: "BatchUpdate",
  service: ScenarioActionDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return ScenarioActionBatch.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...Empty.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ScenarioActionDeleteDesc: UnaryMethodDefinitionish = {
  methodName: "Delete",
  service: ScenarioActionDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...BoolValue.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ScenarioActionGetDesc: UnaryMethodDefinitionish = {
  methodName: "Get",
  service: ScenarioActionDesc,
  requestStream: false,
  responseStream: false,
  requestType: {
    serializeBinary() {
      return IdRequest.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ScenarioActionModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

export const ScenarioActionListDesc: UnaryMethodDefinitionish = {
  methodName: "List",
  service: ScenarioActionDesc,
  requestStream: false,
  responseStream: true,
  requestType: {
    serializeBinary() {
      return ScenarioActionReq.encode(this).finish();
    },
  } as any,
  responseType: {
    deserializeBinary(data: Uint8Array) {
      return {
        ...ScenarioActionModel.decode(data),
        toObject() {
          return this;
        },
      };
    },
  } as any,
};

interface UnaryMethodDefinitionishR
  extends grpc.UnaryMethodDefinition<any, any> {
  requestStream: any;
  responseStream: any;
}

type UnaryMethodDefinitionish = UnaryMethodDefinitionishR;

interface Rpc {
  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any>;
  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any>;
}

export class GrpcWebImpl {
  private host: string;
  private options: {
    transport?: grpc.TransportFactory;
    streamingTransport?: grpc.TransportFactory;
    debug?: boolean;
    metadata?: grpc.Metadata;
  };

  constructor(
    host: string,
    options: {
      transport?: grpc.TransportFactory;
      streamingTransport?: grpc.TransportFactory;
      debug?: boolean;
      metadata?: grpc.Metadata;
    }
  ) {
    this.host = host;
    this.options = options;
  }

  unary<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Promise<any> {
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Promise((resolve, reject) => {
      grpc.unary(methodDesc, {
        request,
        host: this.host,
        metadata: maybeCombinedMetadata,
        transport: this.options.transport,
        debug: this.options.debug,
        onEnd: function (response) {
          if (response.status === grpc.Code.OK) {
            resolve(response.message);
          } else {
            const err = new Error(response.statusMessage) as any;
            err.code = response.status;
            err.metadata = response.trailers;
            reject(err);
          }
        },
      });
    });
  }

  invoke<T extends UnaryMethodDefinitionish>(
    methodDesc: T,
    _request: any,
    metadata: grpc.Metadata | undefined
  ): Observable<any> {
    // Status Response Codes (https://developers.google.com/maps-booking/reference/grpc-api/status_codes)
    const upStreamCodes = [2, 4, 8, 9, 10, 13, 14, 15];
    const DEFAULT_TIMEOUT_TIME: number = 3_000;
    const request = { ..._request, ...methodDesc.requestType };
    const maybeCombinedMetadata =
      metadata && this.options.metadata
        ? new BrowserHeaders({
            ...this.options?.metadata.headersMap,
            ...metadata?.headersMap,
          })
        : metadata || this.options.metadata;
    return new Observable((observer) => {
      const upStream = () => {
        const client = grpc.invoke(methodDesc, {
          host: this.host,
          request,
          transport: this.options.streamingTransport || this.options.transport,
          metadata: maybeCombinedMetadata,
          debug: this.options.debug,
          onMessage: (next) => observer.next(next),
          onEnd: (code: grpc.Code, message: string) => {
            if (code === 0) {
              observer.complete();
            } else if (upStreamCodes.includes(code)) {
              setTimeout(upStream, DEFAULT_TIMEOUT_TIME);
            } else {
              observer.error(new Error(`Error ${code} ${message}`));
            }
          },
        });
        observer.add(() => client.close());
      };
      upStream();
    }).pipe(share());
  }
}

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
