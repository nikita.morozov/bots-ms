/* eslint-disable */
import { messageTypeRegistry } from "./typeRegistry";
import Long from "long";
import _m0 from "protobufjs/minimal";

export const protobufPackage = "nmbot.shared";

export interface Paginator {
  $type: "nmbot.shared.Paginator";
  offset: number;
  totalCount: number;
  countPerPage: number;
}

export interface ListOptions {
  $type: "nmbot.shared.ListOptions";
  offset: number;
  limit: number;
  order?: string | undefined;
}

const basePaginator: object = {
  $type: "nmbot.shared.Paginator",
  offset: 0,
  totalCount: 0,
  countPerPage: 0,
};

export const Paginator = {
  $type: "nmbot.shared.Paginator" as const,

  encode(
    message: Paginator,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.offset !== 0) {
      writer.uint32(8).int64(message.offset);
    }
    if (message.totalCount !== 0) {
      writer.uint32(16).int64(message.totalCount);
    }
    if (message.countPerPage !== 0) {
      writer.uint32(24).int32(message.countPerPage);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Paginator {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...basePaginator } as Paginator;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.offset = longToNumber(reader.int64() as Long);
          break;
        case 2:
          message.totalCount = longToNumber(reader.int64() as Long);
          break;
        case 3:
          message.countPerPage = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Paginator {
    const message = { ...basePaginator } as Paginator;
    message.offset =
      object.offset !== undefined && object.offset !== null
        ? Number(object.offset)
        : 0;
    message.totalCount =
      object.totalCount !== undefined && object.totalCount !== null
        ? Number(object.totalCount)
        : 0;
    message.countPerPage =
      object.countPerPage !== undefined && object.countPerPage !== null
        ? Number(object.countPerPage)
        : 0;
    return message;
  },

  toJSON(message: Paginator): unknown {
    const obj: any = {};
    message.offset !== undefined && (obj.offset = message.offset);
    message.totalCount !== undefined && (obj.totalCount = message.totalCount);
    message.countPerPage !== undefined &&
      (obj.countPerPage = message.countPerPage);
    return obj;
  },

  fromPartial(object: DeepPartial<Paginator>): Paginator {
    const message = { ...basePaginator } as Paginator;
    message.offset = object.offset ?? 0;
    message.totalCount = object.totalCount ?? 0;
    message.countPerPage = object.countPerPage ?? 0;
    return message;
  },
};

messageTypeRegistry.set(Paginator.$type, Paginator);

const baseListOptions: object = {
  $type: "nmbot.shared.ListOptions",
  offset: 0,
  limit: 0,
};

export const ListOptions = {
  $type: "nmbot.shared.ListOptions" as const,

  encode(
    message: ListOptions,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.offset !== 0) {
      writer.uint32(8).int32(message.offset);
    }
    if (message.limit !== 0) {
      writer.uint32(16).int32(message.limit);
    }
    if (message.order !== undefined) {
      writer.uint32(26).string(message.order);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ListOptions {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseListOptions } as ListOptions;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.offset = reader.int32();
          break;
        case 2:
          message.limit = reader.int32();
          break;
        case 3:
          message.order = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ListOptions {
    const message = { ...baseListOptions } as ListOptions;
    message.offset =
      object.offset !== undefined && object.offset !== null
        ? Number(object.offset)
        : 0;
    message.limit =
      object.limit !== undefined && object.limit !== null
        ? Number(object.limit)
        : 0;
    message.order =
      object.order !== undefined && object.order !== null
        ? String(object.order)
        : undefined;
    return message;
  },

  toJSON(message: ListOptions): unknown {
    const obj: any = {};
    message.offset !== undefined && (obj.offset = message.offset);
    message.limit !== undefined && (obj.limit = message.limit);
    message.order !== undefined && (obj.order = message.order);
    return obj;
  },

  fromPartial(object: DeepPartial<ListOptions>): ListOptions {
    const message = { ...baseListOptions } as ListOptions;
    message.offset = object.offset ?? 0;
    message.limit = object.limit ?? 0;
    message.order = object.order ?? undefined;
    return message;
  },
};

messageTypeRegistry.set(ListOptions.$type, ListOptions);

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") return globalThis;
  if (typeof self !== "undefined") return self;
  if (typeof window !== "undefined") return window;
  if (typeof global !== "undefined") return global;
  throw "Unable to locate global object";
})();

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
