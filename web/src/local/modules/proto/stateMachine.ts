/* eslint-disable */
import { messageTypeRegistry } from "./typeRegistry";
import Long from "long";
import _m0 from "protobufjs/minimal";
import { Paginator } from "./paginatior";

export const protobufPackage = "botMs";

export interface StateMachine {
  $type: "botMs.StateMachine";
  id: number;
  name: string;
  botId: number;
  startStateId: number;
  meta: string;
}

export interface StateMachineResp {
  $type: "botMs.StateMachineResp";
  items: StateMachine[];
  paginator?: Paginator;
}

export interface Transition {
  $type: "botMs.Transition";
  id: number;
  stateId: number;
  trigger: string;
  toStateId?: number | undefined;
  meta: string;
}

export interface State {
  $type: "botMs.State";
  id: number;
  name: string;
  scenarioId: number;
  stateMachineId: number;
  meta: string;
  transitions: Transition[];
}

export interface StateBatch {
  $type: "botMs.StateBatch";
  items: State[];
}

const baseStateMachine: object = {
  $type: "botMs.StateMachine",
  id: 0,
  name: "",
  botId: 0,
  startStateId: 0,
  meta: "",
};

export const StateMachine = {
  $type: "botMs.StateMachine" as const,

  encode(
    message: StateMachine,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    if (message.botId !== 0) {
      writer.uint32(24).uint32(message.botId);
    }
    if (message.startStateId !== 0) {
      writer.uint32(32).uint32(message.startStateId);
    }
    if (message.meta !== "") {
      writer.uint32(42).string(message.meta);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): StateMachine {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseStateMachine } as StateMachine;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.name = reader.string();
          break;
        case 3:
          message.botId = reader.uint32();
          break;
        case 4:
          message.startStateId = reader.uint32();
          break;
        case 5:
          message.meta = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): StateMachine {
    const message = { ...baseStateMachine } as StateMachine;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.name =
      object.name !== undefined && object.name !== null
        ? String(object.name)
        : "";
    message.botId =
      object.botId !== undefined && object.botId !== null
        ? Number(object.botId)
        : 0;
    message.startStateId =
      object.startStateId !== undefined && object.startStateId !== null
        ? Number(object.startStateId)
        : 0;
    message.meta =
      object.meta !== undefined && object.meta !== null
        ? String(object.meta)
        : "";
    return message;
  },

  toJSON(message: StateMachine): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    message.botId !== undefined && (obj.botId = message.botId);
    message.startStateId !== undefined &&
      (obj.startStateId = message.startStateId);
    message.meta !== undefined && (obj.meta = message.meta);
    return obj;
  },

  fromPartial(object: DeepPartial<StateMachine>): StateMachine {
    const message = { ...baseStateMachine } as StateMachine;
    message.id = object.id ?? 0;
    message.name = object.name ?? "";
    message.botId = object.botId ?? 0;
    message.startStateId = object.startStateId ?? 0;
    message.meta = object.meta ?? "";
    return message;
  },
};

messageTypeRegistry.set(StateMachine.$type, StateMachine);

const baseStateMachineResp: object = { $type: "botMs.StateMachineResp" };

export const StateMachineResp = {
  $type: "botMs.StateMachineResp" as const,

  encode(
    message: StateMachineResp,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.items) {
      StateMachine.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.paginator !== undefined) {
      Paginator.encode(message.paginator, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): StateMachineResp {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseStateMachineResp } as StateMachineResp;
    message.items = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.items.push(StateMachine.decode(reader, reader.uint32()));
          break;
        case 2:
          message.paginator = Paginator.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): StateMachineResp {
    const message = { ...baseStateMachineResp } as StateMachineResp;
    message.items = (object.items ?? []).map((e: any) =>
      StateMachine.fromJSON(e)
    );
    message.paginator =
      object.paginator !== undefined && object.paginator !== null
        ? Paginator.fromJSON(object.paginator)
        : undefined;
    return message;
  },

  toJSON(message: StateMachineResp): unknown {
    const obj: any = {};
    if (message.items) {
      obj.items = message.items.map((e) =>
        e ? StateMachine.toJSON(e) : undefined
      );
    } else {
      obj.items = [];
    }
    message.paginator !== undefined &&
      (obj.paginator = message.paginator
        ? Paginator.toJSON(message.paginator)
        : undefined);
    return obj;
  },

  fromPartial(object: DeepPartial<StateMachineResp>): StateMachineResp {
    const message = { ...baseStateMachineResp } as StateMachineResp;
    message.items = (object.items ?? []).map((e) =>
      StateMachine.fromPartial(e)
    );
    message.paginator =
      object.paginator !== undefined && object.paginator !== null
        ? Paginator.fromPartial(object.paginator)
        : undefined;
    return message;
  },
};

messageTypeRegistry.set(StateMachineResp.$type, StateMachineResp);

const baseTransition: object = {
  $type: "botMs.Transition",
  id: 0,
  stateId: 0,
  trigger: "",
  meta: "",
};

export const Transition = {
  $type: "botMs.Transition" as const,

  encode(
    message: Transition,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.stateId !== 0) {
      writer.uint32(16).uint32(message.stateId);
    }
    if (message.trigger !== "") {
      writer.uint32(26).string(message.trigger);
    }
    if (message.toStateId !== undefined) {
      writer.uint32(32).uint32(message.toStateId);
    }
    if (message.meta !== "") {
      writer.uint32(42).string(message.meta);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Transition {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseTransition } as Transition;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.stateId = reader.uint32();
          break;
        case 3:
          message.trigger = reader.string();
          break;
        case 4:
          message.toStateId = reader.uint32();
          break;
        case 5:
          message.meta = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Transition {
    const message = { ...baseTransition } as Transition;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.stateId =
      object.stateId !== undefined && object.stateId !== null
        ? Number(object.stateId)
        : 0;
    message.trigger =
      object.trigger !== undefined && object.trigger !== null
        ? String(object.trigger)
        : "";
    message.toStateId =
      object.toStateId !== undefined && object.toStateId !== null
        ? Number(object.toStateId)
        : undefined;
    message.meta =
      object.meta !== undefined && object.meta !== null
        ? String(object.meta)
        : "";
    return message;
  },

  toJSON(message: Transition): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.stateId !== undefined && (obj.stateId = message.stateId);
    message.trigger !== undefined && (obj.trigger = message.trigger);
    message.toStateId !== undefined && (obj.toStateId = message.toStateId);
    message.meta !== undefined && (obj.meta = message.meta);
    return obj;
  },

  fromPartial(object: DeepPartial<Transition>): Transition {
    const message = { ...baseTransition } as Transition;
    message.id = object.id ?? 0;
    message.stateId = object.stateId ?? 0;
    message.trigger = object.trigger ?? "";
    message.toStateId = object.toStateId ?? undefined;
    message.meta = object.meta ?? "";
    return message;
  },
};

messageTypeRegistry.set(Transition.$type, Transition);

const baseState: object = {
  $type: "botMs.State",
  id: 0,
  name: "",
  scenarioId: 0,
  stateMachineId: 0,
  meta: "",
};

export const State = {
  $type: "botMs.State" as const,

  encode(message: State, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).uint32(message.id);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    if (message.scenarioId !== 0) {
      writer.uint32(24).uint32(message.scenarioId);
    }
    if (message.stateMachineId !== 0) {
      writer.uint32(32).uint32(message.stateMachineId);
    }
    if (message.meta !== "") {
      writer.uint32(42).string(message.meta);
    }
    for (const v of message.transitions) {
      Transition.encode(v!, writer.uint32(50).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): State {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseState } as State;
    message.transitions = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.uint32();
          break;
        case 2:
          message.name = reader.string();
          break;
        case 3:
          message.scenarioId = reader.uint32();
          break;
        case 4:
          message.stateMachineId = reader.uint32();
          break;
        case 5:
          message.meta = reader.string();
          break;
        case 6:
          message.transitions.push(Transition.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): State {
    const message = { ...baseState } as State;
    message.id =
      object.id !== undefined && object.id !== null ? Number(object.id) : 0;
    message.name =
      object.name !== undefined && object.name !== null
        ? String(object.name)
        : "";
    message.scenarioId =
      object.scenarioId !== undefined && object.scenarioId !== null
        ? Number(object.scenarioId)
        : 0;
    message.stateMachineId =
      object.stateMachineId !== undefined && object.stateMachineId !== null
        ? Number(object.stateMachineId)
        : 0;
    message.meta =
      object.meta !== undefined && object.meta !== null
        ? String(object.meta)
        : "";
    message.transitions = (object.transitions ?? []).map((e: any) =>
      Transition.fromJSON(e)
    );
    return message;
  },

  toJSON(message: State): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    message.name !== undefined && (obj.name = message.name);
    message.scenarioId !== undefined && (obj.scenarioId = message.scenarioId);
    message.stateMachineId !== undefined &&
      (obj.stateMachineId = message.stateMachineId);
    message.meta !== undefined && (obj.meta = message.meta);
    if (message.transitions) {
      obj.transitions = message.transitions.map((e) =>
        e ? Transition.toJSON(e) : undefined
      );
    } else {
      obj.transitions = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<State>): State {
    const message = { ...baseState } as State;
    message.id = object.id ?? 0;
    message.name = object.name ?? "";
    message.scenarioId = object.scenarioId ?? 0;
    message.stateMachineId = object.stateMachineId ?? 0;
    message.meta = object.meta ?? "";
    message.transitions = (object.transitions ?? []).map((e) =>
      Transition.fromPartial(e)
    );
    return message;
  },
};

messageTypeRegistry.set(State.$type, State);

const baseStateBatch: object = { $type: "botMs.StateBatch" };

export const StateBatch = {
  $type: "botMs.StateBatch" as const,

  encode(
    message: StateBatch,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.items) {
      State.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): StateBatch {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseStateBatch } as StateBatch;
    message.items = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.items.push(State.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): StateBatch {
    const message = { ...baseStateBatch } as StateBatch;
    message.items = (object.items ?? []).map((e: any) => State.fromJSON(e));
    return message;
  },

  toJSON(message: StateBatch): unknown {
    const obj: any = {};
    if (message.items) {
      obj.items = message.items.map((e) => (e ? State.toJSON(e) : undefined));
    } else {
      obj.items = [];
    }
    return obj;
  },

  fromPartial(object: DeepPartial<StateBatch>): StateBatch {
    const message = { ...baseStateBatch } as StateBatch;
    message.items = (object.items ?? []).map((e) => State.fromPartial(e));
    return message;
  },
};

messageTypeRegistry.set(StateBatch.$type, StateBatch);

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends {}
  ? { [K in Exclude<keyof T, "$type">]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
