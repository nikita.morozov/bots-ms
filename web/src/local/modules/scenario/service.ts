import { inject, injectable } from 'inversify';
import { ScenarioInitialRequest, ScenarioListReq, ScenarioModel, ScenarioModelListResp } from '../proto/botCommon';
import {
  ScenarioCountDesc,
  ScenarioCreateDesc,
  ScenarioDeleteDesc, ScenarioGetDesc,
  ScenarioListDesc,
  ScenarioSetInitialActionIdDesc,
  ScenarioUpdateDesc,
} from '../proto/scenario';
import { ApiClient } from '@sharedModules/grpcClient';
import StreamHandler from '@sharedModules/Stream/service/StreamHandler';
import { Empty } from '@sharedModules/shared/empty';
import { map, mergeMap, Observable } from 'rxjs';
import { ListOptions } from '@sharedModules/shared/paginatior';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import { UInt64Value } from '@local/modules/proto/commonTypes';

export interface IScenarioService {
  create(request: ScenarioModel): Observable<IdRequest>;
  update(request: ScenarioModel): Observable<void>;
  delete(request: IdRequest): Observable<void>;
  setInitialActionId(request: ScenarioInitialRequest): Observable<void>;
  list(request: ScenarioListReq): Observable<ScenarioModelListResp>;
  getById(request: IdRequest): Observable<ScenarioModel>,
}

@injectable()
class ScenarioService extends StreamHandler<ScenarioModel> implements IScenarioService {
  public static diKey = Symbol.for('ScenarioServiceKey');
  private api: ApiClient;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
  ) {
    super();
    this.api = api;
    this.add('default', this.streamDefault);
  }

  private streamDefault = {
    stream: (opts: ListOptions): Observable<ScenarioModel> => {
      return this.list(ScenarioListReq.fromPartial({ opts })).pipe(mergeMap(e => e.items));
    },
    loadCount: (): Observable<number> => {
      return this.count().pipe(map(res => res.value));
    }
  }

  create(request: ScenarioModel): Observable<IdRequest> {
    return this.api.unary$(
      ScenarioCreateDesc,
      {
        obj: request,
        coder: ScenarioModel,
      }
    )
  }

  delete(request: IdRequest): Observable<void> {
    return this.api.unary$(
      ScenarioDeleteDesc,
      {
        obj: request,
        coder: IdRequest,
      }
    )
  }

  list(request: ScenarioListReq): Observable<ScenarioModelListResp> {
    return this.api.unary$(
      ScenarioListDesc,
      {
        obj: request,
        coder: ScenarioListReq,
      }
    );
  }

  setInitialActionId(request: ScenarioInitialRequest): Observable<void> {
    return this.api.unary$(
      ScenarioSetInitialActionIdDesc,
      {
        obj: request,
        coder: ScenarioInitialRequest,
      }
    )
  }

  update(request: ScenarioModel): Observable<void> {
    return this.api.unary$(
      ScenarioUpdateDesc,
      {
        obj: request,
        coder: ScenarioModel,
      }
    );
  }

  count(): Observable<UInt64Value> {
    return this.api.unary$(
      ScenarioCountDesc,
      {
        obj: {},
        coder: Empty,
      }
    )
  }

  getById(request: IdRequest): Observable<ScenarioModel> {
    return this.api.unary$(
      ScenarioGetDesc,
      {
        obj: request,
        coder: IdRequest,
      }
    )
  }
}

export default ScenarioService;