import { ScenarioInitialRequest, ScenarioListReq, ScenarioModel, ScenarioModelListResp } from '../proto/botCommon';
import { inject, injectable } from 'inversify';
import ScenarioService from './service';
import { IdRequest } from '@sharedModules/shared/commonTypes';
import { Observable } from 'rxjs';
import { IBaseListUsecase, IPageService } from '@sharedModules/Stream/types';
import { PageService } from '@sharedModules/Stream/service/ListService';
import ListMV from '@sharedModules/Stream/model/ListMV';
import { ListOptions } from '@sharedModules/shared/paginatior';

export interface IScenarioUsecase {
  create(request: ScenarioModel): Observable<IdRequest>;
  update(request: ScenarioModel): Observable<void>;
  delete(request: IdRequest): Observable<void>;
  setInitialActionId(request: ScenarioInitialRequest): Observable<void>;
  list(request: ScenarioListReq): Observable<ScenarioModelListResp>;
  getById(request: IdRequest): Observable<ScenarioModel>,
}

@injectable()
class ScenarioUsecase implements IScenarioUsecase, IBaseListUsecase<ScenarioModel> {
  public static diKey = Symbol.for('ScenarioUsecaseDiKey');

  private scenarioService: ScenarioService;
  private pageService: IPageService<ScenarioModel>;

  constructor(
    @inject(ScenarioService.diKey) scenarioService: ScenarioService,
  ) {
    this.scenarioService = scenarioService;
    this.pageService = new PageService(this.scenarioService);
  }

  create(request: ScenarioModel): Observable<IdRequest> {
    return this.scenarioService.create(request);
  }

  delete(request: IdRequest): Observable<void> {
    return this.scenarioService.delete(request);
  }

  list(request: ScenarioListReq): Observable<ScenarioModelListResp> {
    return this.scenarioService.list(request);
  }

  setInitialActionId(request: ScenarioInitialRequest): Observable<void> {
    return this.scenarioService.setInitialActionId(request);
  }

  update(request: ScenarioModel): Observable<void> {
    return this.scenarioService.update(request);
  }

  loadItems(model: ListMV<ScenarioModel>, opts: ListOptions, method?: string, query?: object): Observable<ScenarioModel> {
    return this.pageService.loadPage(model, opts, method, query);
  }

  loadTotalCount(model: ListMV<ScenarioModel>, method?: string, query?: object): Observable<number> {
    return this.pageService.loadTotalCount(model, method, query);
  }

  getById(request: IdRequest): Observable<ScenarioModel> {
    return this.scenarioService.getById(request);
  }
}

export default ScenarioUsecase;