import { ContainerModule, interfaces } from 'inversify';
import ScenarioService, { IScenarioService } from './service';
import ScenarioUsecase, { IScenarioUsecase } from './usecase';

export const ScenarioModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<IScenarioService>(ScenarioService.diKey).to(ScenarioService);

  bind<IScenarioUsecase>(ScenarioUsecase.diKey).to(ScenarioUsecase).inSingletonScope();
});