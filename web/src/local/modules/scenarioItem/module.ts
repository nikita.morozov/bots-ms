import { ContainerModule, interfaces } from 'inversify';
import ScenarioItemService, { IScenarioItemService } from './service';
import ScenarioItemUsecase, { IScenarioItemUsecase } from './usecase';

export const ScenarioItemModule = new ContainerModule((bind: interfaces.Bind) => {
  bind<IScenarioItemService>(ScenarioItemService.diKey).to(ScenarioItemService);

  bind<IScenarioItemUsecase>(ScenarioItemUsecase.diKey).to(ScenarioItemUsecase).inSingletonScope();
});