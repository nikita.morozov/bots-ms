import { inject, injectable } from 'inversify';
import { ScenarioItemBatch, ScenarioItemModel } from '../proto/botCommon';
import {
  ScenarioItemBatchUpdateDesc,
  ScenarioItemCreateDesc,
  ScenarioItemDeleteDesc,
  ScenarioItemListDesc,
  ScenarioItemUpdateDesc,
} from '../proto/scenarioItem';
import { ApiClient } from '@sharedModules/grpcClient';
import { Observable } from 'rxjs';
import { IdRequest } from '@sharedModules/shared/commonTypes';

export interface  IScenarioItemService {
  create(request: ScenarioItemModel): Observable<IdRequest>,
  delete(request: IdRequest): Observable<void>,
  list(request: IdRequest): Observable<ScenarioItemModel>,
  batchUpdate(request: ScenarioItemBatch): Observable<void>,
  update(request: ScenarioItemModel): Observable<void>,
}

@injectable()
class ScenarioItemService implements IScenarioItemService {
  public static diKey = Symbol.for('ScenarioItemService');
  private api: ApiClient;

  public constructor(
    @inject(ApiClient.diKey) api: ApiClient,
  ) {
    this.api = api;
  }

  create(request: ScenarioItemModel): Observable<IdRequest> {
    return this.api.unary$(
      ScenarioItemCreateDesc,
      {
        obj: request,
        coder: ScenarioItemModel,
      }
    );
  }

  delete(request: IdRequest): Observable<void> {
    return this.api.unary$(
      ScenarioItemDeleteDesc,
      {
        obj: request,
        coder: IdRequest,
      }
    )
  }

  list(request: IdRequest): Observable<ScenarioItemModel> {
    return this.api.stream$(
      ScenarioItemListDesc,
      {
        obj: request,
        coder: IdRequest,
      }
    )
  }

  update(request: ScenarioItemModel): Observable<void> {
    return this.api.unary$(
      ScenarioItemUpdateDesc,
      {
        obj: request,
        coder: ScenarioItemModel,
      }
    )
  }

  batchUpdate(request: ScenarioItemBatch): Observable<void> {
    return this.api.unary$(
      ScenarioItemBatchUpdateDesc,
      {
        obj: request,
        coder: ScenarioItemBatch,
      }
    )
  }
}

export default ScenarioItemService;