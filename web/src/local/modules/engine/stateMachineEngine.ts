import { inject, injectable } from 'inversify';
import EngineMV from './model';
import CustomDiagramModel from './diagramModel';
import { StateNodeFactory } from '@factories/stateMachine/NodeFactory';
import { StateMachine } from '@local/modules/proto/stateMachine';
import { DiagramModel } from '@projectstorm/react-diagrams-core';
import { action } from 'mobx';
import StateServiceNodeModel from '@local/modules/stateService/node';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import ScenarioItemUsecase from '@local/modules/scenarioItem/usecase';
import StateServiceModuleVM from '@local/modules/engine/modelViews/stateService';
import StateUsecase from '@local/modules/stateService/usecase';
import TransitionUsecase from '@local/modules/transition/usecase';
import ScenarioModuleVM from '@local/modules/engine/modelViews/scenario';
import ScenarioUsecase from '@local/modules/scenario/usecase';

@injectable()
class StateMachineEngineMV extends EngineMV<StateMachine>{
  public static diKey = Symbol.for('StateMachineEngineDiKey');

  scenarioModule: ScenarioModuleVM;
  stateServiceModule: StateServiceModuleVM;

  constructor(
    @inject(ScenarioEngineMV.diKey) scenarioEngineMV: ScenarioEngineMV,
    @inject(ScenarioItemUsecase.diKey) scenarioItemUC: ScenarioItemUsecase,
    @inject(StateUsecase.diKey) stateUC: StateUsecase,
    @inject(TransitionUsecase.diKey) transitionUC: TransitionUsecase,
    @inject(ScenarioUsecase.diKey) scenarioUC: ScenarioUsecase,
  ) {
    super(scenarioItemUC);
    const model = new CustomDiagramModel();

    this.stateServiceModule = new StateServiceModuleVM(this, stateUC, transitionUC);
    this.scenarioModule = new ScenarioModuleVM(this, scenarioUC);

    this.engine.getNodeFactories().deregisterFactory('default');
    this.engine.getNodeFactories().registerFactory(new StateNodeFactory(this, scenarioEngineMV));

    this.engine.setModel(model as unknown as DiagramModel);
  }

  @action
  setInitialState(stateId: number) {
    const stateServiceNodes = this.engine.getModel().getNodes() as StateServiceNodeModel[];

    const current = stateServiceNodes.find(i => i.model().initial);
    const next = stateServiceNodes.find(i => i.model().id === stateId);

    if (current) {
      current.model().initial = false;
    }

    if (next) {
      next.model().initial = true;
    }

    this.engine.repaintCanvas();
  }
}

export default StateMachineEngineMV;