import { inject, injectable } from 'inversify';
import EngineMV from './model';
import CustomDiagramModel from './diagramModel';
import { NodeFactory } from '@factories/scenario/NodeFactory';
import { CommentBoxFactory } from '@factories/scenario/CommentBoxFactory';
import { ScenarioInitialRequest, ScenarioModel } from '@local/modules/proto/botCommon';
import { DiagramModel } from '@projectstorm/react-diagrams-core';
import { action } from 'mobx';
import ScenarioActionNode from '@local/modules/scenarioAction/node';
import ActionsModuleVM from '@local/modules/engine/modelViews/actions';
import ScenarioActionsModulesVM from '@local/modules/engine/modelViews/scenarioActions';
import ScenarioActionUsecase from '@local/modules/scenarioAction/usecase';
import ConnectorUsecase from '@local/modules/connector/usecase';
import ScenarioItemUsecase from '@local/modules/scenarioItem/usecase';
import ScenarioUsecase from '@local/modules/scenario/usecase';
import ActionUsecase from '@local/modules/action/usecase';
import { DefaultState } from '@local/states/defaultState';
import CopyItemAction from '@local/actions/ScenarioCopyItem';

@injectable()
class ScenarioEngineMV extends EngineMV<ScenarioModel> {
  public static diKey = Symbol.for('ScenarioEngineDiKey');

  actionsModule: ActionsModuleVM;
  scenarioActionsModule: ScenarioActionsModulesVM;

  private scenarioUC: ScenarioUsecase;

  constructor(
    @inject(ScenarioActionUsecase.diKey) scenarioActionUC: ScenarioActionUsecase,
    @inject(ConnectorUsecase.diKey) connectorsUC: ConnectorUsecase,
    @inject(ScenarioItemUsecase.diKey) scenarioItemUC: ScenarioItemUsecase,
    @inject(ScenarioUsecase.diKey) scenarioUC: ScenarioUsecase,
    @inject(ActionUsecase.diKey) actionUC: ActionUsecase,
  ) {
    super(scenarioItemUC);

    this.scenarioUC = scenarioUC;

    const model = new CustomDiagramModel();

    this.actionsModule = new ActionsModuleVM(this, actionUC);
    this.scenarioActionsModule = new ScenarioActionsModulesVM(this, scenarioActionUC, connectorsUC);

    this.engine.getNodeFactories().deregisterFactory('default');
    this.engine.getNodeFactories().registerFactory(new NodeFactory(this));
    this.engine.getNodeFactories().registerFactory(new CommentBoxFactory(this));

    this.engine.getActionEventBus().registerAction(new CopyItemAction(this.engine, this.scenarioActionsModule));

    this.engine.getStateMachine().pushState(new DefaultState());

    this.engine.setModel(model as unknown as DiagramModel);
  }

  @action
  setInitialAction(actionId: number) {
    this.scenarioUC.setInitialActionId(ScenarioInitialRequest.fromJSON({
      actionId,
      scenarioId: this.layerModel.getItem()!.id,
    })).subscribe(() => {
      const nodes = this.engine.getModel().getNodes() as ScenarioActionNode[];

      const current = nodes.find(i => i.model().initial);
      const next = nodes.find(i => i.model().id === actionId);

      if (current) {
        current.model().initial = false;
      }

      if (next) {
        next.model().initial = true;
      }

      this.engine.repaintCanvas();
    });
  }
}

export default ScenarioEngineMV;