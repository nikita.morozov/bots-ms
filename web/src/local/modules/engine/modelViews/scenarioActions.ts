import BaseEngineModuleVM, { IBaseEngineModuleVM } from '@local/modules/engine/modelViews/base';
import {
  ScenarioActionBatch,
  ScenarioActionModel,
  ScenarioActionReq,
  ScenarioModel,
} from '@local/modules/proto/botCommon';
import ScenarioActionNode from '@local/modules/scenarioAction/node';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import { action } from 'mobx';
import ScenarioActionUsecase from '@local/modules/scenarioAction/usecase';
import { debounce } from 'lodash';
import { Id64Request } from '@sharedModules/shared/commonTypes';
import ConnectorsModulesVM from '@local/modules/engine/modelViews/connectors';
import ConnectorUsecase from '@local/modules/connector/usecase';
import { map, Observable } from 'rxjs';

class ScenarioActionsModulesVM extends BaseEngineModuleVM<ScenarioActionModel, ScenarioActionNode, ScenarioModel, ScenarioEngineMV> implements IBaseEngineModuleVM<ScenarioActionModel> {
  private scenarioActionUC: ScenarioActionUsecase;
  private batchUpdateItems: ScenarioActionModel[] = [];

  connectorsModule: ConnectorsModulesVM;

  constructor(engine: ScenarioEngineMV, scenarioActionUC: ScenarioActionUsecase, connectorsUC: ConnectorUsecase) {
    super(engine);
    this.scenarioActionUC = scenarioActionUC;
    this.connectorsModule = new ConnectorsModulesVM(this.engineVM, connectorsUC);
  }

  @action
  load(): Observable<ScenarioActionModel> {
    this.list.clear();
    return this.scenarioActionUC.list(ScenarioActionReq.fromPartial({ scenarioId: this.engineVM.layerModel.getItem()!.id })).pipe(map(res => {
      const action = this.engineVM.actionsModule.list.items.find(a => a.model.id === res.actionId);

      const item = new ScenarioActionNode({
        ...res,
        initial: this.engineVM.layerModel.getItem()!.initial === res.id,
        meta: JSON.parse(res.meta),
        name: action ? action.model.name : 'Undefined Action',
        sockets: action ? action.model.sockets : [],
      }, this);

      this.engineVM.getModel().addNode(item);
      this.list.add(item);

      this.engineVM.engine.repaintCanvas();

      return res;
    }));
  }

  @action
  add(value: ScenarioActionModel) {
    this.scenarioActionUC.create(value).subscribe(res => {
      const action = this.engineVM.actionsModule.list.items.find(a => a.model.id === value.actionId);

      if (action) {
        const item = new ScenarioActionNode({
          ...value,
          id: res.id,
          name: action.model.name,
          initial: false,
          sockets: action.model.sockets,
          meta: JSON.parse(value.meta),
        }, this);

        this.engineVM.engine.getModel().addNode(item);
        this.list.add(item);
        this.engineVM.engine.repaintCanvas();
      }
    });
  }

  updatePosition(value: ScenarioActionModel) {
    this.batchUpdateItems.push(value);
    this._updatePosition();
  }

  private _updatePosition = debounce(() => {
    this.scenarioActionUC.batchUpdate(ScenarioActionBatch.fromPartial({ items: this.batchUpdateItems })).subscribe(() => {
      this.batchUpdateItems = [];
    });
  }, 100);

  @action
  remove(value: Id64Request) {
    this.scenarioActionUC.delete(value).subscribe(() => {
      const list = this.list.items;
      const index = this.list.items.findIndex(i => i.model().id === value.id);

      if (!!~index) {
        const item = list[index];
        this.engineVM.engine.getModel().removeNode(item);
        item.remove();
        this.list.removeById(value.id);
      }

      this.engineVM.engine.repaintCanvas();
    });
  }

  @action
  update(value: ScenarioActionModel) {
    this.scenarioActionUC.update(value).subscribe(() => {
      const item = this.list.items.find(i => i.model().id === value.id);

      if (item) {
        item.setModel({ ...item.model(), ...value, meta: JSON.parse(value.meta) });
        this.list.update(item);
      }
    })
  }

  @action
  batchUpdate(value: ScenarioActionBatch) {
    this.scenarioActionUC.batchUpdate(value).subscribe();
  }
}

export default ScenarioActionsModulesVM;