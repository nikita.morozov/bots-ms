import { ObjectWithId } from '@sharedModules/types';
import EngineMV from '@local/modules/engine/model';
import ListMV from '@sharedModules/Stream/model/ListMV';
import { Id64Request } from '@sharedModules/shared/commonTypes';
import { Observable } from 'rxjs';

export interface IBaseEngineModuleVM<MODEL extends object> {
  load: (values: MODEL[]) => Observable<MODEL>,
  remove?: (value: Id64Request) => void,
  update?: (value: MODEL) => void,
  add?: (value: MODEL) => void,
}

abstract class BaseEngineModuleVM<MODEL extends ObjectWithId, NODE extends ObjectWithId, ENGINE_MODEL extends ObjectWithId, ENGINE extends EngineMV<ENGINE_MODEL>> {
  protected engineVM: ENGINE;
  list: ListMV<NODE> = new ListMV<NODE>();

  protected constructor(engine: ENGINE) {
    this.engineVM = engine;
  }
}

export default BaseEngineModuleVM;