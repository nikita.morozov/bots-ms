import BaseEngineModuleVM from '@local/modules/engine/modelViews/base';
import { ConnectorModel, ConnectorReq, ScenarioModel } from '@local/modules/proto/botCommon';
import ConnectorLink from '@local/modules/connector/node';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import SocketNode from '@local/modules/socket/node';
import { action } from 'mobx';
import ConnectorUsecase from '@local/modules/connector/usecase';
import { map, Observable } from 'rxjs';
import { Id64Request } from '@sharedModules/shared/commonTypes';

class ConnectorsModulesVM extends BaseEngineModuleVM<ConnectorModel, ConnectorLink, ScenarioModel, ScenarioEngineMV> {
  private connectorsUC: ConnectorUsecase;

  constructor(engine: ScenarioEngineMV, connectorsUC: ConnectorUsecase) {
    super(engine);
    this.connectorsUC = connectorsUC;
  }

  @action
  load(): Observable<ConnectorModel> {
    this.list.clear();
    return this.connectorsUC.list(ConnectorReq.fromPartial({ scenarioId: this.engineVM.layerModel.getItem()!.id }))
      .pipe(map(i => {
        const fromScenarioAction = this.engineVM.scenarioActionsModule.list.items.find(x => x.model().id === i.fromScenarioAction);
        const toScenarioAction = this.engineVM.scenarioActionsModule.list.items.find(x => x.model().id === i.toScenarioAction);

        if (fromScenarioAction && toScenarioAction) {
          const fromSocket = fromScenarioAction.getPort(String(i.fromSocket)) as SocketNode;
          const toSocket = toScenarioAction.getPort(String(i.toSocket)) as SocketNode;

          const link = new ConnectorLink(this, {
            ...i,
            fromScenarioAction,
            toScenarioAction,
            fromSocket,
            toSocket,
          });

          this.list.add(link);
          // this.getPathFactory().generateModel(link);
          this.engineVM.getModel().addLink(link);
          this.engineVM.engine.repaintCanvas();
          return i;
        }

        const link = new ConnectorLink(this);
        this.list.add(link);
        this.engineVM.getModel().addLink(link);
        // this.getPathFactory().generateModel(link);

        this.engineVM.engine.repaintCanvas();
        return i;
      }));
  }

  @action
  add(value: ConnectorModel, node?: ConnectorLink): void {
    const existingConnector = this.list.items.findIndex(i =>
      ((i.model().fromSocket.id === value.fromSocket) && (i.model().fromScenarioAction.model().id === value.fromScenarioAction)) &&
      ((i.model().toSocket.id === value.toSocket) && (i.model().toScenarioAction.model().id === value.toScenarioAction)),
    ) + 1;

    if (existingConnector) return;

    this.connectorsUC.create(value).pipe(map(res => {
      value.id = res.id;

      if (node) {
        node.setId(res.id);
        node.model().id = res.id;

        this.list.add(node);
      }

      this.engineVM.engine.repaintCanvas();
      return;
    })).subscribe();
  }

  @action
  remove(value: Id64Request) {
    if (!value.id) {
      this.engineVM.engine.repaintCanvas();
      return;
    }
    this.connectorsUC.delete(value).subscribe(() => {
      const list = this.list.items;
      const index = list.findIndex(i => i.model().id === value.id);

      if (!!~index) {
        const item = list[index];
        item.getSourcePort().removeLink(list[index]);
        item.getTargetPort().removeLink(list[index]);
        (item.getTargetPort() as SocketNode).model.manual = true;
        this.engineVM.engine.getModel().removeLink(item);
        item.remove();
        this.list.removeById(value.id);
      }

      this.engineVM.engine.repaintCanvas();
    });
  }

  // @action
  // getPathFactory(): PathFindingLinkFactory {
  //   return this.engineVM.engine.getLinkFactories().getFactory<PathFindingLinkFactory>(PathFindingLinkFactory.NAME);
  // }
}

export default ConnectorsModulesVM;