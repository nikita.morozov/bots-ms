import BaseEngineModuleVM from '@local/modules/engine/modelViews/base';
import { State, StateBatch, StateMachine } from '@local/modules/proto/stateMachine';
import StateServiceNodeModel from '@local/modules/stateService/node';
import StateMachineEngineMV from '@local/modules/engine/stateMachineEngine';
import { action } from 'mobx';
import StateUsecase from '@local/modules/stateService/usecase';
import { map, Observable } from 'rxjs';
import TransitionNode from '@local/modules/transition/node';
import { IdRequest } from '@local/modules/proto/commonTypes';
import TransitionModuleVM from '@local/modules/engine/modelViews/transition';
import TransitionUsecase from '@local/modules/transition/usecase';
import { debounce } from 'lodash';
import { Id64Request } from '@sharedModules/shared/commonTypes';

class StateServiceModuleVM extends BaseEngineModuleVM<State, StateServiceNodeModel, StateMachine, StateMachineEngineMV> {
  private stateServiceUC: StateUsecase;
  private batchUpdateItems: State[] = [];

  transitionsModule: TransitionModuleVM;

  constructor(engine: StateMachineEngineMV, stateServiceUC: StateUsecase, transitionUC: TransitionUsecase) {
    super(engine);
    this.stateServiceUC = stateServiceUC;
    this.transitionsModule = new TransitionModuleVM(this.engineVM, transitionUC);
  }

  @action
  load(): Observable<State> {
    this.list.clear();
    return this.stateServiceUC.list(IdRequest.fromPartial({ id: this.engineVM.layerModel.getItem()!.id })).pipe(map(res => {
      const node = new StateServiceNodeModel({
        ...res,
        initial: this.engineVM.layerModel.getItem()!.startStateId === res.id,
        meta: JSON.parse(res.meta),
        transitions: res.transitions.map(transition => {
          const newTransition = new TransitionNode(this.transitionsModule, {
            ...transition,
            meta: JSON.parse(transition.meta),
          });

          this.transitionsModule.list.add(newTransition);
          return newTransition;
        }),
      }, this);
      this.engineVM.engine.getModel().addNode(node);

      this.list.add(node);
      this.engineVM.engine.repaintCanvas();

      return res;
    }));
  }

  @action
  add(value: State) {
    this.stateServiceUC.create(value)
      .pipe(map(res => {
        value.id = res.id;
        const item = new StateServiceNodeModel({
          ...value,
          initial: false,
          meta: JSON.parse(value.meta),
          transitions: [],
        }, this);
        item.setId(res.id);
        this.engineVM.engine.getModel().addNode(item);
        this.list.add(item);
        this.engineVM.engine.repaintCanvas();
      })).subscribe();
  }

  updatePosition(value: State) {
    this.batchUpdateItems.push(value);
    this._updatePosition();
  }

  private _updatePosition = debounce(() => {
    this.stateServiceUC.batchUpdate(StateBatch.fromPartial({ items: this.batchUpdateItems })).subscribe(() => {
      this.batchUpdateItems = [];
    });
  }, 100);

  @action
  remove(value: Id64Request) {
    this.stateServiceUC.delete(value).subscribe(() => {
      const item = this.list.items.find(i => i.model().id === value.id);

      if (item) {
        this.engineVM.engine.getModel().removeNode(item);
        item.remove();
        this.engineVM.engine.repaintCanvas();
      }

      this.list.removeById(value.id);
    });
  }

  @action
  update(value: State) {
    this.stateServiceUC.update(value).subscribe(() => {
      const stateItem = this.list.items.find(i => i.model().id === value.id);

      if (stateItem) {
        stateItem.model().name = value.name;
        stateItem.model().scenarioId = value.scenarioId;
        stateItem.model().stateMachineId = value.stateMachineId;
        stateItem.model().meta = JSON.parse(value.meta);
      }
    });
  }
}

export default StateServiceModuleVM;