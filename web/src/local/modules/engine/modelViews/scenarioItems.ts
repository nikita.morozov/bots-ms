import { ScenarioItemBatch, ScenarioItemModel } from '@local/modules/proto/botCommon';
import EngineMV from '@local/modules/engine/model';
import { action, observable } from 'mobx';
import ScenarioItemUsecase from '@local/modules/scenarioItem/usecase';
import { DefaultNodeModel } from '@projectstorm/react-diagrams';
import { map, Observable } from 'rxjs';
import { debounce } from 'lodash';
import { Id64Request, IdRequest } from '@sharedModules/shared/commonTypes';
import { CommentBoxNodeModel } from '@local/modules/scenarioItem/nodes/commentBox/node';
import ObservableVM from '@sharedModules/Stream/model/BaseVM';
import { ObjectWithId } from '@sharedModules/types';

class ScenarioItemsModuleVM<ENGINE_MODEL extends ObjectWithId> {
  protected engineVM: EngineMV<ENGINE_MODEL>;

  private scenarioItemUC: ScenarioItemUsecase;
  private batchUpdateItems: ScenarioItemModel[] = [];

  public scenarioItemsHandler = {
    commentBox: (value: ScenarioItemModel) => new CommentBoxNodeModel(value, this),
  };

  constructor(engine: EngineMV<ENGINE_MODEL>, scenarioItemUC: ScenarioItemUsecase) {
    this.engineVM = engine;
    this.scenarioItemUC = scenarioItemUC;
  }

  @observable
  public items: ObservableVM<{ [slug: string]: (DefaultNodeModel & ObjectWithId)[] }> = new ObservableVM<{ [p: string]: (DefaultNodeModel & ObjectWithId)[] }>({});

  @action
  public getBySlug(slug: string): DefaultNodeModel[] {
    return this.items.getItem()![slug] || [];
  }

  @action
  public add(item: ScenarioItemModel, slug: string) {
    this.scenarioItemUC.create(item)
      .pipe(map(res => {
        item.id = res.id;
        const scenarioItem = this.scenarioItemsHandler[item.type](item);

        const items = this.items.getItem()!;

        if (items[slug]) {
          this.items.setItem({
            ...items,
            [slug]: [
              ...items[slug],
              scenarioItem,
            ],
          });
        } else {
          this.items.setItem({
            ...items,
            [slug]: [scenarioItem],
          });
        }

        this.engineVM.engine.getModel().addNode(scenarioItem);
        this.engineVM.engine.repaintCanvas();

        return res;
      })).subscribe();
  }

  @action
  public updateItem(item: ScenarioItemModel) {
    this.scenarioItemUC.update(item).subscribe();
  }

  @action
  public removeItem(slug: string, id: number) {
    const index = this.items.getItem()![slug].findIndex(i => i.id === id);

    if (!!~index) {
      const item = this.items.getItem()![slug][index];

      this.scenarioItemUC.delete(Id64Request.fromPartial({ id: item.id })).subscribe(() => {
        this.engineVM.engine.getModel().removeNode(this.getBySlug(slug)[index]);

        this.items.setItem({
          ...this.items.getItem(),
          [slug]: [
            ...this.items.getItem()![slug].slice(0, index),
            ...this.items.getItem()![slug].slice(index + 1),
          ],
        });

        item.remove();
        this.engineVM.engine.repaintCanvas();
      });
    }
  }

  @action
  load(): Observable<ScenarioItemModel> {
    this.items.setItem({});

    return this.scenarioItemUC.list(IdRequest.fromPartial({ id: this.engineVM.layerModel.getItem()!.id })).pipe(map(res => {
      const scenarioItem = this.scenarioItemsHandler[res.type](res);

      const thisItem = this.items.getItem()!;
      if (thisItem[res.type]) {
        this.items.setItem({
          ...thisItem,
          [res.type]: [...thisItem[res.type], scenarioItem],
        });
      } else {
        this.items.setItem({
          ...thisItem,
          [res.type]: [scenarioItem],
        });
      }

      this.engineVM.getModel().addNode(scenarioItem);

      this.engineVM.engine.repaintCanvas();

      return res;
    }));
  }

  updatePosition(value: ScenarioItemModel) {
    this.batchUpdateItems.push(value);
    this._updatePosition();
  }

  private _updatePosition = debounce(() => {
    this.scenarioItemUC.batchUpdate(ScenarioItemBatch.fromPartial({ items: this.batchUpdateItems })).subscribe(() => {
      this.batchUpdateItems = [];
    });
  }, 100);
}

export default ScenarioItemsModuleVM;