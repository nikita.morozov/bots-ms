import {
  BaseModel,
  CanvasModel,
  DeserializeEvent,
} from '@projectstorm/react-canvas-core';
import { DiagramModelGenerics, LinkLayerModel } from '@projectstorm/react-diagrams-core';
import CustomNodeLayerModel from '../../../layers/node';
import * as _ from 'lodash';
import { NodeModel } from '@projectstorm/react-diagrams-core';
import { LinkModel } from '@projectstorm/react-diagrams-core';
import { LayerModel } from '@projectstorm/react-canvas-core';

class CustomDiagramModel<G extends DiagramModelGenerics = DiagramModelGenerics> extends CanvasModel<G> {
  protected activeNodeLayer: CustomNodeLayerModel = {} as CustomNodeLayerModel;
  protected activeLinkLayer: LinkLayerModel = {} as LinkLayerModel;

  constructor(options: G['OPTIONS'] = {}) {
    super(options);
    this.addLayer(new LinkLayerModel());
    this.addLayer(new CustomNodeLayerModel());
  }

  deserialize(event: DeserializeEvent<this>) {
    this.layers = [];
    super.deserialize(event);
  }

  addLayer(layer: LayerModel | CustomNodeLayerModel | LinkLayerModel) {
    super.addLayer(layer as LayerModel);
    if (layer instanceof CustomNodeLayerModel) {
      this.activeNodeLayer = layer;
    }
    if (layer instanceof LinkLayerModel) {
      this.activeLinkLayer = layer;
    }
  }

  getLinkLayers(): LinkLayerModel[] {
    return _.filter(this.layers, (layer) => {
      return layer instanceof LinkLayerModel;
    }) as unknown as LinkLayerModel[];
  }

  getNodeLayers(): CustomNodeLayerModel[] {
    return _.filter(this.layers, (layer) => {
      return layer instanceof CustomNodeLayerModel;
    }) as unknown as CustomNodeLayerModel[];
  }

  getActiveNodeLayer(): CustomNodeLayerModel {
    if (!this.activeNodeLayer) {
      const layers = this.getNodeLayers();
      if (layers.length === 0) {
        this.addLayer(new CustomNodeLayerModel());
      } else {
        this.activeNodeLayer = layers[0];
      }
    }
    return this.activeNodeLayer;
  }

  getActiveLinkLayer(): LinkLayerModel {
    if (!this.activeLinkLayer) {
      const layers = this.getLinkLayers();
      if (layers.length === 0) {
        this.addLayer(new LinkLayerModel());
      } else {
        this.activeLinkLayer = layers[0];
      }
    }
    return this.activeLinkLayer;
  }

  getNode(node: string) {
    for (const layer of this.getNodeLayers()) {
      const model = layer.getModel(node);
      if (model) {
        return model;
      }
    }
  }

  getLink(link: string) {
    for (const layer of this.getLinkLayers()) {
      const model = layer.getModel(link);
      if (model) {
        return model;
      }
    }
  }

  addAll(...models: BaseModel[]): BaseModel[] {
    _.forEach(models, (model) => {
      if (model instanceof LinkModel) {
        this.addLink(model);
      } else if (model instanceof NodeModel) {
        this.addNode(model);
      }
    });
    return models;
  }

  addLink(link: LinkModel): LinkModel {
    this.getActiveLinkLayer().addModel(link);
    this.fireEvent(
      {
        link,
        isCreated: true
      },
      'linksUpdated'
    );
    return link;
  }

  addNode(node: NodeModel): NodeModel {
    this.getActiveNodeLayer().addModel(node);
    this.fireEvent({ node, isCreated: true }, 'nodesUpdated');
    return node;
  }

  removeLink(link: LinkModel) {
    const removed = _.some(this.getLinkLayers(), (layer) => {
      return layer.removeModel(link);
    });
    if (removed) {
      this.fireEvent({ link, isCreated: false }, 'linksUpdated');
    }
  }

  removeNode(node: NodeModel) {
    const removed = _.some(this.getNodeLayers(), (layer) => {
      return layer.removeModel(node);
    });
    if (removed) {
      this.fireEvent({ node, isCreated: false }, 'nodesUpdated');
    }
  }

  getLinks(): LinkModel[] {
    return _.flatMap(this.getLinkLayers(), (layer) => {
      return _.values(layer.getModels());
    });
  }

  getNodes(): NodeModel[] {
    return _.flatMap(this.getNodeLayers(), (layer) => {
      return _.values(layer.getModels());
    });
  }
}

export default CustomDiagramModel;