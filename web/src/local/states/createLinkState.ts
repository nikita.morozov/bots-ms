import { Action, ActionEvent, DragCanvasState, InputType, Mapping, State } from '@projectstorm/react-canvas-core';
import { DiagramEngine, LinkModel, PortModel } from '@projectstorm/react-diagrams-core';
import { KeyboardEvent, MouseEvent } from 'react';

function getScaleValue(element: any) {
  const style = window.getComputedStyle(element)
  const matrix = style.transform

  // No transform property. Simply return 1 value.
  if (matrix === 'none') {
    return {
      scale: 1
    }
  }

  const matrixValues = matrix.match(/matrix.*\((.+)\)/)![1].split(', ')

  return {
    scale: Number(matrixValues[0])
  }
}

/**
 * This state is controlling the creation of a link.
 */
export class CreateLinkState extends State<DiagramEngine> {
  dragCanvas: DragCanvasState;

  sourcePort?: PortModel;
  link?: LinkModel;

  constructor() {
    super({ name: 'create-new-link' })
    this.dragCanvas = new DragCanvasState();

    this.registerAction(
      new Action({
        type: InputType.MOUSE_DOWN,
        fire: (actionEvent: ActionEvent<Mapping[InputType]>) => {
          const element = this.engine.getActionEventBus().getModelForEvent(actionEvent as ActionEvent<MouseEvent>)
          if (!(element instanceof PortModel)) {
            this.transitionWithEvent(this.dragCanvas, actionEvent);
          }
        }
      })
    )

    this.registerAction(
      new Action({
        type: InputType.MOUSE_UP,
        fire: (actionEvent: ActionEvent<Mapping[InputType]>) => {
          const element = this.engine.getActionEventBus().getModelForEvent(actionEvent as ActionEvent<MouseEvent>)

          if (element instanceof PortModel && !this.sourcePort) {
            this.sourcePort = element

            const link = this.sourcePort.createLinkModel()

            if (link) {
              link.setSourcePort(this.sourcePort)
              // link.getFirstPoint().setPosition(this.sourcePort.getPosition().x, this.sourcePort.getPosition().y)
              // link.getLastPoint().setPosition(this.sourcePort.getPosition().x + 20, this.sourcePort.getPosition().y + 20)

              this.link = this.engine.getModel().addLink(link)
            }
          } else if (element instanceof PortModel && this.sourcePort && element !== this.sourcePort) {
            if (this.sourcePort.canLinkToPort(element)) {
              if (this.link) {
                this.link.setTargetPort(element);
                element.reportPosition();
                this.clearState();
                this.eject();
              }
            }
          }

          this.engine.repaintCanvas()
        }
      })
    )

    this.registerAction(
      new Action({
        type: InputType.MOUSE_MOVE,
        fire: (actionEvent: ActionEvent<Mapping[InputType]>) => {
          if (!this.link) {
            return
          }

          const { event } = actionEvent as ActionEvent<MouseEvent>;
          const { clientX, clientY } = event
          const svgCanvas = document.querySelector('.diagram-container svg') // here you should take svg canvas element using correct selector

          if (svgCanvas) {
            const clientRect = svgCanvas.getBoundingClientRect()
            const { scale } = getScaleValue(svgCanvas) // get scale value of svg

            this.link.getLastPoint().setPosition((clientX - clientRect.x) / scale, (clientY - clientRect.y) / scale)
            this.engine.repaintCanvas()
          }
        }
      })
    )

    this.registerAction(
      new Action({
        type: InputType.KEY_UP,
        fire: (actionEvent: ActionEvent<Mapping[InputType]>) => {
          // on esc press remove any started link and pop back to default state
          if ((actionEvent.event as KeyboardEvent).keyCode === 27) {
            if (this.link) {
              this.link.remove()
              this.clearState()
              this.eject()
              this.engine.repaintCanvas()
            }
          }
        }
      })
    )
  }

  clearState() {
    this.link = undefined
    this.sourcePort = undefined
  }
}