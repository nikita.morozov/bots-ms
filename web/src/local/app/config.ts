import { injectable } from 'inversify';

@injectable()
class AppConfig {
  public readonly app = import.meta.env.APP_NAME || 'bots';
  public readonly org = import.meta.env.ORG_NAME || 'dao2';
  public readonly authUrlServer = 'https://grpc-auth.cryptofairies.club';
}

export default AppConfig;