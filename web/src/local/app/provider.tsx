import { useInjection } from 'inversify-react';
import App from '@local/app/usecase';

const AppProvider = () => {
  const app = useInjection<App>(App.diKey)
  return null;
}

export default AppProvider;