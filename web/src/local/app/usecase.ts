import AppConfig from '@local/app/config';
import { AppConfigDiKey } from '@modules/app/constants';
import SessionCookieStorage, { ISessionCookieReader } from '@modules/auth/session/cookie';
import SessionMiddleware from '@modules/auth/session/middleware';
import { ApiClient } from '@modules/grpcClient';
import { inject, injectable } from 'inversify';

@injectable()
class App {
  static diKey = Symbol.for('UniqueAppDiKey');

  constructor(
    @inject(ApiClient.diKey) api: ApiClient,
    @inject(AppConfigDiKey) cfg: AppConfig,
    @inject(SessionCookieStorage.diKey) storage: ISessionCookieReader,
  ) {
    const tokenMiddleware = new SessionMiddleware(cfg.app, storage);

    const appMiddleware = {
      ExtraMetadata: (options: any) => ({
        ...options?.metadata,
        app: cfg.app,
      }),
    }

    api.addMiddleware(tokenMiddleware, appMiddleware);
  }
}

export default App;