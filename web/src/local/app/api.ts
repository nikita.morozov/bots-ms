import {injectable} from 'inversify';
import { IApiConfig, ApiClientMiddleware } from '@modules/client/types';

@injectable()
abstract class BaseConfig implements IApiConfig {
    private middlewares: ApiClientMiddleware[] = [];

    constructor() {
    }

    getMiddlewares(): ApiClientMiddleware[] {
        return this.middlewares;
    }

    getDebug(): boolean {
        return import.meta.env.VITE_NODE_ENV === 'development';
    }

    getHost(): string {
        return '';
    }
}

@injectable()
export class GraphQLConfig extends BaseConfig {
  constructor() {
    super();
  }

  getHost(): string {
    return import.meta.env.VITE_PUBLIC_GQL || 'http://localhost:8082';
  }

  getHostWs(): string  {
    return import.meta.env.VITE_PUBLIC_GQL_WS || 'ws://localhost:8082';
  }
}


@injectable()
export class ServerApiConfigImpl extends BaseConfig {
    constructor() {
        super();
    }

    getHost(): string {
        return <string>import.meta.env.VITE_PUBLIC_SERVER_API_DOMAIN;
    }
}

@injectable()
export class ClientApiConfigImpl extends BaseConfig {
    constructor() {
        super();
    }

    getHost(): string {
        return <string>import.meta.env.VITE_PUBLIC_API_DOMAIN;
    }
}

@injectable()
export class HttpServerApiConfigImpl extends BaseConfig {
  constructor() {
    super();
  }

  getHost(): string {
    return <string>import.meta.env.VITE_PUBLIC_SERVER_API_DOMAIN_HTTP;
  }
}

@injectable()
export class HttpClientApiConfigImpl extends BaseConfig {
  constructor() {
    super();
  }

  getHost(): string {
    return <string>import.meta.env.VITE_PUBLIC_API_DOMAIN_HTTP;
  }
}