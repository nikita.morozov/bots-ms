import AppConfig from '@local/app/config';
import App from '@local/app/usecase';
import CookieHandler from '@local/cookie/handler';
import { ActionModule } from '@local/modules/action/module';
import { BotModule } from '@local/modules/bot/module';
import { BotUserModule } from '@local/modules/botUser/module';
import { ConnectorModule } from '@local/modules/connector/module';
import ScenarioEngineMV from '@local/modules/engine/scenarioEngine';
import StateMachineEngineMV from '@local/modules/engine/stateMachineEngine';
import { EventModule } from '@local/modules/event/module';
import { ScenarioModule } from '@local/modules/scenario/module';
import { ScenarioActionModule } from '@local/modules/scenarioAction/module';
import { ScenarioItemModule } from '@local/modules/scenarioItem/module';
import { StateMachineServiceModule } from '@local/modules/stateMachineService/module';
import { stateServiceModule } from '@local/modules/stateService/module';
import { transitionModule } from '@local/modules/transition/module';
import { ToastMessageModule } from '@modules/toaster/module';
import { AppConfigDiKey } from '@modules/app/constants';
import SessionCookieStorage from '@modules/auth/session/cookie';
import CookieStorage, { CookieHandlerDiKey, ICookieStorage } from '@modules/cookie/storage';
import { ApiClient } from '@modules/grpcClient';
import { ApiHttpClientKey } from '@modules/httpClient/constants';
import { HttpClient } from '@modules/httpClient';
import { interfaces } from 'inversify';

const initBaseContainers = (container: interfaces.Container) => {
  container.bind<AppConfig>(AppConfigDiKey).to(AppConfig).inSingletonScope();
  container.bind<ApiClient>(ApiClient.diKey).to(ApiClient).inSingletonScope();
  container.bind<HttpClient>(ApiHttpClientKey).to(HttpClient).inSingletonScope();

  container.bind<ScenarioEngineMV>(ScenarioEngineMV.diKey).to(ScenarioEngineMV).inSingletonScope();
  container.bind<StateMachineEngineMV>(StateMachineEngineMV.diKey).to(StateMachineEngineMV).inSingletonScope();

  container.load(
    ActionModule,
    BotModule,
    BotUserModule,
    EventModule,
    ScenarioModule,
    ScenarioActionModule,
    ConnectorModule,
    ScenarioItemModule,
    StateMachineServiceModule,
    stateServiceModule,
    transitionModule,
    ToastMessageModule,
  );

  container.bind<SessionCookieStorage>(SessionCookieStorage.diKey).to(SessionCookieStorage);

  // Cookie storage
  container.bind<ICookieStorage>(CookieHandlerDiKey).to(CookieHandler);
  container.bind<ICookieStorage>(CookieStorage.diKey).to(CookieStorage).inSingletonScope();

  // App
  container.bind<App>(App.diKey).to(App).inSingletonScope();
};

export default initBaseContainers;