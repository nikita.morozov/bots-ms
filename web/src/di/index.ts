import serverContainer from "@di/server";
import clientContainer from "@di/client";

const container = (typeof window === 'undefined') ? serverContainer : clientContainer;

export default container;