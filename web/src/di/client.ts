import { Container, interfaces } from 'inversify';
import { ClientApiConfigImpl, HttpClientApiConfigImpl } from '@local/app/api';
import initBaseContainers from '@di/container';
import { HttpApiConfigDiKey } from '@modules/httpClient/constants';
import { IApiConfig } from '@modules/client/types';
import { ApiConfigDiKey } from '@modules/grpcClient/types';

let container: interfaces.Container;

const LoadContainer = () => {
  if (!container) {
    container = new Container();
    container.bind<IApiConfig>(ApiConfigDiKey).to(ClientApiConfigImpl);
    container.bind<IApiConfig>(HttpApiConfigDiKey).to(HttpClientApiConfigImpl);
    initBaseContainers(container);
  }

  return container;
}

export default LoadContainer;