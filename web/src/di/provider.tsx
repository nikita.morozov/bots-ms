import React, { FC, PropsWithChildren } from 'react';
import container from '@di/index';
import useContainer from '@modules/tools/container';
import { Provider } from 'inversify-react';

const DiProvider: FC<PropsWithChildren> = ({ children }) => {
  const cont = useContainer(container);

  if (!cont) {
    return null;
  }

  return (
    <Provider
      key={cont.id}
      container={cont}
      standalone
    >
      {children}
    </Provider>
  );
}

export default DiProvider;