const replace = require('replace-in-file');

console.log('post_process started');
for (const [key, value] of Object.entries(process.env)) {
  if (!key.startsWith('VITE_')) {
    continue;
  }

  console.log(key, value);

  const from = new RegExp(`{}.${key}\\b`, 'g');
  const result = replace.sync({
    files: './assets/*.js',
    from: from,
    to: `"${value}"`,
  });

  console.log(result.filter(i => !!i.hasChanged));
}
