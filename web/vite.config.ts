import { nodeResolve } from '@rollup/plugin-node-resolve';
import react from '@vitejs/plugin-react';
import tsconfigPaths from 'vite-tsconfig-paths';
import { defineConfig } from 'vitest/config';

export default () => {
  return defineConfig({
    // To access env vars here use process.env.TEST_VAR
    plugins: [
      react({
        babel: {
          parserOpts: {
            plugins: ['decorators-legacy'],
          },
        },
      }),
      nodeResolve(),
      tsconfigPaths(),
    ],
  });
}